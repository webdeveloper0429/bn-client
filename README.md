# BN-Client

**Setup**

1. remove node_module, iOS, android folder in project folder
2. run 'npm i'
3. run 'react-native upgrade'
4. run 'react-native link'


**Compile and deploy React Native Android app of Release version**

Create bundle

1. cd to the project directory
2. Start the react-native packager if not started

~~3. Download the bundle to the asset folder:
curl "http://localhost:8081/index.android.bundle?platform=android" -o "android/app/src/main/assets/index.android.bundle"~~

3. To create bundle and assets: 

react-native bundle --assets-dest ./android/app/src/main/res/ --entry-file ./index.android.js --bundle-output ./android/app/src/main/assets/index.android.bundle --platform android --dev false

Compile release version & sign the apk.

1. Open terminal on android studio and enter the command ./gradlew assembleRelease
2. Click on Build -> Generate Signed Apk.
