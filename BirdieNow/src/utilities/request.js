import _ from 'lodash';
import React, { PropTypes, Component } from 'react';
import { AsyncStorage } from 'react-native';
import AppConfig from 'AppConfig';
import { Platform, StatusBar, Alert } from 'react-native';
import { GlobalStorage  } from 'AppUtilities';
import { StartLoginView, MainScene } from 'AppScenes';


//TEST to see if I can call props from request page by passing props
  // export const BNS_InitUser = (props) => {

  //  
  //   try {
  //     AppConfig.global_userToken = GlobalStorage.getItem("storage_token");
  //     if (AppConfig.global_userToken != null) {
  //       AppConfig.global_isLoggedIn = true;
  //       GetAccountInfo(); //Will get user data and will reset globals if not authenticated
  //       props.resetToScene(MainScene, null, "FadeAndroid");
  //     }
  //     else {
  //       props.resetToScene(StartLoginView, null, "FadeAndroid");
  //     }
  //   }
  //   catch (error) {
  //     //TODO test this to see if necessary
  //     AppConfig.global_isLoggedIn = false;
  //     props.resetToScene(StartLoginView, null, "FadeAndroid");
  //   }
  // }






//  ********************************** ALL NEW STANDARDS ABOVE BNS ******************************************
//  ********************************** ALL NEW STANDARDS ABOVE BNS ******************************************
//  ********************************** ALL NEW STANDARDS ABOVE BNS ******************************************
//  ********************************** ALL NEW STANDARDS ABOVE BNS ******************************************



export const RequestApi = (url, body = '', method = 'GET') => {
  let header;
  if (method === 'GET') {
    header = {
      method,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    };
  } else {
    header = {
      method,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body)
    };
  }

  return new Promise((resolve, reject) => {
    fetch(url, header)
      .then(response => {
         
        if (response.status === 200) {
          resolve(response.json());
        } else {
          resolve(response.json());
        }
      })
      .catch(error => reject(error));
  });
};

export const StandardRequestApi = (url, body = '', method = 'GET') => {
  let header;
  if (method === 'GET') {
    header = {
      method,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    };
  } else {
    header = {
      method,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body)
    };
  }

  return new Promise((resolve, reject) => {
    fetch(url, header)
      .then(response => {
        resolve(response.json());
      })
      .catch(error => reject(error));
  });
};

export const GetLessonTypes = (id) =>
  StandardRequestApi(`${AppConfig.apiUrl}/api/LessonTypes?instructorId=${id}`);

export const GetLocations = (id) =>
  StandardRequestApi(`${AppConfig.apiUrl}/api/Locations?instructorId=${id}`);

export const GetOpenDatesAvailable = (instructorId, locationId, lessonTypeId) =>
  StandardRequestApi(`${AppConfig.apiUrl}/api/OpenDatesAvailable?instructorId=${instructorId}&locationId=${locationId}&lessonTypeId=${lessonTypeId}`);

export const StandardRequestWithTokenApi = (url, token, body = '', method = 'GET') => {
  let header;
  if (method === 'GET') {
    header = {
      method,
      headers: {
        'Authorization': "Bearer " + token,
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    };
  } else {
    header = {
      method,
      headers: {
        'Authorization': "Bearer " + token,
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: JSON.stringify(body)
    };
  }

  return new Promise((resolve, reject) => {
    fetch(url, header)
      .then(response => {
        
        resolve(response.json());
      })
      .catch(error =>{
        
        reject(error)});
  });
};

export const ReviewCheck = (token) => 
  StandardRequestWithTokenApi(`${AppConfig.apiUrl}/api/reviewCheck`, token, '', 'GET');


export const RequestMSApi = (url, body = '', method = 'GET') => {
  let header;
  if (method === 'GET') {
    header = {
      method,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    };
  } else {

    var details = {
      'grant_type': 'password',
      'username': body['username'],
      'password': body['password']
    };

    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");

    header = {
      method,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formBody//JSON.stringify(body)
    };
  }

  return new Promise((resolve, reject) => {
    fetch(url, header)
      .then(response => {
         
        if (response.status === 200) {

          resolve(response.json());
          // resolve(null);
        } else {
          resolve(response.json());
        }
      })
      .catch(error => reject(error));
  });
};

export const ContactRequestApi = (url, params_from=[], params_to=[], token='', method = 'POST') => {
  let header;
  if (method === 'GET') {
    header = {
      method,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    };
  } else {

    const details = {
      'id': 1,
      'userId': '',
      'instructorId': params_to['id'],
      "instructorFirstName": params_to['firstName'],
      "instructorLastName": params_to['lastName'],
      "locationId": 0,
      "appointmentType": "Personal",
      "lessonTypeId": 0,
      "lessonTypeName": "lesson type name",
      "lessonDurationMinutes": 60,
      "openDateId": 9,
      "personalTitle": "personal title",
      "note": "note",
      "clientNote": "client note",
      "startDate": "2017-03-06T14:17:20.9836924-05:00",
      "startTime": "2017-03-06T14:17:20.9836924-05:00",
      "endDate": "2017-03-06T14:17:20.9836924-05:00",
      "endTime": "2017-03-06T14:17:20.9836924-05:00",
      "createdDate": "2017-03-06T14:17:20.9836924-05:00",
      "lastUpdatedDate": "2017-03-06T14:17:20.9836924-05:00",
      "userReviewID": 151,
      "instructorClientId": 295,
      "firstName": params_from['firstName'],
      "lastName": params_from['lastName'],
      "phone":  params_from['phone'],
      "email": params_from['email'],
      "sendEmail": true,
      "sendSMS": true,
      "userSkillLevelId": params_from['userSkillLevelId']
    };

    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");

    header = {
      method,
      headers: {
        'Authorization': "Bearer " + token,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formBody//JSON.stringify(body)
    };
  }

  return new Promise((resolve, reject) => {
    fetch(url, header)
      .then(response => {
        if (response.status === 200) {
          resolve(response.json());
          // resolve(null);
        } else {
          resolve(response.json());
        }
      })
      .catch(error => reject(error));
  });
};

export const RegisterApi = (url, userData, method = 'POST') => {
    let header;

    header = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        'email': userData.email, 
      'password': userData.password, 
      'confirmPassword': userData.confirmPassword,
      'firstName': userData.firstName,
      'lastName': userData.lastName,
      'phoneNumber': userData.phoneNumber
      })
    };

  return new Promise((resolve, reject) => {
    fetch(url, header)
      .then(response => {
         
        if (response.status === 200) {
          
          resolve(response);
          // resolve(null);
        } else {
        
          resolve(response.json());
        }
      })
      .catch(error => reject(error));
  });
};

export const BookRequestApi = (url, openDateId='', lessonTypeId='', firstName='', lastName='', phone='', email='', instructorId='', locationId='', startDate='', clientNote='', token='', method = 'POST') => {
    let header;

    header = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'Bearer ' + AppConfig.global_userToken,
      },
      body: JSON.stringify({
        'openDateId': openDateId,
        'lessonTypeId': lessonTypeId,
        'firstName': firstName,
        'lastName': lastName,
        'phone': phone,
        'email': email,
        'instructorId': instructorId,
        'locationId': locationId,
        'startDate': startDate,
        'clientNote': clientNote
      })
    };

  return new Promise((resolve, reject) => {
    fetch(url, header)
      .then(response => {
        if (response.status === 200) {
         
          resolve(response.json());
          // resolve(null);
        } else {
         
          resolve(response.json());
        }
      })
      .catch(error => reject(error));
  });
};

export const RequestAppointmentsApi = (url, token, method = 'GET') => {
  let header;
  header = {
    method,
    headers: {
      'Authorization': "Bearer " + token,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  };
  return new Promise((resolve, reject) => {
    fetch(url, header)
      .then(response => {
        if (response.status === 200) {
          resolve(response.json());
          // resolve(null);
        } else {
          resolve(null);
        }
      })
      .catch(error => console.info('error', error));
  });
};

export const RequestPurchaseLessonsApi = (url, token, method = 'GET') => {
  let header;
  header = {
    method,
    headers: {
      'Authorization': "Bearer " + token,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  };
  return new Promise((resolve, reject) => {
    fetch(url, header)
      .then(response => {
        if (response.status === 200) {
          resolve(response.json());
          // resolve(null);
        } else {
          resolve(null);
        }
      })
      .catch(error => console.info('error', error));
  });
};

export const AccountInfoApi = (url, token, method = 'GET') => {
  let header;
  header = {
    method,
    headers: {
      'Authorization': "Bearer " + token,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  };
  return new Promise((resolve, reject) => {
    fetch(url, header)
      .then(response => {
        if (response.status === 200) {
          
          resolve(response.json());
          // resolve(null);
        } else {
          //RESET DATA
            
          AppConfig.global_userToken = null;
          AppConfig.global_isLoggedIn = false;
          GlobalStorage.setItem("storage_user", "");
          GlobalStorage.setItem("storage_password", "");
          GlobalStorage.setItem("storage_loggedIn", "false");
          GlobalStorage.setItem("storage_token", "");
          resolve(null);
        }
      })
      .catch(error => console.info('error', error));
  });
};

export const BookInfoApi = (url, token, method = 'GET') => {
  let header;
  header = {
    method,
    headers: {
      'Authorization': "Bearer " + token,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  };
  return new Promise((resolve, reject) => {
    fetch(url, header)
      .then(response => {
        if (response.status === 200) {
          
          resolve(response.json());
          // resolve(null);
        } else {
          
          resolve(null);
        }
      })
      .catch(error => console.info('error', error));
  });
};



export const ContactFormRequestApi = (url, method = 'POST') => {
  let header;
  header = {
    method,
    headers: {
      // 'Authorization': "Bearer " + token,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  };
  return new Promise((resolve, reject) => {
    fetch(url, header)
      .then(response => {
        if (response.status === 200) {
          resolve(response.json());
          // resolve(null);
        } else {
          resolve(null);
        }
      })
      .catch(error => console.info('error', error));
  });
};

export const SignUp = (userData) => (
  RegisterApi(`${AppConfig.apiUrl}/api/account/register`, userData, 'POST')
);

export const SignIn = (grant_type, username, password) => (
  RequestMSApi(`${AppConfig.apiUrl}/token`, {grant_type, username, password}, 'POST')
);



export const Appointments = (token) => (
  RequestAppointmentsApi(`${AppConfig.apiUrl}/api/clientAppointments?auth=auth`, token, 'GET')
);

export const PurchaseLessons = (token) => (
  RequestPurchaseLessonsApi(`${AppConfig.apiUrl}/api/purchaseLessons?auth=auth`, token, 'GET')
);

export const ContactRequest = (params_from, params_to, token) => (
  ContactRequestApi(`${AppConfig.apiUrl}/api/ContactRequests`, params_from, params_to, token, 'POST')
);

export const BookRequest = (openDateId, lessonTypeId, firstName, lastName, phone, email, instructorId, locationId, startDate, clientNote, token) => (
  BookRequestApi(`${AppConfig.apiUrl}/api/BookAppointments`, openDateId, lessonTypeId, firstName, lastName, phone, email, instructorId, locationId, startDate, clientNote, token, 'POST')
);

export const GetAccountInfo = (token) => (
  AccountInfoApi(`${AppConfig.apiUrl}/api/Account/UserInfo`, token, 'GET')
);

export const BookInfo = (token) => (
  BookInfoApi(`${AppConfig.apiUrl}/api/OpenDates/${AppConfig.instructorId_forBook}?lessonTypeId=${AppConfig.lessonTypeId_forBook}`, token, 'GET')
);

// For Stripe
const StripeCreateTokenApi = (body = '') => {
  const header = {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization: `Bearer ${AppConfig.stripeApiKey}`,
    },
    body,
  };
  return new Promise((resolve, reject) => {
    fetch(AppConfig.stripeApiUrl, header)
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const CheckVersionApi = () => {
  const header = {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${AppConfig.global_userToken}`,
    },
  };
  let url = '';
  if (Platform.OS == 'ios') 
    url = `${AppConfig.apiUrl}/api/Versions/${AppConfig.iosBuildVer}?Platform=${Platform.OS}`;
  else 
    url = `${AppConfig.apiUrl}/api/Versions/${AppConfig.androidBuildVer}?Platform=${Platform.OS}`;

  //test android
  //url = `${AppConfig.apiUrl}/api/Versions/${AppConfig.androidId}?Platform=android`;

  return new Promise((resolve, reject) => {
    fetch(url, header)
      .then((response) => response.json())
      .then((responseData) => {
            
            if (responseData != null) {
              //critical error
              if (responseData.id == 1) {
                Alert.alert(responseData.title, responseData.message);
              }
              //warning
              else if (responseData.id == 2) {
                Alert.alert(responseData.title, responseData.message);
              }
              
            } 
          })
      .catch(error => {
        //alert('outside error');
      
        
      });
  });
};

const StripePurchaseApi = (tokenID, totalAmount, instructorId, lessonRateId, promoCodeId) => {
  const header = {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${AppConfig.global_userToken}`,
    },
  };
  const url = `${AppConfig.apiUrl}/api/Payment?tokenId=${tokenID}&amount=${totalAmount}&instructorId=${instructorId}&lessonRateId=${lessonRateId}&promoCodeId=${promoCodeId}`;
  return new Promise((resolve, reject) => {
    fetch(url, header)
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const RequestStripeRegister = (cardDetails, totalAmount, instructorId, lessonRateId, promoCodeId) => {
  let formBody = [];
  _.forEach(cardDetails, (property, key) => {
    const encodedKey = encodeURIComponent(key);
    const encodedValue = encodeURIComponent(property);
    formBody.push(`${encodedKey}=${encodedValue}`);
  });

  formBody = formBody.join('&');

  return new Promise((resolve, reject) => {
    StripeCreateTokenApi(formBody)
      .then(response => {
        response.json()
          .then(json => {
            if (response.status === 200) {
              StripePurchaseApi(json.id, totalAmount, instructorId, lessonRateId, promoCodeId)
                .then(respone => resolve(respone.json()))
                .catch(error => reject(error));
            } else {
              reject(json.error);
            }
          })
      })
      .catch(error => reject(error));
  });
};


export const RequestFreeRegister = (totalAmount, instructorId, lessonRateId, promoCodeId) => {

  return new Promise((resolve, reject) => {
    StripePurchaseApi('', totalAmount, instructorId, lessonRateId, promoCodeId)
          .then(respone => resolve(respone.json()))
          .catch(error => reject(error));
  });
};

export const ForgotPassword = (data) => {
  const header = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    };
  const url = `${AppConfig.apiUrl}/api/Account/ForgotPassword`;
  return new Promise((resolve, reject) => {
    fetch(url, header)
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};

export const ChangePassword = (data) => {
  const header = {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${AppConfig.global_userToken}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    };
  const url = `${AppConfig.apiUrl}/api/Account/ChangePassword`;
  return new Promise((resolve, reject) => {
    fetch(url, header)
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
};
