import Dimensions from 'Dimensions';
import { Platform, StatusBar } from 'react-native';

const window = Dimensions.get('window');

export default {
  // App Details
  appName: 'BirdieNow',
  iosBuildVer: '123',
  androidBuildVer: '12',

  // Window Dimensions
  windowHeight: window.height,
  windowWidth: window.width,

  // App Strings
  sceneFromBottom: 'FloatFromBottom',
  sceneFromRight: 'FloatFromRight',
  sceneFade: 'FadeAndroid',

  globalStoragePrefix: 'TIX_DATA_',
  stor_state: 'State',
  stor_ticket: 'Ticket',
  stor_isFirst: 'First',
  stor_events: 'Events',

  // PRODUCTION
  //apiUrl: 'https://api.birdienow.com',
  //stripeApiKey: 'pk_live_BbhRWJZ9FOlgIKW0uutyorU9',


  // UAT
  apiUrl: 'https://uat-api.birdienow.com',
  //apiUrl: 'http://pkimpc:1243',
  stripeApiKey: 'pk_test_Wkc1loOi7E0WDgCHsCbhXglQ',



  // For Stripe Payment
  stripeApiUrl: 'https://api.stripe.com/v1/tokens',


  //googleAnalyticsTrackerId: 'UA-221020-10',
  googleAnalyticsTrackerId: 'UA-99746694-1',

  

  leftAction: null,
  rightAction: null,
  rightAction2: null,

  // GlobalConstants
  global_userToken: '',
  global_userSkillLevel: '1',
  global_appointments_normaldata: [],
  global_insID: "",
  global_instructor_from: false,
  searchResultDisplay: "",
  global_instructor_data: [],
  openDateId_forBook: 0,
  startDate_forBook: "",
  locationId_forBook: 0,

  // from global
  city:'',
  data:[],
  insID: '',
  insPGA: false,
  insPHONE: "",
  keyboardHeight: 0,
  userInfo: [],
  bookId_row: 0,
  bookId_col: 0,
  lessonTypeId_forBook : 0,
  instructorId_forBook : 0,
  isCurrentLoc: false,
  isInstructor: false,
  checkFocus: false,
  checkGPS: false,
  checkCity: false,
  checkCityName: '',
  checkCityZip: '',
  writeReviewData: [],


  // General Element Dimensions
  navbarHeight: 50,
  statusBarHeight: Platform.OS === 'ios' ? 20 : StatusBar.currentHeight,

  // Fonts
  baseFont: 'Gotham-Book',
  basefontSize: 13,

  // Colors
  primaryColor: '#F9F9F9',
  secondaryColor: '#464646',
  textColor: '#464646',
  borderColor: '#E7E7E7',
};
