import { Dimensions, StyleSheet } from 'react-native';
import AppConfig from 'AppConfig';

var { width, height } = Dimensions.get('window');
var r_width =  width / 1125;
var r_height = height / 1659;

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  topView:{
    flexDirection: 'row',
    height: 50,
    // backgroundColor: '#aac1ce',
    backgroundColor: '#ffffff',
    borderBottomWidth: 1,
    borderBottomColor:'#D3D3D3',
    justifyContent: "center"
  },
  titleView:{flexDirection:'row', alignItems:'center', marginTop: 30 * r_width,},
  title:{marginLeft:32 * r_width, fontSize: 13, color: '#4b5b64', fontFamily: 'Gotham-Bold', paddingTop: 5 },
  location:{marginLeft:2,fontSize: 12,color:'#474747',marginTop:7, marginBottom:5.5, fontFamily: 'Gotham-Medium'},
  locationAddress:{marginLeft:2,fontSize: 12,color:'#474747',fontFamily: 'Gotham-Book'},
  content:{marginLeft:2,fontSize: 12,color:'#474747',fontFamily: 'Gotham-Book'},
  contentMore:{marginLeft:2,fontSize: 13,color:'#aac1ce',fontFamily: 'Gotham-Book', marginTop: 10, marginBottom: 10},
  contentBold:{marginLeft:2,fontSize: 12,color:'#474747',marginTop:7, marginBottom:5.5, fontFamily: 'Gotham-Medium'},
  blueContent:{fontSize: 12, color:'#0086F8', marginTop:10, marginBottom:10, fontFamily: 'Gotham-Book'}
});
