import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  rowPanel: {
    paddingHorizontal: 30,
  },
  rowTextFont: {
    fontSize: 14,
    fontFamily: 'Gotham-Book',
    padding: 5,
  },
  txtMonth: {
    backgroundColor: '#f7f7f7',
    color: '#8c8c8c',
    fontFamily: 'Gotham-Bold',
    padding:10
  },
  btnTime: {
    color: '#fff',
    fontSize: 12, 
    fontFamily: 'Gotham-Book',
    textAlign: 'center' 
  },
  btnTimeWrapper: {
    height: 40,
    width: 75,
    borderRadius: 6,
    backgroundColor: '#32b3fa',
    marginHorizontal: 5,
    padding: 3,
    justifyContent: 'center',
    marginBottom: 10,
  },
});
