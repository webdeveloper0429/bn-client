import React, { Component, PropTypes } from 'react';
import { View, Alert, Text, ScrollView, TouchableWithoutFeedback, ListView, ActivityIndicator,
  TouchableHighlight, TouchableOpacity, BackAndroid } from 'react-native';
import styles from './styles';
import { GetLessonTypes, GetLocations, GetOpenDatesAvailable, MakeCancelable } from 'AppUtilities';
import { HeaderBar, Panel, Calendar } from 'AppComponents';
import _, { isEqual } from 'lodash';
import moment from 'moment';
import AppConfig from 'AppConfig';
import { BookScene } from 'AppScenes';

const leftLabel = (
  <Text allowFontScaling={false} style={{ fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#ffffff', paddingLeft: 15, padding: 5 }}>Back</Text>
);

const centerLabel = (
   <Text allowFontScaling={false} style={{ fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#ffffff' }}>Calendar</Text>
);

const calHeaderStyle = {
  calendarControls: { backgroundColor: '#f7f7f7' },
  calendarHeading: {
    backgroundColor: '#f7f7f7',
    borderColor: '#fff',
    borderBottomWidth: 0,
  },
  title: { color: '#adadad' },
  dayHeading: { color: '#adadad' },
  weekendHeading: { color: '#adadad' },
}, calContentStyle = {
  calendarContainer: { backgroundColor: '#fff', },
  currentDayCircle: { backgroundColor: '#da3743' },
  currentDayText: { color: '#32b3fa' },
  selectedDayCircle: { backgroundColor: '#da3743' },
  selectedDayText: { color: '#fff' },
};
class FullCalendarScene extends Component {
  static propTypes = {
    popNavBack: PropTypes.func.isRequired,
    pushNavScene: PropTypes.func.isRequired,
    popNavNBack: PropTypes.func,
    InstructorID: PropTypes.number.isRequired,
    LocationID: PropTypes.number.isRequired,
    LessonTypeID: PropTypes.number.isRequired,
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      selectedLessonType: { id: this.props.LessonTypeID },
      lessonTypes: [],
      selectedLocation: { id: this.props.LocationID },
      locations: [],
      selectedDate: null,
      calendarListSource: null,
      isCalendarLoading: true,
      isFirstLoading: true,
      OpenDates: []
    };
    this.ListOpenDates = {};
    this.sectionKeys = [];
    this.apiCalendarPromise = null;

    this.ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => !isEqual(r1, r2),
      sectionHeaderHasChanged: (s1, s2) => !isEqual(s1, s2),
    });

    this.state.calendarListSource = this.ds.cloneWithRowsAndSections(this.ListOpenDates);
    this.renderCalendarRow = ::this.renderCalendarRow;
    this.CallBackBookScene = ::this.CallBackBookScene;

    

    this.handlePop = ::this.handlePop;
  }

  handlePop(id) {
    if (id == 0) {
      if (this.refBookScene != null) {
        this.props.popNavNBack(4);
      } else {
        this.props.popNavNBack(3);
      }
    } else {
      if (this.refBookScene != null) {
        this.props.popNavNBack(3);
      } else {
        this.props.popNavNBack(2);
      }
    }
    // this.props.popNavBack();
  }

  CallBackBookScene()
  {
    this.updateCalendar(this.state.selectedLocation.id, this.state.selectedLessonType.id);
  }

  componentDidMount() {
		//BackAndroid.addEventListener('hardwareBackPress', this.onBackPress.bind(this));
    GetLessonTypes(this.props.InstructorID)
      .then(response => {
        if (this.refContainer) {
          const lessonType = _.find(response, _.matches({ id: this.props.LessonTypeID }));
          this.setState({ selectedLessonType: lessonType, lessonTypes: response });
        }
      })
      .catch(error => {
        Alert.alert(error.message);
      });

    GetLocations(this.props.InstructorID)
      .then(response => {
        if (this.refContainer) {
          const location = _.find(response, _.matches({ id: this.props.LocationID }));
          this.setState({ selectedLocation: location, locations: response });
        }
      })
      .catch(error => {
        Alert.alert(error.message);
      });
    this.updateCalendar(this.state.selectedLocation.id, this.state.selectedLessonType.id);

    
    if (this.props.SetDate != null)
    {
      this.setState({ selectedDate: this.props.SetDate });
      //this.onDateSelect(this.props.SetDate);
    }
      

      
  }

	onBackPress() {
		this.props.popNavBack();
	}

	onBook(sectionID, rowID, time) {
    let i = this.ListOpenDates[sectionID][0][rowID].indexOf(time);
    let selectedDate = moment(this.state.selectedDate).format('YYYY-MM-DD');

    let openDate = this.state.OpenDates.filter(function(item) {
      var date = moment(item.localStartDate).format('YYYY-MM-DD');
      return date == selectedDate;
    });
    if (this.openDate !== null) {
      // AppConfig.lessonTypeId_forBook = this.state.selectedLessonType.id;
      // AppConfig.instructorId_forBook = this.props.InstructorID;
      // AppConfig.locationId_forBook = this.state.selectedLocation.id;
      // AppConfig.openDateId_forBook = openDate[i].id;
      // AppConfig.startDate_forBook = openDate[i].startDate;
      // AppConfig.endDate_forBook = openDate[i].endDate;

      if (!AppConfig.global_isLoggedIn) {
        Alert.alert("Please log in to access this feature");
      } else {
        this.props.pushNavScene(BookScene, { ref: (c) => { this.refBookScene = c; }, 
      passProps: { openDateId: openDate[i].id, 
        lessonTypeId: this.state.selectedLessonType.id,      
      }, callback: this.CallBackBookScene });
      }
    }
  }

  onDateSelect(date) {
    this.setState({
      calendarListSource: this.ds.cloneWithRowsAndSections(this.ListOpenDates, this.sectionKeys),
      selectedDate: date });
  }

  onLessonItemClicked(lesson) {
    if (this.state.selectedLessonType.id === lesson.id) {
      return;
    }
    this.setState({ selectedLessonType: lesson });
    this.updateCalendar(this.state.selectedLocation.id, lesson.id);
  }

  onLocationItemClicked(location) {
    if (this.state.selectedLocation.id === location.id) {
      return;
    }
    this.setState({ selectedLocation: location });
    this.updateCalendar(location.id, this.state.selectedLessonType.id);
  }

  updateCalendar(locationID, lessonTypeID) {
    this.setState({ isCalendarLoading: true });
    if (this.apiCalendarPromise !== null) {
      this.apiCalendarPromise.cancel();
    }
    this.apiCalendarPromise = MakeCancelable(GetOpenDatesAvailable(this.props.InstructorID,
      locationID, lessonTypeID));

    this.apiCalendarPromise.promise
      .then(response => {
    
        this.state.OpenDates = response.sort(function(a, b){
          var keyA = new Date(a.startDate),
              keyB = new Date(b.startDate);

          if(keyA < keyB) return -1;
          if(keyA > keyB) return 1;
          return 0;
        });

        this.ListOpenDates = {};
        _.forEach(this.state.OpenDates, (date) => {
          const startDate = moment(date.localStartDate);
          const txtSection = startDate.format('YYYY-MM');
          const txtRow = startDate.format('YYYY-MM-DD');
          const timeOfDate = startDate.format('h:mm A');

          if (this.ListOpenDates[txtSection]) {
            this.ListOpenDates[txtSection][0][txtRow] = this.ListOpenDates[txtSection][0][txtRow] || [];
            if (_.indexOf(this.ListOpenDates[txtSection][0][txtRow], timeOfDate) < 0) {
              this.ListOpenDates[txtSection][0][txtRow].push(timeOfDate);
            }
          } else {
            this.ListOpenDates[txtSection] = [{ [txtRow]: [timeOfDate] }];
          }
        });

        let selectedDate = null;
        if (_.isEmpty(this.ListOpenDates)) {
          this.ListOpenDates[moment().format('YYYY-MM')] = [{ }];
        } else {
          let minKeySection, minKeyRow;
          _.forEach(this.ListOpenDates, (value, key) => {
            if (!minKeySection || minKeySection > key) {
              minKeySection = key;
            }
            this.ListOpenDates[key][0].availDates = _.map(value[0], (val, ke) => moment(ke).date());
          });

          _.forEach(this.ListOpenDates[minKeySection][0], (value, key) => {
            if (key === 'availDates') {
              return;
            }
            if (!minKeyRow || minKeyRow > key) {
              minKeyRow = key;
            }
            //this.ListOpenDates[minKeySection][0][key] = _.orderBy(this.ListOpenDates[minKeySection][0][key], null, ['asc']);
          });

          if (this.state.selectedDate == null)
            selectedDate = minKeyRow;
          else
            selectedDate = this.state.selectedDate;

          
        }

        this.sectionKeys = _.sortBy(Object.keys(this.ListOpenDates));

        if (this.state.isFirstLoading) {
          setTimeout(() => {
            if (this.refContainer) {
              this.setState({
                calendarListSource: this.ds.cloneWithRowsAndSections(this.ListOpenDates, this.sectionKeys),
                isCalendarLoading: false,
                isFirstLoading: false,
                selectedDate
              });
            }
          }, 100);
        } else if (this.refContainer) {
          this.setState({
            calendarListSource: this.ds.cloneWithRowsAndSections(this.ListOpenDates, this.sectionKeys),
            isCalendarLoading: false,
            selectedDate
          });
        }
      })
      .catch(error => {
        if (error.name !== 'CancelledPromiseError') {
          Alert.alert(error.message);
          this.setState({ isCalendarLoading: false });
        }
      });
  }

  renderCalendarRow(rowData, sectionID, rowID) {
    return (
      <Calendar
        customStyle={calContentStyle}
        startDate={`${sectionID}-01`}
        availDates={rowData.availDates}
        selectedDate={this.state.selectedDate}
        onDateSelect={(date) => this.onDateSelect(date)}
        headerEnabled={false}
        weekStart={0}
      />
    );
  }

  renderCalendarHeader(sectionData, sectionID) {
    return (
      <Calendar
        customStyle={calHeaderStyle}
        startDate={`${sectionID}-01`}
        headerEnabled={true}
        weekStart={0}
      />
    );
  }

  renderCalendarSeparator(sectionID, rowID) {
    return <View key={`${sectionID}-${rowID}`} style={{ backgroundColor: '#ebe8e6', height: 2 }} />;
  }


  renderLessonItems() {
    return _.map(this.state.lessonTypes, (lesson) => (
      <TouchableWithoutFeedback
        key={lesson.id}
        onPress={() => this.onLessonItemClicked(lesson)}
      >
        <View style={{ opacity: lesson.id === this.state.selectedLessonType.id ? 1 : 0.6, paddingHorizontal: 30 }}>
          <Text allowFontScaling={false} style={styles.rowTextFont}>{lesson.name}</Text>
        </View>
      </TouchableWithoutFeedback>
    ));
  }

  renderLocationItems() {
    return _.map(this.state.locations, (location) => (
      <TouchableWithoutFeedback
        key={location.id}
        onPress={() => this.onLocationItemClicked(location)}
      >
        <View style={{ opacity: location.id === this.state.selectedLocation.id ? 1 : 0.6, paddingHorizontal: 30 }}>
          <Text allowFontScaling={false} style={styles.rowTextFont}>{location.name}</Text>
        </View>
      </TouchableWithoutFeedback>
    ));
  }

  renderTimeButtons() {
    const sectionID = moment(this.state.selectedDate).format('YYYY-MM');
    const rowID = moment(this.state.selectedDate).format('YYYY-MM-DD');

    if (!this.ListOpenDates[sectionID]) {
      return null;
    }
    return _.map(this.ListOpenDates[sectionID][0][rowID], (time) =>
      <TouchableOpacity key={time} underlayColor="#046da7" onPress={() => {this.onBook(sectionID, rowID, time);}} style={styles.btnTimeWrapper}>
        <Text allowFontScaling={false} style={styles.btnTime}>{time}</Text>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View ref={(ref) => this.refContainer = ref} style={styles.container}>
        <HeaderBar
          backgroundColor='#aac1ce'
          leftLabel={leftLabel}
          leftAction={this.props.popNavBack}
          centerLabel={centerLabel}
        />
        <View style={{ alignSelf: 'stretch' }}>
          { this.state.selectedLessonType.name ?
            <Panel
              fontStyle={{ flex: 1, fontFamily: 'Gotham-Bold', fontSize: 15 }}
              titleStyle={{ paddingHorizontal: 20 }}
              title={this.state.selectedLessonType.name}
              index={1}
            >
              {this.renderLessonItems()}
            </Panel> : null }
          { this.state.selectedLocation.name ?
            <Panel
              fontStyle={{ flex: 1, fontFamily: 'Gotham-Bold', fontSize: 15  }}
              titleStyle={{ paddingHorizontal: 20 }}
              title={this.state.selectedLocation.name}
              index={1}
            >
              {this.renderLocationItems()}
            </Panel> : null }
        </View>

        <View style={{ flex: 1, backgroundColor: '#f7f7f7' }}>
          { this.state.isCalendarLoading ?
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <ActivityIndicator
                animating={true}
                size="small"
              />
            </View> :
            <ListView
              dataSource={this.state.calendarListSource}
              renderRow={this.renderCalendarRow}
              renderSectionHeader={this.renderCalendarHeader}
              renderSeparator={this.renderCalendarSeparator}
              enableEmptySections={true}
              automaticallyAdjustContentInsets={false}
            />
          }
        </View>
        {this.state.selectedDate ? 
        <Text allowFontScaling={false} style={styles.txtMonth}>
          {this.state.selectedLessonType.name} on {moment(this.state.selectedDate).format('ddd')}, {moment(this.state.selectedDate).format('MMMM D')}
        </Text>
        : null}
        
        <View style={{ backgroundColor: '#fff', height: 180 }}>
          <ScrollView  contentContainerStyle={{ paddingLeft: 10, paddingRight: 10, paddingTop: 10, paddingBottom: 15 }}>
            <View style={{ flex: 1, flexDirection: 'row',  flexWrap: 'wrap', justifyContent: 'center', backgroundColor: '#fff' }}>
              
                {this.renderTimeButtons()}
              
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

export default FullCalendarScene;
