import { Dimensions, StyleSheet } from 'react-native';
import AppConfig from 'AppConfig';

var { width, height } = Dimensions.get('window');
var r_width =  width / 1125;
var r_height = height / 1659;

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  topView:{
    flexDirection: 'row',
    height: 65,
    backgroundColor: '#aac1ce',
    borderBottomWidth: 1,
    borderBottomColor:'#D3D3D3',
    justifyContent: "center"
  },
  titleView:{flexDirection:'row', alignItems:'center', marginTop: 50 * r_width,},
  title:{marginLeft:32 * r_width, fontSize: 13, fontFamily: 'Gotham-Bold'},
  content:{marginLeft:2,fontSize: 12, color:'#474747', fontFamily: 'Gotham-Book'},
  
  
});