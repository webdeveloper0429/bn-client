import React, { Component, PropTypes } from 'react';
import { Dimensions, View, TouchableOpacity, Text, ListView, ScrollView, ActivityIndicator, Platform, BackAndroid } from 'react-native';
import styles from './styles';
import AppStyles from 'AppStyles';
import _, { isEqual } from 'lodash';
import StarRating from 'react-native-star-rating';
import moment from 'moment';

class ReviewScene extends Component {
  static propTypes = {
    popNavBack: PropTypes.func.isRequired,
    pushNavScene: PropTypes.func.isRequired,
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      data: [],
      showloading: false
    };

    let { width, height } = Dimensions.get('window');
    this.width = width;
    this.height = height;
    this.r_width =  width / 1125;
    this.r_height = height / 1659;
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.pageNum = 2;

    this.handlePop = ::this.handlePop;
  }

	onBackPress() {
		this.props.popNavBack();
	}

  componentDidMount() {
		//BackAndroid.addEventListener('hardwareBackPress', this.onBackPress.bind(this));
		if (this.props.passProps != null) {
      this.setState({ data: this.props.passProps.instructor });      
    }
  }

  handlePop() {
    this.props.popNavBack();
  }

  render() {
    const render_TopBar = (
      <View style={[styles.topView,{height: (Platform.OS === 'ios') ? 65 : 40}]}>
        <TouchableOpacity style={{flex: 1, justifyContent: "center"}} onPress={() => {this.props.popNavBack();}}>
          <Text allowFontScaling={false} style={{fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, marginTop: (Platform.OS === 'ios') ? 20 : 0, color: '#ffffff'}}> Back </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{flex: 2, justifyContent: "center"}}>
          <Text allowFontScaling={false} style={{textAlign: "center", fontSize: 15, marginTop: (Platform.OS === 'ios') ? 20 : 0, fontFamily: 'Gotham-Bold', color: '#ffffff'}}>Reviews</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{flex: 1, justifyContent: "center"}}>
          <Text allowFontScaling={false} style={{flex: 1, marginTop: (Platform.OS === 'ios') ? 20 : 0, textAlign: "center" }}/>
        </TouchableOpacity>
      </View>
    );

    const render_Reviews = [];
    if (this.state.data.id == null || this.state.data.reviewCount == 0) {
      render_Reviews.push(
        <Text allowFontScaling={false} key="render_ratebar" style={[styles.content, {marginTop: 20, textAlign:'center'}]}>
          There are no reviews for this intructor
        </Text>
      );
    } else {
      let reviewCount = this.state.data.reviewCount;

      for (let i = 0; i < reviewCount; i++) {
        let key1 = "reviewkey1" + i;
        let key2 = "reviewkey2" + i;
        let key3 = "reviewkey3" + i;
        let key4 = "reviewkey4" + i;
        let rating = this.state.data.reviews[i].overallRating;
        let reviewdate = this.state.data.reviews[i].reviewDate;
        let skilllevel = this.state.data.reviews[i].userSkillLevelName;
        let lessonfocus = this.state.data.reviews[i].reviewLessonTypeNames;
        let reviewsaid = this.state.data.reviews[i].statement;
        let clientfirstname = this.state.data.reviews[i].clientFirstName;
        let clientlastname = this.state.data.reviews[i].clientLastName;
        clientlastname = clientlastname.charAt(0).toUpperCase() + '.';

        render_Reviews.push(
          <View key={key1} style={{flexDirection:'row', flex:1, marginTop:15, marginBottom:0}}>
            <View style={{flex: 2}}>
              <StarRating
                starColor = {'#32b3fa'}
                emptyStar={'ios-star-outline'}
                fullStar={'ios-star'}
                halfStar={'ios-star-half'}
                iconSet="Ionicons"
                disabled={true}
                maxStars={5}
                rating={rating}
                selectedStar={(rating) => this.onStarRatingPress(rating)}
                starSize = {15}
                emptyStarColor={'#32b3fa'}
              />
            </View>
            <View style={{flex: 6, alignSelf: 'flex-end'}}>
              
            </View>
            <View style={{flex: 2, alignSelf: 'flex-end', paddingRight: 10}}>
              <Text allowFontScaling={false} style={[styles.content,{ alignSelf: 'flex-end'}]}>{moment(reviewdate).format("MM/DD/YY")}</Text>
            </View>
            
          </View>
        );
        render_Reviews.push(
          <Text key={key2} style={[styles.content, { fontFamily: 'Gotham-Medium', fontSize: 12 }]}>Skill Level:
            <Text allowFontScaling={false} style={[styles.content, { fontFamily: 'Gotham-Book', fontSize: 12 }]}> {skilllevel}</Text>
          </Text>
        );
        render_Reviews.push(
          <Text allowFontScaling={false} key={key3} style={[styles.content, { fontFamily: 'Gotham-Medium', fontSize: 12 }]}>Lesson Focus:
            <Text allowFontScaling={false} style={[styles.content, { fontFamily: 'Gotham-Book', fontSize: 12 }]}> {lessonfocus}</Text>
          </Text>
        );
        render_Reviews.push(
          <Text allowFontScaling={false} key={key4} style={[styles.content, { fontFamily: 'Gotham-Medium', fontSize: 12 }]}>{clientfirstname} {clientlastname} said:
            <Text allowFontScaling={false} style={[styles.content, { fontFamily: 'Gotham-Book', fontSize: 12 }]}> {reviewsaid}</Text>
          </Text>
        );

      }
    }

    return (
      <View style={styles.container}>
        {render_TopBar}
        <ScrollView>
          <View style={{marginRight: 5, marginLeft: 5}}>
            {render_Reviews}
          </View>
        </ScrollView>
        {this.state.showloading ?
          <View style={{position:'absolute', top: 0, left:0, bottom:0, right:0, alignItems:'center', justifyContent:'center', alignSelf: 'stretch', backgroundColor: 'transparent'}}>
            <ActivityIndicator animating={true} size="small" color="gray" />
          </View>:null}
      </View>
    );
  }
}

export default ReviewScene;
