import React, { Component, PropTypes } from 'react';
import {
  Dimensions,
  Alert,
  View,
  Image,
  TouchableOpacity,
  Platform,
  Text,
  ListView,
  RefreshControl,
  ScrollView,
  ActivityIndicator,
  PanResponder,
	BackAndroid,
  Linking
} from 'react-native';
import styles from './styles';
import AppConfig from 'AppConfig';
import AppStyles from 'AppStyles';
import { SearchBar, HeaderBar } from 'AppComponents';
import { FilterScene, ListDetailScene, InstructorContact, FullCalendarScene, BookScene, ReviewScene, PaymentScene } from 'AppScenes';
import { RequestApi, MakeCancelable, GlobalStorage } from 'AppUtilities';
import _, { isEqual } from 'lodash';
import StarRating from 'react-native-star-rating';
import moment from 'moment';
import { GoogleAnalyticsTracker } from 'react-native-google-analytics-bridge';




class InstructorScene extends Component {
  static propTypes = {
    popNavBack: PropTypes.func.isRequired,
    pushNavScene: PropTypes.func.isRequired,
    callback: PropTypes.func,
    popNavNBack: PropTypes.func,
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      data: [],
      lessonTypes: [],
      instructorId: '',
      lessonTypeId: '',
      isDataLoaded: false,
      showloading: false,
      showAllQualificationsAndExperience: false
    };

    let { width, height } = Dimensions.get('window');
    this.width = width;
    this.height = height;
    this.r_width =  width / 1125;
    this.r_height = height / 1659;
    this.r2_width =  width / 1125;
    this.r2_height = height / 1659;
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.pageNum = 2;

    this.onContactInstructor = this.onContactInstructor.bind(this);
    this.onViewFullCalendar = this.onViewFullCalendar.bind(this);
    this.onBook = this.onBook.bind(this);
    this.OnBuy = this.OnBuy.bind(this);
    this.onMoreReviews = this.onMoreReviews.bind(this);
    this.onShowQualificationsAndExperience = this.onShowQualificationsAndExperience.bind(this);
    this.onViewFullCalendar = this.onViewFullCalendar.bind(this);

    this.handlePopCheck = ::this.handlePopCheck;
    this.CallBackBookScene = ::this.CallBackBookScene;
    this.openDirections = ::this.openDirections;
  }

  handlePopCheck(id) {
    if (id == 0) {
      if (this.refFullCalendarScene != null) {
        this.refFullCalendarScene.handlePop(0);
      } else if (this.refBookScene != null) {
        // this.refBookScene.handlePop();
        this.props.popNavNBack(3);
      } else if (this.refInstructorContact != null) {
        // this.refInstructorContact.handlePop();
        this.props.popNavNBack(3);
      } else if (this.refReviewScene != null) {
        // this.refReviewScene.handlePop();
        this.props.popNavNBack(3);
      } else {
        this.props.popNavNBack(2);
      }
    } else {
      if (this.refFullCalendarScene != null) {
        this.refFullCalendarScene.handlePop();
      } else if (this.refBookScene != null) {
        // this.refBookScene.handlePop();
        this.props.popNavNBack(2);
      } else if (this.refInstructorContact != null) {
        // this.refInstructorContact.handlePop();
        this.props.popNavNBack(2);
      } else if (this.refReviewScene != null) {
        // this.refReviewScene.handlePop();
        this.props.popNavNBack(2);
      } else {
        this.props.popNavBack();
      }
    }
  }

  openDirections(address) {
    Platform.select({
        ios: () => {
            Linking.openURL('http://maps.apple.com/maps?q=' +  address);
        },
        android: () => {
            Linking.openURL('http://maps.google.com/maps?z=12&t=m&q=' + address);
        }
    })();
  }

  onContactInstructor() {
    if (!AppConfig.global_isLoggedIn) {
      Alert.alert("Please log in to access this feature");
    } else {
      this.props.pushNavScene(InstructorContact, { ref: (c) => { this.refInstructorContact = c; }, passProps: { instructor: this.state.data } });
    }
  }

  onViewFullCalendar(index) {
    //TODO: pass the following param: InstructorID; LocationID; LessonType
    this.props.pushNavScene(FullCalendarScene, {
      ref:(c) => { this.refFullCalendarScene = c; },
      InstructorID: this.state.data.id,
      LocationID: this.state.data.locations[index].id,
      LessonTypeID: this.state.data.lessonTypes[0].id,
    });
  }

  onMoreReviews() {
    this.props.pushNavScene(ReviewScene, {ref:(c) => { this.refReviewScene = c; }, passProps: { instructor: this.state.data } });
  }

  onShowQualificationsAndExperience(value) {
    this.setState({showAllQualificationsAndExperience: value});
  }

  componentDidMount() {
    this.setState({showloading: true});
    

		//BackAndroid.addEventListener('hardwareBackPress', this.onBackPress.bind(this));
    //Do you have input params?
    if (this.props.passProps != null) {     
      //If you have some preloaded instructor data
      if (this.props.passProps.instructor != null)
      {
        this.setState({ data: this.props.passProps.instructor });            
      }
      else {
        //You don't have preloaded instructor id, use id only
        if (this.props.passProps.id != null) {          

        }
      }
    }

    var lessonTypeId = 0;
    if (this.props.passProps.instructor.lessonTypeId != null)
      lessonTypeId = this.props.passProps.instructor.lessonTypeId;

    this.setState({ lessonTypeId: this.props.passProps.instructor.lessonTypeId, 
      instructorId: this.props.passProps.id });   
 

    fetch(AppConfig.apiUrl + '/api/InstructorSearchViewModels/' + this.props.passProps.id + '?lessonTypeId=' + lessonTypeId + '&attachOpenDates=false' )
      .then((response) => response.json())
      .then((responseData) => {
;
        //get distinct lesson types
        var distinct = [];
        for (let i=0; i<responseData.lessonRates.length; i++)
        {
          var found = false;
          for (let j=0; j<distinct.length; j++)
            if (distinct[j] == responseData.lessonRates[i].lessonTypeName)
              found = true;

          if (!found)
            distinct.push(responseData.lessonRates[i].lessonTypeName);
        }

        let tracker = new GoogleAnalyticsTracker(AppConfig.googleAnalyticsTrackerId);
        tracker.trackScreenView('InstructorView: ' + responseData.firstName + ' ' + responseData.lastName + ' ' + responseData.id);


        this.setState({showloading: false, data: responseData, isDataLoaded: true, lessonTypes: distinct});
      })
  }

	onBackPress() {
    this.props.popNavBack();
  }

  CallBackBookScene()
  {
    
    this.setState({showloading: true});
    var lessonTypeId = 0;
    if (this.state.lessonTypeId != null)
      lessonTypeId = this.state.lessonTypeId;
    fetch(AppConfig.apiUrl + '/api/InstructorSearchViewModels/' + this.state.instructorId + '?lessonTypeId=' + lessonTypeId)
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({showloading: false, data: responseData, isDataLoaded: true});
        
      })
  }

  // onBook(i, j) {
  //   if (!AppConfig.global_isLoggedIn) {
  //     Alert.alert("Please log in to access this feature");
  //   } else {
  //     //this.props.pushNavScene(BookScene, {ref:(c) => { this.refBookScene = c; },});
  //     this.props.pushNavScene(BookScene, { ref: (c) => { this.refBookScene = c; }, 
  //     passProps: { openDateId: this.state.data.locations[i].openDates[j].id, 
  //       openDateLocalStartDate: this.state.data.locations[i].openDates[j].localStartDate, 
  //       lessonTypeId: this.state.data.lessonTypeId,
  //       lessonTypeName: this.state.data.lessonTypeName,
  //       lessonTypeDescription: this.state.data.lessonTypeDescription,
  //       lessonTypeDurationMinutes: this.state.data.lessonTypeDurationMinutes,
  //       instructorId: this.state.data.id,
  //       instructorFirstNamne: this.state.data.firstName,
  //       instructorLastName: this.state.data.lastName,
  //       instructorIsPga: this.state.data.id.isPga,
  //       instructorIsProfilePhoto: this.state.data.id.isProfilePhoto,
  //       address1: this.state.data.locations[i].address1,
  //       address2: this.state.data.locations[i].address2,
  //       city: this.state.data.locations[i].city,
  //       state: this.state.data.locations[i].state,
  //       zip: this.state.data.locations[i].zip 
  //     }, callback: this.CallBackBookScene});
  //   }
  // }

  onBook(instructorId, locationId, lessonTypeId, setDate)
  {
    this.props.pushNavScene(FullCalendarScene, {
      ref:(c) => { this.refFullCalendarScene = c; },
      InstructorID: instructorId,
      LocationID: locationId,
      LessonTypeID: lessonTypeId,
      SetDate: setDate
    });
  }

  OnBuy(i) {
    if (!AppConfig.global_isLoggedIn) {
      Alert.alert("Please log in to access this feature");
    } else {
      this.props.pushNavScene(PaymentScene, { ref: (c) => { this.refPaymentScene = c; }, passProps: { instructor: this.state.data, lessonRate: this.state.data.lessonRates[i] }});
    }
  }

  render() {
    const render_TopBar = (
      <View style={{
        flexDirection: 'row',
        alignItems: 'center', backgroundColor: '#aac1ce', borderBottomWidth: 0, borderBottomColor: '#85D3DD',
        height: (Platform.OS === 'ios') ? 65 : 40,
        justifyContent: "center"
      }}>
        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={() => { this.props.popNavBack(); }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#ffffff',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Back</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 2, justifyContent: "center" }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#ffffff',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Profile</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#ffffff',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}></Text>
        </TouchableOpacity>
      </View>

/*

<Text allowFontScaling={false} style={{
          textAlign: "center", color: '#ffffff', fontSize: 15,
          marginTop: (Platform.OS === 'ios') ? 10 : 0,

          fontFamily: 'Gotham-Bold'
        }}>
          Profile
          </Text>

      <View style={styles.topView}>
        <TouchableOpacity style={{flex: 1, justifyContent: "center"}} onPress={() => {this.props.popNavBack();}}>
          <Text allowFontScaling={false} style={{fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 12, marginTop: 10}}>Back</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{flex: 2, justifyContent: "center"}}>
          <Text allowFontScaling={false} style={{textAlign: "center", fontSize: 13, marginTop: 10, fontFamily: 'Gotham-Bold'}}>Profile</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{flex: 1, justifyContent: "center"}}>
          <Text allowFontScaling={false} style={{flex: 1, textAlign: "center", }}/>
        </TouchableOpacity>
      </View>
      */
    );


    const render_ImageBar = (
      <View style={{ flexDirection: 'row', marginTop: 25 * this.r_width, }}>
        {this.state.data.isProfilePhoto ?
          <Image key="render_image"
            style={{ width: 246 * this.r_width, height: 246 * this.r_width, borderRadius: 123 * this.r_width, marginLeft: 36 * this.r_width }}
            source={{ uri: 'https://www.birdienow.com/img/instructor/profile/' + this.state.data.id + '.jpg' }}  />         
          :
        <Image key="render_image"
          style={{ width: 246 * this.r_width, height: 246 * this.r_width, borderRadius: 123 * this.r_width, marginLeft: 36 * this.r_width }}
          source={require('img/profile/profile.png')}  />       
        }

        {/*{this.state.data.isPga ?
          <Image
            style={{ width: 30, height: 30, position: "absolute", marginLeft: 25 * this.r_width, marginTop: 150 * this.r_width }}
            source={{ uri: 'https://www.birdienow.com/img/badge/pga_badge_85.png' }}
          /> : null}*/}


        <View style={{ marginLeft: 36 * this.r_width, justifyContent: 'center' }}>
          <View style={{flexDirection: 'row'}}>
            <Text allowFontScaling={false} style={{ fontSize: 17, color: '#474747', fontFamily: 'Gotham-Bold' }}>
              {this.state.data.firstName} {this.state.data.lastName}
            </Text>
            {/*{this.state.data.isPga ?
              <Image
                style={{ width: 25, height:25, marginLeft:5   }}
                source={{ uri: 'https://www.birdienow.com/img/badge/pga_badge_85.png' }}
              /> : null}*/}
          </View>
          {
            this.state.data.lessonCount > 0 ?
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 3}}>
            <Text allowFontScaling={false} style={{ color: '#6f6f6f', fontSize: 12, fontFamily: 'Gotham-Medium' }}>
              {this.state.data.lessonCount} Verified Lessons
            </Text>
          </View>
          : null
          }
          <View>


            <TouchableOpacity
              onPress={() => { this.onMoreReviews(); }}
              style={{ flexDirection: 'row', marginBottom: 5, marginTop: 3, alignItems: 'center' }}>
              <StarRating
                starColor={'#32b3fa'}
                emptyStar={'ios-star-outline'}
                fullStar={'ios-star'}
                halfStar={'ios-star-half'}
                iconSet="Ionicons"
                disabled={false}
                maxStars={5}
                rating={this.state.data.averageRating}
                selectedStar={() => { this.onMoreReviews(); }}
                starSize={12}
                emptyStarColor={'#32b3fa'}
              />
              <Text allowFontScaling={false} style={{ color: '#32b3fa', marginLeft: 5, fontSize: 12, fontFamily: 'Gotham-Book' }}>
                {this.state.data.reviewCount} Reviews
            </Text>

            </TouchableOpacity>
          </View>
          <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center', justifyContent: 'flex-start' }}>
            {this.state.data.isPga ?
              <Image
                style={{ marginLeft: 0, width: 40, height: 40, }}
                source={{ uri: 'https://www.birdienow.com/img/badge/pga_badge_85.png' }}
              /> : null}
            <TouchableOpacity onPress={() => { this.onContactInstructor(); }}
              style={{ marginLeft: 7, height: 35, width: 35, borderRadius: 40, borderColor: '#e7e7e9', borderWidth: 0, backgroundColor: '#F9F9F9', justifyContent: "center", flexDirection: 'row', alignItems: 'center' }}>
              <View style={{}}>
                <Image style={{ width: 35, height: 35 }} source={require('img/icon/profile-button-contact.png')} />
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => { this.onMoreReviews(); }}
              style={{ marginLeft: 10, height: 35, width: 35, borderRadius: 40, borderColor: '#e7e7e9', borderWidth: 0, backgroundColor: '#F9F9F9', justifyContent: "center", flexDirection: 'row', alignItems: 'center' }}>
              <View style={{}}>
                <Image style={{ width: 35, height: 35 }} source={require('img/icon/profile-button-reviews.png')} />
              </View>
            </TouchableOpacity>
            {this.state.data.isPromo ? <View style={{ flexDirection: 'row', marginLeft: 10, }}>
                      <View>
                        <Image  style={{ width: 20, height: 25 }} source={require('img/icon/exclusive_60x75.png')}  /> 
                      </View>
                      <View style={{ paddingTop: 2, paddingLeft: 3}}>
                        <Text style={{ fontSize: 10, color: '#ff4b00', fontFamily: 'Gotham-Medium' }}>Exclusive</Text>
                        <Text style={{ fontSize: 10, color: '#ff4b00', fontFamily: 'Gotham-Medium' }}>Deal</Text>
                      </View>
                    </View> : null}
              {/*<Text style={{ fontSize: 9}}>Message Instructor</Text>*/}
            </View>
        </View>

        
       

      </View>
    );

    const render_BookBar_Locations = [];
     if (this.state.isDataLoaded) {
      for (let i = 0; i < this.state.data.locations.length; i++) {
        let key1view = "render_bookbar_locations_text_1view" + i;

        let key1 = "render_bookbar_locations_text_1" + i;
        let key2 = "render_bookbar_locations_text_2" + i;
        let key3 = "render_bookbar_locations_text_3" + i;
        let key4 = "render_bookbar_locations_text_4" + i;

        let addr = "";
        //  if (this.state.data.locations[i].address1 != null) {
        //     addr = this.state.data.locations[i].address1;
        //   }
        //  if (this.state.data.locations[i].address2 != null) {
        //    addr = addr + " " + this.state.data.locations[i].address2;
        //  }
        if (this.state.data.locations[i].city != null) {
          //addr =  addr + ' ' + this.state.data.locations[i].city;
          addr =  this.state.data.locations[i].city;
        }
        if (this.state.data.locations[i].state != null) {
          addr = addr + ", " + this.state.data.locations[i].state;
        }
        // if (this.state.data.locations[i].zip != null) {
        //   addr = addr + " " + this.state.data.locations[i].zip;
        // }

        //render_BookBar_Locations.push(<Text key={key1} style={[styles.location, {marginLeft:39 * this.r_width}]}>{this.state.data.locations[i].name}</Text>); //add
        //render_BookBar_Locations.push(<Text key={key2} style={[styles.locationAddress,{marginLeft:39 * this.r_width, paddingBottom: 10 }]}>{addr}</Text>); //add

        let OpenDaysCount = this.state.data.locations[i].openDays.length;

        //If No Open Dates
          let tArray = [];

          //Loop through each date
          for (let j = 0; j < OpenDaysCount; j++) {

            //limit open dates to only show first 20
            if (j > 20) break;

            let displayDay = moment(this.state.data.locations[i].openDays[j]).format('ddd').toUpperCase();
            let displayDate = moment(this.state.data.locations[i].openDays[j]).format('M/DD');

            //Generate unique key
            let key4_1 = "key4_1" + j;
            let key4_1_date = "key4_1_date" + j;

            /*//Display Time and Date horizontal headers
            if (j == 0 || (initDate != moment(this.state.data.locations[i].openDates[j].localStartDate).format('M/DD'))) {
              initDate = moment(this.state.data.locations[i].openDates[j].localStartDate).format('M/DD');
              tArray.push(
                <View
                  key={key4_1_date}
                  style={{ marginLeft: 20 * this.r_width, width: 180 * this.r_width, height: 93 * this.r_width, borderRadius: 15 * this.r_width, backgroundColor: 'transparent', alignItems: 'center', justifyContent: 'center' }}>
                  <Text allowFontScaling={false} style={{ fontSize: 10, color: 'black', fontFamily: 'Gotham-Book' }}>{displayDay} {displayDate}</Text>
                </View>

              );
            }*/
             {/*<TouchableOpacity
                onPress={() => { this.onBook(i,j); }}
                key={key4_1} style={{ marginLeft: 20 * this.r_width, width: 160 * this.r_width, height: 93 * this.r_width, borderRadius: 15 * this.r_width, backgroundColor: '#32b3fa', alignItems: 'center', justifyContent: 'center' }}>
                <Text allowFontScaling={false} style={{ fontSize: 10, color: '#ffffff', fontFamily: 'Gotham-Book' }}>{displayTime}</Text>
              </TouchableOpacity>*/}

            //Display time buttons
            tArray.push(
              <TouchableOpacity
                onPress={() => { this.onBook(this.state.data.id, this.state.data.locations[i].id, this.state.data.lessonTypeId, this.state.data.locations[i].openDays[j]); }}
                key={key4_1} style={{ marginRight: 10 * this.r2_width, width: 160 * this.r2_width, borderRadius: 15 * this.r2_width, backgroundColor: 'transparent', alignItems: 'center', justifyContent: 'center' }}>
                <Text key={key4_1 + 't'} allowFontScaling={false} style={{ fontSize: 10, color: '#32b3fa', fontFamily: 'Gotham-Medium' }}>{displayDay}</Text>
                <Text key={key4_1 + 'F'} allowFontScaling={false} style={{ fontSize: 10, color: '#32b3fa', fontFamily: 'Gotham-Medium' }}>{displayDate}</Text>
              </TouchableOpacity>
            ); 
          }




          let key5 = "view-full-calendar_" + i;
          let key5section = "view-section" + i;
          let key1main = "key1main" + i;
          let key2main = "key2main" + i;

          render_BookBar_Locations.push(<View key={key1main} style={{ }}>
            <View key={key5section} style={{ flexDirection: 'row', borderTopColor: '#e7e7e9', borderTopWidth: 1}}>
            <View key={key1view} style={{ flex: 3, paddingRight:10 }}>
              <Text key={key1} style={[styles.location, { marginLeft: 39 * this.r_width }]}>{this.state.data.locations[i].name}</Text>
              
              <TouchableOpacity key={key2} onPress={() => { this.openDirections(this.state.data.locations[i].address1 + ', ' + addr + ' ' + this.state.data.locations[i].zip) }}>
                <Text style={[styles.locationAddress, { marginLeft: 39 * this.r_width}]}>{addr}</Text>
                <Text key={key1 + 'map'} style={[styles.location, { color: '#32b3fa', marginLeft: 39 * this.r_width, paddingBottom: 10  }]}>View Map</Text>
              </TouchableOpacity>
            </View>
            <View key={key5section} style={{ flex: 7 }}>

                {OpenDaysCount == 0 ?
                <View key={key1main} style={{ flex:1, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center'}}>
                  <Text allowFontScaling={false} key={key3}
                    style={[styles.content, { fontSize: 12, justifyContent: 'center', color: '#6f6f6f', fontFamily: 'Gotham-Medium' }]}>Lesson dates not available</Text>
                 </View>
                  : 
                  <View key={key1main} style={{ flex:1, backgroundColor: '#fff'}}>

                    <TouchableOpacity key={key5} onPress={() => { this.onViewFullCalendar(i); }} 
                      style={{ marginLeft: 0, marginTop: 0, height: 40, borderTopColor: '#e7e7e9', borderBottomWidth: 0, borderBottomColor: '#e7e7e9', 
                      backgroundColor: '#fff', flexDirection: 'row', alignItems: 'center' }}>
                      <View style={{flex: 7, justifyContent: "center"}}>
                        
                        <Text allowFontScaling={false} style={[styles.locationAddress, {  textAlign: "center", color: '#32b3fa', fontFamily: 'Gotham-Medium'  }]}>View full calendar</Text>
                     
                      </View>
                      <View style={{flex: 1.5, justifyContent: 'flex-start', alignItems: 'center'}}>
                        <Image style={{width: 20, height: 20}} source={require('img/icon/icon_arrow.png')}/>
                      </View>
                    </TouchableOpacity>

                    <View key={key4} style={{ flexDirection: 'row', marginTop: 10, marginBottom: 10, backgroundColor: '#fff' }}>
                      <View style={{ flexDirection: 'row', flex:1, backgroundColor: "#fff"}}  >
                        <View style={{flex: 7, paddingLeft: 0}}>
                          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            {tArray}
                          </ScrollView>
                        </View>
                        <View style={{flex: 1.5, justifyContent: 'flex-start', alignItems: 'center'}}>
                          { this.state.data.locations[i].openDays.length > 4 ? 
                          <Image style={{width: 20, height: 20}} source={require('img/icon/icon_arrow.png')}/>
                          : null
                          }
                        </View>
                      </View>
                      
                    </View>


                  </View>}


            </View>
            </View>
          </View>);




/*
          render_BookBar_Locations.push(
            <TouchableOpacity key={key5} onPress={() => {this.onViewFullCalendar(i);}} style={{marginLeft: 0, marginTop: 0, height:126 * this.r_width, borderTopWidth: 1, borderTopColor:'#e7e7e9', borderBottomWidth:1, borderBottomColor:'#e7e7e9', backgroundColor: '#f4f4f4', flexDirection:'row', alignItems:'center'}}>
              <View style={{flex: 8, justifyContent: "center"}}>
                <Text allowFontScaling={false} style={[styles.content,{marginLeft:45 * this.r_width, textAlign: "left", fontFamily: 'Gotham-Bold' }]}>View all availability</Text>
              </View>
              <View style={{flex: 1, justifyContent: "center"}}>
                <Image style={{width:20, height:20}} source={require('img/icon/icon_arrow.png')}/>
              </View>
            </TouchableOpacity>
          );

          render_BookBar_Locations.push(
            <View key={key4} style={{flexDirection:'row' ,marginTop:20, marginBottom:20}}>
              <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                {tArray}
              </ScrollView>
            </View>
          );*/

          
        
      }
    }

    const render_RateBar = [];
    if (!this.state.isDataLoaded || this.state.data.lessonRates.length == 0) {
      render_RateBar.push(
        <Text allowFontScaling={false} key="render_ratebar" style={[styles.content, {marginTop: 20, marginLeft: 36 * this.r_width,}]}>
          Nothing to display
        </Text>
      );
    } else {
      let rateCount = this.state.data.lessonRates.length;

    
      for (let t = 0; t<this.state.lessonTypes.length; t++)
      {
        let key2 = "key_ratebar_" + t;
        render_RateBar.push(
              <View key={key2} style={{flex:1, height: 30,  borderBottomColor: '#e7e7e9', borderBottomWidth: 1,  justifyContent: 'center', paddingLeft:10, paddingTop:10}}>
                <Text allowFontScaling={false} style={{fontSize: 12,color:'#474747', fontWeight: 'bold', fontFamily: 'Gotham-Book', textAlign:"left", }}>
                  {this.state.lessonTypes[t]}</Text>
              </View>
              );
        for (let i = 0; i < rateCount; i++) {
          let key = "key_ratebar_" + i + t;

          if (this.state.data.lessonRates[i].lessonTypeName == this.state.lessonTypes[t])
          {
            render_RateBar.push(
            <View key={key} style={{flexDirection:'row',marginTop:4, marginBottom:4, justifyContent:"flex-start", paddingLeft:20}}>
              <View style={{flex:5,height:70 * this.r_width, alignItems:'flex-start', justifyContent:'center'}}>
              {this.state.data.lessonRates[i].numLessons == 1 ? 
                <Text allowFontScaling={false} style={{fontSize: 12,color:'#474747', fontFamily: 'Gotham-Book', textAlign:"left"}}>
                  Single Lesson</Text>
              :
                <Text allowFontScaling={false} style={{fontSize: 12,color:'#474747', fontFamily: 'Gotham-Book', textAlign:"left"}}>
                  {this.state.data.lessonRates[i].numLessons} Lesson Package</Text>
              }
                
               {this.state.data.lessonRates[i].isPromo && this.state.data.lessonRates[i].originalPrice && this.state.data.lessonRates[i].price != 0 ?
                   <Text allowFontScaling={false} style={{fontSize: 12,color:'#ff4b00', fontFamily: 'Gotham-Book', textAlign:"center"}}>
                     {'(Save ' + ((this.state.data.lessonRates[i].originalPrice - this.state.data.lessonRates[i].price)/this.state.data.lessonRates[i].originalPrice*100).toFixed(0) + '%)'}
                </Text> : null }
                  
              </View>


             

              {this.state.data.lessonRates[i].lessonAgeType == 'Junior' ? 
              <View style={{flex:2,height:70 * this.r_width, alignItems:'center', justifyContent:'center'}}>
                <Text allowFontScaling={false} style={{fontSize: 12,color:'#474747', fontFamily: 'Gotham-Book', textAlign:"center"}}>{this.state.data.lessonRates[i].lessonAgeType}</Text>
              </View>
              :
              <View style={{flex:2,height:70 * this.r_width, alignItems:'center', justifyContent:'center'}}>
                <Text allowFontScaling={false} style={{fontSize: 12,color:'#474747', fontFamily: 'Gotham-Book', textAlign:"center"}}></Text>
              </View>

              }
              
              
              
              <View style={{flex:3,height:70 * this.r_width, alignItems:'center', justifyContent:'center'}}>
                <Text allowFontScaling={false} style={{fontSize: 12,color:'#474747', fontFamily: 'Gotham-Book', textAlign:"center"}}>
                  {this.state.data.lessonRates[i].price == 0 ?
                   'Free'
                   : '$'  + this.state.data.lessonRates[i].price }
                </Text>
              </View>
              <TouchableOpacity onPress={() => { this.OnBuy(i); }} 
                style={{flex:2, height:70 * this.r_width, borderRadius:15 * this.r_width, backgroundColor: (this.state.data.lessonRates[i].isPromo) ? '#ff4b00' :'#32b3fa', alignItems:'center', justifyContent:'center'}}>
                <View>
                  <Text allowFontScaling={false} style={{fontSize: 11, paddingTop: 3, color:'#ffffff', fontFamily: 'Gotham-Book'}}>{this.state.data.lessonRates[i].price == 0 ? 'Redeem' : 'Buy'}</Text>
                </View>
              </TouchableOpacity>
              <View style={{flex:1}}/>
            </View>
          );
          }
          
        }
      }
    }

    const render_FullSummary = [];
    if (this.state.showAllQualificationsAndExperience == false) {
      render_FullSummary.push(
        <TouchableOpacity
          onPress={() => {this.onShowQualificationsAndExperience(true);}}
          key="render_fullSummary">
          <Text allowFontScaling={false} style={{ fontSize: 11, color:'#0086F8', fontFamily: 'Gotham-Book'}}>
            Show more
          </Text>
        </TouchableOpacity>
      );
    }

    const render_Summary = [];
    if (!this.state.isDataLoaded) {
      render_Summary.push(
        <Text allowFontScaling={false} key="render_summary" style={styles.content}>
          {/*Nothing to display.*/}
        </Text>
      );
    } else {
      let statement = this.state.data.statement;
      if (statement != null) {
        if (statement.length > 200 && this.state.showAllQualificationsAndExperience == false) {
          statement = String(this.state.data.statement).substring(0, 200);
          statement = statement + "... ";
          render_Summary.push(
            <View key="render_summary">
              <Text allowFontScaling={false} style={styles.content}>
                {statement}
              </Text>
              {render_FullSummary}
            </View>
          );
        } else {
          render_Summary.push(
            <Text allowFontScaling={false} key="render_summary" style={styles.content}>
              {this.state.data.statement}
            </Text>
          );
        }
      }
    }

    const render_Experience = [];
    const render_Awards = [];
    const render_Certifications = [];
    const render_Languages = [];
    const render_Educations = [];
    if (!this.state.isDataLoaded) {
      render_Experience.push(
        <Text allowFontScaling={false} key="render_renderExperience" style={styles.content}>
          Nothing to display.
        </Text>
      );
      render_Awards.push(
        <Text allowFontScaling={false} key="render_renderAwards" style={styles.content}>
          Nothing to display.
        </Text>
      );
      render_Certifications.push(
        <Text allowFontScaling={false} key="render_renderCertifications" style={styles.content}>
          Nothing to display.
        </Text>
      );
      render_Languages.push(
        <Text allowFontScaling={false} key="render_renderLanguages" style={styles.content}>
          Nothing to display.
        </Text>
      );
      render_Educations.push(
        <Text allowFontScaling={false} key="render_renderEducations" style={styles.content}>
          Nothing to display.
        </Text>
      );
    } else {
      let itemCount = this.state.data.instructorItems.length;
      for (let i = 0; i < itemCount; i++) {
        if (this.state.data.instructorItems[i].instructorItemTypeID == "Experience") {
          let key = "render_experience_text" + i;
          render_Experience.push(
            <Text allowFontScaling={false} key={key} style={styles.content}>
              {this.state.data.instructorItems[i].value}
            </Text>
          );
        } else if (this.state.data.instructorItems[i].instructorItemTypeID == "Award") {
          let key = "render_awards_text" + i;
          render_Awards.push(
            <Text allowFontScaling={false} key={key} style={styles.content}>
              {this.state.data.instructorItems[i].value}
            </Text>
          );
        } else if (this.state.data.instructorItems[i].instructorItemTypeID == "Certification") {
          let key = "render_certifications_text" + i;
          render_Certifications.push(
            <Text allowFontScaling={false} key={key} style={styles.content}>
              {this.state.data.instructorItems[i].value}
            </Text>
          );
        } else if (this.state.data.instructorItems[i].instructorItemTypeID == "Language") {
          let key = "render_languages_text" + i;
          render_Languages.push(
            <Text allowFontScaling={false} key={key} style={styles.content}>
              {this.state.data.instructorItems[i].value}
            </Text>
          );
        } else if (this.state.data.instructorItems[i].instructorItemTypeID == "Education") {
          let key = "render_education_text" + i;
          render_Educations.push(
            <Text allowFontScaling={false} key={key} style={styles.content}>
              {this.state.data.instructorItems[i].value}
            </Text>
          );
        }
      }
    }

    const render_QualificationsAndExperience = [];
    if (this.state.showAllQualificationsAndExperience == false) {
      render_QualificationsAndExperience.push(
        <View key = "render_qualificationsAndExperience_view">
          <View>
            <Text allowFontScaling={false} style={styles.contentBold}>
              Summary
            </Text>
            {render_Summary}
          </View>
          <View>
            <Text allowFontScaling={false} style={styles.contentBold}>
              Experience
            </Text>
            {render_Experience}
          </View>
        </View>
      );
    } else {
      render_QualificationsAndExperience.push(
        <View key = "render_qualificationsAndExperience_view">
          <View>
            <Text allowFontScaling={false} style={styles.contentBold}>
              Summary
            </Text>
            {render_Summary}
          </View>
          <View>
            <Text allowFontScaling={false} style={styles.contentBold}>
              Experience
            </Text>
            {render_Experience}
          </View>
          <View>
            <Text allowFontScaling={false} style={styles.contentBold}>
              Awards
            </Text>
            {render_Awards}
          </View>
          <View>
            <Text allowFontScaling={false} style={styles.contentBold}>
              Certifications
            </Text>
            {render_Certifications}
          </View>
          <View>
            <Text allowFontScaling={false} style={styles.contentBold}>
              Languages
            </Text>
            {render_Languages}
          </View>
          <View>
            <Text allowFontScaling={false} style={styles.contentBold}>
              Education
            </Text>
            {render_Educations}
          </View>
        </View>
      );
    }

    const render_ReadFullQualificationsAndExperience = [];
    if (this.state.showAllQualificationsAndExperience == false) {
      render_ReadFullQualificationsAndExperience.push(
        <TouchableOpacity
          onPress={() => {this.onShowQualificationsAndExperience(true);}}
          key="render_read_full_qualifications_and_experience">
          <Text allowFontScaling={false} style={styles.blueContent}>
            Read full Qualifications/Experience
          </Text>
        </TouchableOpacity>
      );
    }

    const render_ReadLessQualificationsAndExperience = [];
    if (this.state.showAllQualificationsAndExperience == true) {
      render_ReadLessQualificationsAndExperience.push(
        <TouchableOpacity
          onPress={() => {this.onShowQualificationsAndExperience(false);}}
          key="render_read_less_qualifications_and_experience">
          <Text allowFontScaling={false} style={styles.blueContent}>
            Hide Qualifications/Experience
          </Text>
        </TouchableOpacity>
      );
    }

    const render_Reviews = [];
    if (!this.state.isDataLoaded || this.state.data.reviewCount == 0) {
      render_Reviews.push(
        <Text allowFontScaling={false} key="render_ratebar" style={[styles.content, {marginTop: 20}]}>
          There are no reviews for this intructor
        </Text>
      );
    } else {
      let reviewCount = this.state.data.reviewCount;
      if (reviewCount > 3) {
        reviewCount = 3;
      }
      for (let i = 0; i < reviewCount; i++) {
        let key1 = "reviewkey1" + i;
        let key2 = "reviewkey2" + i;
        let key3 = "reviewkey3" + i;
        let key4 = "reviewkey4" + i;
        let rating = this.state.data.reviews[i].overallRating;
        let reviewdate = this.state.data.reviews[i].reviewDate;
        let skilllevel = this.state.data.reviews[i].userSkillLevelName;
        let lessonfocus = this.state.data.reviews[i].reviewLessonTypeNames;
        let reviewsaid = this.state.data.reviews[i].statement;
        let clientfirstname = this.state.data.reviews[i].clientFirstName;
        let clientlastname = this.state.data.reviews[i].clientLastName;
        clientlastname = clientlastname.charAt(0).toUpperCase() + '.';


        render_Reviews.push(
          <View key={key1} style={{flexDirection:'row', marginTop:10, marginBottom:10}}>
            <View style={{flex: 2}}>
              <StarRating
                starColor = {'#32b3fa'}
                emptyStar={'ios-star-outline'}
                fullStar={'ios-star'}
                halfStar={'ios-star-half'}
                iconSet="Ionicons"
                disabled={true}
                maxStars={5}
                rating={rating}
                selectedStar={(rating) => this.onStarRatingPress(rating)}
                starSize = {15}
                emptyStarColor={'#32b3fa'}
              />
            </View>
            <View style={{flex: 6, alignSelf: 'flex-end'}}>
              
            </View>
            <View style={{flex: 2, alignSelf: 'flex-end', paddingRight: 10}}>
              <Text allowFontScaling={false} style={[styles.content,{ alignSelf: 'flex-end'}]}>{moment(reviewdate).format("MM/DD/YY")}</Text>
            </View>
          </View>
        );
        render_Reviews.push(
          <Text allowFontScaling={false} key={key2} style={[styles.content, { fontFamily: 'Gotham-Medium', fontSize: 12 }]}>Skill Level:
            <Text allowFontScaling={false} style={[styles.content, { fontFamily: 'Gotham-Book', fontSize: 12 }]}> {skilllevel}</Text>
          </Text>
        );
        render_Reviews.push(
          <Text allowFontScaling={false} key={key3} style={[styles.content, { fontFamily: 'Gotham-Medium', fontSize: 12 }]}>Lesson Focus:
            <Text allowFontScaling={false} style={[styles.content, { fontFamily: 'Gotham-Book', fontSize: 12 }]}> {lessonfocus}</Text>
          </Text>
        );
        render_Reviews.push(
          <Text allowFontScaling={false} key={key4} style={[styles.content, { fontFamily: 'Gotham-Medium', fontSize: 12 }]}>{clientfirstname} {clientlastname} said:
            <Text allowFontScaling={false} style={[styles.content, { fontFamily: 'Gotham-Book', fontSize: 12 }]}> {reviewsaid}</Text>
          </Text>
        );

      }
    }

    const render_MoreReviews = [];
    if (this.state.isDataLoaded) {
      if (this.state.data.reviewCount > 3) {
        render_MoreReviews.push(
          <TouchableOpacity onPress={() => {this.onMoreReviews();}} key="render_show_more_review" style={{marginLeft: 1, marginTop: 15, height:126 * this.r_width, borderTopWidth: 1, borderTopColor:'#e7e7e9', borderBottomWidth:1, borderBottomColor:'#e7e7e9', flexDirection:'row', alignItems:'center'}}>
            <View style={{flex: 8, justifyContent: "center"}}>
              <Text allowFontScaling={false} style={[styles.content,{marginLeft:45 * this.r_width, textAlign: "left"}]}>More Reviews</Text>
            </View>
            <View style={{flex: 1, justifyContent: "center"}}>
              <Image style={{width:20, height:20}} source={require('img/icon/icon_arrow.png')}/>
            </View>
          </TouchableOpacity>
        );
      }
    }

    return (
      <View style={styles.container}>
        {render_TopBar}
        <ScrollView>
          {render_ImageBar}
          {/*<TouchableOpacity onPress={() => {this.onContactInstructor();}} style={{marginLeft: 1, marginTop: 15, height:126 * this.r_width, borderTopWidth: 1, borderTopColor:'#e7e7e9', borderBottomWidth:1, borderBottomColor:'#e7e7e9', flexDirection:'row', alignItems:'center'}}>
            <View style={{flex: 1, justifyContent: "center"}}>
              <Image style={{marginLeft: 10, width:75 * this.r_width, height:72 * this.r_width}} source={require('img/profile/profile-contact.png')}/>
            </View>
            <View style={{flex: 8, justifyContent: "center"}}>
              <Text allowFontScaling={false} style={[styles.content,{marginLeft:45 * this.r_width, textAlign: "left"}]}>Contact Instructor</Text>
            </View>
            <View style={{flex: 1, justifyContent: "center"}}>
              <Image style={{width:20, height:20}} source={require('img/icon/icon_arrow.png')}/>
            </View>
          </TouchableOpacity>*/}
          <View style={{ borderBottomWidth: 1, borderBottomColor: '#e7e7e9'}}>
            <View>
              <View style={[styles.titleView, {backgroundColor: '#d9e7ef', paddingTop: 3, paddingBottom: 3 }]}>
                <Image style={{marginLeft:39 * this.r_width, width:73 * this.r_width, height:73 * this.r_width}} source={require('img/icon/profile-book-50.png')}/>
                <Text allowFontScaling={false} style={styles.title}>Book a Lesson
                </Text>
              </View>
              <View style={{ }}  >
                {render_BookBar_Locations}
              </View>
            </View>
          </View>
          <View style={{   }}>
            <View style={[styles.titleView, { backgroundColor: '#d9e7ef', marginTop:0, paddingTop: 3, paddingBottom: 3 }]}>
              <Image style={{ marginLeft: 39 * this.r_width, width: 73 * this.r_width, height: 73 * this.r_width }} source={require('img/icon/profile-rates-50.png')} />
              <Text allowFontScaling={false} style={styles.title}>Rates</Text>
            </View>
            {render_RateBar}
          </View>
          <View style={{ }}>
            <View style={[styles.titleView, { backgroundColor: '#d9e7ef', paddingTop: 3, paddingBottom: 3 }]}>
              <Image style={{ marginLeft: 39 * this.r_width, width: 73 * this.r_width, height: 73 * this.r_width }} source={require('img/icon/profile-quals-50.png')} />
              <Text allowFontScaling={false} style={styles.title}>Qualifications/Experience</Text>
            </View>
            <View style={{ marginLeft: 36 * this.r_width, marginRight: 36 * this.r_width }}>
              {render_QualificationsAndExperience}
              {render_ReadFullQualificationsAndExperience}
              {render_ReadLessQualificationsAndExperience}
            </View>


            
          </View>
          <View>
            <View style={[styles.titleView, { backgroundColor: '#d9e7ef', paddingTop: 3, paddingBottom: 3 }]}>
              <Image style={{ marginLeft: 39 * this.r_width, width: 73 * this.r_width, height: 73 * this.r_width }} source={require('img/icon/profile-reviews-50.png')} />
              <Text allowFontScaling={false} style={styles.title}>Reviews</Text>
            </View>
            <View style={{marginLeft:36 * this.r_width}}>
              {render_Reviews}
            </View>
            {render_MoreReviews}
          </View>
          <Text allowFontScaling={false} style={styles.blueContent}/>
        </ScrollView>
        {this.state.showloading ?
          <View
            style={{position:'absolute', top: 0, left:0, bottom:0, right:0, alignItems:'center', justifyContent:'center', alignSelf: 'stretch', backgroundColor: 'transparent'}}
          >
            <ActivityIndicator
              animating={true}
              size="small"
              color="gray"
            />
          </View>:null}
      </View>
    );
  }
}

export default InstructorScene;
