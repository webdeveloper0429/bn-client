import { StyleSheet } from 'react-native';

import Dimensions from 'Dimensions';

const window = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'flex-start',
    //justifyContent: 'flex-start',
    paddingBottom: 0,
  },
  inputEmail: {
    alignSelf: 'stretch',
    flex: 1,
    backgroundColor: '#ffffff',
    borderWidth: 1,
    borderColor: '#f4f4f4',
    borderRadius: 5,
    paddingVertical: 0,
    paddingHorizontal: 10,
  },
  btnLogin: {
    color: '#fff',
    fontSize: 16,
    borderWidth: 2,
    borderColor: '#fff',
    borderRadius: 5,
    paddingLeft: 10,
    paddingRight: 7,
    paddingTop: 10,
    paddingBottom: 7,
  },
  loadingScene: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: window.width,
    height: window.height,
    alignSelf: 'stretch',
    backgroundColor: 'rgba(0,0,0,0.5)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconContact: {
    width: 23,
    height: 23,
    marginHorizontal: 10,
    marginTop: 4,
  },
  txtContact: {
    flex: 1,
    fontSize: 14,
    textAlign: 'left'
  },
  txtContactWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#eeeff0',
    padding: 5,
  },
});
