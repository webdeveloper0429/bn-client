import React, { Component, PropTypes } from 'react';
import { Dimensions, ActivityIndicator, Alert, View, Image, Text, TouchableOpacity, TouchableHighlight, TextInput, Platform, BackAndroid } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import styles from './styles';
import _, { isEqual } from 'lodash';
import { RequestStripeRegister, RequestFreeRegister, ContactRequest, GetAccountInfo } from 'AppUtilities';
import AppConfig from 'AppConfig';
import moment from 'moment';

class BookPaymentScene extends Component {
  static propTypes = {
    popNavBack: PropTypes.func.isRequired,
    pushNavScene: PropTypes.func.isRequired,
    goToNavTab: PropTypes.func.isRequired,
  };

  constructor(props, context) {
    super(props, context);

    this.state = {
        data: [],
        lessonRate: [],
        user: [],
        promoCodeResponse: [],
        requestParams: [],
        lessonRates: [],
        lessonRateId: '',
        totalAmount: 0,
        price: 0,
        promoCode: '',
        promoDiscount: 0,
        invalidPromoCode: '',
        validPromoCode: '',
        lessonCount: 0,
        lessonExpirationDate: '',
        lessonId: '',
        isLoading: false,
        cardNumber: '',
        expiry: '',
        cardName: '',
        cvc: '',
        showloading: false
        
    };

    const { width, height } = Dimensions.get('window');
    this.width = width;
    this.height = height;
    this.r_width =  width / 1125;
    this.r_height = height / 1659;
    this.phone = "";
    this.comment = "";

    this.OnSubmit = this.OnSubmit.bind(this);
    this.OnPayment = this.OnPayment.bind(this);
    this.OnBookCredit = this.OnBookCredit.bind(this);
    this.BookSuccess = this.BookSuccess.bind(this);
    this.GetLessonRates = this.GetLessonRates.bind(this);
    this.GetPurchaseLessons = this.GetPurchaseLessons.bind(this);
    this.renderCardInput = this.renderCardInput.bind(this);
    this.onChangeCardNum = this.onChangeCardNum.bind(this);
    this.onChangeExpiry = this.onChangeExpiry.bind(this);
    this.onChangeCardName = this.onChangeCardName.bind(this);
    this.onChangeCVC = this.onChangeCVC.bind(this);
    this.OnRetriveUserInfo = this.OnRetriveUserInfo.bind(this);
  }
  
  componentDidMount() {
		//BackAndroid.addEventListener('hardwareBackPress', this.onBackPress.bind(this));
    //Get User Profile
    this.OnRetriveUserInfo();

    if (this.props.passProps != null) {
      
      this.setState({ data: this.props.passProps.instructor, requestParams: this.props.passProps.requestParams });
      this.GetPurchaseLessons(this.props.passProps.instructor.instructorId, this.props.passProps.instructor.lessonTypeId);
      
     
    }
  }

	onBackPress() {
		this.props.popNavBack();
	}

  GetLessonRates(instructorId, lessonTypeId) {
    this.setState({ showloading: true });
    //Get Data
    const header = {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${AppConfig.global_userToken}`,
      },
    };
    fetch(AppConfig.apiUrl + '/api/lessonRates?instructorId=' + instructorId + '&lessonTypeId=' + lessonTypeId, header)
      .then((response) => response.json())
      .then((responseData) => {
        
        //received data
        

        if (responseData != null && responseData.length > 0) {
          var lessonRates = this.state.lessonRates;

          //hack to set payment Type as radio button initializes, only if lessonRates is empty (no lesson credits)
          if (lessonRates.length == 0)
            this.setState({ lessonRateId: responseData[0].id, price: responseData[0].price, totalAmount: responseData[0].price });

          for (let i=0; i < responseData.length; i++)
          {
            var label = '';
            if (responseData[i].price == 0)
              label = responseData[i].numLessons + ' ' + responseData[i].lessonAgeType + ' ' + responseData[i].lessonTypeName + ' Free';
            else
              label = responseData[i].numLessons + ' ' + responseData[i].lessonAgeType + ' ' + responseData[i].lessonTypeName + ' $' + responseData[i].price.toFixed(2);
            var value = responseData[i].id;
            var price = responseData[i].price;
            var lesson = {label: label, value: value, price: price};
            lessonRates.push(lesson);
            
          }

          this.setState({ showloading: false});
       
          
        }
        else {
          this.setState({ showloading: false });
        }
      })
      .catch(error => {
        //alert('outside error');
        this.setState({ showloading: false });
      });
  }

  GetPurchaseLessons(instructorId, lessonTypeId) {
    this.setState({ showloading: true });
    //Get Data
    const header = {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${AppConfig.global_userToken}`,
      },
    };
    fetch(AppConfig.apiUrl + '/api/purchaseLessons?auth=auth&instructorId=' + instructorId + '&lessonTypeId=' + lessonTypeId, header)
      .then((response) => response.json())
      .then((responseData) => {

       
        //received data
        if (responseData != null && responseData.length > 0) {
          var lesson = [{label: 'Use a lesson credit ('+ responseData.length +' remaining)', value: -1, price: 0}];
          
          this.setState({ showloading: false, 
            lessonRates: lesson,
            lessonCount: responseData.length, 
            lessonRateId: '-1',
            lessonId: responseData[0].id,
            lessonExpirationDate: moment(responseData[0].expirationDate).format("MMM DD, YYYY")
          });

          for (let i=0; i < responseData.length; i++)
          {
            responseData[i].label = responseData[i].numLessons + ' ' + responseData[i].lessonAgeType + ' ' + responseData[i].lessonTypeName + ' $' + responseData[i].price.toFixed(2);
            responseData[i].value = responseData[i].id;
          }
        }
        else {
          this.setState({ showloading: false });
        }

        this.GetLessonRates(this.props.passProps.instructor.instructorId, this.props.passProps.instructor.lessonTypeId);

      })
      .catch(error => {
        //alert('outside error');
        this.setState({ showloading: false });
        this.GetLessonRates(this.props.passProps.instructor.instructorId, this.props.passProps.instructor.lessonTypeId);
      });
  }
  
  
  OnRetriveUserInfo() {
    this.setState({showloading: true});
    
    GetAccountInfo(AppConfig.global_userToken)
      .then((response) => {
        if (response) { 
          this.setState({showloading: false, user: response});
             
        } else {
          this.setState({showloading: false});
          Alert.alert(response.error_description);
        }
      })
      .catch(error => {
        Alert.alert(error.message);
        this.setState({showloading: false});
      });
  }

  onChangeCardNum(text) {
    if (text.length < 1) {
      this.setState({ cardNumber: '' });
      return;
    }
    let cardNum = text.split('-').join('');
    cardNum = cardNum.match(/.{1,4}/g);
    this.setState({ cardNumber: cardNum.join('-') });
  }

  onChangeExpiry(text) {
    if (text.length > 2) {
      let expiry = text.split('/').join('');
      expiry = `${expiry.substr(0, 2)}/${expiry.substr(2)}`;
      this.setState({ expiry });
    } else {
      this.setState({ expiry: text });
    }
  }

  onChangeCardName(text) {
    this.setState({ cardName: text });
  }

  onChangeCVC(text) {
    this.setState({ cvc: text });
  }

  applyPromoCode() {
    this.setState({ showloading: true });

    //Get Data
    const header = {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${AppConfig.global_userToken}`,
      },
    };
    fetch(AppConfig.apiUrl + '/api/promoCodes?auth=auth&id=' + this.state.promoCode + '&lessonRateId=' + this.state.lessonRateId, header)
      .then((response) => response.json())
      .then((responseData) => {

        
        //received data
        if (responseData.id != null) {
          this.setState({ showloading: false, promoCodeResponse: responseData, isDataLoaded: true });

          if (this.state.promoCodeResponse.percentageOff != null) {
              this.state.promoDiscount = this.state.price * this.state.promoCodeResponse.percentageOff;
              this.state.totalAmount = this.state.price - this.state.promoDiscount;
            }
            
            else if (this.state.promoCodeResponse.priceOff != null) {
              this.state.promoDiscount = this.state.promoCodeResponse.priceOff;
              this.state.totalAmount = this.state.price - this.state.promoDiscount;
              if (this.state.totalAmount < 0) 
                this.state.totalAmount = 0;
            }

            //bookModel.loadingPromo = "";
            this.setState({ showloading: false, validPromoCode: "Code applied!", invalidPromoCode: '' });
            //alert(this.state.validPromoCode);

          }
          else {
            this.setState({ showloading: false, validPromoCode: '', invalidPromoCode: responseData, promoDiscount: 0, totalAmount: this.state.price  });
          }

          
      })
      .catch(error => {
        alert('outside error');
        this.setState({ showloading: false, validPromoCode: '', invalidPromoCode: "There was an error" });
      });



  }

  OnPayment() {

    if (this.state.totalAmount == 0) {
      RequestFreeRegister(this.state.totalAmount, this.state.data.instructorId, this.state.lessonRateId, this.state.promoCode)
        .then(response => {
          if (response === 'Charged') {
            this.OnBookCredit();
          } else {
            Alert.alert(`Payment Error: ${response}`);
          }
          this.setState({ isLoading: false });
        })
        .catch(error => {
          Alert.alert(`Payment Error: ${error.message}`);
          this.setState({ isLoading: false });
        });
    }
    else {
      const { cardNumber, expiry, cardName, cvc } = this.state;
      if (cardNumber.length < 1) {
        Alert.alert('Invalid card number');
        return;
      }
      if (expiry.length < 5) {
        Alert.alert('Invalid expiration date');
        return;
      }
      if (cardName.length < 1) {
        Alert.alert('Please input card name');
        return;
      }
      if (cvc.length < 1) {
        Alert.alert('Invalid cvc code');
        return;
      }

      this.setState({ isLoading: true });

      const cardDetails = {
        'card[number]': this.state.cardNumber.split('-').join(''),
        'card[exp_month]': expiry.split('/')[0],
        'card[exp_year]': expiry.split('/')[1],
        'card[cvc]': cvc
      };


      RequestStripeRegister(cardDetails, this.state.totalAmount, this.state.data.instructorId, this.state.lessonRateId, this.state.promoCode)
        .then(response => {
          if (response === 'Charged') {
            this.OnBookCredit();
            // Alert.alert('Thank you for your purchase. Time to book your next lesson!');
            // this.props.popNavBack();
          } else {
            Alert.alert(`Payment Error: ${response}`);
          }
          this.setState({ isLoading: false });
        })
        .catch(error => {
          Alert.alert(`Payment Error: ${error.message}`);
          this.setState({ isLoading: false });
        });
    }
    
  }

  OnBookCredit() {

    // let requestParams = this.state.requestParams JSON.stringify({
    //   openDateId: this.state.data.id,
    //   lessonTypeId: this.state.data.lessonTypeId, 
    //   firstName: AppConfig.userInfo.firstName,
    //   lastName: AppConfig.userInfo.lastName,
    //   phone: this.state.userPhone,
    //   email: AppConfig.userInfo.email,
    //   instructorId: this.state.data.instructorId,
    //   locationId: this.state.data.locationId,
    //   startDate: this.state.data.startDate,
    //   clientNote: this.comment});
    this.setState({ isLoading: true });

    let test = this.state.requestParams;
    

    fetch(AppConfig.apiUrl + '/api/BookAppointments', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${AppConfig.global_userToken}`,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: this.state.requestParams
    })
    .then((response) => response.json())
    .then((responseData) => {
      
      
      
      if (this.state.lessonRateId != -1)
      {

        if (this.state.totalAmount == 0) {
          Alert.alert(
            'CONFIRMATION',
            'Your book request has been successfully submitted',
            [
              { text: 'OK', onPress: () => { this.BookSuccess(); } },
            ],
            { cancelable: false }
          );
        }
        else {
          Alert.alert(
            'CONFIRMATION',
            'Your payment was processed and book request has been successfully submitted',
            [
              { text: 'OK', onPress: () => { this.BookSuccess(); } },
            ],
            { cancelable: false }
          );
        }

      }
      else {
        Alert.alert(
          'CONFIRMATION',
          'Your book request has been successfully submitted',
          [
            {text: 'OK', onPress: () => {this.BookSuccess();}},
          ],
          { cancelable: false }
        );
      }
      this.setState({isLoading: false});
      this.props.goToNavTab(2);
      
    })
    .catch(error => {
      
      //broken for this request for errors.  Need to update WEB API for better error messaging when there is conflict booking
      //alert(error.message);
      if (this.state.lessonRateId != -1) {
        Alert.alert("ERROR",'Your payment was processed but there was an error with the booking. Please select another time',
          [
            {text: 'OK', onPress: () => {this.BookSuccess();}},
          ],
          { cancelable: false }
        );
      }
      else
        Alert.alert("ERROR", "There was an error processing your request.  Please select another time.",
          [
            {text: 'OK', onPress: () => {this.BookSuccess();}},
          ],
          { cancelable: false }
        );
        
      
      this.setState({isLoading: false});

    });
  }

  OnSubmit() {
    if (this.state.lessonRateId == -1)
      this.OnBookCredit();
    else
      this.OnPayment();
  }

  BookSuccess() {
    this.props.callback();
    this.props.popNavBack();
  }

  renderCardInput(label, inputKey, changeEvent, flexValue, maxLen = undefined, keyType = 'default', placeHolder) {
    return (
      <View style={{ flexDirection: 'column', flex: flexValue, marginHorizontal: 5 }}>
        <Text style={{ fontSize: 9, color: "#7e7e7e", paddingBottom: 2}} >{label}</Text>
        <TextInput
          underlineColorAndroid='transparent'
          style={styles.inputEmail}
          fontSize={15}
          onChangeText={changeEvent}
          value={this.state[inputKey]}
          placeholderTextColor="#999"
          placeholder={placeHolder}
          keyboardType={keyType}
          maxLength={maxLen}
        />
      </View>
    );
  }

  render() {
    const render_TopBar = (
      <View style={{
        flexDirection: 'row', alignItems: "center", justifyContent: "center",
        backgroundColor: '#fafafa', borderBottomWidth: 1, borderBottomColor: '#D3D3D3',
        height: (Platform.OS === 'ios') ? 65 : 40 }}>

        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={() => { this.onBackPress(); }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#000',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Back</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 2, justifyContent: "center" }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#000',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Checkout</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={this.OnSubmit} >
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#32b3fa',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Submit</Text>
        </TouchableOpacity>
      </View>
    );

    const render_ImageBar = (
        <Image source={require('img/image/ui_back_blue.png')} style={{ alignItems: 'center', justifyContent: 'center', height: 200 }}>
          <Text allowFontScaling={false} style={{
            fontSize: 19, color: '#4b5b64', fontFamily: 'Gotham-Medium', backgroundColor: 'transparent',
            marginBottom: 10
          }}>
            {this.state.data.firstName} {this.state.data.lastName}
          </Text>
          <View>
            {this.state.data.isProfilePhoto ?
              <Image key="render_image"
                style={{ width: 246 * this.r_width, height: 246 * this.r_width, borderRadius: 123 * this.r_width, marginLeft: 36 * this.r_width }}
                source={{ uri: 'https://www.birdienow.com/img/instructor/profile/' + this.state.data.id + '.jpg' }} />
              :
              <Image key="render_image"
                style={{ width: 246 * this.r_width, height: 246 * this.r_width, borderRadius: 123 * this.r_width, marginLeft: 36 * this.r_width }}
                source={require('img/profile/profile.png')} />
            }

            {this.state.data.isPga ?
              <Image
                style={{ width: 30, height: 30, position: "absolute", marginLeft: 25 * this.r_width, marginTop: 150 * this.r_width }}
                source={{ uri: 'https://www.birdienow.com/img/badge/pga_badge_85.png' }}
              /> : null}
          </View>
          <Text allowFontScaling={false} style={{
            fontSize: 13, color: '#4b5b64', fontFamily: 'Gotham-Medium', backgroundColor: 'transparent',
            marginTop: 10
          }}>
            Lesson Type: {this.state.lessonRate.lessonTypeName}
          </Text>
          <Text allowFontScaling={false} style={{ fontSize: 11, color: '#4b5b64', fontFamily: 'Gotham-Medium', backgroundColor: 'transparent' }}>
            Ages: {this.state.lessonRate.lessonAgeType} / # of Lessons: {this.state.lessonRate.numLessons} / Price: ${this.state.lessonRate.price}
          </Text>
        </Image>
      );

    return (
        <View style={styles.container}>
          {render_TopBar}
          <KeyboardAwareScrollView
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={true} >
                
              {/*{render_ImageBar}*/}

                <View>
                  <Text style={{ fontSize: 9, color: "#7e7e7e", paddingLeft: 5, paddingBottom: 2, paddingTop: 10}} >Reservation Details</Text>
                  <View style={[ styles.txtContactWrapper, { flexDirection: 'row', justifyContent: "center", marginTop: 0 }]}>
                    <Image source={require('img/icon/icon_sign.png')} style={styles.iconContact} />
                    <Text allowFontScaling={false} style={{ flex: 3, textAlign: "left", fontFamily: "Arial" }}>
                      {this.state.data.firstName} {this.state.data.lastName}
                      {"\n"}
                      {this.state.data.locationName}
                      </Text>
                  </View>
                  <View style={[ styles.txtContactWrapper, { flexDirection: 'row', justifyContent: "center", marginTop: 0 }]}>
                    <Image source={require('img/form/form-calendar.png')} style={styles.iconContact} />
                    <Text allowFontScaling={false} style={{ flex: 3, textAlign: "left", fontFamily: "Arial" }}>{moment(this.state.data.localStartDate).format('dddd, ll')}</Text>
                  </View>
                  <View style={[ styles.txtContactWrapper, { flexDirection: 'row', justifyContent: "center", marginTop: 0 }]}>
                    <Image source={require('img/form/form-time.png')} style={styles.iconContact} />
                    <Text allowFontScaling={false} style={{ flex: 3, textAlign: "left", fontFamily: "Arial" }}>{moment(this.state.data.localStartDate).format('LT')} - {moment(this.state.data.localEndDate).format('LT')} </Text>
                  </View>
                </View>

                <View>
                  <Text style={{ fontSize: 9, color: "#7e7e7e", paddingLeft: 5, paddingBottom: 2, paddingTop: 10}} >Checkout Options</Text>

                  <RadioForm
                    style={{paddingLeft: 10}}
                    ref="radioForm"
                    radio_props={this.state.lessonRates}
                    initial={0}
                    formHorizontal={false}
                    labelHorizontal={true}
                    buttonColor={'#2196f3'}
                    labelColor={'#000'}
                    animation={true}
                    onPress={(value, index) => {
                      this.setState({
                        lessonRateId: value,
                        value1Index: index,
                        price: this.state.lessonRates[index].price, 
                        totalAmount: this.state.lessonRates[index].price,
                        promoDiscount: 0,
                        validPromoCode: '',
                        invalidPromoCode: ''
                      })
                    }}
                  />
                </View>


                <View style={{  }}>
                    {/*<View style={[ styles.txtContactWrapper, { flexDirection: 'row', justifyContent: "center", marginTop: 0 }]}>
                        <Image source={require('img/icon/icon_sign.png')} style={styles.iconContact} />
                        <Text allowFontScaling={false} style={{ flex: 3, textAlign: "left", fontFamily: "Arial" }}>
                          {this.state.user.firstName} {this.state.user.lastName} 
                          {"\n"}
                          {this.state.user.email }
                          </Text>
                       
                    </View>*/}
                    {/*<View style={[ styles.txtContactWrapper, { flexDirection: 'row', justifyContent: "center", marginTop: 0 }]}>
                        <Image source={require('img/icon/icon_home.png')} style={styles.iconContact} />
                        <Text allowFontScaling={false} style={{ flex: 3, textAlign: "left", fontFamily: "Arial" }}>{ this.state.user.email } </Text>
                    </View>*/}
                    
                </View>

                {this.state.lessonRateId != -1 && this.state.lessonRateId != '' ?
                  <View>

                    {this.state.totalAmount > 0 ? 

                    <View>
                      <View style={{ flexDirection: 'row', height: 50, marginTop: 10 }}>
                        {this.renderCardInput('Card Number', 'cardNumber', this.onChangeCardNum, 0.6, 19, 'numeric')}
                        {this.renderCardInput('Expiration', 'expiry', this.onChangeExpiry, 0.4, 7, 'numeric', 'mm/yyyy')}
                      </View>

                      <View style={{ flexDirection: 'row', height: 50, marginTop: 10 }}>
                        {this.renderCardInput('Name On Card', 'cardName', this.onChangeCardName, 0.6)}
                        {this.renderCardInput('Security Code', 'cvc', this.onChangeCVC, 0.4, 3, 'numeric', 'cvc')}
                      </View>
                    </View>
                    : null
                    }

                    {this.state.price > 0 ? 

                    <View>
                      {this.state.promoDiscount == 0 ?
                        <View style={{ flexDirection: 'row', justifyContent: "flex-start", marginTop: 0, paddingTop: 10, padding: 3, paddingLeft: 20 }}>
                          <Text allowFontScaling={false} style={{ textAlign: "left", fontFamily: "Arial" }}>Payment Total: ${this.state.totalAmount.toFixed(2)} </Text>
                        </View>
                        : null}

                      {this.state.promoDiscount != 0 ?
                        <View style={{ paddingTop: 10 }}>
                          <View style={{ flexDirection: 'row', justifyContent: "flex-start", marginTop: 0, padding: 3, paddingLeft: 20 }}>
                            <Text allowFontScaling={false} style={{ textAlign: "left", fontFamily: "Arial" }}>Original Total: ${this.state.price.toFixed(2)} </Text>
                          </View>
                          <View style={{ flexDirection: 'row', justifyContent: "flex-start", marginTop: 0, padding: 3, paddingLeft: 20 }}>
                            <Text allowFontScaling={false} style={{ textAlign: "left", fontFamily: "Arial" }}>Discount: ${this.state.promoDiscount.toFixed(2)} </Text>
                          </View>
                          <View style={{ flexDirection: 'row', justifyContent: "flex-start", marginTop: 0, padding: 3, paddingLeft: 20 }}>
                            <Text allowFontScaling={false} style={{ textAlign: "left", fontFamily: "Arial" }}>New Total: ${this.state.totalAmount.toFixed(2)} </Text>
                          </View>
                        </View>
                        : null}

                      <View style={{ flexDirection: 'row', height: 50, marginTop: 10 }}>
                        <View style={{ flexDirection: 'column', flex: 0.6, marginHorizontal: 5 }}>
                          <Text style={{ fontSize: 9, color: "#7e7e7e", paddingBottom: 2 }} >Promo Code</Text>
                          <TextInput
                            underlineColorAndroid='transparent'
                            style={styles.inputEmail}
                            fontSize={15}

                            placeholderTextColor="#999"
                            placeholder=''
                            onChangeText={(text) => { this.state.promoCode = text; }}
                          />
                        </View>
                        <View style={{ flexDirection: 'column', flex: 0.4, marginHorizontal: 5, paddingTop: 15, }}>

                          <TouchableOpacity
                            onPress={() => { this.applyPromoCode(); }}
                            style={{ width: 80, height: 30, borderRadius: 5, backgroundColor: 'transparent', alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 10, color: '#32b3fa', fontFamily: 'Gotham-Book' }}>Apply Code</Text>
                          </TouchableOpacity>
                        </View>
                      </View>

                      {this.state.validPromoCode != "" ?
                        <View style={{ marginTop: 0, paddingTop: 10, padding: 3, paddingLeft: 20 }}>
                          <Text allowFontScaling={false} style={{ color: '#3AB180', textAlign: "left", fontFamily: "Arial" }}>{this.state.validPromoCode} </Text>
                        </View>
                        : null}

                      {this.state.invalidPromoCode != "" ?
                        <View style={{ marginTop: 0, paddingTop: 10, padding: 3, paddingLeft: 20 }}>
                          <Text allowFontScaling={false} style={{ color: 'red', textAlign: "left", fontFamily: "Arial" }}>{this.state.invalidPromoCode} </Text>
                        </View>
                        : null}
                    </View>
                    : null
                    }

                    




                    




                    <View style={{ flex: 1, alignSelf: 'center', marginHorizontal: 5, paddingTop: 15, }}>

                      <TouchableOpacity
                        onPress={() => { this.OnPayment(); }}
                        style={{ width: this.width * 0.9, height: 35, borderRadius: 5, backgroundColor: '#32b3fa', alignSelf: 'center', justifyContent: 'center' }}>
                        <Text style={{ alignSelf: 'center', fontSize: 16, fontWeight: 'bold', color: '#ffffff', fontFamily: 'Gotham-Book' }}>{this.state.totalAmount > 0 ? 'Purchase & Book' : 'Redeem & Book'}</Text>
                      </TouchableOpacity>
                    </View>

                  </View> : null

                }

                {this.state.lessonRateId == -1 ?
                  <View> 
                    <View style={{ flex: 1, alignSelf: 'center', marginHorizontal: 5, paddingTop: 15, }}>

                      <TouchableOpacity
                        onPress={() => { this.OnBookCredit(); }}
                        style={{ width: this.width * 0.9, height: 35, borderRadius: 5, backgroundColor: '#32b3fa', alignSelf: 'center', justifyContent: 'center' }}>
                        <Text style={{ alignSelf: 'center', fontSize: 16, fontWeight: 'bold', color: '#ffffff', fontFamily: 'Gotham-Book' }}>Redeem & Book</Text>
                      </TouchableOpacity>
                    </View>



                  </View>

                  : null }


                
            
           </KeyboardAwareScrollView>

            {this.state.isLoading ?
            <View style={styles.loadingScene}>
                <ActivityIndicator
                    animating={true}
                    size="small"
                    color="white"
                    />
            </View> : null}

           
        </View>
    );
  }
}

export default BookPaymentScene;
