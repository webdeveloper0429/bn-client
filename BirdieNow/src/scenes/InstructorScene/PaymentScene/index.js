import React, { Component, PropTypes } from 'react';
import { Dimensions, ActivityIndicator, Alert, View, Image, Text, TouchableOpacity, TouchableHighlight, TextInput, Platform, BackAndroid } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styles from './styles';
import _, { isEqual } from 'lodash';
import { RequestStripeRegister, RequestFreeRegister, ContactRequest, GetAccountInfo } from 'AppUtilities';
import AppConfig from 'AppConfig';

class PaymentScene extends Component {
  static propTypes = {
    popNavBack: PropTypes.func.isRequired,
    pushNavScene: PropTypes.func.isRequired,
    goToNavTab: PropTypes.func.isRequired,
  };

  constructor(props, context) {
    super(props, context);

    this.state = {
        data: [],
        lessonRate: [],
        user: [],
        promoCodeResponse: [],
        totalAmount: 0,
        price: 0,
        promoCode: '',
        promoDiscount: 0,
        invalidPromoCode: '',
        validPromoCode: '',
        isLoading: false,
        cardNumber: '',
        expiry: '',
        cardName: '',
        cvc: '',
        showloading: false,
    };

    const { width, height } = Dimensions.get('window');
    this.width = width;
    this.height = height;
    this.r_width =  width / 1125;
    this.r_height = height / 1659;
    this.phone = "";
    this.comment = "";

    this.OnSubmit = this.OnSubmit.bind(this);
    this.renderCardInput = this.renderCardInput.bind(this);
    this.onChangeCardNum = this.onChangeCardNum.bind(this);
    this.onChangeExpiry = this.onChangeExpiry.bind(this);
    this.onChangeCardName = this.onChangeCardName.bind(this);
    this.onChangeCVC = this.onChangeCVC.bind(this);
    this.OnRetriveUserInfo = this.OnRetriveUserInfo.bind(this);
  }
  
  componentDidMount() {
		//BackAndroid.addEventListener('hardwareBackPress', this.onBackPress.bind(this));
    //Get User Profile
    this.OnRetriveUserInfo();

    if (this.props.passProps != null) {
      this.setState({ data: this.props.passProps.instructor, lessonRate: this.props.passProps.lessonRate,
          price: this.props.passProps.lessonRate.price, totalAmount: this.props.passProps.lessonRate.price });
     
    }
  }

	onBackPress() {
		this.props.popNavBack();
	}
  
  OnRetriveUserInfo() {
    this.setState({showloading: true});
    
    GetAccountInfo(AppConfig.global_userToken)
      .then((response) => {
        if (response) { 
          this.setState({showloading: false, user: response});
             
        } else {
          this.setState({showloading: false});
          Alert.alert(response.error_description);
        }
      })
      .catch(error => {
        Alert.alert(error.message);
        this.setState({showloading: false});
      });
  }

  onChangeCardNum(text) {
    if (text.length < 1) {
      this.setState({ cardNumber: '' });
      return;
    }
    let cardNum = text.split('-').join('');
    cardNum = cardNum.match(/.{1,4}/g);
    this.setState({ cardNumber: cardNum.join('-') });
  }

  onChangeExpiry(text) {
    if (text.length > 2) {
      let expiry = text.split('/').join('');
      expiry = `${expiry.substr(0, 2)}/${expiry.substr(2)}`;
      this.setState({ expiry });
    } else {
      this.setState({ expiry: text });
    }
  }

  onChangeCardName(text) {
    this.setState({ cardName: text });
  }

  onChangeCVC(text) {
    this.setState({ cvc: text });
  }

  applyPromoCode() {
    this.setState({ showloading: true });

    //Get Data
    const header = {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${AppConfig.global_userToken}`,
    },
  };
      fetch(AppConfig.apiUrl + '/api/promoCodes?auth=auth&id=' + this.state.promoCode + '&lessonRateId=' + this.state.lessonRate.id, header)
        .then((response) => response.json())
        .then((responseData) => {

          
          //received data
          if (responseData.id != null) {
            this.setState({ showloading: false, promoCodeResponse: responseData, isDataLoaded: true });

            if (this.state.promoCodeResponse.percentageOff != null) {
              this.state.promoDiscount = this.state.price * this.state.promoCodeResponse.percentageOff;
              this.state.totalAmount = this.state.price - this.state.promoDiscount;
              if (this.state.totalAmount < 0) 
                this.state.totalAmount = 0;
            }
            else if (this.state.promoCodeResponse.priceOff != null) {
              this.state.promoDiscount = this.state.promoCodeResponse.priceOff;
              this.state.totalAmount = this.state.price - this.state.promoDiscount;
              if (this.state.totalAmount < 0) 
                this.state.totalAmount = 0;
            }

            //bookModel.loadingPromo = "";
            this.setState({ showloading: false, validPromoCode: "Code applied!", invalidPromoCode: '' });
            //alert(this.state.validPromoCode);

          }
          else {
            this.setState({ showloading: false, validPromoCode: '', invalidPromoCode: responseData, promoDiscount: 0, totalAmount: this.state.price  });
          }

          
      })
      .catch(error => {
        //alert('outside error');
        this.setState({ showloading: false, validPromoCode: '', invalidPromoCode: "There was an error" });
      });



  }

  OnSubmit() {
    if (this.state.isLoading === true) {
      return;
    } 

    if (this.state.totalAmount == 0) {
      RequestFreeRegister(this.state.totalAmount, this.state.data.id, this.state.lessonRate.id, this.state.promoCode)
        .then(response => {
          if (response === 'Charged') {
            Alert.alert('Thank you for redeeming. Book your next lesson!');
            this.props.goToNavTab(3, 'PurchaseLessonsView');
          } else {
            Alert.alert(`Payment Error: ${response}`);
          }
          this.setState({ isLoading: false });
        })
        .catch(error => {
          Alert.alert(`Payment Error: ${error.message}`);
          this.setState({ isLoading: false });
        });
    }
    else {
      const { cardNumber, expiry, cardName, cvc } = this.state;
      if (cardNumber.length < 1) {
        Alert.alert('Invalid card number');
        return;
      }
      if (expiry.length < 5) {
        Alert.alert('Invalid expiration date');
        return;
      }
      if (cardName.length < 1) {
        Alert.alert('Please input card name');
        return;
      }
      if (cvc.length < 1) {
        Alert.alert('Invalid cvc code');
        return;
      }

      this.setState({ isLoading: true });

      const cardDetails = {
        'card[number]': this.state.cardNumber.split('-').join(''),
        'card[exp_month]': expiry.split('/')[0],
        'card[exp_year]': expiry.split('/')[1],
        'card[cvc]': cvc
      };

      RequestStripeRegister(cardDetails, this.state.totalAmount, this.state.data.id, this.state.lessonRate.id, this.state.promoCode)
        .then(response => {
          if (response === 'Charged') {
            Alert.alert('Thank you for your purchase. Book your next lesson!');
            this.props.goToNavTab(3, 'PurchaseLessonsView');
          } else {
            Alert.alert(`Payment Error: ${response}`);
          }
          this.setState({ isLoading: false });
        })
        .catch(error => {
          Alert.alert(`Payment Error: ${error.message}`);
          this.setState({ isLoading: false });
        });
    } 
  }

  renderCardInput(label, inputKey, changeEvent, flexValue, maxLen = undefined, keyType = 'default', placeHolder) {
    return (
      <View style={{ flexDirection: 'column', flex: flexValue, marginHorizontal: 5 }}>
        <Text style={{ fontSize: 9, color: "#7e7e7e", paddingBottom: 2}} >{label}</Text>
        <TextInput
          underlineColorAndroid='transparent'
          style={styles.inputEmail}
          fontSize={15}
          onChangeText={changeEvent}
          value={this.state[inputKey]}
          placeholderTextColor="#999"
          placeholder={placeHolder}
          keyboardType={keyType}
          maxLength={maxLen}
        />
      </View>
    );
  }

  render() {
    const render_TopBar = (
      <View style={{
        flexDirection: 'row', alignItems: "center", justifyContent: "center",
        backgroundColor: '#fafafa', borderBottomWidth: 1, borderBottomColor: '#D3D3D3',
        height: (Platform.OS === 'ios') ? 65 : 40 }}>

        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={() => { this.onBackPress(); }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#000',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Back</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 2, justifyContent: "center" }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#000',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Checkout</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={this.OnSubmit} >
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#32b3fa',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Submit</Text>
        </TouchableOpacity>
      </View>
    );

    const render_ImageBar = (
        <Image source={require('img/image/ui_back_blue.png')} style={{ alignItems: 'center', justifyContent: 'center', height: 200, width }}>
          <Text allowFontScaling={false} style={{
            fontSize: 19, color: '#4b5b64', fontFamily: 'Gotham-Medium', backgroundColor: 'transparent',
            marginBottom: 10
          }}>
            {this.state.data.firstName} {this.state.data.lastName}
          </Text>
          <View>
            {this.state.data.isProfilePhoto ?
              <Image key="render_image"
                style={{ width: 246 * this.r_width, height: 246 * this.r_width, borderRadius: 123 * this.r_width, marginLeft: 36 * this.r_width }}
                source={{ uri: 'https://www.birdienow.com/img/instructor/profile/' + this.state.data.id + '.jpg' }} />
              :
              <Image key="render_image"
                style={{ width: 246 * this.r_width, height: 246 * this.r_width, borderRadius: 123 * this.r_width, marginLeft: 36 * this.r_width }}
                source={require('img/profile/profile.png')} />
            }

            {this.state.data.isPga ?
              <Image
                style={{ width: 30, height: 30, position: "absolute", marginLeft: 25 * this.r_width, marginTop: 150 * this.r_width }}
                source={{ uri: 'https://www.birdienow.com/img/badge/pga_badge_85.png' }}
              /> : null}
          </View>
          <Text allowFontScaling={false} style={{
            fontSize: 13, color: '#4b5b64', fontFamily: 'Gotham-Medium', backgroundColor: 'transparent',
            marginTop: 10
          }}>
            Lesson Type: {this.state.lessonRate.lessonTypeName}
          </Text>
          <Text allowFontScaling={false} style={{ fontSize: 11, color: '#4b5b64', fontFamily: 'Gotham-Medium', backgroundColor: 'transparent' }}>
            Ages: {this.state.lessonRate.lessonAgeType} / # of Lessons: {this.state.lessonRate.numLessons} / Price: ${this.state.lessonRate.price}
          </Text>
        </Image>
      );

    return (
        <View style={styles.container}>
          {render_TopBar}
          <KeyboardAwareScrollView
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={true} >
            
            
                {render_ImageBar}

                {this.state.lessonRate.description != null && this.state.lessonRate.description != '' ?
                 <View style={{ flexDirection: 'row', marginTop: 10, paddingLeft: this.width * 0.05, paddingRight: this.width * 0.05 }}>
                    <Text allowFontScaling={false} style={{fontSize: 12,color:'#ff4b00', fontFamily: 'Gotham-Book', textAlign:"left"}}>
                      {this.state.lessonRate.description}
                    </Text>
                 </View> : null
                 }

                {this.state.totalAmount > 0 ?
                
               <View>

                 

                 
                <View style={{ flexDirection: 'row', height: 50, marginTop: 10 }}>
                    {this.renderCardInput('Card Number', 'cardNumber', this.onChangeCardNum, 0.6, 19, 'numeric')}
                    {this.renderCardInput('Expiration', 'expiry', this.onChangeExpiry, 0.4, 7, 'numeric', 'mm/yyyy')}
                </View>

                <View style={{ flexDirection: 'row', height: 50, marginTop: 10 }}>
                    {this.renderCardInput('Name On Card', 'cardName', this.onChangeCardName, 0.6)}
                    {this.renderCardInput('Security Code', 'cvc', this.onChangeCVC, 0.4, 3, 'numeric', 'cvc')}
                </View>
                </View>
                  : null
                  }

                     {this.state.price > 0 ? 

                    <View>

 

                {this.state.promoDiscount == 0 ? 
                    <View style={{ flexDirection: 'row', justifyContent: "flex-start", marginTop: 0, paddingTop:10, padding: 3, paddingLeft: 20 }}>
                      <Text allowFontScaling={false} style={{ textAlign: "left", fontFamily: "Arial" }}>Payment Total: ${this.state.totalAmount.toFixed(2)} </Text>
                    </View> 
                     : null }

                {this.state.promoDiscount != 0 ? 
                    <View style={{paddingTop: 10}}>
                      <View style={{ flexDirection: 'row', justifyContent: "flex-start", marginTop: 0, padding: 3, paddingLeft: 20 }}>
                        <Text allowFontScaling={false} style={{ textAlign: "left", fontFamily: "Arial" }}>Original Total: ${this.state.price.toFixed(2)} </Text>
                      </View>
                      <View style={{ flexDirection: 'row', justifyContent: "flex-start", marginTop: 0, padding: 3, paddingLeft: 20 }}>
                        <Text allowFontScaling={false} style={{ textAlign: "left", fontFamily: "Arial" }}>Discount: ${this.state.promoDiscount.toFixed(2)} </Text>
                      </View>
                      <View style={{ flexDirection: 'row', justifyContent: "flex-start", marginTop: 0, padding: 3, paddingLeft: 20 }}>
                        <Text allowFontScaling={false} style={{ textAlign: "left", fontFamily: "Arial" }}>New Total: ${this.state.totalAmount.toFixed(2)} </Text>
                      </View>
                    </View>
                     : null }

                <View style={{ flexDirection: 'row', height: 50, marginTop: 10 }}>
                  <View style={{ flexDirection: 'column', flex: 0.6, marginHorizontal: 5 }}>
                    <Text style={{ fontSize: 9, color: "#7e7e7e", paddingBottom: 2 }} >Promo Code</Text>
                    <TextInput
                      underlineColorAndroid='transparent'
                      style={styles.inputEmail}
                      fontSize={15}
                      
                      placeholderTextColor="#999"
                      placeholder=''
                      onChangeText={(text) => { this.state.promoCode = text; }}
                    />
                  </View>
                  <View style={{ flexDirection: 'column', flex: 0.4, marginHorizontal: 5, paddingTop: 15, }}>

                    <TouchableOpacity
                      onPress={() => { this.applyPromoCode(); }}
                      style={{ width: 80, height: 30, borderRadius: 5, backgroundColor: 'transparent', alignItems: 'center', justifyContent: 'center' }}>
                      <Text style={{ fontSize: 10, color: '#32b3fa', fontFamily: 'Gotham-Book' }}>Apply Code</Text>
                    </TouchableOpacity>
                  </View>
                </View>
                 
                 {this.state.validPromoCode != "" ? 
                    <View style={{ marginTop: 0, paddingTop:10, padding: 3, paddingLeft: 20 }}>
                      <Text allowFontScaling={false} style={{ color: '#3AB180',textAlign: "left", fontFamily: "Arial" }}>{this.state.validPromoCode} </Text>
                    </View> 
                     : null }

                     {this.state.invalidPromoCode != "" ? 
                    <View style={{ marginTop: 0, paddingTop:10, padding: 3, paddingLeft: 20 }}>
                      <Text allowFontScaling={false} style={{ color: 'red', textAlign: "left", fontFamily: "Arial" }}>{this.state.invalidPromoCode} </Text>
                    </View> 
                     : null }

                     </View>
                     : null

                  }


                  <View style={{ flex:1, alignSelf: 'center', marginHorizontal: 5, paddingTop: 15, }}>

                    <TouchableOpacity
                      onPress={() => { this.OnSubmit(); }}
                      style={{ width: this.width * 0.9, height: 35, borderRadius: 5, backgroundColor: '#32b3fa', alignSelf: 'center', justifyContent: 'center' }}>
                      <Text style={{ alignSelf: 'center', fontSize: 16, fontWeight: 'bold', color: '#ffffff', fontFamily: 'Gotham-Book' }}>{ this.state.totalAmount == 0 ? 'Redeem' : 'Purchase'}</Text>
                    </TouchableOpacity>
                  </View>

                
            
           </KeyboardAwareScrollView>

            {this.state.isLoading ?
            <View style={styles.loadingScene}>
                <ActivityIndicator
                    animating={true}
                    size="small"
                    color="white"
                    />
            </View> : null}

           
        </View>
    );
  }
}

export default PaymentScene;
