import { Dimensions, StyleSheet } from 'react-native';
import AppConfig from 'AppConfig';

var { width, height } = Dimensions.get('window');
var r_width =  width / 1125;
var r_height = height / 1659;

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  topView:{
    flexDirection: 'row',
    height: 65,
    backgroundColor: '#aac1ce',
    borderBottomWidth: 1,
    borderBottomColor:'#D3D3D3',
    justifyContent: "center"
  },
  inputEmail: {
    flex: 3,
    alignSelf: 'stretch',
    height: 40,
    backgroundColor: '#faffbd',
    paddingHorizontal: 10,
    paddingVertical: 0,
    borderWidth: 1,
    borderColor: '#008000',
    borderRadius: 5,
  },
  inputText: {
    flex: 3,
    fontSize: 14,
    textAlign: 'left',
    //backgroundColor: '#f7f7f7'
  },
  iconContact: {
    width: 23,
    height: 23,
    marginHorizontal: 10,
    marginTop: 4,
  },
  txtContact: {
    flex: 1,
    fontSize: 14,
    textAlign: 'left'
  },
  txtContactWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    borderTopWidth: 1,
    borderColor: '#eeeff0',
    padding: 5,
  },
  titleView:{flexDirection:'row', alignItems:'center', marginTop: 50 * r_width,},
  title:{marginLeft:32 * r_width, fontSize: 13.5, fontFamily: 'Arial'},
  content:{marginLeft:2,fontSize: 14,color:'#474747',fontFamily: 'Arial'},
  contentBold:{marginLeft:2,fontSize: 16,color:'#474747',marginTop:7, marginBottom:5.5, fontFamily: 'Arial'},
  blueContent:{marginLeft:36 * r_width, fontSize: 14, color:'#0086F8', marginTop:20, marginBottom:20, fontFamily:'Arial'}
});
