import React, { Component, PropTypes } from 'react';
import { Dimensions, Alert, View, Image, TouchableOpacity, Platform, Text, TextInput, ListView, RefreshControl, ScrollView, ActivityIndicator, PanResponder, BackAndroid } from 'react-native';
import styles from './styles';
import AppConfig from 'AppConfig';
import AppStyles from 'AppStyles';
import { SearchBar, HeaderBar } from 'AppComponents';
import { FilterScene, ListDetailScene, BookPaymentScene } from 'AppScenes';
import { RequestApi, MakeCancelable, GlobalStorage, ContactRequest, GetAccountInfo, BookRequest, BookInfo  } from 'AppUtilities';
import _, { isEqual } from 'lodash';
import StarRating from 'react-native-star-rating';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import moment from 'moment';

class BookScene extends Component {
  static propTypes = {
    popNavBack: PropTypes.func.isRequired,
    pushNavScene: PropTypes.func.isRequired,
    popNavToTop: PropTypes.func.isRequired,
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      //openDate: [],
      data: [],
      location: [],
      isDataLoaded: false,
      showloading: false,
      city: '',
      userPhone: ''
    };

    let { width, height } = Dimensions.get('window');
    this.width = width;
    this.height = height;
    this.r_width =  width / 1125;
    this.r_height = height / 1659;
    this.phonenumber = "";
    this.comment = "";
    this.startHour = "";
    this.startMinute = "";
    this.endHour = "";
    this.endMinute = "";
    

    this.startDate = '';
    this.displayDate = '';

    this.OnRetriveUserInfo = this.OnRetriveUserInfo.bind(this);
    this.OnSubmit = this.OnSubmit.bind(this);
    this.OnNext = this.OnNext.bind(this);
    this.OnChangePhone = this.OnChangePhone.bind(this);
    this.BookSuccess = this.BookSuccess.bind(this);

    this.CallBackBookPaymentScene = ::this.CallBackBookPaymentScene;

    this.handlePop = ::this.handlePop;
  }

  handlePop() {
    this.props.popNavBack();
  }

  componentDidMount() {
		//BackAndroid.addEventListener('hardwareBackPress', this.onBackPress.bind(this));

    this.setState({showloading: true});
    //Get User Profile
    this.OnRetriveUserInfo();


    //Do you have input params?
    if (this.props.passProps != null) {
      this.setState({ openDate: this.props.passProps.openDate, data: this.props.passProps.instructor, location: this.props.passProps.location });


      //Get Data
      fetch(AppConfig.apiUrl + '/api/OpenDates/' + this.props.passProps.openDateId + '?lessonTypeId=' + this.props.passProps.lessonTypeId)
        .then((response) => response.json())
        .then((responseData) => {
          this.setState({ showloading: false, data: responseData, isDataLoaded: true });
        })


    }
  }

	onBackPress() {
		this.props.popNavBack();
	}

  CallBackBookPaymentScene()
  {
    this.props.callback();
    this.props.popNavBack();
  }


  OnRetriveUserInfo() {
    this.setState({showloading: true});
    
    GetAccountInfo(AppConfig.global_userToken)
      .then((response) => {
        if (response) { //success 200
          AppConfig.userInfo = response;
          this.OnChangePhone(AppConfig.userInfo.phone);
          this.setState({showloading: false});
        } else { // failed 400
          this.setState({showloading: false});
          Alert.alert(response.error_description);
        }
      })
      .catch(error => {
        Alert.alert(error.message);
        this.setState({showloading: false});
      });
  }

  OnChangePhone(text) {
    let phone = text.split('(').join('');
    phone = phone.split(')').join('');
    phone = phone.split('-').join('');
    phone = phone.split(' ').join('');
    if (phone.length > 6) {
      phone = `(${phone.substr(0, 3)}) ${phone.substr(3, 3)}-${phone.substr(6)}`;
    } else if (phone.length > 3) {
      phone = `(${phone.substr(0, 3)}) ${phone.substr(3, 3)}`;
    } else if (phone.length > 0) {
      phone = `(${phone.substr(0, 3)}`;
    } else {
      phone = '';
    }
    this.setState({ userPhone: phone });
  }

  OnNext()
  {

    let requestParams = JSON.stringify({
      openDateId: this.state.data.id,
      lessonTypeId: this.state.data.lessonTypeId, 
      firstName: AppConfig.userInfo.firstName,
      lastName: AppConfig.userInfo.lastName,
      phone: this.state.userPhone,
      email: AppConfig.userInfo.email,
      instructorId: this.state.data.instructorId,
      locationId: this.state.data.locationId,
      startDate: this.state.data.startDate,
      clientNote: this.comment});

      
      
    this.props.pushNavScene(BookPaymentScene, {ref:(c) => { this.refBookPayment = c; } , 
    passProps: { instructor: this.state.data, requestParams: requestParams }, callback: this.CallBackBookPaymentScene });
    
  }

//OLD SUBMIT - this has moved to BookPayentScene
  OnSubmit() {



    let requestParams = JSON.stringify({
      openDateId: this.state.data.id,
      lessonTypeId: this.state.data.lessonTypeId, 
      firstName: AppConfig.userInfo.firstName,
      lastName: AppConfig.userInfo.lastName,
      phone: this.state.userPhone,
      email: AppConfig.userInfo.email,
      instructorId: this.state.data.instructorId,
      locationId: this.state.data.locationId,
      startDate: this.state.data.startDate,
      clientNote: this.comment});
    this.setState({showloading: true});
    fetch(AppConfig.apiUrl + '/api/BookAppointments', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${AppConfig.global_userToken}`,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: requestParams
    })
    .then((response) => response.json())
    .then((responseData) => {
      this.setState({showloading: false});
      Alert.alert(
        'CONFIRMATION',
        'Your book request has been successfully submitted',
        [
          {text: 'OK', onPress: () => {this.BookSuccess();}},
        ],
        { cancelable: false }
      );
    })
    .catch(error => {
      Alert.alert(error.message);
      this.setState({showloading: false});
    });
  }

  BookSuccess() {
    this.props.popNavBack();
  }

  render() {
    const render_TopBar = (
      <View style={{
        flexDirection: 'row', alignItems: "center", justifyContent: "center",
        backgroundColor: '#fafafa', borderBottomWidth: 1, borderBottomColor: '#D3D3D3',
        height: (Platform.OS === 'ios') ? 65 : 40 }}>

        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={() => { this.onBackPress(); }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#000',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Back</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 2, justifyContent: "center" }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#000',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Book</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={this.OnNext} >
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#32b3fa',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Next</Text>
        </TouchableOpacity>
      </View>
    );

    let render_ImageBar = [];
    if (this.state.isDataLoaded) {
      render_ImageBar = (
        <Image source={require('img/image/ui_back_blue.png')} style={{ alignItems: 'center', justifyContent: 'center', height: 200, width }}>
          <Text allowFontScaling={false} style={{
            fontSize: 19, color: '#4b5b64', fontFamily: 'Gotham-Medium', backgroundColor: 'transparent',
            marginBottom: 10
          }}>
            {this.state.data.firstName} {this.state.data.lastName}
          </Text>
          <View>
            {this.state.data.isProfilePhoto ?
              <Image key="render_image"
                style={{ width: 246 * this.r_width, height: 246 * this.r_width, borderRadius: 123 * this.r_width, marginLeft: 36 * this.r_width }}
                source={{ uri: 'https://www.birdienow.com/img/instructor/profile/' + this.state.data.instructorId + '.jpg' }} />
              :
              <Image key="render_image"
                style={{ width: 246 * this.r_width, height: 246 * this.r_width, borderRadius: 123 * this.r_width, marginLeft: 36 * this.r_width }}
                source={require('img/profile/profile.png')} />
            }

            {this.state.data.isPga ?
              <Image
                style={{ width: 30, height: 30, position: "absolute", marginLeft: 25 * this.r_width, marginTop: 150 * this.r_width }}
                source={{ uri: 'https://www.birdienow.com/img/badge/pga_badge_85.png' }}
              /> : null}
          </View>
          <Text allowFontScaling={false} style={{
            fontSize: 13, color: '#4b5b64', fontFamily: 'Gotham-Medium', backgroundColor: 'transparent',
            marginTop: 10
          }}>
            {this.state.data.locationName}
          </Text>
          <Text allowFontScaling={false} style={{ fontSize: 11, color: '#4b5b64', fontFamily: 'Gotham-Medium', backgroundColor: 'transparent' }}>
            {this.state.data.address1} {this.state.data.city}, {this.state.data.state} {this.state.data.zip}
          </Text>
        </Image>
      );

    }

    let render_Lesson = [];
    if (this.state.isDataLoaded) {
      render_Lesson = (
        <View>
          <View style={[ styles.txtContactWrapper, { flexDirection: 'row', justifyContent: "center", marginTop: 0 }]}>
            <Image source={require('img/form/form-calendar.png')} style={styles.iconContact} />
            {/*<Text allowFontScaling={false} style={{ flex: 2, textAlign: "left", fontFamily: "Arial", marginLeft: 30 }}>Date</Text>*/}
            <Text allowFontScaling={false} style={{ flex: 3, textAlign: "left", fontFamily: "Arial" }}>{moment(this.state.data.localStartDate).format('dddd, ll')}</Text>
            {/*<Text allowFontScaling={false} style={{ flex: 1, textAlign: "center", fontFamily: "Arial" }} />*/}
          </View>
          <View style={[ styles.txtContactWrapper, { flexDirection: 'row', justifyContent: "center", marginTop: 0 }]}>
            <Image source={require('img/form/form-time.png')} style={styles.iconContact} />
            {/*<Text allowFontScaling={false} style={{ flex: 2, textAlign: "left", fontFamily: "Arial", marginLeft: 30 }}>Time</Text>*/}
            <Text allowFontScaling={false} style={{ flex: 3, textAlign: "left", fontFamily: "Arial" }}>{moment(this.state.data.localStartDate).format('LT')} - {moment(this.state.data.localEndDate).format('LT')} </Text>
            {/*<Text allowFontScaling={false} style={{ flex: 1, textAlign: "center", fontFamily: "Arial" }} />*/}
          </View>
        </View>
      );
    }

    return (
      <View style={styles.container}>
        {render_TopBar}
         <KeyboardAwareScrollView
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={false} >
          {render_ImageBar}
          {render_Lesson}
          

          
         

          {/*<View style={{flexDirection: 'row', justifyContent: "center", marginTop: 15}}>
            <Text allowFontScaling={false} style={{flex: 2, textAlign: "left", fontFamily: "Arial", marginLeft:30}}>Name</Text>
            <Text allowFontScaling={false} style={{flex: 3, textAlign: "left", fontFamily: "Arial"}}>{AppConfig.userInfo.firstName} {AppConfig.userInfo.lastName}</Text>
            <Text allowFontScaling={false} style={{flex: 1, textAlign: "center", fontFamily: "Arial"}}/>
          </View>
          <View style={{flexDirection: 'row', justifyContent: "center", marginTop: 10}}>
            <Text allowFontScaling={false} style={{flex: 2, textAlign: "left", fontFamily: "Arial", marginLeft:30}}>Email</Text>
            <Text allowFontScaling={false} style={{flex: 3, textAlign: "left", fontFamily: "Arial"}}>{AppConfig.userInfo.email}</Text>
            <Text allowFontScaling={false} style={{flex: 1, textAlign: "center", fontFamily: "Arial"}}/>
          </View>*/}
          <View style={[ styles.txtContactWrapper, {flexDirection: 'row', justifyContent: "center", borderTopWidth: 1, borderColor: '#eeeff0'}]}>
            {/*<Text allowFontScaling={false} style={{flex: 2, textAlign: "left", fontFamily: "Arial", marginLeft:30}}>Phone</Text>*/}
            <Image source={require('img/form/form-contact.png')} style={styles.iconContact} />
            <TextInput
              underlineColorAndroid='transparent'
              style={styles.inputText}
              keyboardType="phone-pad"
              maxLength={14}
              placeholder="Phone Number"
              placeholderTextColor="#9b9b9b"
              defaultValue={AppConfig.userInfo.phone} 
              onChangeText={this.OnChangePhone}
              value={this.state.userPhone}
              //onChangeText={(text) => {this.phonenumber = text}}
              />
            <Text allowFontScaling={false} style={{flex: 1, textAlign: "left", fontFamily: "Arial"}}/>
          </View>
          {/*<Text allowFontScaling={false} style={{textAlign: "center", color :"gray", fontFamily: "Arial", marginTop: 10,
            }}>
            *Contact information will be provided to the instructor
          </Text>*/}


          <View style={[styles.txtContactWrapper, { alignItems: 'flex-start', borderBottomWidth: 1 }]}>
            <Image source={require('img/form/form-comments.png')} style={styles.iconContact} />
            <TextInput
              underlineColorAndroid="transparent"
              style={[styles.txtContact, { textAlignVertical: 'top', height: 200, marginTop: -3 }]}
              placeholder="Additional comments"
              placeholderTextColor="#9b9b9b"
              multiline={true}
              numberOfLines={100}
              onChangeText={(text) => { this.comment = text; }}
            />
          </View>






          {/*<TextInput
            underlineColorAndroid='transparent'
            style={{marginLeft: 20, marginRight: 20, fontSize: 16, marginTop: 20, height: 200, padding: 5, textAlignVertical: 'top', borderWidth: 1}}
            placeholder="Introduce yourself and write any questions/comments you have for the instructor"
            placeholderTextColor="#999"
            multiline = {true}
            numberOfLines = {100}
            onChangeText={(text) => {this.comment = text}}
          />*/}




          {/*<View style={{flexDirection: 'row', justifyContent: "center", marginTop: 10}}>
            <View style={{flex: 1}}/>
            <TouchableOpacity
              style={{flex: 2, height: 30, backgroundColor: "orange", justifyContent: "center", borderRadius: 5}}
              underlayColor="#bde7ff"
              onPress={() => {this.OnSubmit();}}
            >
              <Text allowFontScaling={false} style={{textAlign: "center", color: "white"}}>Submit</Text>
            </TouchableOpacity>
            <View style={{flex: 1}}/>
            <TouchableOpacity
              style={{flex: 2, height: 30,  backgroundColor: "lightgray", justifyContent: "center", borderRadius: 5}}
              underlayColor="#bde7ff"
              onPress={() => {this.props.popNavBack();}}
            >
              <Text allowFontScaling={false} style={{textAlign: "center", color: "black"}}>Cancel</Text>
            </TouchableOpacity>
            <View style={{flex: 1}}/>
          </View>*/}

        </KeyboardAwareScrollView>
        {this.state.showloading ?
          <View
            style={{position:'absolute', top: 0, left:0, bottom:0, right:0, alignItems:'center', justifyContent:'center', alignSelf: 'stretch', backgroundColor: 'transparent'}}
          >
            <ActivityIndicator
              animating={true}
              size="small"
              color="gray"
            />
          </View>:null}
      </View>
    );
  }
}

export default BookScene;
