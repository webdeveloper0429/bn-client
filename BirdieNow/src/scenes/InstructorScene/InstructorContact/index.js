import React, { Component, PropTypes } from 'react';
import { Dimensions, Alert, View, Image, TouchableOpacity, Text, TextInput, ActivityIndicator, KeyboardAvoidingView, Platform, BackAndroid } from 'react-native';
import styles from './styles';
import AppConfig from 'AppConfig';
import { ContactRequest, GetAccountInfo } from 'AppUtilities';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

class InstructorContact extends Component {
  static propTypes = {
    popNavBack: PropTypes.func.isRequired,
    pushNavScene: PropTypes.func.isRequired,
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      data: [],
      showloading: false,
      phone: '',
      city: '',
    };

    const { width, height } = Dimensions.get('window');
    this.width = width;
    this.height = height;
    this.r_width =  width / 1125;
    this.r_height = height / 1659;
    this.phone = "";
    this.comment = "";

    this.OnRetriveUserInfo = this.OnRetriveUserInfo.bind(this);
    this.OnSubmit = this.OnSubmit.bind(this);
    this.OnChangePhone = this.OnChangePhone.bind(this);
    this.ContactSuccess = this.ContactSuccess.bind(this);

    this.handlePop = ::this.handlePop;
  }

  handlePop() {
    this.props.popNavBack();
  }

  componentDidMount() {
		//BackAndroid.addEventListener('hardwareBackPress', this.onBackPress.bind(this));
    //Get User Profile
    this.OnRetriveUserInfo();
    //Do you have input params?
    if (this.props.passProps != null) {

      this.setState({ data: this.props.passProps.instructor });
    }
  }

	onBackPress() {
		this.props.popNavBack();
	}

  OnChangePhone(text) {
    let phone = text.split('(').join('');
    phone = phone.split(')').join('');
    phone = phone.split('-').join('');
    phone = phone.split(' ').join('');
    if (phone.length > 6) {
      phone = `(${phone.substr(0, 3)}) ${phone.substr(3, 3)}-${phone.substr(6)}`;
    } else if (phone.length > 3) {
      phone = `(${phone.substr(0, 3)}) ${phone.substr(3, 3)}`;
    } else if (phone.length > 0) {
      phone = `(${phone.substr(0, 3)}`;
    } else {
      phone = '';
    }
    this.setState({ phone });
  }

  OnRetriveUserInfo() {
    this.setState({showloading: true});
    GetAccountInfo(AppConfig.global_userToken)
      .then((response) => {
    
        if (response) { //success 200
          AppConfig.userInfo = response;
          this.OnChangePhone(AppConfig.userInfo.phone);
          this.setState({ showloading: false });
        } else { // failed 400
          this.setState({showloading: false});
          Alert.alert(response.error_description);
        }
      })
      .catch(error => {
        Alert.alert(error.message);
        this.setState({showloading: false});
      });
  }

  OnSubmit() {
    this.setState({showloading: true});

    let requestParams = JSON.stringify({
      firstName: AppConfig.userInfo.firstName,
      lastName: AppConfig.userInfo.lastName,
      phone: this.state.phone,
      email: AppConfig.userInfo.email,
      instructorId: this.state.data.id,
      clientNote: this.comment,
    });

    let test = this.state.data;
    


    
    this.setState({showloading: true});
    fetch(AppConfig.apiUrl + '/api/ContactRequests', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${AppConfig.global_userToken}`,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: requestParams
    })
      .then((response) => {
          
        if (response.status == 200 || response.status == 201) {
          this.setState({ showloading: false });
          Alert.alert(
            'CONFIRMATION',
            'Your contact request has been successfully submitted',
            [
              {
                text: 'OK', onPress: () => {
                  this.ContactSuccess();
                }
              },
            ],
            { cancelable: false }
          );
        }
        else {
          alert("ERROR: There was a system error submitting a contact request");
        }
      })
      .catch(error => {
        Alert.alert(error.message);
        this.setState({showloading: false});
      });
  }

  ContactSuccess() {
    this.props.popNavBack();
  }

  render() {
    const render_TopBar = (
      <View style={{
        flexDirection: 'row', alignItems: "center", justifyContent: "center",
        backgroundColor: '#fafafa', borderBottomWidth: 1, borderBottomColor: '#D3D3D3',
        height: (Platform.OS === 'ios') ? 65 : 40 }}>


        {/*<TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={() => { this.props.popNavBack(); }}>
          <Text allowFontScaling={false} style={{ fontFamily: 'Gotham-Book', fontSize: 15, textAlign: "center", 
          marginTop: (Platform.OS === 'ios') ? 20 : 0, color: '#000', marginRight: 10 }}>Back</Text>
        </TouchableOpacity>
        <View style={{ flex: 2, justifyContent: "center"}}>
          <Text allowFontScaling={false} style={{textAlign: "center", fontSize: 15, 
          marginTop: (Platform.OS === 'ios') ? 20 : 0}} />
        </View>
        <TouchableOpacity style={{flex: 1, justifyContent: "center"}} onPress={this.OnSubmit}>
          <Text allowFontScaling={false} style={{ fontFamily: 'Gotham-Bold', fontSize: 15, textAlign: "center", 
          marginTop: (Platform.OS === 'ios') ? 20 : 0, color: '#33b3fa', marginLeft: 10 }}>Submit</Text>
        </TouchableOpacity>*/}

        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={() => { this.props.popNavBack(); }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#000',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Back</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 2, justifyContent: "center" }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#000',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Contact</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={this.OnSubmit} >
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#32b3fa',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Submit</Text>
        </TouchableOpacity>
      </View>
    );

    const render_image = [];
    if (this.state.data.checkImageFlag) {
      render_image.push(
        <Image
          key="render_image2"
          style={{marginVertical: 7, width: 246 * this.r_width, height: 246 * this.r_width, borderRadius:123 * this.r_width, marginLeft:36 * this.r_width}}
          source={{uri: 'https://www.birdienow.com/img/instructor/profile/'+this.state.data.id+'.jpg'}}
        />
      );
    } else {
      render_image.push(
        <Image
          key="render_image2"
          style={{marginVertical: 7, width: 246 * this.r_width, height: 246 * this.r_width, borderRadius:123 * this.r_width, marginLeft:36 * this.r_width}}
          source={require('img/profile/profile.png')}
        />
      );
    }

    const render_ImageBar = (
      <Image source={require('img/image/ui_back_blue.png')} style={{alignItems: 'center', justifyContent: 'center', height: 200, width }}>
        <Text allowFontScaling={false} style={{ fontSize: 19, color: '#4b5b64', fontFamily: 'Gotham-Medium', backgroundColor: 'transparent',
          marginBottom: 10 }}>
          {this.state.data.firstName} {this.state.data.lastName}
        </Text>
        <View>
          {this.state.data.isProfilePhoto ?
          <Image key="render_image"
            style={{ width: 246 * this.r_width, height: 246 * this.r_width, borderRadius: 123 * this.r_width, marginLeft: 36 * this.r_width }}
            source={{ uri: 'https://www.birdienow.com/img/instructor/profile/' + this.state.data.id + '.jpg' }}  />         
          :
        <Image key="render_image"
          style={{ width: 246 * this.r_width, height: 246 * this.r_width, borderRadius: 123 * this.r_width, marginLeft: 36 * this.r_width }}
          source={require('img/profile/profile.png')}  />       
        }

        {this.state.data.isPga ?
          <Image
            style={{width: 30, height: 30, position: "absolute", marginLeft: 25 * this.r_width, marginTop: 150 * this.r_width}}
            source={{uri: 'https://www.birdienow.com/img/badge/pga_badge_85.png'}}
          />:null}
        </View>
      </Image>
    );

    return (
      <View style={styles.container}>
        {render_TopBar}
        <KeyboardAwareScrollView
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={false} >
          {render_ImageBar}
          <View style={styles.txtContactWrapper}>
            <Image source={require('img/form/form-contact.png')} style={styles.iconContact} />
            <TextInput
              underlineColorAndroid="transparent"
              style={styles.txtContact}
              keyboardType="phone-pad"
              maxLength={14}
              placeholder="Phone Number"
              placeholderTextColor="#9b9b9b"
              onChangeText={this.OnChangePhone}
              value={this.state.phone}
            />
          </View>

          <View style={[styles.txtContactWrapper, { alignItems: 'flex-start', borderBottomWidth: 1 }]}>
            <Image source={require('img/form/form-comments.png')} style={styles.iconContact} />
            <TextInput
              underlineColorAndroid="transparent"
              style={[styles.txtContact, { textAlignVertical: 'top', height: 200, marginTop: -3 }]}
              placeholder="Additional comments"
              placeholderTextColor="#9b9b9b"
              multiline={true}
              numberOfLines={100}
              onChangeText={(text) => { this.comment = text; }}
            />
          </View>
        </KeyboardAwareScrollView>
        {this.state.showloading ?
          <View
            style={{position:'absolute', top: 0, left:0, bottom:0, right:0, alignItems:'center', justifyContent:'center', alignSelf: 'stretch', backgroundColor: 'transparent'}}
          >
            <ActivityIndicator
              animating={true}
              size="small"
              color="gray"
            />
          </View>:null}
      </View>
    );
  }
}

export default InstructorContact;
