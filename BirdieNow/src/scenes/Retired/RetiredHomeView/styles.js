import { StyleSheet } from 'react-native';
import AppConfig from 'AppConfig';
import { Dimensions } from 'react-native';
let { width, height } = Dimensions.get('window');
this.r_width =  width / 356;
this.r_height = height / 647;

export default StyleSheet.create({
  container: {
    flex: 1,
    width: width,
    height: height,
    //paddingTop: 20,
    //backgroundColor: "#aac1ce",
    backgroundColor: "#F9F9F9",
  },
  back: {
    position: "absolute",
    width: window.width,
    height: window.height
  },
  inputSearch: {
    position: 'absolute',
    marginTop: 10,
    width: width - 40,
    height: 26 * this.r_height,
    marginLeft: 20,
    textAlign: "center",
    fontFamily: "Gotham-Book",
    backgroundColor: '#fff',
    paddingHorizontal: 10,
    borderWidth: 1,
    //borderColor: '#008000',
    borderColor: '#b5b4b4',
    borderRadius: 5,
    fontSize: 13,
    paddingVertical: 0
  },
  searchImage: {
    position: 'absolute',
    marginTop: 10 + 5 * this.r_height,
    marginLeft: 135 * this.r_width,
    width: 16 * this.r_height,
    height: 16 * this.r_height
  },
  bottomScrollView: {

    width: width,
    height: 120 * this.r_height,
    marginTop: height - 70 - 120 * this.r_height,
    backgroundColor: '#faffbd'
  },
  scrollContentItem: {
    marginTop:10 * this.r_height,
    marginLeft:10 * this.r_width,
    marginRight:10 * this.r_width,
    width: 100 * this.r_height,
    height: 100 * this.r_height,
    borderRadius:15 * this.r_width,
    //backgroundColor: '#f3f3f3',
    backgroundColor: '#ffffff',
    alignItems:'center',
    justifyContent:'center'
  },
  horizontalWrapper: {
    flexDirection: 'row'
  },
  animal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  animalAnimatedBox: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
  },
  loadingScene: {
    top: 0,
    left: 0,
    width: width,
    height: height,
    alignSelf: 'stretch',
    backgroundColor: 'rgba(0,0,0,0.0)',
    alignItems: 'center',
    justifyContent: 'center'
  },
  nearbutton: { 
    color: '#fff',
    textAlign: 'center',
    height: 80,
   // opacity: 0.5,
    fontSize: 16,
    borderWidth: 2,
    borderColor: '#fff',
    borderRadius: 8,
    paddingLeft: 10,
    paddingRight: 7,
    paddingTop: 10,
    paddingBottom: 7
  }
});
