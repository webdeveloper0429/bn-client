import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    marginBottom: 50
  },
  cardview:{height:150, width:83, flex:1, backgroundColor: '#ffffff', borderWidth:1, borderColor: '#123456', marginTop: 0, marginLeft: 5, marginRight: 5, marginBottom: 5, alignItems:'center', justifyContent: 'center'},
  cardtext:{fontSize: 39,},
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 7
  },
});



