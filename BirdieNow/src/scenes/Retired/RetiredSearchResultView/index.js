import React, { Component, PropTypes } from 'react';
import {
  Alert,
  StyleSheet,
  Dimensions,
  Keyboard,
  ActivityIndicator,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Platform,
  View,
  Image,
  TouchableOpacity,
  Text,
  TextInput,
  ListView,
  RefreshControl,
  ScrollView
} from 'react-native';
import styles from './styles';
import AppConfig from 'AppConfig';
import AppStyles from 'AppStyles';
import { SearchBar, HeaderBar, RefreshListView } from 'AppComponents';
import { FilterScene, ListDetailScene, InstructorScene } from 'AppScenes';
import { MakeCancelable } from 'AppUtilities';
import _, { isEqual } from 'lodash';
import StarRating from 'react-native-star-rating';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

class RetiredSearchResultView extends Component {
  static propTypes = {
    popNavBack: PropTypes.func.isRequired,
    pushNavScene: PropTypes.func.isRequired,
    callback: PropTypes.func,
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      text:AppConfig.searchResultDisplay,
      data:AppConfig.data,
      showloading: false,
      focusFlag: false,

      refreshData: [],
      city:AppConfig.city,
      instructor:'',
      isInstructor: AppConfig.isInstructor,
      instructorflag:0,
      facitity:'',
      flag:0,

      isRefreshing: false,
      isInfiniting: false,
      canLoadMore: true,

      selectedRowID: -1,

      citysearchstatus: false,
      instructorsearchstatus: false,
    };
    let { width, height } = Dimensions.get('window');
    this.width = width;
    this.height = height;
    this.r_width =  width / 356;
    this.r_height = height / 647;
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => !isEqual(r1, r2)});
    this.ds_result = new ListView.DataSource({rowHasChanged: (r1, r2) => !isEqual(r1, r2)});
    this.pageNum = 2;
    this.pageNum_Refresh = 2;
    this.longitude = "";
    this.latitude = "";
    this.isCurrentLoc = AppConfig.isCurrentLoc;
    this.apiPromise = null;
    this.started = false;
    this.pushSceneFlag = false;

    this.OnCurrentMyLoc = this.OnCurrentMyLoc.bind(this);
    this.OnCurrentMyLoc_Check = this.OnCurrentMyLoc_Check.bind(this);
    this._onRefresh = this._onRefresh.bind(this);
    this.onKeyboardUpdated = this.onKeyboardUpdated.bind(this);
    this.onSearchResult = this.onSearchResult.bind(this);
    this._onPressButton = this._onPressButton.bind(this);
    this.onPriceLevel = this.onPriceLevel.bind(this);
    this._loadMoreContentAsync = this._loadMoreContentAsync.bind(this);
    this._loadMoreContentAsync_Refresh = this._loadMoreContentAsync_Refresh.bind(this);
    this.onStarRatingPress = this.onStarRatingPress.bind(this);
    this.onDistance = this.onDistance.bind(this);
    this.onProfilePage = this.onProfilePage.bind(this);
    this.focusedSearch = this.focusedSearch.bind(this);
    this.endedSearch = this.endedSearch.bind(this);
    this.onCancelCitySearch = this.onCancelCitySearch.bind(this);
    this.onCancelInstructorSearch = this.onCancelInstructorSearch.bind(this);
    this.checkFocus = ::this.checkFocus;
    this.CallBackInstructorScene = ::this.CallBackInstructorScene;
  }

  checkFocus() {
    if (AppConfig.checkFocus) {
      if (this._textInput) {
        this._textInput.focus();
        AppConfig.checkFocus = false;
      } else {
        AppConfig.checkFocus = false;
        this.pageNum = 2;
        this.isCurrentLoc = false;
        this.setState({refreshData:[], selectedRowID: -1, canLoadMore: true, text: "", city: "", instructor: "", citysearchstatus: false, instructorsearchstatus: false});
        this.searchListRef.scrollTo({x: 0, y: 0, animated: false});
      }
    } else if (AppConfig.checkGPS) {
      AppConfig.checkGPS = false;
      //go to gps
      this.pageNum = 2;
      this.OnCurrentMyLoc_Check();
    } else if (AppConfig.checkCity) {
      this.isCurrentLoc = false;
      this.setState({refreshData:[], selectedRowID: -1, canLoadMore: true, text: "", city: "", instructor: "", citysearchstatus: false, instructorsearchstatus: false});
      this.searchListRef.scrollTo({x: 0, y: 0, animated: false});
      if (this.apiPromise !== null) {
        this.apiPromise.cancel();
      }

      AppConfig.checkCity = false;
      //go to
      AppConfig.city = AppConfig.checkCityName;
      this.setState({showloading: true});
      fetch(AppConfig.apiUrl + '/api/InstructorSearchViewModels?zip='+AppConfig.checkCityZip+'&'+
        'miles=100&'+
        'gender=Any&'+
        'priceLevel=All&'+
        'pageNum=1&'+
        'numResults=20')
        .then((response) => response.json())
        .then((responseData) => {

          if (responseData.length > 0) {
            
              this.searchListRef.scrollTo({x: 0, y: 0, animated: false});
              
              //TODO: need to eliminate this call
              AppConfig.data = responseData;
              
              this.setState({showloading: false, data: responseData, focusFlag: false, text: AppConfig.checkCityName, canLoadMore: true});
              
              // TODO: Need to increment page counter at infinite scroll level
              this.pageNum = 2;
              AppConfig.city = AppConfig.checkCityZip;

              //TODO: Need to retrieve header data total row count and store data
           
          } else{
            this.setState({showloading:false});
            Alert.alert('No Instructors found near ' + AppConfig.checkCityName);
          }

        })

    }

  }

  componentDidMount() {
    this.checkFocus();
  }

  componentWillMount() {
    console.info(AppConfig.searchResultDisplay);
    if (Platform.OS === 'ios') {
      this.subscriptions = [
        Keyboard.addListener('keyboardWillHide', (event) => this.onKeyboardUpdated(event, false)),
        Keyboard.addListener('keyboardWillShow', (event) => this.onKeyboardUpdated(event, true)),
      ];
    } else {
      this.subscriptions = [
        Keyboard.addListener('keyboardDidHide', (event) => this.onKeyboardUpdated(event, false)),
        Keyboard.addListener('keyboardDidShow', (event) => this.onKeyboardUpdated(event, true)),
      ];
    }
  }

  CallBackInstructorScene() {
    this.pushSceneFlag = false;
  }

  onKeyboardUpdated(event, type) {
    if (type == true) {
      this.setState({focusFlag: true});
    } else {
      // this.setState({focusFlag: false});
    }
  }


 

  componentWillUnmount() {
    // this.keyboardDidHideListener.remove();
    // this.keyboardDidShowListener.remove();
  }

  focusedSearch() {
    // this.props.callback();
    // this.props.popNavBack();
    this.pageNum = 2;
    this.isCurrentLoc = false;
    this.setState({refreshData:[], selectedRowID: -1, canLoadMore: true, text: "", city: "", instructor: "", citysearchstatus: false, instructorsearchstatus: false});
    this.searchListRef.scrollTo({x: 0, y: 0, animated: false});
  }

  endedSearch() {
  }

  OnCurrentMyLoc() {
    //poland lat: 52.129695, long: 18.830566
    //warsaw lat: 52.183616, long: 21.049805
    //berlin lat: 52.532514, long: 13.381348
    //new york lat: c, long: -75.256348
    //san fransisko lat: 37.800561, long: -122.409668
    //califonia lat: 36.645834, long: -119.838867
    //client lat: 40.7128, long: -74.0059
    Keyboard.dismiss();
    this.setState({ canLoadMore: true });
    if (this.apiPromise !== null) {
      this.apiPromise.cancel();
    }
    this.searchListRef.scrollTo({x: 0, y: 0, animated: false});

    this.isCurrentLoc = true;
    this.setState({showloading:true});

    navigator.geolocation.getCurrentPosition(
      (position) => {
        // var initialPosition = JSON.stringify(position);
        // this.setState({initialPosition});
        console.info(position.coords.longitude);
        console.info(position.coords.latitude);

        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        // this.latitude = "37.800561";
        // this.longitude = "-122.409668";

        fetch(AppConfig.apiUrl + '/api/InstructorSearchViewModels?lat='+position.coords.latitude
          +'&lng='+position.coords.longitude+'&miles=100&gender=Any&priceLevel=All&pageNum=1&numResults=20')
          .then((response) => response.json())
          .then((responseData) => {
            if (responseData.length > 0) {
              for (let i = 0; i < responseData.length; i++) {
                Image.getSize(`https://www.birdienow.com/img/instructor/profile/${responseData[i].instructorId}.jpg`, (width, height) => {
                  if (width == 0) {
                    responseData[i].checkImageFlag = false;
                  } else {
                    responseData[i].checkImageFlag = true;
                  }
                });
              }
              setTimeout(() => {
                this.setState({showloading:false,  data: responseData, focusFlag: false, text: 'current location', city: ""});
                AppConfig.searchResultDisplay = 'current location';
                AppConfig.data = responseData;
              }, 1000);

            }else{
              this.setState({showloading:false});
              Alert.alert('No Instructors found near your current position');
            }
          });
      },
      (error) => {
        this.setState({showloading:false});
        Alert.alert("Please check your GPS Network Status");
      },
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
    );
    this.watchID = navigator.geolocation.watchPosition((position) => {
      // var lastPosition = JSON.stringify(position);
      // this.setState({lastPosition});
    });
  }

  OnCurrentMyLoc_Check() {
    //poland lat: 52.129695, long: 18.830566
    //warsaw lat: 52.183616, long: 21.049805
    //berlin lat: 52.532514, long: 13.381348
    //new york lat: c, long: -75.256348
    //san fransisko lat: 37.800561, long: -122.409668
    //califonia lat: 36.645834, long: -119.838867
    //client lat: 40.7128, long: -74.0059
    Keyboard.dismiss();
    this.setState({ canLoadMore: true });
    if (this.apiPromise !== null) {
      this.apiPromise.cancel();
    }
    this.searchListRef.scrollTo({x: 0, y: 0, animated: false});

    this.isCurrentLoc = true;
    this.setState({showloading:true});

    navigator.geolocation.getCurrentPosition(
      (position) => {
        // var initialPosition = JSON.stringify(position);
        // this.setState({initialPosition});
        console.info(position.coords.longitude);
        console.info(position.coords.latitude);

        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        // this.latitude = "37.800561";
        // this.longitude = "-122.409668";

        fetch(AppConfig.apiUrl + '/api/InstructorSearchViewModels?lat='+position.coords.latitude
          +'&lng='+position.coords.longitude+'&miles=100&gender=Any&priceLevel=All&pageNum=1&numResults=20')
          .then((response) => response.json())
          .then((responseData) => {
            if (responseData.length > 0) {
              for (let i = 0; i < responseData.length; i++) {
                Image.getSize(`https://www.birdienow.com/img/instructor/profile/${responseData[i].instructorId}.jpg`, (width, height) => {
                  if (width == 0) {
                    responseData[i].checkImageFlag = false;
                  } else {
                    responseData[i].checkImageFlag = true;
                  }
                });
              }
              setTimeout(() => {
                this.setState({showloading:false,  data: responseData, focusFlag: false, text: 'current location', city: "", canLoadMore: true});
                AppConfig.searchResultDisplay = 'current location';
                AppConfig.data = responseData;
                this.pageNum = 2;
              }, 1000);
            }else{
              this.setState({showloading:false});
              Alert.alert('No Instructors found near your current position');
            }
          });
      },
      (error) => {
        this.setState({showloading:false});
        Alert.alert("Please check your GPS Network Status");
      },
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
    );
    this.watchID = navigator.geolocation.watchPosition((position) => {
      // var lastPosition = JSON.stringify(position);
      // this.setState({lastPosition});
    });
  }

  _onPressButton(rowData, flag){
    Keyboard.dismiss();
    this.setState({ canLoadMore: true });
    if (this.apiPromise !== null) {
      this.apiPromise.cancel();
    }
    if (flag) {
      if (this.state.isInstructor) { //instructor search
        AppConfig.insPGA = rowData.isPga;
        AppConfig.insID = rowData.id;
        if (!this.pushSceneFlag) {
          this.pushSceneFlag = true;
          this.props.pushNavScene(InstructorScene, {callback: this.CallBackInstructorScene});
        }
      } else {
        AppConfig.city = rowData.code;
        this.setState({showloading: true});
        fetch(AppConfig.apiUrl + '/api/InstructorSearchViewModels?zip='+rowData.code+'&'+
          'miles=100&'+
          'gender=Any&'+
          'priceLevel=All&'+
          'pageNum=1&'+
          'numResults=20')
          .then((response) => response.json())
          .then((responseData) => {
            if (responseData.length > 0) {
              // for (let i = 0; i < responseData.length; i++) {
              //   Image.getSize(`https://www.birdienow.com/img/instructor/profile/${responseData[i].instructorId}.jpg`, (width, height) => {
              //     if (width == 0) {
              //       responseData[i].checkImageFlag = false;
              //     } else {
              //       responseData[i].checkImageFlag = true;
              //     }
              //   });
              // }
              // setTimeout(() => {
                this.searchListRef.scrollTo({x: 0, y: 0, animated: false});
                AppConfig.data = responseData;
                this.setState({showloading: false, data: responseData, focusFlag: false, text: rowData.display});
              // }, 1000);
            }else{
              this.setState({showloading:false});
              Alert.alert('No Instructors found near ' + rowData.display);
            }
          })
      }
    }else {
      if (parseInt(rowData)) {
        AppConfig.city = rowData;
        this.setState({showloading: true});
        fetch(AppConfig.apiUrl + '/api/InstructorSearchViewModels?zip='+rowData+'&'+
          'miles=100&'+
          'gender=Any&'+
          'priceLevel=All&'+
          'pageNum=1&'+
          'numResults=20')
          .then((response) => response.json())
          .then((responseData) => {
            if (responseData.length > 0) {
              // for (let i = 0; i < responseData.length; i++) {
              //   Image.getSize(`https://www.birdienow.com/img/instructor/profile/${responseData[i].instructorId}.jpg`, (width, height) => {
              //     if (width == 0) {
              //       responseData[i].checkImageFlag = false;
              //     } else {
              //       responseData[i].checkImageFlag = true;
              //     }
              //   });
              // }
              // setTimeout(() => {
                this.searchListRef.scrollTo({x: 0, y: 0, animated: false});
                AppConfig.searchResultDisplay = rowData;
                AppConfig.data = responseData;
                this.setState({showloading: false, data: responseData, focusFlag: false, text: rowData.display});
              // }, 1000);
            }else{
              this.setState({showloading:false});
              Alert.alert('No Instructors found near ' + rowData);
            }
          })
      }
    }
  }

  _onPressButton_Instructor(rowData, flag){
    Keyboard.dismiss();
    this.setState({ canLoadMore: true });
    if (this.apiPromise !== null) {
      this.apiPromise.cancel();
    }
    if (flag) {
    }else {
      AppConfig.city = rowData;
      this.setState({showloading: true});
      fetch(AppConfig.apiUrl + '/api/InstructorSearchViewModels?find_ins='+rowData+'&'+
        'miles=100&'+
        'gender=Any&'+
        'priceLevel=All&'+
        'pageNum=1&'+
        'numResults=20')
        .then((response) => response.json())
        .then((responseData) => {
          if (responseData.length > 0) {
            for (let i = 0; i < responseData.length; i++) {
              Image.getSize(`https://www.birdienow.com/img/instructor/profile/${responseData[i].instructorId}.jpg`, (width, height) => {
                if (width == 0) {
                  responseData[i].checkImageFlag = false;
                } else {
                  responseData[i].checkImageFlag = true;
                }
              });
            }
            setTimeout(() => {
              AppConfig.searchResultDisplay = rowData;
              AppConfig.data = responseData;
              this.setState({showloading:false, data: responseData, focusFlag: false, text: rowData, city:""});
            }, 1000);
          }else{
            this.setState({showloading:false});
            Alert.alert('No Instructors found with the name ' + rowData);
          }
        });
    }
  }

  _loadMoreContentAsync(){
    this.setState({isInfiniting: true});
    if (this.isCurrentLoc) {
      this.apiPromise = MakeCancelable(fetch(AppConfig.apiUrl + '/api/InstructorSearchViewModels?lat='+this.latitude
        +'&lng='+this.longitude+'&miles=100&gender=Any&priceLevel=All&pageNum=' + this.pageNum + '&numResults=20'));

      this.apiPromise.promise
        .then((response) => response.json())
        .then((responseData) => {
          // for (let i = 0; i < responseData.length; i++) {
          //   Image.getSize(`https://www.birdienow.com/img/instructor/profile/${responseData[i].instructorId}.jpg`, (width, height) => {
          //     if (width == 0) {
          //       responseData[i].checkImageFlag = false;
          //     } else {
          //       responseData[i].checkImageFlag = true;
          //     }
          //   });
          // }
          // setTimeout(() => {
            console.log(this.pageNum);
            console.log(this.latitude);
            const array = this.state.data;
            if (responseData.length > 0) {
              for (let i = 0; i < responseData.length; i++) {
                array.push(responseData[i])
              }
            } else {
              this.setState({canLoadMore: false});
            }
            this.setState({data:array, isInfiniting: false});
          // }, 1000);
        })
        .catch(error => {
        });
    } else {
      if (this.state.isInstructor) {
        this.apiPromise = MakeCancelable(fetch(AppConfig.apiUrl + '/api/InstructorSearchViewModels?find_ins='+this.state.text+'&'+
          'miles=100&'+
          'gender=Any&'+
          'priceLevel=All&'+
          'pageNum=' + this.pageNum + '&'+
          'numResults=20'));

        this.apiPromise.promise
          .then((response) => response.json())
          .then((responseData) => {
            for (let i = 0; i < responseData.length; i++) {
              Image.getSize(`https://www.birdienow.com/img/instructor/profile/${responseData[i].instructorId}.jpg`, (width, height) => {
                if (width == 0) {
                  responseData[i].checkImageFlag = false;
                } else {
                  responseData[i].checkImageFlag = true;
                }
              });
            }
            setTimeout(() => {
              const array = this.state.data;
              if (responseData.length > 0) {
                for (let i = 0; i < responseData.length; i++) {
                  array.push(responseData[i])
                }
              } else {
                this.setState({canLoadMore: false});
              }
              this.setState({data:array, isInfiniting: false});
            }, 1000);
          })
          .catch(error => {
          });
      } else {
        this.apiPromise = MakeCancelable(fetch(AppConfig.apiUrl + '/api/InstructorSearchViewModels?zip='+AppConfig.city+'&'+
          'miles=100&'+
          'gender=Any&'+
          'priceLevel=All&'+
          'pageNum='+this.pageNum+'&'+
          'numResults=20'));

        this.apiPromise.promise
          .then((response) => response.json())
          .then((responseData) => {
            // for (let i = 0; i < responseData.length; i++) {
            //   Image.getSize(`https://www.birdienow.com/img/instructor/profile/${responseData[i].instructorId}.jpg`, (width, height) => {
            //     if (width == 0) {
            //       responseData[i].checkImageFlag = false;
            //     } else {
            //       responseData[i].checkImageFlag = true;
            //     }
            //   });
            // }
            // setTimeout(() => {
              const array = this.state.data;
              if (responseData.length > 0) {
                for (let i = 0; i < responseData.length; i++) {
                  array.push(responseData[i])
                }
              } else {
                this.setState({canLoadMore: false});
              }
              this.setState({data:array, isInfiniting: false});
            // }, 1000);
          })
          .catch(error => {
          });
      }
    }
    this.pageNum = this.pageNum+1;
  }

  _onRefresh() {
    this.setState({isRefreshing: true});
    let p = this.pageNum - 1;
    fetch(AppConfig.apiUrl + '/api/InstructorSearchViewModels?zip='+AppConfig.city+'&'+
      'miles=100&'+
      'gender=Any&'+
      'priceLevel=All&'+
      'pageNum='+p+'&'+
      'numResults=20')
      .then((response) => response.json())
      .then((responseData) => {
        // for (let i = 0; i < responseData.length; i++) {
        //   Image.getSize(`https://www.birdienow.com/img/instructor/profile/${responseData[i].instructorId}.jpg`, (width, height) => {
        //     if (width == 0) {
        //       responseData[i].checkImageFlag = false;
        //     } else {
        //       responseData[i].checkImageFlag = true;
        //     }
        //   });
        // }
        // setTimeout(() => {
          const array = this.state.data;
          this.setState({data:array, isRefreshing: false});
        // }, 1000);
      });
  }

  _loadMoreContentAsync_Refresh(){
    // console.log("on end reached 2");
    // this.setState({showloading: true});
    // fetch(AppConfig.apiUrl + '/api/InstructorSearchViewModels?zip='+AppConfig.city+'&'+
    //   'miles=100&'+
    //   'gender=Any&'+
    //   'priceLevel=All&'+
    //   'pageNum='+this.pageNum_Refresh+'&'+
    //   'numResults=20')
    //   .then((response) => response.json())
    //   .then((responseData) => {
    //     const array = this.state.refreshData;
    //     if (responseData.length > 0) {
    //       for (let i = 0; i < responseData.length; i++) {
    //         array.push(responseData[i])
    //       }
    //     }
    //     this.setState({refreshData:array, showloading: false});
    //   });
    // this.pageNum_Refresh = this.pageNum_Refresh + 1
  }

  onStarRatingPress(rating){
  }

  onPriceLevel(data){
    var PriceLevel = '';
    for (var i = 0; i < parseInt(data); i++) {
      PriceLevel = PriceLevel + '$'
    }
    return PriceLevel
  }

  onDistance(distance){
    var text = '';
    var anyString = 'Mozilla';
    text = parseFloat(distance).toString().substring(0,4);
    return text
  }

  onCancelCitySearch() {
    this.setState({refreshData:[], city: "", citysearchstatus: false});
  }

  onCancelInstructorSearch() {
    this.setState({refreshData:[], instructor: "", instructorsearchstatus: false});
  }

  onSherchResult(rowData, rowID){
    if (rowData.type === 'Loading') {
      return this.state.canLoadMore ? (<View style={ styles.loading }>
        <ActivityIndicator
          animating={ true }
          style={[ styles.loading ]}
          size="small"
          color="#000"
        />
      </View>) : null;
    }

    const render_image1 = [];

    // if (rowData.checkImageFlag) {
      render_image1.push(
        <Image
          key="render_location"
          style={{width: 70 * this.r_width, height: 70 * this.r_width, borderRadius:35 * this.r_width,marginLeft:5}}
          source={{uri: 'https://www.birdienow.com/img/instructor/profile/'+ rowData.instructorId +'.jpg'}}
        />
      );
    /*} else {
      render_image1.push(
        <Image
          key="render_location"
          style={{width: 70 * this.r_width, height: 70 * this.r_width, borderRadius:35 * this.r_width,marginLeft:5}}
          source={require('img/profile/profile.png')}
        />
      );
    }*/


    return(
      <TouchableOpacity
        onPress={()=>{
          this.onProfilePage(rowData.instructorId, rowData.isPga, rowID);
        }}
      >
        <View>
          <View  style={{height:93 * this.r_height, alignItems:'center', flexDirection:'row'}}>
            <View style={{ marginLeft:5,width: 75 * this.r_width, height: 75 * this.r_width}}>
              {render_image1}
              {rowData.isPga ?
                <Image
                  style={{width: 30, height: 30, position:'absolute',left:0,bottom:0}}
                  source={{uri: 'https://www.birdienow.com/img/badge/pga_badge_85.png'}}
                />:null}
            </View>
            <View style={{marginLeft:5,flex:1}}>
              <View style={{flexDirection: 'row'}}>
                <Text allowFontScaling={false} style={{flex:1, fontWeight: 'bold', fontFamily: 'Gotham-Book'}}>
                  {rowData.firstName} {rowData.lastName}
                </Text>
                <Text allowFontScaling={false} style={{marginRight:8 * this.r_width, fontSize: 11, fontFamily: 'Gotham-Book'}}>
                  {parseFloat(rowData.distance).toString().substring(0,4)} miles <Text allowFontScaling={false} style={{fontSize: 11, fontFamily: 'Gotham-Book',marginLeft:3 * this.r_width}}>{this.onPriceLevel(rowData.priceLevel)}</Text>
                </Text>
              </View>
              <View style={{flexDirection: 'row', alignItems:'center', marginTop:3 * this.r_width, marginBottom:3 * this.r_width}}>
                <StarRating
                  disabled={true}
                  emptyStar={'ios-star-outline'}
                  fullStar={'ios-star'}
                  halfStar={'ios-star-half'}
                  iconSet="Ionicons"
                  starColor = {'#E59F68'}
                  maxStars={5}
                  rating={rowData.averageRating}
                  selectedStar={(rating) => this.onStarRatingPress(rating)}
                  starSize = {10}
                  emptyStarColor={'blue'}
                />
                <Text allowFontScaling={false} style={{color: '#E59F68',marginLeft:5, fontSize: 11, fontFamily: 'Gotham-Book'}}>
                  {rowData.reviewCount} Reviews
                </Text>
              </View>
              <Text allowFontScaling={false} style={{color: '#000000', fontSize: 11, fontFamily: 'Gotham-Book'}}>
                {rowData.name}
              </Text>
              <Text allowFontScaling={false} style={{color: '#000000', fontSize: 11, fontFamily: 'Gotham-Book'}}>
                {rowData.address1} {rowData.address2} {rowData.city} {rowData.state} {rowData.zip}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  onProfilePage(id, isPGA, rowID){
    this.setState({selectedRowID: rowID});
    AppConfig.insID = id;
    AppConfig.insPGA = isPGA;
    setTimeout(() => {
      if (!this.pushSceneFlag) {
        this.pushSceneFlag = true;
        this.props.pushNavScene(InstructorScene, {callback: this.CallBackInstructorScene});
      }
    }, 100);
  }

  render() {
    const render_cancelsearch_city = [];
    if (this.state.citysearchstatus) {
      render_cancelsearch_city.push(
        <TouchableOpacity
          key="render_cancelsearch_city_result"
          style={{marginRight:5 * this.r_width, width:22 * this.r_height, height:22 * this.r_height}}
          onPress={() => {this.onCancelCitySearch();}}>
          <Image style={{marginTop: 3 * this.r_height, width:16 * this.r_height, height:16 * this.r_height}} source={require('img/searchimage/cancelsearch.png')}/>
        </TouchableOpacity>
      );
    }

    const render_cancelsearch_instructor = [];
    if (this.state.instructorsearchstatus) {
      render_cancelsearch_instructor.push(
        <TouchableOpacity
          key="render_cancelsearch_instructor_result"
          style={{marginRight:5 * this.r_width, width:22 * this.r_height, height:22 * this.r_height}}
          onPress={() => {this.onCancelInstructorSearch();}}>
          <Image style={{marginTop: 3 * this.r_height, width:16 * this.r_height, height:16 * this.r_height}} source={require('img/searchimage/cancelsearch.png')}/>
        </TouchableOpacity>
      );
    }

    

    return (
      <View style={styles.container}>
        <View style={{flex:1}}>
          <View style={{height:30 * this.r_height, alignItems:'center', backgroundColor:'#aac1ce'/*'#75CEF6'*/, borderBottomWidth:0, borderBottomColor:'#85D3DD'}}>
            <Text allowFontScaling={false} style={{marginTop:10 * this.r_height, fontSize: 13 * this.r_height, color: '#ffffff', fontFamily: 'Gotham-Bold'}}>
              Instructors
            </Text>
          </View>
          <View style={{height: 45 * this.r_height, backgroundColor:'#2FBC95', alignItems:'center', justifyContent: 'center'}}>
            <View style={{width:325 * this.r_width, height:25 * this.r_width, backgroundColor:'#ffffff',alignItems:'center', borderRadius:10,flexDirection: 'row'}}>
              <Image style={{marginLeft:5 * this.r_width,width:20 * this.r_height, height:20 * this.r_height}} source={require('img/image/search-location.png')}/>
              <TextInput
                underlineColorAndroid='transparent'
                placeholder="Enter text to see location"
                style={{height: 25 * this.r_width,flex:1, fontSize: 13, paddingVertical: 0}}
                onChangeText={(text) => this.setState({text})}
                onFocus={() => {this.focusedSearch();}}
                clearTextOnFocus={true}
                value={this.state.text}
                defaultValue={AppConfig.searchResultDisplay}
                onSubmitEditing={() => {this.endedSearch();}}
              />
            </View>
          </View>
          <View style={{height:this.height - 100*this.r_height - 50, backgroundColor:'#ffffff'}}>
            <ListView
              ref={(ref) => { this.searchListRef = ref; }}
              keyboardShouldPersistTaps="always"
              style={{}}
              dataSource={this.ds.cloneWithRows([...this.state.data, { type: 'Loading' }])}
              renderRow={(rowData, sectionID, rowID) =>this.onSherchResult(rowData, rowID)}
              refreshControl={
                <RefreshControl
                  refreshing={ this.state.isRefreshing }
                  onRefresh={ this._onRefresh }
                />
              }
              onEndReached={this._loadMoreContentAsync}
              enableEmptySections={true}
              automaticallyAdjustContentInsets={ false }
            />
          </View>
          <View style={{height:100}}/>
          {this.state.focusFlag ? arrayView : null}
          {this.state.showloading ?
            <View
              style={{position:'absolute', top: 0, left:0, bottom:0, right:0, alignItems:'center', justifyContent:'center', alignSelf: 'stretch', backgroundColor: 'transparent'}}
            >
              <ActivityIndicator
                animating={true}
                size="small"
                color="gray"
              />
            </View>:null}
        </View>
      </View>
    );
  }
}

export default RetiredSearchResultView;
