import React, { PropTypes, Component } from 'react';
import { Text, View, Image, StatusBar, Alert } from 'react-native';
import { default as Spinner } from 'react-native-spinkit';
import { default as styles} from './styles';
import { StartLoginView, MainScene, WriteReviewSceneNew } from 'AppScenes';
import { GlobalStorage, SignIn, GetAccountInfo, ReviewCheck, BNS_InitUser } from 'AppUtilities';
import AppConfig from 'AppConfig';

class SplashScene extends Component {
  static propTypes = {
    resetToScene: PropTypes.func.isRequired,
  };

  constructor(props, context) {
    super(props, context);
    this.flag = "false";
    this.autoUser = "";
    this.autoPass = "";
    this.OnAutoSignIn = this.OnAutoSignIn.bind(this);
    this.GetUserData = this.GetUserData.bind(this);
    this.CallBackWriteReviewScene = this.CallBackWriteReviewScene.bind(this);
    
   
  }

  async componentDidMount() {
    StatusBar.setHidden(true);
    try {
        
      this.token = await GlobalStorage.getItem("storage_token");
      
       
      AppConfig.global_userToken = this.token;
      if (AppConfig.global_userToken != null) {
        AppConfig.global_isLoggedIn = true;
        this.GetUserData();  //Will get user data and will reset globals if not authenticated
      }
      else {
        this.props.resetToScene(StartLoginView, null, "FadeAndroid");
      }
    }
    catch (error) {
      
      //TODO test this to see if necessary
      AppConfig.global_isLoggedIn = false;
      this.props.resetToScene(StartLoginView, null, "FadeAndroid");
    }
    setTimeout(() => {
      if (this.flag == "true") {
        //TODO Developer was checking online
        //this.OnAutoSignIn();
      } else {
        //this.props.resetToScene(StartLoginView, null, "FadeAndroid");
      }
    }, 2000);
    // try {
    //   this.flag = await GlobalStorage.getItem("storage_loggedIn");
    //   this.token = await GlobalStorage.getItem("storage_token");
    //   if (this.flag == "true") {
    //     this.autoUser = await GlobalStorage.getItem("storage_user");
    //     this.autoPass = await GlobalStorage.getItem("storage_password");
    //     // AppConfig.global_userToken = this.token;
    //   }
    // } catch (error) {
    //    
    // }

    // setTimeout(() => {
    //   if (this.flag == "true") {
    //     //TODO Developer was checking online
    //     //this.OnAutoSignIn();
    //   } else {
    //     this.props.resetToScene(StartLoginView, null, "FadeAndroid");
    //   }
    // }, 2000);
  }

  CallBackWriteReviewScene() {
    this.props.resetToScene(MainScene, null, "FadeAndroid");
  }

  GetUserData() {
    GetAccountInfo(AppConfig.global_userToken)
      .then((response) => {
       AppConfig.global_userSkillLevel = response.userSkillLevelId;
       
        if (response) { 
          //Logged in user - check if they have appoinment to write review
          ReviewCheck(AppConfig.global_userToken)
            .then((response) => {
              this.props.resetToScene(WriteReviewSceneNew, {ref:(c) => { this.refWriteReviewSceneNew = c; }, passProps: {source: 'SplashScene', appointment: response}, callback: this.CallBackWriteReviewScene}, "FadeAndroid");
            }).catch(error =>
            {
              this.props.resetToScene(MainScene, null, "FadeAndroid");
            });

           
          //Logged in user - go to home page  
          //this.props.resetToScene(MainScene, null, "FadeAndroid");


          // var method = 'GET';
          // let header;
          // header = {
          //   method,
          //   headers: {
          //     'Authorization': "Bearer " + AppConfig.global_userToken,
          //     Accept: 'application/json',
          //     'Content-Type': 'application/x-www-form-urlencoded',
          //   },
          // };
          // fetch(AppConfig.apiUrl + '/api/reviewCheck', header)
          //   .then((response) => {
          //     debugger;
          //     response.json()
          //   }
          //   )
          //   .then((responseData) => {
          //     debugger;
          //   })
          //   .catch(error =>
          //   {
          //     debugger;
          //     alert('review check ERROR');
          //   });
         
             
        } else {
             
          AppConfig.global_userToken = null;
          AppConfig.global_isLoggedIn = false;
          GlobalStorage.setItem("storage_token", '');
           
          this.props.resetToScene(StartLoginView, null, "FadeAndroid");
        }
      })
      .catch(error => {
           
        //User has invalid token, clear global properties
          AppConfig.global_userToken = null;
          AppConfig.global_isLoggedIn = false;
          //GlobalStorage.setItem("storage_user", null);
          //GlobalStorage.setItem("storage_password", null);
          //GlobalStorage.setItem("storage_loggedIn", null);
          GlobalStorage.setItem("storage_token", '');
          this.props.resetToScene(StartLoginView, null, "FadeAndroid");
          //Alert.alert(error.message);
        
      });
  }

  OnAutoSignIn(){
    this.setState({ isLoading: true });
    SignIn(this.grant_type, this.autoUser, this.autoPass)
      .then((response) => {
        if (!response.error_description) { //success 200
          // Alert.alert(response.access_token);
          AppConfig.global_userToken = response.access_token; // set the user token
          this.setState({ isLoading: false });
          this.props.resetToScene(MainScene, null, "FadeAndroid");
        } else { // failed 400
          // Alert.alert("error1");
          Alert.alert(response.error_description);
          this.setState({ isLoading: false });
        }
      })
      .catch(error => {
        // Alert.alert("error2");
        Alert.alert(error.message);
        this.setState({ isLoading: false });
      });
  }

  render() {
    return (
      <View style={{flex: 1, flexDirection: "column"}}>
        <Image
          style={styles.back}
          source={require('img/image/ui_back.png')}
        />
        <View style={{flex: 1}}/>
        <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
          <Image
            style={styles.splash}
            source={require('img/image/logo_whiteface.png')}
          />
          <Spinner style={styles.spinner} type="FadingCircleAlt" color="#4c616f" />
        </View>
        <View style={{flex: 1}}/>
      </View>
    );
  }
}

export default SplashScene;
