import { StyleSheet } from 'react-native';
import Dimensions from 'Dimensions';

const window = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  background: {
    resizeMode: 'contain',
    marginVertical: 50,
  },
  splash: {
    width: window.width - 50,
    height: (window.width - 150) * 51 / 160,
    marginBottom: 20
  },
  back: {
    position: "absolute",
    width: window.width,
    height: window.height
  }
});
