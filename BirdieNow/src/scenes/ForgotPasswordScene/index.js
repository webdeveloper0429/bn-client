import React, { PropTypes, Component } from 'react';
import { ActivityIndicator, Alert, Platform, Keyboard, TextInput, View, StatusBar, Text, TouchableOpacity } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styles from './styles';
import _, { isEqual } from 'lodash';
import { MainScene, ProfileView } from 'AppScenes';
import { ForgotPassword } from 'AppUtilities';
import AppConfig from 'AppConfig';

class ForgotPasswordScene extends Component {
  static propTypes = {
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      isLoading: false,
      email: ''
    };

    this.OnSubmit = this.OnSubmit.bind(this);
  }

  OnBack() {
    //This page is used in two places, one inside Main Tabs and one in start, therefore two different calls.
    if (this.props.passProps.source == 'profile')
        this.props.popNavBack();
      else
        this.props.popBack();
  }

  OnSubmit(){
    Keyboard.dismiss();
    
    if (this.state.email == '') {
      Alert.alert("Email is required");
      return
    }

    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(this.state.email)) {
      Alert.alert("Email address is not valid");
      return
    }

    this.setState({ isLoading: true });
  
    let data = {
      hostName: "www.birdienow.com",
      email: this.state.email
    };

    ForgotPassword(data)
      .then((response) => {
        debugger;
        if (response.status == 200) {
          if (this.props.passProps.source == 'profile') {
            Alert.alert("Your password reset link was sent to your email address.")
            AppConfig.global_isLoggedIn = false;
            this.props.popNavBack();
          }
          else {
            Alert.alert("Your password reset link was sent to your email address.")
            AppConfig.global_isLoggedIn = false;
            this.props.popBack();
            this.props.resetToScene(MainScene);
          }
        } else {
          Alert.alert("The email address you provided is not recognized.");
        }
        this.setState({ isLoading: false });
      })
      .catch(error => {
        debugger;
        Alert.alert(error.message);
        this.setState({ isLoading: false });
      });
  }

  render() {
    const render_TopBar = (
      <View style={{
        flexDirection: 'row', alignItems: "center", justifyContent: "center",
        backgroundColor: '#fafafa', borderBottomWidth: 1, borderBottomColor: '#D3D3D3',
        height: (Platform.OS === 'ios') ? 65 : 40 }}>

        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={() => { this.OnBack(); }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#000',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Back</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 2, justifyContent: "center" }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#000',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Forgot Password</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={() => { this.OnSubmit(); }} >
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#32b3fa',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Reset</Text>
        </TouchableOpacity>
      </View>
    );

    return (
    <View style={styles.container}>
        {render_TopBar}
        <KeyboardAwareScrollView
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={true} >
            <View>
                <View style={[styles.inputWrapper, {}]}>
                    <TextInput
                    underlineColorAndroid='transparent'
                    style={styles.inputEmail}
                    placeholder="Your Email Address"
                    placeholderTextColor="#999"
                    keyboardType='email-address'
                    autoCapitalize="none"
                    onChangeText={(text) => {this.state.email = text}} />
                </View>
                <View style={{ paddingTop: 20, paddingLeft: window.width * 0.05  }}>
                    <TouchableOpacity
                    onPress={() => { this.OnSubmit(); }}
                    style={{ width: window.width * 0.9, height: 35, borderRadius: 5, backgroundColor: '#32b3fa', justifyContent: 'center' }}>
                    <Text style={{ alignSelf: 'center', fontSize: 16, fontWeight: 'bold', color: '#ffffff', fontFamily: 'Gotham-Book' }}>Reset</Text>
                    </TouchableOpacity>
                </View>      
            </View>
        </KeyboardAwareScrollView>

        {this.state.isLoading ?
          <View style={styles.loadingScene}>
            <ActivityIndicator
              animating={true}
              size="small"
              color="white"
            />
          </View> : null}      
      </View>
    );
  }
}

export default ForgotPasswordScene;
