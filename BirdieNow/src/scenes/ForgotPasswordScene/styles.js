import { StyleSheet } from 'react-native';
import Dimensions from 'Dimensions';

const window = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    backgroundColor: '#F9F9F9',
    paddingBottom: 0,
  }, 
  inputWrapper: {
    backgroundColor: '#ffffff',
    borderColor: '#f4f4f4',
    borderWidth: 0,
    borderBottomWidth: 1,
  },
  inputEmail: {
    height: 40,
    fontSize: 13,
    backgroundColor: '#ffffff',
    borderWidth: 0,
    paddingVertical: 0,
    paddingHorizontal: 0,
    paddingLeft: 10
  },
  loadingScene: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: window.width,
    height: window.height,
    alignSelf: 'stretch',
    backgroundColor: 'rgba(0,0,0,0.5)',
    alignItems: 'center',
    justifyContent: 'center',
  }
});
