import React, { Component, PropTypes } from 'react';
import { Dimensions, ActivityIndicator, Alert, View, Image, TouchableOpacity, Text, ListView, RefreshControl, ScrollView, TextInput, Platform} from 'react-native';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import StarRating from 'react-native-star-rating';
import { CheckboxField, Checkbox } from 'react-native-checkbox-field';
import styles from './styles';
import AppConfig from 'AppConfig';
import AppStyles from 'AppStyles';
import { SearchBar, HeaderBar } from 'AppComponents';
import { FilterScene, ListDetailScene, } from 'AppScenes';
import { RequestApi, MakeCancelable, GlobalStorage } from 'AppUtilities';
import _, { isEqual } from 'lodash';
import moment from 'moment';

class WriteReviewScene extends Component {
  static propTypes = {
    popNavBack: PropTypes.func.isRequired,
    callback: PropTypes.func.isRequired,
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      showloading: false,
      warning_state: 0,
      star_state: 0,
      loadFinished: false,
      data: [],
      image: {},
      radioOptionType: [{label: 'Beginner', value: 0}, {label: 'Bogey Player', value: 1}, {
        label: 'Single Handicap',
        value: 2
      }, {label: 'Scratch Player', value: 3}],
      value1: 0,
      value1Index: 0,
      starCount: 0,
      checkBox1Selected: false,
      checkBox1Label: 'Setup',
      checkBox2Selected: false,
      checkBox2Label: 'Irons',
      checkBox3Selected: false,
      checkBox3Label: 'Driver',
      checkBox4Selected: false,
      checkBox4Label: 'Chipping',
      checkBox5Selected: false,
      checkBox5Label: 'Putting',
      checkBox6Selected: false,
      checkBox6Label: 'Pitch Shots',
      checkBox7Selected: false,
      checkBox7Label: 'Sand Shots',
      currentText: ''
    };

    const { width, height } = Dimensions.get('window');
    this.width = width;
    this.height = height;
    this.r_width =  width / 1125;
    this.r_height = height / 1659;
    this.comment = "";


    //functions
    this.onStarRatingPress = this.onStarRatingPress.bind(this);
    this.selectCheckbox1 = this.selectCheckbox1.bind(this);
    this.selectCheckbox2 = this.selectCheckbox2.bind(this);
    this.selectCheckbox3 = this.selectCheckbox3.bind(this);
    this.selectCheckbox4 = this.selectCheckbox4.bind(this);
    this.selectCheckbox5 = this.selectCheckbox5.bind(this);
    this.selectCheckbox6 = this.selectCheckbox6.bind(this);
    this.selectCheckbox7 = this.selectCheckbox7.bind(this);
    this.onChangeTextField = this.onChangeTextField.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.SubmitSuccess = ::this.SubmitSuccess;
  }

  componentDidMount() {
    this.onChangeTextField("");
    this.setState({showloading: true, loadFinished: false});

    fetch(AppConfig.apiUrl + '/api/Appointments/?id=' + this.props.passProps.appointment.id + '&auth=auth', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${AppConfig.global_userToken}`,
      }
    })
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({showloading: false, loadFinished: true, data: responseData, image: { uri: 'https://www.birdienow.com/img/instructor/profile/' + responseData.instructorId + '.jpg' }});
      })
      .catch(error => {
        Alert.alert(error.message);
        this.setState({showloading: false});
      });
  }

  onError(error){
   this.setState({ image: require('img/profile/profile.png')})
  }

  onSubmit() {
    let checkCount = 0;
    let checkArray = [];
    if (this.state.checkBox1Selected) {
      checkCount++;
      checkArray.push("1");
    }
    if (this.state.checkBox2Selected) {
      checkCount++;
      checkArray.push("2");
    }
    if (this.state.checkBox3Selected) {
      checkCount++;
      checkArray.push("3");
    }
    if (this.state.checkBox4Selected) {
      checkCount++;
      checkArray.push("4");
    }
    if (this.state.checkBox5Selected) {
      checkCount++;
      checkArray.push("5");
    }
    if (this.state.checkBox6Selected) {
      checkCount++;
      checkArray.push("6");
    }
    if (this.state.checkBox7Selected) {
      checkCount++;
      checkArray.push("7");
    }

    if (checkCount == 0) {
      if (this.state.starCount == 0) {
        this.setState({star_state: -1, warning_state: 1});
      } else {
        this.setState({star_state: 0, warning_state: 1});
      }
    } else if (checkCount > 2) {
      if (this.state.starCount == 0) {
        this.setState({star_state: -1, warning_state: 2});
      } else {
        this.setState({star_state: 0, warning_state: 2});
      }
    } else {
      if (this.state.starCount == 0) {
        this.setState({star_state: -1, warning_state: 0});
      } else {
        this.setState({star_state: 0, warning_state: 0});
        let requestParams = JSON.stringify({
          appointmentId: this.state.data.id,
          instructorId: this.state.data.instructorId,
          overallRating: this.state.starCount,
          userSkillLevelId: this.state.value1 + 1,
          reviewLessonTypeIds: checkArray,
          statement: this.state.currentText
        });
        this.setState({showloading: true});

        fetch(AppConfig.apiUrl + '/api/Reviews', {
          method: 'POST',
          headers: {
            Authorization: `Bearer ${AppConfig.global_userToken}`,
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          body: requestParams
        })
          .then((response) => response)
          .then((responseData) => {
            Alert.alert(
              'CONFIRMATION',
              'Your review has been submitted. Thank you.',
              [
                {text: 'OK', onPress: () => {this.SubmitSuccess();}},
              ],
              { cancelable: false }
            );
            this.setState({showloading: false});
          })
          .catch(error => {
            Alert.alert(error.message);
            this.setState({showloading: false});
          });
      }
    }
  }

  SubmitSuccess() {
    this.props.callback();
    this.props.popNavBack();
  }

  selectCheckbox1() {
    this.setState({
      checkBox1Selected: !this.state.checkBox1Selected
    });
  }

  selectCheckbox2() {
    this.setState({
      checkBox2Selected: !this.state.checkBox2Selected
    });
  }

  selectCheckbox3() {
    this.setState({
      checkBox3Selected: !this.state.checkBox3Selected
    });
  }

  selectCheckbox4() {
    this.setState({
      checkBox4Selected: !this.state.checkBox4Selected
    });
  }

  selectCheckbox5() {
    this.setState({
      checkBox5Selected: !this.state.checkBox5Selected
    });
  }

  selectCheckbox6() {
    this.setState({
      checkBox6Selected: !this.state.checkBox6Selected
    });
  }

  selectCheckbox7() {
    this.setState({
      checkBox7Selected: !this.state.checkBox7Selected
    });
  }

  onChangeTextField(text) {
    this.setState({
      currentText: text
    });
  }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating
    });
  }

  render() {
    let render_warning_star = [];
    if (this.state.star_state == -1) {
      render_warning_star.push(
        <Text key="render_warning" allowFontScaling={false} style={{fontSize: 17, color: "red", marginLeft: 20, marginRight: 20, marginTop: 20, fontFamily: 'Gotham-Bold'}}>Overall rating is required</Text>
      );
    }

    let render_warning = [];
    if (this.state.warning_state == 1) {
      render_warning.push(
        <Text key="render_warning" allowFontScaling={false} style={{fontSize: 17, color: "red", marginLeft: 20, marginRight: 20, marginTop: 20, fontFamily: 'Gotham-Bold'}}>Select at least one</Text>
      );
    } else if (this.state.warning_state == 2) {
      render_warning.push(
        <Text key="render_warning" allowFontScaling={false} style={{fontSize: 17, color: "red", marginLeft: 20, marginRight: 20, marginTop: 20, fontFamily: 'Gotham-Bold'}}>Select no more than two</Text>
      );
    } 
    
    const defaultColor = '#fff';
    const startDate = moment(this.state.data.startDate), endDate = moment(this.state.data.endDate);
    let strDate;

    if (this.state.loadFinished) {
      if (startDate.isSame(endDate, 'day')) {
        strDate = `${startDate.format('LLLL')} - ${endDate.format('h:mm A')}`;
      } else {
        strDate = `${startDate.format('LLLL')} - ${endDate.format('LLLL')}`;
      }
    } else {
      strDate = "";
    }

    const render_TopBar = (
      <View style={{
        flexDirection: 'row', alignItems: "center", justifyContent: "center",
        backgroundColor: '#fafafa', borderBottomWidth: 1, borderBottomColor: '#D3D3D3',
        height: (Platform.OS === 'ios') ? 65 : 40 }}>

        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={() => { this.props.popNavBack(); }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#000',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Back</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 2, justifyContent: "center" }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#000',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Review</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={this.onSubmit} >
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#32b3fa',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Submit</Text>
        </TouchableOpacity>
      </View>
    );

    let render_ImageBar = [];
    render_ImageBar = (
      <Image source={require('img/image/ui_back_blue.png')} style={{ alignItems: 'center', justifyContent: 'center', height: 200, width }}>
        <Text allowFontScaling={false} style={{ fontSize: 19, color: '#4b5b64', fontFamily: 'Gotham-Medium', backgroundColor: 'transparent', marginBottom: 10 }}>
          {this.state.data.instructorFirstName} {this.state.data.instructorLastName}
        </Text>
        <View>
          <Image key="render_image"
            style={{ width: 246 * this.r_width, height: 246 * this.r_width, borderRadius: 123 * this.r_width, marginLeft: 36 * this.r_width }}
            source={ this.state.image  }
            onError={ this.onError.bind(this) }
            />    
        </View>
        <Text allowFontScaling={false} style={{
          fontSize: 13, color: '#4b5b64', fontFamily: 'Gotham-Medium', backgroundColor: 'transparent',
          marginTop: 10, textAlign: 'center' 
        }}>
          {strDate}
        </Text>
      </Image>
    );
    


    return (
      <View style={styles.container}>
        {render_TopBar}
        <ScrollView>
        {render_ImageBar}
          <Text allowFontScaling={false} style={{fontSize: 12, color: "black", marginLeft: 20, marginTop: 15, fontFamily: 'Gotham-Bold'}}>YOUR SKILL LEVEL</Text>
          <View style={{flexDirection: 'row', marginLeft: 20, marginTop: 10}}>
            <RadioForm
              ref="radioForm"
              radio_props={this.state.radioOptionType}
              initial={0}
              formHorizontal={false}
              labelHorizontal={true}
              buttonColor={'#ddd'}
              labelColor={'#000'}
              labelStyle={{fontSize: 11}}
              animation={true}
              onPress={(value, index) => {
              this.setState({
                value1:value,
                value1Index:index
              })
            }}
            />
          </View>

          <Text allowFontScaling={false} style={{fontSize: 12, color: "black", marginLeft: 20, 
          marginRight: 20, marginTop: 20, fontFamily: 'Gotham-Bold'}}>1. HOW WAS THE LESSON?</Text>
          <View style={{marginTop: 5}}>
            <View style={{}}/>
            <View style={{flex:1, alignItems: 'stretch', justifyContent: 'space-between', paddingLeft:20, paddingRight: 20}}>
              <StarRating
                disabled={false}
                emptyStar={'ios-star-outline'}
                fullStar={'ios-star'}
                halfStar={'ios-star-half'}
                iconSet="Ionicons"
                maxStars={5}
                rating={this.state.starCount}
                selectedStar={(rating) => this.onStarRatingPress(rating)}
                starColor={'#ddd'}
                emptyStarColor={'#ddd'}
                starSize={60}
              />
            </View>
            <View style={{flex: 1.5}}/>
          </View>
          {render_warning_star}
          <Text allowFontScaling={false} style={{fontSize: 12, color: "black", marginLeft: 20, 
          marginTop: 30, marginRight: 20, fontFamily: 'Gotham-Bold'}}>2. WHAT WAS THE FOCUS ON THE LESSON? SELECT UP TO TWO.</Text>
          <View style={{flexDirection: 'row', height: 30}}>
            <View style={{flex: 1, justifyContent: "center"}}>
              <CheckboxField
                label={this.state.checkBox1Label}
                onSelect={this.selectCheckbox1}
                disabled={this.props.disabled}
                disabledColor='rgb(236,236,236)'
                selected={this.state.checkBox1Selected}
                defaultColor={defaultColor}
                selectedColor="#247fd2"
                containerStyle={styles.containerStyle}
                labelStyle={styles.labelStyle}
                checkboxStyle={styles.checkboxStyle}
                labelSide="left">
              </CheckboxField>
            </View>
            <View style={{flex: 2}}>
              <Text allowFontScaling={false} style={{textAlign: "center", marginTop: 10, fontFamily: 'Gotham-Book'}}>{this.state.checkBox1Label}</Text>
            </View>
            <View style={{flex: 1}}/>
            <View style={{flex: 1, justifyContent: "center"}}>
              <CheckboxField
                label={this.state.checkBox2Label}
                onSelect={this.selectCheckbox2}
                disabled={this.props.disabled}
                disabledColor='rgb(236,236,236)'
                selected={this.state.checkBox2Selected}
                defaultColor={defaultColor}
                selectedColor="#247fd2"
                containerStyle={styles.containerStyle}
                labelStyle={styles.labelStyle}
                checkboxStyle={styles.checkboxStyle}
                labelSide="left">
              </CheckboxField>
            </View>
            <View style={{flex: 2}}>
              <Text allowFontScaling={false} style={{textAlign: "center", marginTop: 10, fontFamily: 'Gotham-Book'}}>{this.state.checkBox2Label}</Text>
            </View>
            <View style={{flex: 1}}/>
          </View>

          <View style={{flexDirection: 'row', height: 30}}>
            <View style={{flex: 1, justifyContent: "center"}}>
              <CheckboxField
                label={this.state.checkBox3Label}
                onSelect={this.selectCheckbox3}
                disabled={this.props.disabled}
                disabledColor='rgb(236,236,236)'
                selected={this.state.checkBox3Selected}
                defaultColor={defaultColor}
                selectedColor="#247fd2"
                containerStyle={styles.containerStyle}
                labelStyle={styles.labelStyle}
                checkboxStyle={styles.checkboxStyle}
                labelSide="left">
              </CheckboxField>
            </View>
            <View style={{flex: 2}}>
              <Text allowFontScaling={false} style={{textAlign: "center", marginTop: 10, fontFamily: 'Gotham-Book'}}>{this.state.checkBox3Label}</Text>
            </View>
            <View style={{flex: 1}}/>
            <View style={{flex: 1, justifyContent: "center"}}>
              <CheckboxField
                label={this.state.checkBox4Label}
                onSelect={this.selectCheckbox4}
                disabled={this.props.disabled}
                disabledColor='rgb(236,236,236)'
                selected={this.state.checkBox4Selected}
                defaultColor={defaultColor}
                selectedColor="#247fd2"
                containerStyle={styles.containerStyle}
                labelStyle={styles.labelStyle}
                checkboxStyle={styles.checkboxStyle}
                labelSide="left">
              </CheckboxField>
            </View>
            <View style={{flex: 2}}>
              <Text allowFontScaling={false} style={{textAlign: "center", marginTop: 10, fontFamily: 'Gotham-Book'}}>{this.state.checkBox4Label}</Text>
            </View>
            <View style={{flex: 1}}/>
          </View>

          <View style={{flexDirection: 'row', height: 30}}>
            <View style={{flex: 1, justifyContent: "center"}}>
              <CheckboxField
                label={this.state.checkBox5Label}
                onSelect={this.selectCheckbox5}
                disabled={this.props.disabled}
                disabledColor='rgb(236,236,236)'
                selected={this.state.checkBox5Selected}
                defaultColor={defaultColor}
                selectedColor="#247fd2"
                containerStyle={styles.containerStyle}
                labelStyle={styles.labelStyle}
                checkboxStyle={styles.checkboxStyle}
                labelSide="left">
              </CheckboxField>
            </View>
            <View style={{flex: 2}}>
              <Text allowFontScaling={false} style={{textAlign: "center", marginTop: 11, fontFamily: 'Gotham-Book'}}>{this.state.checkBox5Label}</Text>
            </View>
            <View style={{flex: 1}}/>
            <View style={{flex: 1, justifyContent: "center"}}>
              <CheckboxField
                label={this.state.checkBox6Label}
                onSelect={this.selectCheckbox6}
                disabled={this.props.disabled}
                disabledColor='rgb(236,236,236)'
                selected={this.state.checkBox6Selected}
                defaultColor={defaultColor}
                selectedColor="#247fd2"
                containerStyle={styles.containerStyle}
                labelStyle={styles.labelStyle}
                checkboxStyle={styles.checkboxStyle}
                labelSide="left">
              </CheckboxField>
            </View>
            <View style={{flex: 2}}>
              <Text allowFontScaling={false} style={{textAlign: "center", marginTop: 11, fontFamily: 'Gotham-Book'}}>{this.state.checkBox6Label}</Text>
            </View>
            <View style={{flex: 1}}/>
          </View>

          <View style={{flexDirection: 'row', height: 30}}>
            <View style={{flex: 1, justifyContent: "center"}}>
              <CheckboxField
                label={this.state.checkBox7Label}
                onSelect={this.selectCheckbox7}
                disabled={this.props.disabled}
                disabledColor='rgb(236,236,236)'
                selected={this.state.checkBox7Selected}
                defaultColor={defaultColor}
                selectedColor="#247fd2"
                containerStyle={styles.containerStyle}
                labelStyle={styles.labelStyle}
                checkboxStyle={styles.checkboxStyle}
                labelSide="left">
              </CheckboxField>
            </View>
            <View style={{flex: 2}}>
              <Text allowFontScaling={false} style={{textAlign: "center", marginTop: 12, fontFamily: 'Gotham-Book'}}>{this.state.checkBox7Label}</Text>
            </View>
            <View style={{flex: 5}}/>
          </View>
          { render_warning }
          {/*<Text allowFontScaling={false} style={{fontSize: 12, color: "black", marginLeft: 20, 
          marginRight: 20, marginTop: 20, fontFamily: 'Gotham-Bold'}}>3. IS THERE ANYTHING ELSE ABOUT THE LESSON THAT A FELLOW GOLFER WOULD FIND HELPFUL?</Text>*/}
          <TextInput
            underlineColorAndroid='transparent'
            style={{marginLeft: 10, marginRight: 10, marginTop: 20, height: 300, padding: 5, textAlignVertical: 'top', borderWidth: 0, fontFamily: 'Gotham-Book'}}
            placeholder="Describe your lesson experience..."
            multiline = {true}
            numberOfLines = {100}
            onChangeText={(text) => this.onChangeTextField(text)}
          />

          {/*<TouchableOpacity
            style={{width: 100, height: 40, marginLeft: 20, marginTop: 10, justifyContent: "center", backgroundColor: "lightgray", borderRadius: 3}}
            onPress={() => {this.onSubmit();}}
          >
            <Text allowFontScaling={false} style={{textAlign: "center", fontFamily: 'Gotham-Book'}}>SUBMIT</Text>
          </TouchableOpacity>*/}
          <View style={{height:30}}/>
        </ScrollView>
        {this.state.showloading ?
          <View
            style={{position:'absolute', top: 0, left:0, bottom:0, right:0, alignItems:'center', justifyContent:'center', alignSelf: 'stretch', backgroundColor: 'transparent'}}
          >
            <ActivityIndicator
              animating={true}
              size="small"
              color="gray"
            />
          </View>:null}

      </View>

    );
  }
}

export default WriteReviewScene;
