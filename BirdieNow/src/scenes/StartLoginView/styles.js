import { StyleSheet } from 'react-native';
import Dimensions from 'Dimensions';

const window = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    paddingTop: window.height * .05,
    paddingHorizontal: window.width * .05,
    backgroundColor: "#aac1ce",
    justifyContent: 'center',
    alignItems: 'center',
    // paddingHorizontal: 30,
    // paddingBottom: 70
  },
  topLabel: {
    alignSelf: 'stretch',
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  inputEmail: {
    marginTop: 10,
    justifyContent: 'center',
    //alignSelf: 'stretch',
    height: 40,
    width: window.width * 0.9,
    backgroundColor: '#ffffff',
    paddingHorizontal: 10,
    // borderWidth: 1,
    // borderColor: '#008000',
    borderRadius: 5,
  },
  forgotLabel: {
    marginTop: 5,
    color: '#fff',
    fontSize: 15,
    alignSelf: 'stretch'
  },
  btnLogin: {
    color: '#868788',
    fontFamily: 'Gotham-Bold',
    fontWeight: 'bold',
    fontSize: 15,
    width: 305,
    textAlign: 'center', 
    borderWidth: 0,
    backgroundColor: '#ffffff',
    borderColor: '#fff',
    borderRadius: 5,    
    paddingTop: 8,
    paddingBottom: 7,
  },
  btnRegister: {
    color: '#ffffff',
    fontFamily: 'Gotham-Bold',
    fontWeight: 'bold',
    fontSize: 15,
    width: 305,
    textAlign: 'center', 
    borderWidth: 0,
    backgroundColor: '#868788',
    borderColor: '#fff',
    borderRadius: 5,   
    paddingTop: 8,
    paddingBottom: 7,
  },
  loadingScene: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: window.width,
    height: window.height,
    alignSelf: 'stretch',
    backgroundColor: 'rgba(0,0,0,0.5)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  splash: {
    width: window.width - 50,
    height: (window.width - 150) * 51 / 160,
    marginBottom: 20
  },
});
