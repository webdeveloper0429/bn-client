import React, { PropTypes, Component } from 'react';
import { ActivityIndicator, Alert, Image, TextInput, View, StatusBar, Text,TouchableOpacity, TouchableHighlight, Keyboard } from 'react-native';
import styles from './styles';
import _, { isEqual } from 'lodash';
import { StartRegisterViewRetired, RegisterNavScene, SplashScene, MainScene, ForgotPasswordScene } from 'AppScenes';
import { SignIn, GlobalStorage, GetAccountInfo } from 'AppUtilities';
import AppConfig from 'AppConfig';


class StartLoginView extends Component {
  static propTypes = {
    pushScene: PropTypes.func.isRequired,
    resetToScene: PropTypes.func.isRequired,
  };

  constructor(props, context) {
    super(props, context);

    this.state = {
      isLoading: false,
    };

    StatusBar.setHidden(false);
    this.OnSignIn = this.OnSignIn.bind(this);
    this.grant_type = 'password';
    this.username = "";
    this.password = "";
  }

  OnSignIn(){
    Keyboard.dismiss();
    this.setState({ isLoading: true });
    SignIn(this.grant_type, this.username, this.password)
      .then((response) => {
       
        if (!response.error_description) { //success 200
          GlobalStorage.setItem("storage_user", this.username);
          GlobalStorage.setItem("storage_password", this.password);
          GlobalStorage.setItem("storage_loggedIn", "true");
          GlobalStorage.setItem("storage_token", response.access_token);
          AppConfig.global_userToken = response.access_token; // set the user token
          this.setState({ isLoading: false });
          this.props.resetToScene(MainScene);
        } else { // failed 400
          Alert.alert(response.error_description);
          this.setState({ isLoading: false });
        }
      })
      .catch(error => {
        Alert.alert(error.message);
        this.setState({ isLoading: false });
      });
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "#aac1ce" }}>
        


        <View style={styles.container}>

          <View style={{ alignSelf: "flex-end", borderRadius: 20, backgroundColor: '#F9F9F9' }}>
            <TouchableHighlight
              onPress={() => {
                AppConfig.global_isLoggedIn = false;
                this.props.resetToScene(MainScene);
              }} >
              <Text allowFontScaling={false} style={{ fontSize: 19, fontWeight: 'bold', color: '#868788', paddingTop: 3, 
              paddingBottom: 5, paddingLeft: 12, paddingRight: 12 }}>x</Text>
            </TouchableHighlight>
          </View>


          <Image
            style={styles.splash}
            source={require('img/image/logo_whiteface.png')}
          />

          <View style={{ alignItems: 'center' }}>


          <Text allowFontScaling={false} style={styles.topLabel}>ALREADY A MEMBER?</Text>
          <TextInput
            style={styles.inputEmail}
            fontSize={17}
            placeholder="Email"
            defaultValue=""
            placeholderTextColor="#999"
            onChangeText={(text) => { this.username = text }}
            underlineColorAndroid='transparent'
            keyboardType='email-address'
            autoCapitalize="none"
            autoCorrect={false}
          //value={this.username}
          />
          <TextInput
            style={styles.inputEmail}
            fontSize={17}
            placeholder="Password"
            placeholderTextColor="#999"
            defaultValue=""
            secureTextEntry={true}
            onChangeText={(text) => { this.password = text }}
            underlineColorAndroid='transparent'
             //value={this.password}
          />
          {/*<Text allowFontScaling={false} style={styles.forgotLabel}>Forgot your password?</Text>*/}

          <View style={{ flexDirection: 'row', paddingRight: window.width * 0.05, justifyContent:"flex-end",  alignItems: "flex-end" }}>
            <TouchableOpacity
              onPress={() => { this.props.pushScene(ForgotPasswordScene, {passProps: { source: 'start' }} ) }}
            underlayColor="#bde7ff"
            style={{ flex: 1, marginTop: 10, justifyContent: "flex-end" }} >
              <Text style={{ fontSize: 13, textAlign: "right", fontWeight: 'bold', color: '#03a2f9', fontFamily: 'Gotham-Book' }}>Forgot password?</Text>
            </TouchableOpacity>
          </View>

          <View style={{ paddingTop: 10 }}>
            <TouchableOpacity
              onPress={() => {
                AppConfig.global_isLoggedIn = true;
                this.OnSignIn();
              }}
              style={{ width: window.width * 0.9, height: 40, borderRadius: 5, backgroundColor: '#32b3fa', justifyContent: 'center' }}>
              <Text style={{ alignSelf: 'center', fontSize: 16, fontWeight: 'bold', color: '#ffffff', fontFamily: 'Gotham-Book' }}>Login</Text>
            </TouchableOpacity>
          </View>

          <View style={{ paddingTop: 20 }}>
            <TouchableOpacity
              onPress={() => { this.props.pushScene(RegisterNavScene, {passProps: { source: 'start' }} ) }}
              style={{ width: window.width * 0.9, height: 40, borderRadius: 5, backgroundColor: '#32b3fa', justifyContent: 'center' }}>
              <Text style={{ alignSelf: 'center', fontSize: 16, fontWeight: 'bold', color: '#fff', fontFamily: 'Gotham-Book' }}>Sign Up</Text>
            </TouchableOpacity>
          </View>

          

          {/*
        <View>
          <  TouchableOpacity
            onPress={() => {
              AppConfig.global_isLoggedIn = true;
              this.OnSignIn();
            }}
            style={{ borderRadius: 20 }}
            underlayColor="#3c8671" >
            <Text allowFontScaling={false} style={styles.btnLogin}>LOGIN</Text>
          </  TouchableOpacity>


        </View>*/}
          {/*
        <View>
          <  TouchableOpacity
            onPress={() => { this.props.pushScene(StartRegisterViewRetired) }}
            style={{ }}
            underlayColor="#3c8671" >
            <Text allowFontScaling={false} style={styles.btnLogin}>REGISTER</Text>
          </  TouchableOpacity>

        </View>*/}
          {/*
        <View style={{ marginTop: 100 }}>
          <  TouchableOpacity
            onPress={() => {
              AppConfig.global_isLoggedIn = false;
              this.props.resetToScene(MainScene);
            }}
            underlayColor="#3c8671">
            <Text allowFontScaling={false} style={styles.btnRegister}>Continue without Login</Text>
          </  TouchableOpacity></View>*/}



          </View>




          {this.state.isLoading ?
            <View style={styles.loadingScene}>
              <ActivityIndicator
                animating={true}
                size="small"
                color="white"
              />
            </View> : null}

        </View>
      </View>
    );
  }
}

export default StartLoginView;
