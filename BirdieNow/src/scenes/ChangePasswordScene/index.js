import React, { PropTypes, Component } from 'react';
import { ActivityIndicator, Alert, Platform, Keyboard, TextInput, View, StatusBar, Text, TouchableOpacity } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styles from './styles';
import _, { isEqual } from 'lodash';
import { MainScene, ProfileView } from 'AppScenes';
import { ChangePassword } from 'AppUtilities';
import AppConfig from 'AppConfig';

class ChangePasswordScene extends Component {
  static propTypes = {
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      isLoading: false,
      currentPassword: '',
      password: '',
      confirmPassword: '',
    };

    this.OnSubmit = this.OnSubmit.bind(this);
  }

  OnBack() {
    this.props.popNavBack();
  }

  OnSubmit(){
    Keyboard.dismiss();

    if (this.state.currentPassword == '') {
      Alert.alert("Current password is required");
      return
    }

    if (this.state.password == '') {
      Alert.alert("New password is required");
      return
    }

    if (this.state.confirmPassword == '') {
      Alert.alert("Confirm Password is required");
      return
    }

    if (this.state.password != this.state.confirmPassword) {
      Alert.alert("Passwords do not match");
      return
    }
    
    this.setState({ isLoading: true });
  
    let data = {
        oldPassword: this.state.currentPassword,
        newPassword: this.state.password,
        confirmPassword: this.state.confirmPassword
    };

    ChangePassword(data)
      .then((response) => {
          debugger;
        if (response.status == 200) {
            Alert.alert("Your password has been updated.")
            AppConfig.global_isLoggedIn = false;
            this.props.popNavBack();
        } else {          
          Alert.alert("The new password you provided is incorrect.");
        }
        this.setState({ isLoading: false });
      })
      .catch(error => {
        Alert.alert(error.message);
        this.setState({ isLoading: false });
      });
  }

  render() {
    const render_TopBar = (
      <View style={{
        flexDirection: 'row', alignItems: "center", justifyContent: "center",
        backgroundColor: '#fafafa', borderBottomWidth: 1, borderBottomColor: '#D3D3D3',
        height: (Platform.OS === 'ios') ? 65 : 40 }}>

        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={() => { this.OnBack(); }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#000',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Back</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 2, justifyContent: "center" }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#000',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Change Password</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={() => { this.OnSubmit(); }} >
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#32b3fa',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Save</Text>
        </TouchableOpacity>
      </View>
    );

    return (
    <View style={styles.container}>
        {render_TopBar}
        <KeyboardAwareScrollView
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={true} >
            <View>
                <View style={[styles.inputWrapper, { }]}>
                    <TextInput
                    underlineColorAndroid='transparent'
                    style={styles.inputEmail}
                    placeholder="Current Password"
                    placeholderTextColor="#999"
                    secureTextEntry={true}
                    onChangeText={(text) => {this.state.currentPassword = text}}
                    />
                </View>
                <View style={[styles.inputWrapper, { }]}>
                    <TextInput
                    underlineColorAndroid='transparent'
                    style={styles.inputEmail}
                    placeholder="Password"
                    placeholderTextColor="#999"
                    secureTextEntry={true}
                    onChangeText={(text) => {this.state.password = text}}
                    />
                </View>
                <View style={[styles.inputWrapper, {}]}>
                    <TextInput
                    underlineColorAndroid='transparent'
                    style={styles.inputEmail}
                    placeholder="Confirm Password"
                    placeholderTextColor="#999"
                    secureTextEntry={true}
                    onChangeText={(text) => {this.state.confirmPassword = text}}
                    />
                </View>  
                <View style={{ paddingTop: 20, paddingLeft: window.width * 0.05  }}>
                    <TouchableOpacity
                    onPress={() => { this.OnSubmit(); }}
                    style={{ width: window.width * 0.9, height: 35, borderRadius: 5, backgroundColor: '#32b3fa', justifyContent: 'center' }}>
                    <Text style={{ alignSelf: 'center', fontSize: 16, fontWeight: 'bold', color: '#ffffff', fontFamily: 'Gotham-Book' }}>Save</Text>
                    </TouchableOpacity>
                </View>      
            </View>
        </KeyboardAwareScrollView>

        {this.state.isLoading ?
          <View style={styles.loadingScene}>
            <ActivityIndicator
              animating={true}
              size="small"
              color="white"
            />
          </View> : null}      
      </View>
    );
  }
}

export default ChangePasswordScene;
