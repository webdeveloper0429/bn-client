import React, { PropTypes, Component } from 'react';
import { ActivityIndicator, Alert, TextInput, View, StatusBar, Text, TouchableHighlight, Keyboard, TouchableOpacity } from 'react-native';
import styles from './styles';
import _, { isEqual } from 'lodash';
import { SignIn, GlobalStorage, GetAccountInfo } from 'AppUtilities';
import AppConfig from 'AppConfig';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

class LoginNavScene extends Component {
  static propTypes = {
    popNavBack: PropTypes.func.isRequired,
    callBack: PropTypes.func.isRequired,
  };

  constructor(props, context) {
    super(props, context);

    this.state = {
      isLoading: false,
    };

    StatusBar.setHidden(false);
    this.OnSignIn = this.OnSignIn.bind(this);
    this.grant_type = 'password';
    this.username = "";
    this.password = "";
  }

  OnSignIn(){
    Keyboard.dismiss();
    this.setState({ isLoading: true });
    SignIn(this.grant_type, this.username, this.password)
      .then((response) => {
        if (!response.error_description) { //success 200
          GlobalStorage.setItem("storage_user", this.username);
          GlobalStorage.setItem("storage_password", this.password);
          GlobalStorage.setItem("storage_loggedIn", "true");
          GlobalStorage.setItem("storage_token", response.access_token);
          AppConfig.global_userToken = response.access_token; // set the user token
          this.setState({ isLoading: false });
          this.props.callBack();
          this.props.popNavBack();
        } else { // failed 400
          this.setState({ isLoading: false });
          Alert.alert(response.error_description);
        }
      })
      .catch(error => {
        Alert.alert(error.message);
        this.setState({ isLoading: false });
      });
  }

  render() {
    const render_TopBar = (
      <View style={styles.topView}>
        <TouchableOpacity style={{flex: 1, justifyContent: "center"}} onPress={() => {this.props.popNavBack();}}>
          <Text allowFontScaling={false} style={{textAlign: "center", fontSize: 12, marginTop: 10}}>Back</Text>
        </TouchableOpacity>
        <View style={{flex: 2, justifyContent: "center"}}>
          <Text allowFontScaling={false} style={{textAlign: "center", fontSize: 15, marginTop: 10}}>Login</Text>
        </View>
        <View style={{flex: 1, justifyContent: "center"}}>
          <Text allowFontScaling={false} style={{flex: 1, textAlign: "center", }}/>
        </View>
      </View>
    );

    return (
      <View>
        <View style={styles.container}>
          <Text allowFontScaling={false} style={styles.topLabel}>ALREADY A MEMBER?</Text>
          <TextInput
            underlineColorAndroid='transparent'
            style={styles.inputEmail}
            fontSize={17}
            placeholder="Email"
            defaultValue=""
            placeholderTextColor="#999"
            onChangeText={(text) => {this.username = text}}
            keyboardType='email-address'
            autoCapitalize="none"
            autoCorrect={false}
          />
          <TextInput
            underlineColorAndroid='transparent'
            style={styles.inputEmail}
            fontSize={17}
            placeholder="Password"
            placeholderTextColor="#999"
            defaultValue=""
            secureTextEntry={true}
            onChangeText={(text) => {this.password = text}}
          />
          <View style={{ flexDirection: 'row', marginTop: 10, alignSelf: 'flex-end' }}>
            <  TouchableOpacity
              onPress={() => {
              AppConfig.global_isLoggedIn = true;
              this.OnSignIn();
            }}
              style={{ marginRight: 10 }}
              underlayColor="#bde7ff" >
              <Text allowFontScaling={false} style={styles.btnLogin}>LOGIN</Text>
            </  TouchableOpacity>
          </View>

          {this.state.isLoading ?
            <View style={styles.loadingScene}>
              <ActivityIndicator
                animating={true}
                size="small"
                color="white"
              />
            </View> : null}
          {render_TopBar}
        </View>
      </View>
    );
  }
}

export default LoginNavScene;
