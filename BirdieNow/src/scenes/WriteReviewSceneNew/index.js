import React, { Component, PropTypes } from 'react';
import { Dimensions, ActivityIndicator, Alert, View, Image, TouchableOpacity, Text, ListView, RefreshControl, ScrollView, TextInput, Platform} from 'react-native';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import StarRating from 'react-native-star-rating';
import { CheckboxField, Checkbox } from 'react-native-checkbox-field';
import styles from './styles';
import AppConfig from 'AppConfig';
import AppStyles from 'AppStyles';
import { SearchBar, HeaderBar } from 'AppComponents';
import { FilterScene, ListDetailScene, SelectSkillLevelScene } from 'AppScenes';
import {  } from 'AppUtilities';
import _, { isEqual } from 'lodash';
import moment from 'moment';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

class WriteReviewSceneNew extends Component {
  static propTypes = {
    popNavBack: PropTypes.func,
    callback: PropTypes.func.isRequired,
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      source: '',
      showloading: false,
      warning_state: 0,
      star_state: 0,
      loadFinished: false,
      data: [],
      image: {},
      starCount: 0,
      checkBox1Selected: false,
      checkBox1Label: 'Setup',
      checkBox2Selected: false,
      checkBox2Label: 'Irons',
      checkBox3Selected: false,
      checkBox3Label: 'Driver',
      checkBox4Selected: false,
      checkBox4Label: 'Chipping',
      checkBox5Selected: false,
      checkBox5Label: 'Putting',
      checkBox6Selected: false,
      // checkBox6Label: 'Pitch Shots',
      // checkBox7Selected: false,
      checkBox7Label: 'Sand Shots',
      currentText: '',
      userSkillLevel: {label: 'Beginner', value: 1},
      toggleSkillLevel: false
    };

    const { width, height } = Dimensions.get('window');
    this.width = width;
    this.height = height;
    this.r_width =  width / 1125;
    this.r_height = height / 1659;
    this.comment = "";

    //functions
    this.onStarRatingPress = this.onStarRatingPress.bind(this);
    this.selectCheckbox1 = this.selectCheckbox1.bind(this);
    this.selectCheckbox2 = this.selectCheckbox2.bind(this);
    this.selectCheckbox3 = this.selectCheckbox3.bind(this);
    this.selectCheckbox4 = this.selectCheckbox4.bind(this);
    this.selectCheckbox5 = this.selectCheckbox5.bind(this);
    this.selectCheckbox6 = this.selectCheckbox6.bind(this);
    this.selectCheckbox7 = this.selectCheckbox7.bind(this);
    this.onChangeTextField = this.onChangeTextField.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeSkillLevel = this.onChangeSkillLevel.bind(this);

    this.ChangeUserSkillLevel1 = this.ChangeUserSkillLevel1.bind(this);
    this.ChangeUserSkillLevel2 = this.ChangeUserSkillLevel2.bind(this);
    this.ChangeUserSkillLevel3 = this.ChangeUserSkillLevel3.bind(this);
    this.ChangeUserSkillLevel4 = this.ChangeUserSkillLevel4.bind(this);
    
    

    this.SubmitSuccess = ::this.SubmitSuccess;
    this.CallBackSelectSkillLevelScene = ::this.CallBackSelectSkillLevelScene;
    this.CheckBoxValidation = ::this.CheckBoxValidation;
    this.IsValidCheckBoxSelection = ::this.IsValidCheckBoxSelection;
  }

  componentDidMount() {
    this.onChangeTextField("");
    this.setState({showloading: true, loadFinished: false});

    if (this.props.passProps.source != null){
      this.setState({source: this.props.passProps.source});
    }


    if (AppConfig.global_userSkillLevel == 1)
      this.setState({userSkillLevel: {label: 'Beginner', value: 1}});
    if (AppConfig.global_userSkillLevel == 2)
      this.setState({userSkillLevel: {label: 'Bogey Player', value: 2}});
    if (AppConfig.global_userSkillLevel == 3)
      this.setState({userSkillLevel: {label: 'Single Handicap', value: 3}});
    if (AppConfig.global_userSkillLevel == 4)
      this.setState({userSkillLevel: {label: 'Scratch Player', value: 4}});

    fetch(AppConfig.apiUrl + '/api/Appointments/?id=' + this.props.passProps.appointment.id + '&auth=auth', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${AppConfig.global_userToken}`,
      }
    })
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({showloading: false, loadFinished: true, data: responseData, image: { uri: 'https://www.birdienow.com/img/instructor/profile/' + responseData.instructorId + '.jpg' }});
      })
      .catch(error => {
        Alert.alert(error.message);
        this.setState({showloading: false});
      });
  }

  onError(error){
   this.setState({ image: require('img/profile/profile.png')})
  }

	CallBackSelectSkillLevelScene(row) {
    this.setState({userSkillLevel: row});
	}

  ChangeUserSkillLevel1() {
    this.setState({userSkillLevel: {label: 'Beginner', value: 1}});
    this.setState({toggleSkillLevel: false});
	}

  ChangeUserSkillLevel2() {   
      this.setState({userSkillLevel: {label: 'Bogey Player', value: 2}});
      this.setState({toggleSkillLevel: false});
	}

  ChangeUserSkillLevel3() {
      this.setState({userSkillLevel: {label: 'Single Handicap', value: 3}});
      this.setState({toggleSkillLevel: false});
	}

  ChangeUserSkillLevel4() {
      this.setState({userSkillLevel: {label: 'Scratch Player', value: 4}});
      this.setState({toggleSkillLevel: false});
	}

  CheckBoxValidation() {
    let checkArray = [];
    if (this.state.checkBox1Selected) {
      checkArray.push("1");
    }
    if (this.state.checkBox2Selected) {
      checkArray.push("2");
    }
    if (this.state.checkBox3Selected) {
      checkArray.push("3");
    }
    if (this.state.checkBox4Selected) {
      checkArray.push("4");
    }
    if (this.state.checkBox5Selected) {
      checkArray.push("5");
    }
    if (this.state.checkBox6Selected) {
      checkArray.push("6");
    }
    if (this.state.checkBox7Selected) {
      checkArray.push("7");
    }
    return checkArray;
  }

  onSubmit() {
    let checkArray = this.CheckBoxValidation();

    if (checkArray.length == 0) {
      if (this.state.starCount == 0) {
        this.setState({star_state: -1, warning_state: 1});
      } else {
        this.setState({star_state: 0, warning_state: 1});
      }
    } else if (checkArray.length > 2) {
      if (this.state.starCount == 0) {
        this.setState({star_state: -1, warning_state: 2});
      } else {
        this.setState({star_state: 0, warning_state: 2});
      }
    } else {
      if (this.state.starCount == 0) {
        this.setState({star_state: -1, warning_state: 0});
      } else {
        this.setState({star_state: 0, warning_state: 0});
        let requestParams = JSON.stringify({
          appointmentId: this.state.data.id,
          instructorId: this.state.data.instructorId,
          overallRating: this.state.starCount,
          userSkillLevelId: this.state.userSkillLevel.value,
          reviewLessonTypeIds: checkArray,
          statement: this.state.currentText
        });
        this.setState({showloading: true});

        fetch(AppConfig.apiUrl + '/api/Reviews', {
          method: 'POST',
          headers: {
            Authorization: `Bearer ${AppConfig.global_userToken}`,
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          body: requestParams
        })
          .then((response) => response)
          .then((responseData) => {
            AppConfig.global_userSkillLevel = this.state.userSkillLevel.value;
            Alert.alert(
              'CONFIRMATION',
              'Your review has been submitted. Thank you.',
              [
                {text: 'OK', onPress: () => {this.SubmitSuccess();}},
              ],
              { cancelable: false }
            );
            this.setState({showloading: false});
          })
          .catch(error => {
            Alert.alert(error.message);
            this.setState({showloading: false});
          });
      }
    }
  }

  onChangeSkillLevel() {
   this.setState({toggleSkillLevel: true});
    // if (this.state.source = '')
    //   this.props.pushNavScene(SelectSkillLevelScene, { callback: this.CallBackSelectSkillLevelScene });
    // else
    //   this.props.pushScene(SelectSkillLevelScene, { callback: this.CallBackSelectSkillLevelScene });
  }

  SubmitSuccess() {
    this.props.callback();
    if (this.state.source == '')
    {
      this.props.popNavBack();
    }
  }

  IsValidCheckBoxSelection(isSelected) {
    this.setState({warning_state: 0});
    if (isSelected) { return true; }

    let checkArray = this.CheckBoxValidation(); 
    if (checkArray.length >= 2) {
      this.setState({warning_state: 2});
      return false;
    }
    return true;
  }

  selectCheckbox1() {
    if (!this.IsValidCheckBoxSelection(this.state.checkBox1Selected)) {return; }
    this.setState({checkBox1Selected: !this.state.checkBox1Selected});
  }

  selectCheckbox2() {
    if (!this.IsValidCheckBoxSelection(this.state.checkBox2Selected)) {return; }
    this.setState({checkBox2Selected: !this.state.checkBox2Selected});
  }

  selectCheckbox3() {
    if (!this.IsValidCheckBoxSelection(this.state.checkBox3Selected)) {return; }
    this.setState({checkBox3Selected: !this.state.checkBox3Selected});
  }

  selectCheckbox4() {
    if (!this.IsValidCheckBoxSelection(this.state.checkBox4Selected)) {return; }
    this.setState({checkBox4Selected: !this.state.checkBox4Selected});
  }

  selectCheckbox5() {
    if (!this.IsValidCheckBoxSelection(this.state.checkBox5Selected)) {return; }
    this.setState({checkBox5Selected: !this.state.checkBox5Selected});
  }

  selectCheckbox6() {
    if (!this.IsValidCheckBoxSelection(this.state.checkBox6Selected)) {return; }
    this.setState({checkBox6Selected: !this.state.checkBox6Selected});
  }

  selectCheckbox7() {
    if (!this.IsValidCheckBoxSelection(this.state.checkBox7Selected)) {return; }
    this.setState({checkBox7Selected: !this.state.checkBox7Selected});
  }

  onChangeTextField(text) {
    this.setState({currentText: text});
  }

  onStarRatingPress(rating) {
    this.setState({starCount: rating, star_state: 0});
  }

  render() {
    let render_warning_star = [];
    if (this.state.star_state == -1) {
      render_warning_star.push(
        <Text key="render_warning" allowFontScaling={false} 
          style={{fontSize: 13, color: "red", marginLeft: 20, marginRight: 20, marginTop: 0, marginBottom: -10, fontFamily: 'Gotham-Bold', textAlign: 'center'}}>
            Overall rating is required
        </Text>
      );
    }

    let render_warning = [];
    if (this.state.warning_state == 1) {
      render_warning.push(
        
        <Text key="render_warning" allowFontScaling={false} 
          style={{fontSize: 13, color: "red", marginLeft: 20, marginRight: 20, marginTop: 25, fontFamily: 'Gotham-Bold', textAlign: 'center'}}>
            Select at least one
        </Text>
        
      );
    } else if (this.state.warning_state == 2) {
      render_warning.push(
        
        <Text key="render_warning" allowFontScaling={false} 
          style={{fontSize: 13, color: "red", marginLeft: 20, marginRight: 20, marginTop: 25, fontFamily: 'Gotham-Bold', textAlign: 'center'}}>
            Select no more than two
        </Text>
        
      );
    } 
    
    const defaultColor = '#fff';
    const startDate = moment(this.state.data.startDate), endDate = moment(this.state.data.endDate);
    let strDate;
    let strTime;

    if (this.state.loadFinished) {
      if (startDate.isSame(endDate, 'day')) {
        strDate = `${startDate.format('dddd | MMMM D, YYYY')}`;
        strTime = `${startDate.format('h:mm A')} - ${endDate.format('h:mm A')}`;
      } else {
        strDate = `${startDate.format('dddd | MMMM D, YYYY')} - ${endDate.format('dddd | MMMM D, YYYY')}`;
        strTime = `${startDate.format('h:mm A')} - ${endDate.format('h:mm A')}`;
      }
    } else {
      strDate = "";
    }

    const render_TopBar = (
      <View style={{
        flexDirection: 'row', alignItems: "center", justifyContent: "center",
        backgroundColor: '#FFF', borderBottomWidth: 0, borderBottomColor: '#85D3DD',
        height: (Platform.OS === 'ios') ? 65 : 40 }}>
        
        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={() => { this.props.popNavBack(); }}>
          { this.state.source == '' ?
            <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#000',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Back</Text> : null
          }
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 2, justifyContent: "center" }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#000',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Review</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={this.onSubmit} >
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#32b3fa',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Submit</Text>
        </TouchableOpacity>
      </View>
    );

    let render_ImageBar = [];
    render_ImageBar = (
      <Image source={require('img/image/ui_back_blue.png')} style={{ height: 120, flexDirection:'row', alignItems: 'center' }}>
        <Image key="render_image"
          style={{ width: 246 * this.r_width, height: 246 * this.r_width, borderRadius: 123 * this.r_width, marginLeft: 40 }}
          source={ this.state.image  }
          onError={ this.onError.bind(this) }/>    
        <View style={{ marginLeft: 10, marginTop: 10  }}>
          <Text allowFontScaling={false} style={{ fontSize: 18, color: '#4b5b64', fontFamily: 'Gotham-Medium', backgroundColor: 'transparent' }}>
            {this.state.data.instructorFirstName} {this.state.data.instructorLastName}
          </Text>
          <Text allowFontScaling={false} style={{fontSize: 13, color: '#4b5b64', fontFamily: 'Gotham-Medium', backgroundColor: 'transparent', marginTop: 5, textAlign: 'left' }}>
            {strDate}
          </Text>
          <Text allowFontScaling={false} style={{fontSize: 13, color: '#4b5b64', fontFamily: 'Gotham-Medium', backgroundColor: 'transparent', marginTop: 5, textAlign: 'left' }}>
            {strTime}
          </Text>
          <TouchableOpacity style={{marginTop: 10, flexDirection:'row', flexWrap:'wrap'  }} onPress={()=> { this.onChangeSkillLevel(); }}>
            <Text allowFontScaling={false} style={{fontFamily: 'Gotham-Bold', textAlign: "center", backgroundColor: 'transparent', fontSize: 12, color: '#32b3fa'}}>Your Skill Level: {this.state.userSkillLevel.label}</Text>
          </TouchableOpacity>
        </View>
      </Image>
    );
    
    return (
      <View style={styles.container}>
        {render_TopBar}

        <KeyboardAwareScrollView
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={true} >
        {render_ImageBar}

        { this.state.toggleSkillLevel ? 
        <View style={{flexDirection:'row', alignItems: 'center', height: 90}}>
          <Image source={require('img/image/ui_back_blue.png')} style={{ flex:1, height: 90 , flexDirection:'row', alignItems: 'center' }}>
            {/*<ScrollView horizontal={true} style={{height: 100}}>*/}
                <View style={{flex: 1, flexDirection:'column'}}>
                  <View style={{ alignItems: 'center'}}>
                    <CheckboxField
                      onSelect={this.ChangeUserSkillLevel1}
                      disabled={this.props.disabled}
                      disabledColor='rgb(236,236,236)'
                      defaultColor={defaultColor}
                      selectedColor={defaultColor}
                      containerStyle={styles.containerStyle1}
                      labelStyle={styles.labelStyle}
                      checkboxStyle={styles.checkboxStyle1}
                      labelSide="left">
                        {this.state.userSkillLevel.value == 1 ? 
                          <Image
                            style={{ width: 50, height: 50}}
                            source={ require('img/image/skill1_on.png') }/> :
                          <Image
                            style={{ width: 50, height: 50 }}
                            source={ require('img/image/skill1_off.png') }/> 
                        }
                    </CheckboxField>
                  </View>
                  <View style={{marginTop:5}}>
                    <Text allowFontScaling={false} style={{textAlign: "center", fontFamily: 'Gotham-Medium', fontSize: 10, color: 'gray', backgroundColor: 'transparent'}}>Beginner</Text>
                  </View>
                </View>
        
                <View style={{flex: 1, flexDirection:'column'}}>
                  <View style={{ alignItems: 'center'}}>
                    <CheckboxField
                      onSelect={this.ChangeUserSkillLevel2}
                      disabled={this.props.disabled}
                      disabledColor='rgb(236,236,236)'
                      defaultColor={defaultColor}
                      selectedColor={defaultColor}
                      containerStyle={styles.containerStyle1}
                      labelStyle={styles.labelStyle}
                      checkboxStyle={styles.checkboxStyle1}
                      labelSide="left">
                        {this.state.userSkillLevel.value == 2 ? 
                          <Image
                            style={{ width: 50, height: 50}}
                            source={ require('img/image/skill2_on.png') }/> :
                          <Image
                            style={{ width: 50, height: 50 }}
                            source={ require('img/image/skill2_off.png') }/> 
                        }
                    </CheckboxField>
                  </View>
                  <View style={{marginTop:5}}>
                    <Text allowFontScaling={false} style={{textAlign: "center", fontFamily: 'Gotham-Medium', fontSize: 10, color: 'gray', backgroundColor: 'transparent'}}>Bogey Player</Text>
                  </View>
                </View>
    
                <View style={{flex: 1, flexDirection:'column'}}>
                  <View style={{ alignItems: 'center'}}>
                    <CheckboxField
                      onSelect={this.ChangeUserSkillLevel3}
                      disabled={this.props.disabled}
                      disabledColor='rgb(236,236,236)'
                      defaultColor={defaultColor}
                      selectedColor={defaultColor}
                      containerStyle={styles.containerStyle1}
                      labelStyle={styles.labelStyle}
                      checkboxStyle={styles.checkboxStyle1}
                      labelSide="left">
                        {this.state.userSkillLevel.value == 3 ? 
                          <Image
                            style={{ width: 50, height: 50}}
                            source={ require('img/image/skill3_on.png') }/> :
                          <Image
                            style={{ width: 50, height: 50 }}
                            source={ require('img/image/skill3_off.png') }/> 
                        }
                    </CheckboxField>
                  </View>
                  <View style={{marginTop:5}}>
                    <Text allowFontScaling={false} style={{textAlign: "center", fontFamily: 'Gotham-Medium', fontSize: 10, color: 'gray', backgroundColor: 'transparent'}}>Single Handicap</Text>
                  </View>
                </View>


                 <View style={{flex: 1, flexDirection:'column'}}>
                  <View style={{ alignItems: 'center'}}>
                    <CheckboxField
                      onSelect={this.ChangeUserSkillLevel4}
                      disabled={this.props.disabled}
                      disabledColor='rgb(236,236,236)'
                      defaultColor={defaultColor}
                      selectedColor={defaultColor}
                      containerStyle={styles.containerStyle1}
                      labelStyle={styles.labelStyle}
                      checkboxStyle={styles.checkboxStyle1}
                      labelSide="left">
                        {this.state.userSkillLevel.value == 4 ? 
                          <Image
                            style={{ width: 50, height: 50}}
                            source={ require('img/image/skill4_on.png') }/> :
                          <Image
                            style={{ width: 50, height: 50 }}
                            source={ require('img/image/skill4_off.png') }/> 
                        }
                    </CheckboxField>
                  </View>
                  <View style={{marginTop:5}}>
                    <Text allowFontScaling={false} style={{textAlign: "center", fontFamily: 'Gotham-Medium', fontSize: 10, color: 'gray', backgroundColor: 'transparent'}}>Scratch Player</Text>
                  </View>
                </View>
            {/*</ScrollView>*/}
            </Image>
          </View>
          : null}
        
        <ScrollView ref={component => this._scrollView = component}>
          <Text allowFontScaling={false} style={{fontSize: 12, color: "black", marginLeft: 20, 
            marginRight: 20, marginTop: 20, fontFamily: 'Gotham-Medium', textAlign: 'center'}}>RATE YOUR LESSON</Text>
          <View style={{marginTop: 0}}>
            <View style={{}}/>
            <View style={{flex:1, alignItems: 'stretch', justifyContent: 'space-between', paddingLeft:20, paddingRight: 20}}>
              <StarRating
                disabled={false}
                emptyStar={'ios-star-outline'}
                fullStar={'ios-star'}
                halfStar={'ios-star-half'}
                iconSet="Ionicons"
                maxStars={5}
                rating={this.state.starCount}
                selectedStar={(rating) => this.onStarRatingPress(rating)}
                starColor={'#32b3fa'}
                emptyStarColor={'#ddd'}
                starSize={60}
              />
            </View>
            <View style={{flex: 1.5}}/>
          </View>
          {render_warning_star}
          
          <View style={{ backgroundColor: '#ececec', height: 1, marginRight: 15, marginLeft: 15, marginTop: 15 }} />
          <Text allowFontScaling={false} style={{fontSize: 12, color: "black", marginLeft: 20, 
            marginRight: 20, marginTop: 20, fontFamily: 'Gotham-Medium', textAlign: 'center'}}>FOCUS OF LESSON</Text>

          <View style={{flexDirection:'row', height: 70}}>
            {/*<ScrollView horizontal={true} style={{height: 100}}>*/}
                <View style={{flex: 1, flexDirection:'column'}} >
                  <View style={{alignItems: 'center'}}>
                    <CheckboxField
                      onSelect={this.selectCheckbox1}
                      disabled={this.props.disabled}
                      disabledColor='rgb(236,236,236)'
                      selected={this.state.checkBox1Selected}
                      defaultColor={defaultColor}
                      selectedColor={defaultColor}
                      containerStyle={styles.containerStyle}
                      labelStyle={styles.labelStyle}
                      checkboxStyle={styles.checkboxStyle}
                      labelSide="left">
                        {this.state.checkBox1Selected ? 
                          <Image
                            style={{ width: 55, height: 55 }}
                            source={ require('img/image/review_setup.png') }/> :
                          <Image
                            style={{ width: 55, height: 55 }}
                            source={ require('img/image/review_setup_grey.png') }/> 
                        }
                    </CheckboxField>
                  </View>
                  <View style={{marginTop: 5}}>
                    <Text allowFontScaling={false} style={{textAlign: "center", fontFamily: 'Gotham-Medium', fontSize: 10, color: 'gray'}}>{this.state.checkBox1Label}</Text>
                  </View>
                </View>
        
                <View style={{flex: 1, flexDirection:'column'}} >
                  <View style={{alignItems: 'center'}}>
                    <CheckboxField
                      onSelect={this.selectCheckbox2}
                      disabled={this.props.disabled}
                      disabledColor='rgb(236,236,236)'
                      selected={this.state.checkBox2Selected}
                      defaultColor={defaultColor}
                      selectedColor={defaultColor}
                      containerStyle={styles.containerStyle}
                      labelStyle={styles.labelStyle}
                      checkboxStyle={styles.checkboxStyle}
                      labelSide="left">
                        {this.state.checkBox2Selected ? 
                          <Image
                            style={{ width: 55, height: 55 }}
                            source={ require('img/image/review_iron.png') }/> :
                          <Image
                            style={{ width: 55, height: 55 }}
                            source={ require('img/image/review_iron_grey.png') }/> 
                        }
                    </CheckboxField>
                  </View>
                  <View style={{marginTop: 5}}>
                    <Text allowFontScaling={false} style={{textAlign: "center", fontFamily: 'Gotham-Medium', fontSize: 10, color: 'gray'}}>{this.state.checkBox2Label}</Text>
                  </View>
                </View>
    
                <View style={{flex: 1, flexDirection:'column'}} >
                  <View style={{alignItems: 'center'}}>
                    <CheckboxField
                      onSelect={this.selectCheckbox3}
                      disabled={this.props.disabled}
                      disabledColor='rgb(236,236,236)'
                      selected={this.state.checkBox3Selected}
                      defaultColor={defaultColor}
                      selectedColor={defaultColor}
                      containerStyle={styles.containerStyle}
                      labelStyle={styles.labelStyle}
                      checkboxStyle={styles.checkboxStyle}
                      labelSide="left">
                        {this.state.checkBox3Selected ? 
                          <Image
                            style={{ width: 55, height: 55 }}
                            source={ require('img/image/review_driver.png') }/> :
                          <Image
                            style={{ width: 55, height: 55 }}
                            source={ require('img/image/review_driver_grey.png') }/> 
                        }
                    </CheckboxField>
                  </View>
                  <View style={{marginTop: 5}}>
                    <Text allowFontScaling={false} style={{textAlign: "center", fontFamily: 'Gotham-Medium', fontSize: 10, color: 'gray'}}>{this.state.checkBox3Label}</Text>
                  </View>
                </View>


                 <View style={{flex: 1, flexDirection:'column'}} >
                  <View style={{alignItems: 'center'}}>
                    <CheckboxField
                      onSelect={this.selectCheckbox4}
                      disabled={this.props.disabled}
                      disabledColor='rgb(236,236,236)'
                      selected={this.state.checkBox4Selected}
                      defaultColor={defaultColor}
                      selectedColor={defaultColor}
                      containerStyle={styles.containerStyle}
                      labelStyle={styles.labelStyle}
                      checkboxStyle={styles.checkboxStyle}
                      labelSide="left">
                        {this.state.checkBox4Selected ? 
                          <Image
                            style={{ width: 55, height: 55 }}
                            source={ require('img/image/review_chipping.png') }/> :
                          <Image
                            style={{ width: 55, height: 55 }}
                            source={ require('img/image/review_chipping_grey.png') }/> 
                        }
                    </CheckboxField>
                  </View>
                  <View style={{marginTop: 5}}>
                    <Text allowFontScaling={false} style={{textAlign: "center", fontFamily: 'Gotham-Medium', fontSize: 10, color: 'gray'}}>{this.state.checkBox4Label}</Text>
                  </View>
                </View>
        
                <View style={{flex: 1, flexDirection:'column'}} >
                  <View style={{alignItems: 'center'}}>
                    <CheckboxField
                      onSelect={this.selectCheckbox5}
                      disabled={this.props.disabled}
                      disabledColor='rgb(236,236,236)'
                      selected={this.state.checkBox5Selected}
                      defaultColor={defaultColor}
                      selectedColor={defaultColor}
                      containerStyle={styles.containerStyle}
                      labelStyle={styles.labelStyle}
                      checkboxStyle={styles.checkboxStyle}
                      labelSide="left">
                        {this.state.checkBox5Selected ? 
                          <Image
                            style={{ width: 55, height: 55 }}
                            source={ require('img/image/review_putter.png') }/> :
                          <Image
                            style={{ width: 55, height: 55 }}
                            source={ require('img/image/review_putter_grey.png') }/> 
                        }
                    </CheckboxField>
                  </View>
                  <View style={{marginTop: 5}}>
                    <Text allowFontScaling={false} style={{textAlign: "center", fontFamily: 'Gotham-Medium', fontSize: 10, color: 'gray'}}>{this.state.checkBox5Label}</Text>
                  </View>
                </View>
    
                {/*<View style={{flex: 1, justifyContent: "center"}}>
                  <CheckboxField
                    onSelect={this.selectCheckbox6}
                    disabled={this.props.disabled}
                    disabledColor='rgb(236,236,236)'
                    selected={this.state.checkBox6Selected}
                    defaultColor={defaultColor}
                    selectedColor={defaultColor}
                    containerStyle={styles.containerStyle}
                    labelStyle={styles.labelStyle}
                    checkboxStyle={styles.checkboxStyle}
                    labelSide="left">
                      {this.state.checkBox6Selected ? 
                        <Image
                          style={{ width: 166 * this.r_width, height: 166 * this.r_width}}
                          source={ require('img/image/review_icon_7.png') }/> :
                        <Image
                          style={{ width: 55, height: 55 }}
                          source={ require('img/image/review_icon_7_grey.png') }/> 
                      }
                  </CheckboxField>
                  <View style={{flex: 1, marginTop:40}}>
                    <Text allowFontScaling={false} style={{textAlign: "center", fontFamily: 'Gotham-Medium', fontSize: 10, color: 'gray'}}>{this.state.checkBox6Label}</Text>
                  </View>
                </View>*/}

                 <View style={{flex: 1, flexDirection:'column'}} >
                  <View style={{alignItems: 'center'}}>
                    <CheckboxField
                      onSelect={this.selectCheckbox7}
                      disabled={this.props.disabled}
                      disabledColor='rgb(236,236,236)'
                      selected={this.state.checkBox7Selected}
                      defaultColor={defaultColor}
                      selectedColor={defaultColor}
                      containerStyle={styles.containerStyle}
                      labelStyle={styles.labelStyle}
                      checkboxStyle={styles.checkboxStyle}
                      labelSide="left">
                        {this.state.checkBox7Selected ? 
                          <Image
                            style={{ width: 55, height: 55 }}
                            source={ require('img/image/review_sand.png') }/> :
                          <Image
                            style={{ width: 55, height: 55 }}
                            source={ require('img/image/review_sand_grey.png') }/> 
                        }
                    </CheckboxField>
                  </View>
                  <View style={{marginTop: 5}}>
                    <Text allowFontScaling={false} style={{textAlign: "center", fontFamily: 'Gotham-Medium', fontSize: 10, color: 'gray'}}>{this.state.checkBox7Label}</Text>
                  </View>
                </View>
            {/*</ScrollView>*/}
          </View>
          
          { render_warning }
          

          
          <View style={{ backgroundColor: '#ececec', height: 1, marginRight: 15, marginLeft: 15, marginTop: 15 }} />
          
          <Text allowFontScaling={false} 
            style={{fontSize: 12, color: "black", marginLeft: 20, marginRight: 20, marginTop: 20, 
            fontFamily: 'Gotham-Medium', textAlign: 'center'}}>YOUR REVIEW:</Text>

          <TextInput
            underlineColorAndroid='transparent'
            style={{marginLeft: 10, marginRight: 10, marginTop: 0, height: 120, padding: 5, textAlignVertical: 'top', borderWidth: 0, fontFamily: 'Gotham-Book'}}
            placeholder="Describe your lesson experience..."
            multiline = {true}
            numberOfLines = {100}
            onChangeText={(text) => this.onChangeTextField(text)}
          />
          

          <View style={{height:10}}/>
            <TouchableOpacity style={{height: 35, margin: 20, justifyContent: "center", backgroundColor: "#32b3fa", borderRadius: 3}}
              onPress={() => {this.onSubmit();}}>
            <Text allowFontScaling={false} style={{textAlign: "center", fontFamily: 'Gotham-Bold', color: "#ffffff"}}>Submit</Text>
          </TouchableOpacity>
        </ScrollView>

        {this.state.showloading ?
          <View style={{position:'absolute', top: 0, left:0, bottom:0, right:0, alignItems:'center', justifyContent:'center', alignSelf: 'stretch', backgroundColor: 'transparent'}}>
            <ActivityIndicator
              animating={true}
              size="small"
              color="gray"
            />
          </View>:null}
          </KeyboardAwareScrollView>
      </View>
    );
  }
}

export default WriteReviewSceneNew;