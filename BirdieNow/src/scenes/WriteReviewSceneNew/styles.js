import { StyleSheet } from 'react-native';
import AppConfig from 'AppConfig';

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 0,
    backgroundColor: "white",
    paddingHorizontal: 0,
  },
  headerContainer: {
    flexDirection: 'row',
    width: AppConfig.windowWidth,
    height: 65,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#aac1ce',
    shadowOpacity: 0,
    shadowRadius: 0,
    shadowOffset: {
      height: 1,
      width: 0
    }
  },
  leftImageTitleView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  leftImageTitleViewIcon: {
    alignItems: 'center',
    width: 20,
    height: 20,
    marginLeft: 15,
    marginTop: 10
  },
  centerImageTitleView: {
    flex: 4,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10
  },
  rightImageTitleView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  rightImageTitleViewIcon: {
    alignItems: 'center',
    width: 30,
    height: 30,
  },
  containerStyle: {
    
    flexDirection: 'row',
    paddingTop: 5,
    paddingBottom: 0,
    alignItems: 'center'
  },
  labelStyle: {
    flex: 1
  },
  checkboxStyle: {
    width: 50,
    height: 50,
    borderWidth: 2,
    borderColor: '#ddd',
    borderRadius: 100,
    marginTop:0,
    alignItems: 'center'

  },
  containerStyle1: {
    
    flexDirection: 'row',
    paddingTop: 0,
    paddingBottom: 0,
    alignItems: 'center'
  },
  checkboxStyle1: {
    borderWidth: 2,
    borderColor: '#ddd',
    borderRadius: 100,
    marginTop:0
  }

});
