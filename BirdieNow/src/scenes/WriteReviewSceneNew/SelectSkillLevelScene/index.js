import React, { Component, PropTypes } from 'react';
import { View, ListView, Text, ScrollView, Platform, TouchableOpacity, TextInput, Image, ActivityIndicator, Dimensions, Alert, BackAndroid } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styles from './styles';
import { Calendar, HeaderBar } from 'AppComponents';
import AppConfig from 'AppConfig';

const iOSStatusHeight = Platform.OS === 'ios' ? AppConfig.statusBarHeight : 0;
const { width, height } = Dimensions.get('window');

class SelectSkillLevelScene extends Component {
	static propTypes = {
		popNavBack: PropTypes.func.isRequired,
		pushNavScene: PropTypes.func.isRequired,
		callback: PropTypes.func.isRequired,
	};

	constructor(props, context) {
		super(props, context);
		this.state = {
			ListDataSource: new ListView.DataSource({ rowHasChanged: (r1, r2) => !isEqual(r1, r2) }),
		};
		this.renderRow = ::this.renderRow;
		this.onTypeId = ::this.onTypeId;
		this.ListSource =[{label: 'Beginner', value: 0}, {label: 'Bogey Player', value: 1}, {label: 'Single Handicap', value: 2}, {label: 'Scratch Player', value: 3}];
		this.state.ListDataSource = this.state.ListDataSource.cloneWithRows(this.ListSource);
	}

	onTypeId(rowData) {
		this.props.popNavBack();
		this.props.callback(rowData);
	}

	onBackPress() {
		this.props.popNavBack();
	}

	componentDidMount() {
		BackAndroid.addEventListener('hardwareBackPress', this.onBackPress.bind(this));
	}

	renderRow(rowData, sectionID, rowID) {
		return(
			<TouchableOpacity onPress={() => { this.onTypeId(rowData); }}
			 style={{ marginTop: 10, height: 40, backgroundColor: "#ffffff", borderTopWidth: 1, borderTopColor: '#e7e7e9', borderBottomWidth: 1, borderBottomColor: '#e7e7e9', 
                flexDirection: 'row', alignItems: 'center' }} >
			 	<View style={{ flex: 10, paddingLeft: 10, justifyContent: "center" }}>
					<Text allowFontScaling={false} style={[styles.content, { fontFamily: 'Gotham-Medium', textAlign: "left" }]}>{rowData.label}</Text>
				</View>
			</TouchableOpacity>
		);
	}

	render() {
		const render_TopBar = (
			<View
				style={{
					flexDirection: 'row',
					alignItems: 'center',
					justifyContent: 'center',
					backgroundColor: '#aac1ce',
					borderBottomWidth: 0,
					borderBottomColor: '#85D3DD',
					height: (Platform.OS === 'ios') ? 65 : 40 }}
			>

				<TouchableOpacity style={{ flex: 1, justifyContent: 'center' }} onPress={() => { this.props.popNavBack(); }}>
					<Text
						allowFontScaling={false} style={{
							fontFamily: 'Gotham-Medium',
							textAlign: 'center',
							fontSize: 16,
							color: '#000',
							marginTop: (Platform.OS === 'ios') ? 20 : 0
						}}
					>Back</Text>
				</TouchableOpacity>
				<TouchableOpacity style={{ flex: 2, justifyContent: 'center' }}>
					<Text
						allowFontScaling={false} style={{
							fontFamily: 'Gotham-Medium',
							textAlign: 'center',
							fontSize: 16,
							color: '#000',
							marginTop: (Platform.OS === 'ios') ? 20 : 0
						}}
					>Skill Level</Text>
				</TouchableOpacity>
				<TouchableOpacity style={{ flex: 1, justifyContent: 'center' }} onPress={() => {  }} >
					<Text
						allowFontScaling={false} style={{
							fontFamily: 'Gotham-Medium',
							textAlign: 'center',
							fontSize: 16,
							color: '#2996cc',
							marginTop: (Platform.OS === 'ios') ? 20 : 0
						}}
					></Text>
				</TouchableOpacity>
			</View>
			
		);

		return (
			<View style={{ flex: 1 }}>
				{render_TopBar}
				<KeyboardAwareScrollView
					resetScrollToCoords={{ x: 0, y: 0 }}
					scrollEnabled={true}
				>
					<ListView
						dataSource={this.state.ListDataSource}
						renderRow={this.renderRow}
					/>
				</KeyboardAwareScrollView>

			</View>

		);
	}
}

export default SelectSkillLevelScene;
