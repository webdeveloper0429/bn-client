import React, { Component, PropTypes } from 'react';
import { Alert, View, Image, TouchableOpacity, Text, ListView, RefreshControl, ScrollView, PanResponder, BackAndroid } from 'react-native';
import styles from './styles';
import AppConfig from 'AppConfig';
import AppStyles from 'AppStyles';
import { SearchBar, HeaderBar } from 'AppComponents';
import { FilterScene, ListDetailScene, } from 'AppScenes';
import { RequestApi, MakeCancelable, GlobalStorage } from 'AppUtilities';
import _, { isEqual } from 'lodash';

class ContactorDetailScene extends Component {
  static propTypes = {
    popBack: PropTypes.func.isRequired,
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      isRefreshing: false,
      ListDataSource: new ListView.DataSource({ rowHasChanged: (r1, r2) => !isEqual(r1, r2) }),
    };

    this.ListSource = [{'name':'ReviewHistory'}];
    this.state.ListDataSource = this.state.ListDataSource.cloneWithRows(this.ListSource);
  }

  componentDidMount() {
		//BackAndroid.addEventListener('hardwareBackPress', this.onBackPress.bind(this));
  }


  renderRow(rowData, sectionID, rowID) {
    let pStyle=[styles.rowContainer];
    const availableDataArray=[];
    const upcomingDataArray=[];
    const pastDataArray=[];
    const key = "dis-sep-title";
    availableDataArray.push(<View key={key} style={{ backgroundColor: '#ececec', height: 1, marginRight: 15 }} />);
    for(let i = 0; i < 10; i++) {
      availableDataArray.push(
        <TouchableOpacity
          style={[AppStyles.column, { paddingRight: 15, paddingVertical: 8 }]}
          key={'dis'+i}
        >
          <View key={'dis_view0_'+i} style={{ flex: 1, justifyContent: 'center', flexDirection: 'row' }}>
            <Image key={'dis_image'+i} style={{ flex: 1, width: 70, height: 15}} />
            <Text allowFontScaling={false} style={{flex: 2, fontSize: 11, marginTop: 2}} numberOfLines={1} ellipsizeMode="tail">February 26, 2017</Text>
          </View>
          <View key={'dis_view1_'+i} style={{ flex: 1, justifyContent: 'center', flexDirection: 'row' }}>
            <Text allowFontScaling={false} style={{flex: 1, fontSize: 11, marginTop: 2, textAlign: "right", marginRight: 5}} numberOfLines={1} ellipsizeMode="tail">UserSkillLevel</Text>
            <Text allowFontScaling={false} style={{flex: 2, fontSize: 11, marginTop: 2, textAlign: "left", marginLeft: 5}} numberOfLines={1} ellipsizeMode="tail">ScratchPlayer</Text>
          </View>
          <View key={'dis_view2_'+i} style={{ flex: 1, justifyContent: 'center', flexDirection: 'row' }}>
            <Text allowFontScaling={false} style={{flex: 1, fontSize: 11, marginTop: 2, textAlign: "right", marginRight: 5}} numberOfLines={1} ellipsizeMode="tail">Lesson Focus</Text>
            <Text allowFontScaling={false} style={{flex: 2, fontSize: 11, marginTop: 2, textAlign: "left", marginLeft: 5}} numberOfLines={1} ellipsizeMode="tail">Driver</Text>
          </View>
          <View key={'dis_view3_'+i} style={{ flex: 1, justifyContent: 'center', flexDirection: 'row' }}>
            <Text allowFontScaling={false} style={{flex: 1, fontSize: 11, marginTop: 2, textAlign: "right", marginRight: 5}} numberOfLines={1} ellipsizeMode="tail">Damon A said</Text>
            <Text allowFontScaling={false} style={{flex: 2, fontSize: 11, marginTop: 2, textAlign: "left", marginLeft: 5}} numberOfLines={5} ellipsizeMode="tail">  This guy is awesome! Very kind, dedicated to help his students. Just give him hundred percent trust and follow his instruction, and you will soon become  a single handicapper!</Text>
          </View>
        </TouchableOpacity>);
      const key = "dis-sep" + i;
      availableDataArray.push(<View key={key} style={{ backgroundColor: '#ececec', height: 1, marginRight: 15 }} />);
    }

    return (
      <View style={pStyle}>
        <View style={styles.listRow}>
          <TouchableOpacity style={styles.imageWrapper}>
            {/*<Image source={require('img/icon/icon_arrow.png')} style={styles.rowImage} />*/}
          </TouchableOpacity>
          <View style={styles.contentWrapper}>
            <Text allowFontScaling={false} style={[AppStyles.baseFont, styles.txtTitleDetail]} numberOfLines={1} ellipsizeMode="tail">
              {rowData.tagline} </Text>

            <View style={{ backgroundColor: '#ececec', height: 1, marginRight: 15, marginTop: 7 }} />

            {availableDataArray}
          </View>
        </View>
      </View>
    );
  }

	onBackPress() {
		this.props.popBack();
	}

	render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <View style={styles.leftImageTitleView}>
            <TouchableOpacity
              onPress={() => {this.props.popBack();}}
            >
            <Image source={require('img/icon/icon_back.png')} style={styles.leftImageTitleViewIcon} />
            </TouchableOpacity>
          </View>
          <View style={styles.centerImageTitleView}>
            <Text allowFontScaling={false} numberOfLines={1} ellipsizeMode="tail">
              Contactor Detail Page
            </Text>
          </View>
          <View style={styles.rightImageTitleView}>
            <TouchableOpacity>
              {/*<Image source={require('img/icon/icon_arrow.png')} style={styles.rightImageTitleViewIcon} />*/}
            </TouchableOpacity>
          </View>
        </View>

        <ScrollView>
          <Image style={{ width: 100, height: 100, marginLeft: 30, marginTop: 10}} />
          <Text allowFontScaling={false} style={{fontSize: 17, color: "black", marginLeft: 20, marginTop: 20}}>DERRICK</Text>
          <Text allowFontScaling={false} style={{fontSize: 12, color: "black", marginLeft: 20, marginTop: 5}}>16 Reviews</Text>
          <TouchableOpacity style={{width: 200, height: 30, backgroundColor: "orange", marginLeft: 20, marginTop: 10, justifyContent: "center"}}>
            <Text allowFontScaling={false} style={{textAlign: "center", fontSize: 12, borderRadius: 5, color: "white"}}>CONTACT INSTRUCTOR</Text>
          </TouchableOpacity>
          <Text allowFontScaling={false} style={{fontSize: 17, color: "black", marginLeft: 20, marginTop: 20}}>Books</Text>
          <Text allowFontScaling={false} style={{fontSize: 17, color: "black", marginLeft: 20, marginTop: 20}}>Rates</Text>
          <Text allowFontScaling={false} style={{fontSize: 17, color: "black", marginLeft: 20, marginTop: 20}}>Reviews</Text>

          <ListView
            dataSource={this.state.ListDataSource}
            renderRow={this.renderRow}
          />
        </ScrollView>
      </View>
    );
  }
}

export default ContactorDetailScene;
