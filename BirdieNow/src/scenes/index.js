/**
 * @providesModule AppScenes
 */
import {
  Text
} from 'react-native';
Text.defaultProps.allowFontScaling=false;
export { default as SplashScene }  from './SplashScene';
export { default as StartLoginView } from './StartLoginView';
export { default as MainScene } from './MainScene';
export { default as PaymentView } from './MainScene/EtcView/PaymentView';
export { default as VideoView } from './MainScene/EtcView/VideoView';
export { default as VideoWebView } from './MainScene/EtcView/VideoWebView';
export { default as SearchResultsView } from './SearchResultsView'; 
export { default as InstructorScene } from './InstructorScene';
export { default as InstructorContact } from './InstructorScene/InstructorContact';
export { default as FullCalendarScene } from './InstructorScene/FullCalendarScene';
export { default as PaymentScene } from './InstructorScene/PaymentScene';
export { default as BookScene } from './InstructorScene/BookScene';
export { default as BookPaymentScene } from './InstructorScene/BookPaymentScene';
export { default as ReviewScene } from './InstructorScene/ReviewScene';
export { default as LoginNavScene } from './LoginNavScene';
export { default as RegisterNavScene } from './RegisterNavScene';
export { default as ForgotPasswordScene } from './ForgotPasswordScene';
export { default as ChangePasswordScene } from './ChangePasswordScene';
export { default as WriteReviewScene } from './WriteReviewScene';
export { default as WriteReviewSceneNew } from './WriteReviewSceneNew';
export { default as ContactorDetailScene } from './ContactorDetailScene';
export { default as AppointmentsView } from './MainScene/AppointmentsView';
export { default as FavoritesView } from './MainScene/FavoritesView';
export { default as HomeView } from './MainScene/HomeView';
export { default as ProfileView } from './MainScene/ProfileView';
export { default as PurchaseLessonsView } from './MainScene/PurchaseLessonsView';
export { default as EtcView } from './MainScene/EtcView';
export { default as SearchView } from './MainScene/SearchView';
export { default as CancelAppointment } from './MainScene/AppointmentsView/CancelAppointmentView';
export { default as SelectSkillLevelScene } from './WriteReviewSceneNew/SelectSkillLevelScene';