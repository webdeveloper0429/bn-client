import React, { PropTypes, Component } from 'react';
import { ActivityIndicator, Alert, Platform, Keyboard, TextInput, View, StatusBar, Text, TouchableHighlight, TouchableOpacity } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styles from './styles';
import _, { isEqual } from 'lodash';
import { MainScene, ProfileView } from 'AppScenes';
import { SignUp } from 'AppUtilities';
import AppConfig from 'AppConfig';

class StartRegisterViewRetired extends Component {
  static propTypes = {
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      isLoading: false,
      email: '',
      password: '',
      confirmPassword: '',
      firstName: '',
      lastName: '',
      phone: ''
    };

    this.OnSignUp = this.OnSignUp.bind(this);
    this.OnChangePhone = this.OnChangePhone.bind(this);
  }

  OnBack() {
    //This page is used in two places, one inside Main Tabs and one in start, therefore two different calls.
    if (this.props.passProps.source == 'profile')
        this.props.popNavBack();
      else
        this.props.popBack();
  }

  OnChangePhone(text) {
    let phone = text.split('(').join('');
    phone = phone.split(')').join('');
    phone = phone.split('-').join('');
    phone = phone.split(' ').join('');
    if (phone.length > 6) {
      phone = `(${phone.substr(0, 3)}) ${phone.substr(3, 3)}-${phone.substr(6)}`;
    } else if (phone.length > 3) {
      phone = `(${phone.substr(0, 3)}) ${phone.substr(3, 3)}`;
    } else if (phone.length > 0) {
      phone = `(${phone.substr(0, 3)}`;
    } else {
      phone = '';
    }
    this.setState({ phone });
  }

  OnSignUp(){
    Keyboard.dismiss();

    if (this.state.firstName == '') {
      Alert.alert("First Name is required");
      return
    }

    if (this.state.lastName == '') {
      Alert.alert("Last Name is required");
      return
    }

    if (this.state.email == '') {
      Alert.alert("Email is required");
      return
    }
    
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(this.state.email)) {
      Alert.alert("Email address is not valid");
      return
    }

    if (this.state.password == '') {
      Alert.alert("Password is required");
      return
    }

    if (this.state.confirmPassword == '') {
      Alert.alert("Confirm Password is required");
      return
    }

    if (this.state.password != this.state.confirmPassword) {
      Alert.alert("Passwords do not match");
      return
    }

    if (this.state.phone == '') {
      Alert.alert("Phone number is required");
      return
    }

    this.setState({ isLoading: true });

    let phone = this.state.phone
    phone = phone.replace("(", "");
    phone = phone.replace(")", "");
    phone = phone.replace("-", "");
    phone = phone.replace(" ", "");

    var userData = {
      email: this.state.email, 
      password: this.state.password, 
      confirmPassword: this.state.confirmPassword,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      phoneNumber: phone
    };
  
    SignUp(userData)
      .then((response) => {          
        if (response.status == 200) {
          if (this.props.passProps.source == 'profile') {
            Alert.alert("Welcome","Thanks for registering with BirdieNow! Please log in with your new account.")
            AppConfig.global_isLoggedIn = false;
            this.props.popNavBack();
          }
          else {
            Alert.alert("Welcome","Thanks for registering with BirdieNow!")
            AppConfig.global_isLoggedIn = false;
            this.props.popBack();
            this.props.resetToScene(MainScene);
          }
        } else {          
          var message = JSON.stringify(response.modelState).replace(/"/g, "").replace("{", "").replace("}", "").replace("model.", "").replace("[", "").replace("]", "");
          if (Platform.OS === 'ios') {
            message = message.replace(":", "\n\n");
          } else {
            message = message.replace(":", ": ");
          }
          Alert.alert(message);
        }
        this.setState({ isLoading: false });
      })
      .catch(error => {
        Alert.alert(error.message);
        this.setState({ isLoading: false });
      });
  }

  render() {
    const render_TopBar = (
      <View style={{
        flexDirection: 'row', alignItems: "center", justifyContent: "center",
        backgroundColor: '#fafafa', borderBottomWidth: 1, borderBottomColor: '#D3D3D3',
        height: (Platform.OS === 'ios') ? 65 : 40 }}>

        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={() => { this.OnBack(); }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#000',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Back</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 2, justifyContent: "center" }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#000',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Register</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={() => { this.OnSignUp(); }} >
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#32b3fa',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Submit</Text>
        </TouchableOpacity>
      </View>
    );

    return (
    <View style={styles.container}>
      {render_TopBar}
      <KeyboardAwareScrollView
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={true} >
        <View>
          <View style={[styles.inputWrapper, {marginTop: 10, borderTopWidth: 1}]}>
            <TextInput underlineColorAndroid='transparent' style={styles.inputEmail} 
              placeholder="First Name" 
              placeholderTextColor="#999" onChangeText={(text) => {this.state.firstName = text}} />
          </View>
          <View style={[styles.inputWrapper, {}]}>
            <TextInput underlineColorAndroid='transparent' style={styles.inputEmail} 
            placeholder="Last Name" 
            placeholderTextColor="#999" onChangeText={(text) => {this.state.lastName = text}} />
          </View>
           
          <View style={[styles.inputWrapper, {}]}>
            <TextInput
            underlineColorAndroid='transparent'
            style={styles.inputEmail}
            placeholder="Email Address"
            placeholderTextColor="#999"
            keyboardType='email-address'
            autoCapitalize="none"
            onChangeText={(text) => {this.state.email = text}} />
          </View>
          <View style={[styles.inputWrapper, { }]}>
            <TextInput
              underlineColorAndroid='transparent'
              style={styles.inputEmail}
              placeholder="Password"
              placeholderTextColor="#999"
              secureTextEntry={true}
              onChangeText={(text) => {this.state.password = text}}
            />
          </View>
          <View style={[styles.inputWrapper, {}]}>
            <TextInput
              underlineColorAndroid='transparent'
              style={styles.inputEmail}
              placeholder="Confirm Password"
              placeholderTextColor="#999"
              secureTextEntry={true}
              onChangeText={(text) => {this.state.confirmPassword = text}}
            />
          </View>     
          <View style={[styles.inputWrapper, {}]}>
            <TextInput
              underlineColorAndroid='transparent'
              style={styles.inputEmail}
              placeholder="Phone Number"
              placeholderTextColor="#999"
              keyboardType="phone-pad"
              maxLength={14}
              onChangeText={(text) => {this.OnChangePhone(text); }}
              value={this.state.phone} />
          </View>
          <View style={{ paddingTop: 20, paddingLeft: window.width * 0.05  }}>
            <TouchableOpacity
              onPress={() => { this.OnSignUp(); }}
              style={{ width: window.width * 0.9, height: 35, borderRadius: 5, backgroundColor: '#32b3fa', justifyContent: 'center' }}>
              <Text style={{ alignSelf: 'center', fontSize: 16, fontWeight: 'bold', color: '#ffffff', fontFamily: 'Gotham-Book' }}>Submit</Text>
            </TouchableOpacity>
          </View>      
        </View>

        
        </KeyboardAwareScrollView>

        {this.state.isLoading ?
          <View style={styles.loadingScene}>
            <ActivityIndicator
              animating={true}
              size="small"
              color="white"
            />
          </View> : null}

          
      </View>
    );
  }
}

export default StartRegisterViewRetired;
