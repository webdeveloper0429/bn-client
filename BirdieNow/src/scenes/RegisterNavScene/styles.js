import { StyleSheet } from 'react-native';
import Dimensions from 'Dimensions';

const window = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    // flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    backgroundColor: '#F9F9F9',
    //alignItems: 'center',
    // flex: 1,
    // flexDirection: 'row',
    // justifyContent: 'center',
    //alignItems: 'center',
    //justifyContent: 'flex-start',
    paddingBottom: 0,
  }, 
  topLabel: {
    alignSelf: 'stretch',
    color: '#7e7e7e',
    fontSize: 16,
    fontWeight: 'bold',
  },

  inputWrapper: {
    backgroundColor: '#ffffff',
    borderColor: '#f4f4f4',
    borderWidth: 0,
    borderBottomWidth: 1,
  },
  inputEmail: {
    //width: window.width * 0.9,
    height: 40,
    fontSize: 13,
    //alignSelf: 'stretch',
    //flex: 1,
    backgroundColor: '#ffffff',
    borderWidth: 0,
    //borderColor: '#f4f4f4',
    //borderRadius: 5,
    paddingVertical: 0,
    paddingHorizontal: 0,
    paddingLeft: 10
  },
  inputPassword: {
    marginTop: 10,
    alignSelf: 'stretch',
    height: 40,
    backgroundColor: '#fff',
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: '#008000',
    borderRadius: 5,
  },
  forgotLabel: {
    marginTop: 15,
    color: '#fff',
    fontSize: 15,
    alignSelf: 'stretch'
  },
  btnLogin: {
    color: '#fff',
    fontSize: 16,
    borderWidth: 2,
    borderColor: '#fff',
    borderRadius: 5,
    paddingLeft: 10,
    paddingRight: 7,
    paddingTop: 10,
    paddingBottom: 7,
  },
  loadingScene: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: window.width,
    height: window.height,
    alignSelf: 'stretch',
    backgroundColor: 'rgba(0,0,0,0.5)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  topView:{
    position: 'absolute',
    flexDirection: 'row',
    width: window.width,
    height: 65,
    backgroundColor: '#aac1ce',
    borderBottomWidth: 1,
    borderBottomColor:'#D3D3D3',
    justifyContent: "center",
    top: 0
  },
});
