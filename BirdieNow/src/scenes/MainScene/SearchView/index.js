import React, { Component, PropTypes } from 'react';
import { ActivityIndicator, Platform, Alert, View, Image, TouchableOpacity, Text, TextInput, 
  InteractionManager, ListView, RefreshControl, Navigator, Dimensions, Keyboard } from 'react-native';
import styles from './styles';
import AppConfig from 'AppConfig';
import AppStyles from 'AppStyles';
import { search, searchResult, profile } from 'AppComponents';
import { FilterScene, ListDetailScene, SearchResultsView, InstructorScene } from 'AppScenes';
import { RequestApi, MakeCancelable, GlobalStorage } from 'AppUtilities';
import _, { isEqual } from 'lodash';
import { GoogleAnalyticsTracker } from 'react-native-google-analytics-bridge';



class SearchView extends Component {
  static propTypes = {
    pushNavScene: PropTypes.func.isRequired,
    pushScene: PropTypes.func.isRequired,
    popNavBack: PropTypes.func.isRequired,
    popNavNBack: PropTypes.func,
    popNavMultiBack: PropTypes.func,
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      inputCityString: '',
      inputInstructorString:'',
      searchResultsType:'',
      data:[],     
      showloading: false
    };

    let { width, height } = Dimensions.get('window');
    this.r_width =  width / 356;
    this.r_height = height / 647;
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.navState = 0;
    this.onCheckFocus = ::this.onCheckFocus;
    this.onFocusSearchBar = ::this.onFocusSearchBar;
    this.onTypeChangeSearchCity = ::this.onTypeChangeSearchCity;
    this.onTypeChangeSearchInstructor = ::this.onTypeChangeSearchInstructor;
    this.renderSearchRow = ::this.renderSearchRow;
    this.onSubmitCitySearch = ::this.onSubmitCitySearch;
    this.onSubmitInstructorSearch = ::this.onSubmitInstructorSearch;
    this.onSubmitCurrentLocSearch = ::this.onSubmitCurrentLocSearch;
    this.onCancelCitySearch = ::this.onCancelCitySearch;
    this.onCancelInstructorSearch = ::this.onCancelInstructorSearch;    
    this.CallBackSearchResultsView = ::this.CallBackSearchResultsView;
    this.CallBackInstructorScene = ::this.CallBackInstructorScene;
    this.didAppear = ::this.didAppear;
    this.focusStart = ::this.focusStart;
  }

  didAppear() {
    if (this.refSearchResultsView != null) {
      this.refSearchResultsView.handlePopCheck(1);
    } else {
      if (this.refInstructorScene != null) {
        this.refInstructorScene.handlePopCheck();
      }
    }
  }

  focusStart() {

    //This is a fix for an android bug.  During animation of popping search tab back in place, screen gets stuck
    InteractionManager.runAfterInteractions(() => {
      this._textInputCity.focus();
      if (this.refSearchResultsView != null) {
        this.refSearchResultsView.handlePopCheck(0);
        this._textInputCity.focus();
      } else {
        if (this.refInstructorScene != null) {
          this.refInstructorScene.handlePopCheck();
          this._textInputCity.focus();
        }
      }

      this.setState({ data: [], inputCityString: '', inputInstructorString: '' });
    });
  }

  componentDidMount() {

    let tracker = new GoogleAnalyticsTracker(AppConfig.googleAnalyticsTrackerId);
    tracker.trackScreenView('SearchView');

    if (Platform.OS == 'android') {
      InteractionManager.runAfterInteractions(() => {

        setTimeout(() => {
          this._textInputCity.focus();
        }, 500);

      });
    }
    else {
      this._textInputCity.focus();
    } 
    
  }

  onCheckFocus() {
    
    if (AppConfig.checkFocus) {
      if (this.refSearchResultsView != null) {
        this.refSearchResultsView.checkFocus();
      } else {
        this._textInputCity.focus();
        AppConfig.checkFocus = false;
      }
    } else if (AppConfig.checkGPS) {
      if (this.refSearchResultsView != null) {
        this.refSearchResultsView.checkFocus();
      } else {
        //go to gps
        this.props.pushNavScene(SearchResultsView, {ref:(c) => { this.refSearchResultsView = c; } , callback: this.CallBackSearchResultsView});
        // AppConfig.checkGPS = false;
        // this.onSubmitCurrentLocSearch();
      }
    } else if (AppConfig.checkCity) {
      if (this.refSearchResultsView != null) {
        this.refSearchResultsView.checkFocus();
      } else {
        this.props.pushNavScene(SearchResultsView, {ref:(c) => { this.refSearchResultsView = c; } , callback: this.CallBackSearchResultsView});
      }
    }
  }

  CallBackSearchResultsView() {
    if (this.state.searchResultsType == 'instructor')
    {
      InteractionManager.runAfterInteractions(() => {
        this._textInputInstructor.focus();
      });
      
    }
    else
    {
      InteractionManager.runAfterInteractions(() => {
        this._textInputCity.focus();
      });
      
    }
    // else if (this.state.searchResultsType == 'gps')
    // {
    //   this._textInputCity.focus();
    // }
    //this.navState = state;
    //alert(param1);
    // setTimeout(() => {
    //   this._textInputCity.focus();
    // }, 1000);
  }

  CallBackInstructorScene() {
    //this._textInputCity.focus();
  }

  renderSearchRow(rowData, rowID){
    const renderIcon = [];
    let h = 0;
    if (this.state.searchResultsType == 'instructor') {
      h = 85 * this.r_width;

      if (rowData.isProfilePhoto) {
        renderIcon.push(
          <Image
            key="render_location"
            style={{ width: 70 * this.r_width, height: 70 * this.r_width, borderRadius: 35 * this.r_width, marginLeft: 5 }}
            //defaultSource={require('img/profile/profile.png')}
            //loadingIndicatorSource={{uri: 'https://www.birdienow.com/img/instructor/profile/165616.jpg'}}
            loadingIndicatorSource={require('img/profile/profile.png')}
            source={{ uri: 'https://www.birdienow.com/img/instructor/profile/' + rowData.id + '.jpg' }}
          />
        );
      }
      else {
        renderIcon.push(
          <Image
            key="render_location"
            style={{ width: 70 * this.r_width, height: 70 * this.r_width, borderRadius: 35 * this.r_width, marginLeft: 5 }}
            source={require('img/profile/profile.png')}
          />
        );
      }



      if (rowData.isPga) {
        renderIcon.push(
          <Image
            key="render_pga"
            style={{ width: 30, height: 30, position: 'absolute', left: 0, bottom: 0 }}
            source={require('img/image/pgaImage.png')}
          />
        );
      }

      /*if (this.state.data[rowID].checkImageFlag) {
        renderIcon.push(
          <Image
            key="render_location"
            style={{width: 70 * this.r_width, height: 70 * this.r_width, borderRadius:35 * this.r_width,marginLeft:5}}
            source={{uri: 'https://www.birdienow.com/img/instructor/profile/'+ this.state.data[rowID].id +'.jpg'}}
          />
        );
        if (rowData.isPga) {
          renderIcon.push(
            <Image
              key="render_pga"
              style={{width: 40 * this.r_width, height: 40 * this.r_width, position:'absolute',left:0,bottom:0}}
              source={require('img/image/pgaImage.png')}
            />
          );
        }
      } else {
        renderIcon.push(
          <Image
            key="render_location"
            style={{width: 70 * this.r_width, height: 70 * this.r_width, borderRadius:35 * this.r_width,marginLeft:5}}
            source={require('img/profile/profile.png')}
          />
        );
        if (rowData.isPga) {
          renderIcon.push(
            <Image
              key="render_pga"
              style={{width: 40 * this.r_width, height: 40 * this.r_width, position:'absolute',left:0,bottom:0}}
              source={require('img/image/pgaImage.png')}
            />
          );
        }
      }*/
    } else {
      h = 40;
      renderIcon.push(
        <Image
          key="displayIcon"
          style={{marginLeft:24 * this.r_width,width:22 * this.r_height, height:22 * this.r_height}}
          source={require('img/image/search-location.png')}/>
      );
    }

    return(
      <View>
        <TouchableOpacity onPress={()=>this.onSubmitCitySearch(rowData, true)}>
          <View style={{height:h, alignItems:'center', borderBottomWidth:1, borderBottomColor:'#E0DEDF', flexDirection:'row'}}>
            {renderIcon}
            <Text allowFontScaling={false} style={{fontSize: 13,  color:'#000',marginLeft:5, fontFamily: 'Gotham-Book'}}>
              {rowData.display}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  onCancelCitySearch() {
    //alert('onCancelCitySearch');
    this.setState({data:[], inputCityString: "", searchResultsType: ''});
    InteractionManager.runAfterInteractions(() => {
        Keyboard.dismiss();
    });
    
  }

  onCancelInstructorSearch() {
    //alert('onCancelInstructorSearch');
    this.setState({data:[], inputInstructorString: "", searchResultsType: ''});
    InteractionManager.runAfterInteractions(() => {
        Keyboard.dismiss();
    });
  }

  onSubmitCurrentLocSearch() {
    //this.setState({searchResultsType: 'gps'});
    this.props.pushNavScene(SearchResultsView, { passProps: { searchType: 'gps', hasDates: false }, ref: (c) => { this.refSearchResultsView = c; }, callback: this.CallBackSearchResultsView });
    InteractionManager.runAfterInteractions(() => {
      Keyboard.dismiss();
    });
  }

  onTypeChangeSearchCity(city){ 
    if (city == "") {
      this.setState({inputCityString: city, searchResultsType: 'city', data:[]});
    } else {
      this.setState({inputCityString: city, searchResultsType: 'city', showloading: true});
      fetch(AppConfig.apiUrl + '/api/TypeAheadSearchCity?searchString='+ city
        +'&numResults=20')
        .then((response) => response.json())
        .then((responseData) => {         
            this.setState({data:responseData, showloading: false})
        });
    }   
  }

  onTypeChangeSearchInstructor(instructor) {    
    if (instructor == "") {
      this.setState({inputInstructorString: instructor, searchResultsType: 'instructor', data:[]});
    } else {
      this.setState({inputInstructorString: instructor, searchResultsType: 'instructor', showloading: true});
      fetch(AppConfig.apiUrl + '/api/TypeAheadSearchInstructor?searchString='+instructor
        +'&numResults=20')
        .then((response) => response.json())
        .then((responseData) => {          
            this.setState({data:responseData, showloading: false})         
        });
    }
  }

  onSubmitCitySearch(rowData, flag){
    //alert('onPressButton');
    
    if (flag) {
      if (this.state.searchResultsType == 'instructor') { //instructor search
        //TODO: will have to delete globabl variables and passPRops
        // AppConfig.insPGA = rowData.isPga;
        // AppConfig.insID = rowData.id;
        // AppConfig.isCurrentLoc = false;
        // this.props.pushNavScene(InstructorScene, {callback: this.CallBackInstructorScene});

        this.props.pushNavScene(InstructorScene, {ref:(c) => { this.refInstructorScene = c; } , passProps: { instructor: rowData, id: rowData.id, lessonTypeId: rowData.lessonTypeId }, callback: this.CallBackInstructorScene });
      } else { //city search        
        //alert("Will call city name search result page here: " + rowData.code);       
        this.setState({searchResultsType: 'city'});
        this.props.pushNavScene(SearchResultsView, {ref:(c) => { this.refSearchResultsView = c; } , passProps: {searchType: 'city', zip: rowData.code, city: rowData.display} ,callback: this.CallBackSearchResultsView});
      } 
    } else {  //zip Code Search
      if (parseInt(rowData)) {
        //alert("Will call zip code search result page here: " + rowData);
        this.setState({searchResultsType: 'city'});  //Importatnt Note: this is not a mistake, there is only city or instructor searchResultsType
        this.props.pushNavScene(SearchResultsView, {ref:(c) => { this.refSearchResultsView = c; } , passProps: {searchType: 'zip', zip: rowData, city: ''} ,callback: this.CallBackSearchResultsView});
      }
    }

    InteractionManager.runAfterInteractions(() => {
        Keyboard.dismiss();
    });
  }

  onSubmitInstructorSearch(searchString, flag) {
    //alert('onPressButton_Instructor');

    //DISABLE THE INSTRUCTOR SEARCH FOR NOW
    return;
    
    if (flag) {
    } else {
      //alert("Will call search result page here for instructors: " + searchString);
      if (searchString != '') {    
        this.props.pushNavScene(SearchResultsView, {ref:(c) => { this.refSearchResultsView = c; } , passProps: { searchType: 'ins', query: searchString }, callback: this.CallBackSearchResultsView });
      }
    }

    InteractionManager.runAfterInteractions(() => {
        Keyboard.dismiss();
    });
  }

  onFocusSearchBar(type) {
    if (type == 'city') {
      this.setState({searchResultsType: 'city', data:[], inputCityString: '', inputInstructorString: ''});
    }
    else{
      this.setState({searchResultsType: 'instructor', data:[], inputCityString: '', inputInstructorString: ''});
    }
  }

  render() {
    //alert('render searchView');
    const render_cancelsearch_city = [];
    if (this.state.searchResultsType == 'city' && this.state.inputCityString.length > 0) {
      render_cancelsearch_city.push(
        <TouchableOpacity
          key="render_cancelsearch_city"
          style={{marginRight:5 * this.r_width, width:22 * this.r_height, height:22 * this.r_height}}
          onPress={() => {this.onCancelCitySearch();}}>
          <Image style={{marginTop: 3 * this.r_height, width:16 * this.r_height, height:16 * this.r_height}} source={require('img/searchimage/cancelsearch.png')}/>
        </TouchableOpacity>
      );
    }

    const render_cancelsearch_instructor = [];
    if (this.state.searchResultsType == 'instructor' && this.state.inputInstructorString.length > 0) {
      render_cancelsearch_instructor.push(
        <TouchableOpacity
          key="render_cancelsearch_instructor"
          style={{marginRight:5 * this.r_width, width:22 * this.r_height, height:22 * this.r_height}}
          onPress={() => {this.onCancelInstructorSearch();}}>
          <Image style={{marginTop: 3 * this.r_height, width:16 * this.r_height, height:16 * this.r_height}} source={require('img/searchimage/cancelsearch.png')}/>
        </TouchableOpacity>
      );
    }

    return (
      <View style={styles.container}>
        <View style={{flex:1}}>
          <View style={{alignItems:'center', backgroundColor:'#aac1ce', borderBottomWidth:0 * this.r_height, borderBottomColor:'#85D3DD'}}>
            <View style={{marginTop: (Platform.OS === 'ios') ? 20 : 0, height:10}} />
            {/*<Text allowFontScaling={false} style={{marginTop:0 * this.r_height, height: 25, alignItems:'center', fontSize: 13 * this.r_height, color: '#ffffff', fontFamily: 'Gotham-Bold'}}>
              SearchView1
            </Text>*/}

            {
              this.state.searchResultsType == 'city' || this.state.searchResultsType == '' || this.state.searchResultsType == 'instructor' ?
                <View style={{ marginTop: 0, width: 325 * this.r_width, height: 25 * this.r_width, backgroundColor: '#ffffff', alignItems: 'center', borderRadius: 10, flexDirection: 'row' }}>
                  <Image style={{ marginLeft: 5 * this.r_width, width: 22 * this.r_height, height: 22 * this.r_height }} source={require('img/image/search-location.png')} />
                  <TextInput
                    ref={textInput => (this._textInputCity = textInput)}
                    underlineColorAndroid='transparent'
                    placeholder="Search City or Zip"
                    style={{ height: 25 * this.r_width, flex: 1, fontSize: 13, marginLeft: 5, paddingVertical: 0 }}
                    onChangeText={(userInput) => this.onTypeChangeSearchCity(userInput)}
                    onFocus={() => { this.onFocusSearchBar('city'); }}
                    value={this.state.inputCityString}
                    onSubmitEditing={(event) => this.onSubmitCitySearch(this.state.inputCityString, false)}
                  />
                  {render_cancelsearch_city}
                </View> : null
            }

            {
              this.state.searchResultsType == '' || this.state.searchResultsType == 'city' || this.state.searchResultsType == 'instructor' ?
                <View style={{ height: 10 }} /> : null
            }

            {
              this.state.searchResultsType == 'instructor' || this.state.searchResultsType == '' || this.state.searchResultsType == 'city' ?
                <View style={{ marginTop: 0, width: 325 * this.r_width, height: 25 * this.r_width, backgroundColor: '#ffffff', alignItems: 'center', borderRadius: 10, flexDirection: 'row' }}>
                  <Image style={{ marginLeft: 5 * this.r_width, width: 22 * this.r_height, height: 22 * this.r_height }} source={require('img/image/search-instructor.png')} />
                  <TextInput
                    ref={textInput => (this._textInputInstructor = textInput)}
                    underlineColorAndroid='transparent'
                    placeholder="Search Instructor"
                    style={{ height: 25 * this.r_width, flex: 1, fontSize: 13, marginLeft: 5, paddingVertical: 0 }}
                    onChangeText={(userInput) => this.onTypeChangeSearchInstructor(userInput)}
                    onFocus={() => { this.onFocusSearchBar('instructor'); }}
                    value={this.state.inputInstructorString}
                    onSubmitEditing={(event) => this.onSubmitInstructorSearch(this.state.inputInstructorString, false)}
                  />
                  {render_cancelsearch_instructor}
                </View> : null

            }

             <View style={{height:10}} /> 


            
          </View>

          {/*<View style={{height:20,justifyContent:'center', backgroundColor: '#E0DEDF'}}>
            <Text allowFontScaling={false} style={{marginLeft:20,fontSize: 11, color: '#686868', fontFamily: 'Gotham-Bold'}}>
              RESULTS
            </Text>
          </View>*/}
            <TouchableOpacity
              onPress={() => {this.onSubmitCurrentLocSearch();}}>
              <View
                style={{height:40, alignItems:'center', borderBottomWidth:1, borderBottomColor:'#E0DEDF', flexDirection:'row'}}>
                <Image
                  style={{marginLeft:24 * this.r_width,width:22 * this.r_height, height:22 * this.r_height}}
                  source={require('img/icon/location.png')}/>
                <Text allowFontScaling={false} style={{fontSize: 13,  color:'#aac1ce',marginLeft:5, fontFamily: 'Gotham-Bold'}}>
                  USE MY CURRENT LOCATION
                </Text>
              </View>
            </TouchableOpacity>
            {
              Platform.OS === 'ios' ?
                <ListView
                  keyboardShouldPersistTaps="always"
                  style={{flex:1, height:620 * this.r_height}}
                  dataSource={this.ds.cloneWithRows(this.state.data)}
                  renderRow={(rowData, sectionID, rowID) => this.renderSearchRow(rowData, rowID)}
                  enableEmptySections={true}
                />
                :
                <ListView
                  keyboardShouldPersistTaps="always"
                  style={{flex:1, height:647 * this.r_height}}
                  dataSource={this.ds.cloneWithRows(this.state.data)}
                  renderRow={(rowData, sectionID, rowID) => this.renderSearchRow(rowData, rowID)}
                  enableEmptySections={true}
                />
            }
        </View>
        {this.state.showloading ?
          <View
            style={{position:'absolute', top: 0, left:0, bottom:0, right:0, alignItems:'center', justifyContent:'center', alignSelf: 'stretch', backgroundColor: 'transparent'}}
          >
            <ActivityIndicator
              animating={true}
              size="small"
              color="gray"
            />
          </View>:null}
      </View>
    );
  }
}

export default SearchView;
