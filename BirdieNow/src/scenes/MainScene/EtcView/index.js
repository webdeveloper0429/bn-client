import React, { Component, PropTypes } from 'react';
import { Alert, View, Text, TouchableHighlight, Image } from 'react-native';
import styles from './styles';
import { PaymentView, VideoWebView, VideoView, StartLoginView, LoginNavScene, RegisterNavScene  } from 'AppScenes';
import { GlobalStorage } from 'AppUtilities';
import AppConfig from 'AppConfig';
import ImagePicker from 'react-native-image-crop-picker';

class ProfileView extends Component {
  static propTypes = {
    pushScene: PropTypes.func,
  };

  constructor(props, context) {
    super(props, context);

    this.state = {
      image: null,
      loginStatus: !AppConfig.global_isLoggedIn,
    };

    this.OnPayment = this.OnPayment.bind(this);
    this.OnAvatar = this.OnAvatar.bind(this);
    this.OnVideo = this.OnVideo.bind(this);
    this.OnVideoWeb = this.OnVideoWeb.bind(this);
    this.RegisterCallback = this.RegisterCallback.bind(this);
    this.LoginCallback = this.LoginCallback.bind(this);
    this.GoToLogin = this.GoToLogin.bind(this);
    this.GoToRegister = this.GoToRegister.bind(this);
    this.OnLogout = this.OnLogout.bind(this);
  }

  componentDidMount() {
    this.setState({loginStatus: !AppConfig.global_isLoggedIn});
  }

  //user functions
  RegisterCallback() {
    this.setState({loginStatus: !AppConfig.global_isLoggedIn});
  }

  LoginCallback() {
    this.setState({loginStatus: !AppConfig.global_isLoggedIn});
  }

  GoToLogin() {
    this.props.pushNavScene(LoginNavScene, { callBack: this.LoginCallback});
  }

  OnLogout() {
    AppConfig.global_isLoggedIn = false;
    GlobalStorage.setItem("storage_user", "");
    GlobalStorage.setItem("storage_password", "");
    AppConfig.global_userToken = "";
    this.setState({loginStatus: !AppConfig.global_isLoggedIn});
    GlobalStorage.setItem("storage_loggedIn", "false");
  }

  GoToRegister() {
    this.props.pushNavScene(RegisterNavScene, { callBack: this.RegisterCallback});
  }

  OnAvatar() {
    ImagePicker.openPicker({
      width: 500,
      height: 500,
      cropping: true
    }).then(image => {
      this.setState({ image: { uri: image.path, width: image.width, height: image.height, mime: image.mime } });
    }).catch(error => {
      Alert.alert(error.message);
    });
  }

  OnVideo() {
    this.props.pushScene(VideoView);
  }

  OnVideoWeb() {
    this.props.pushScene(VideoWebView);
  }

  OnPayment() {
    this.props.pushScene(PaymentView);
  }

  render() {

    let RenderUser = [];
    if (!this.state.loginStatus) { //logged in
      RenderUser.push(
        <View key="user_authorized">
          <TouchableHighlight
            onPress={() => {this.OnLogout();}}
            underlayColor="#bde7ff"
          >
            <Text allowFontScaling={false} style={styles.btnLogin}>Logout</Text>
          </TouchableHighlight>
        </View>
      );
    } else {  //logged out, free account
      RenderUser.push(
        <View key="user_free">
          <TouchableHighlight
            onPress={() => {this.GoToLogin();}}
            underlayColor="#bde7ff"
          >
            <Text allowFontScaling={false} style={styles.btnLogin}>Login</Text>
          </TouchableHighlight>

          <TouchableHighlight
            onPress={() => {this.GoToRegister();}}
            underlayColor="#bde7ff"
            style={{ marginTop: 10 }}
          >
            <Text allowFontScaling={false} style={styles.btnLogin}>Register</Text>
          </TouchableHighlight>
        </View>
      );
    }

    return (
      <View style={styles.container}>
        {/*{RenderUser}*/}

        <TouchableHighlight
          onPress={this.props.popBack}
          underlayColor="#bde7ff"
        >
          <Text allowFontScaling={false} style={styles.btnLogin}>Back</Text>
        </TouchableHighlight>


        <TouchableHighlight
          onPress={() => {this.OnPayment();}}
          underlayColor="#bde7ff"
          style={{ marginTop: 10 }}
        >
          <Text allowFontScaling={false} style={styles.btnLogin}>Payment Test</Text>
        </TouchableHighlight>
{/*
        <TouchableHighlight
          onPress={() => {this.OnAvatar();}}
          underlayColor="#bde7ff"
          style={{ marginTop: 10 }}
        >
          <Text allowFontScaling={false} style={styles.btnLogin}>Select avatar photo</Text>
        </TouchableHighlight>*/}

        <TouchableHighlight
          onPress={() => {this.OnVideoWeb();}}
          underlayColor="#bde7ff"
          style={{ marginTop: 10 }}
        >
          <Text allowFontScaling={false} style={styles.btnLogin}>Webview Video Playback</Text>
        </TouchableHighlight>

        <TouchableHighlight
          onPress={() => {this.OnVideo();}}
          underlayColor="#bde7ff"
          style={{ marginTop: 10 }}
        >
          <Text allowFontScaling={false} style={styles.btnLogin}>Native Video Playback</Text>
        </TouchableHighlight>

        {/*<Image style={{ width: 100, height: 100, resizeMode: 'contain', marginTop: 15 }} source={this.state.image} />*/}
      </View>
    );
  }
}

export default ProfileView;
