import React, { Component, PropTypes } from 'react';
import { Alert, View, Text,   TouchableOpacity, WebView, BackAndroid } from 'react-native';
import styles from './styles';
import { RequestStripeRegister } from 'AppUtilities';
import { VideoPlayer } from 'AppComponents';
import { DOMParser } from 'react-native-html-parser';
import URL from 'url-parse';
import _ from 'lodash';

const VIMEO_VIDEO = 'https://player.vimeo.com/video/188693550';
const YOUTUBE_VIDEO = 'https://www.youtube.com/watch?v=ZiRsdLHK2CY';

class VideoView extends Component {
  static propTypes = {
    popBack: PropTypes.func.isRequired,
  };

  constructor(props, context) {
    super(props, context);

    this.setViemoUrl = this.setViemoUrl.bind(this);
    this.setYoutubeUrl = this.setYoutubeUrl.bind(this);

    this.state = {
      vimeoUrl: undefined,
      youtubeUrl: undefined,
    };
  }

  componentDidMount() {
		//BackAndroid.addEventListener('hardwareBackPress', this.onBackPress.bind(this));
    this.setViemoUrl();
    this.setYoutubeUrl();
  }

	onBackPress() {
		this.props.popBack();
	}

  setYoutubeUrl() {
    let youtubeUrl = undefined, youtubeRes = 5000;
    fetch(`http://keepvid.com/?url=${YOUTUBE_VIDEO}`)
      .then(res => res.text())
      .then(res => {
        const doc = new DOMParser().parseFromString(res,'text/html');
        const dls = doc.getElementsByTagName('dl');
        _.forEach(dls, (dl) => {
          const dts = dl.getElementsByTagName('dt');
          _.forEach(dts, (dt) => {
            if (dt.textContent === ' Full Video') {
              const links = dl.getElementsByTagName('a');
              _.forEach(links, (link, index) => {
                const href = link.getAttribute('href');
                if (href && URL(href).host.includes('googlevideo.com')) {
                  const spans = link.getElementsByTagName('span');
                  _.forEach(spans, (span) => {
                    const regExp = new RegExp("^\\s*MP4\\s*-\\s*[^\\d]*(\\d+).*$");
                    const resolution = regExp.exec(span.textContent);
                    if (resolution && resolution.length == 2) {
                      const resol = parseInt(resolution[1], 10);
                      if (youtubeRes > resol) {
                        youtubeUrl = href;
                        youtubeRes = parseInt(resolution[1], 10);
                      }
                    }
                  });
                }
                if (index === links.length - 1 && this.refContent) {
                  this.setState({ youtubeUrl });
                }
              });
              return false;
            }
          });
        });
      })
      .catch(error => Alert.alert(error.message));
  }

  setViemoUrl() {
    fetch(`${VIMEO_VIDEO}/config`)
      .then(res => res.json())
      .then(res => this.setState({
        vimeoUrl: res.request.files.hls.cdns[res.request.files.hls.default_cdn].url,
      }))
      .catch(error => Alert.alert(error.message));
  }

  render() {
    return (
      <View ref={(ref) => { this.refContent = ref; }}style={styles.container}>
        <VideoPlayer
          endWithThumbnail
          video={{ uri: this.state.youtubeUrl }}
        />

        <VideoPlayer
          customStyles={{ wrapper: { marginVertical: 10 } }}
          endWithThumbnail
          video={{ uri: this.state.vimeoUrl }}
        />

        <  TouchableOpacity
          onPress={this.props.popBack}
          underlayColor="#bde7ff"
          style={{ alignSelf: 'center' }}
        >
          <Text allowFontScaling={false} style={styles.btnLogin}>Back</Text>
        </  TouchableOpacity>
      </View>
    );
  }
}

export default VideoView;
