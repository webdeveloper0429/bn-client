import React, { Component, PropTypes } from 'react';
import { ActivityIndicator, Alert, View, Text, TouchableHighlight, TextInput, BackAndroid } from 'react-native';
import styles from './styles';
import _ from 'lodash';
import { RequestStripeRegister } from 'AppUtilities';

class PaymentView extends Component {
  static propTypes = {
    popBack: PropTypes.func.isRequired,
  };

  constructor(props, context) {
    super(props, context);

    this.state = {
      isLoading: false,
      cardNumber: '',
      expiry: '',
      cardName: '',
      cvc: ''
    };

    this.OnSubmit = this.OnSubmit.bind(this);
    this.renderCardInput = this.renderCardInput.bind(this);
    this.onChangeCardNum = this.onChangeCardNum.bind(this);
    this.onChangeExpiry = this.onChangeExpiry.bind(this);
    this.onChangeCardName = this.onChangeCardName.bind(this);
    this.onChangeCVC = this.onChangeCVC.bind(this);
  }

  componentDidMount() {
		//BackAndroid.addEventListener('hardwareBackPress', this.onBackPress.bind(this));
  }

	onBackPress() {
		this.props.popNavBack();
	}

  onChangeCardNum(text) {
    if (text.length < 1) {
      this.setState({ cardNumber: '' });
      return;
    }
    let cardNum = text.split('-').join('');
    cardNum = cardNum.match(/.{1,4}/g);
    this.setState({ cardNumber: cardNum.join('-') });
  }
  onChangeExpiry(text) {
    if (text.length > 2) {
      let expiry = text.split('/').join('');
      expiry = `${expiry.substr(0, 2)}/${expiry.substr(2)}`;
      this.setState({ expiry });
    } else {
      this.setState({ expiry: text });
    }
  }
  onChangeCardName(text) {
    this.setState({ cardName: text });
  }
  onChangeCVC(text) {
    this.setState({ cvc: text });
  }
  OnSubmit() {
    const { cardNumber, expiry, cardName, cvc } = this.state;
    if (cardNumber.length < 1) {
      Alert.alert('Please input the card number!');
      return;
    }
    if (expiry.length !== 7) {
      Alert.alert('Please complete the expiry correctly!');
      return;
    }
    if (cardName.length < 1) {
      Alert.alert('Please input the card name!');
      return;
    }
    if (cvc.length < 1) {
      Alert.alert('Please input the cvc!');
      return;
    }

    this.setState({ isLoading: true });

    const cardDetails = {
      'card[number]': this.state.cardNumber.split('-').join(''),
      'card[exp_month]': expiry.split('/')[0],
      'card[exp_year]': expiry.split('/')[1],
      'card[cvc]': cvc
    };

    RequestStripeRegister(cardDetails)
      .then(response => {
        if (response === 'Charged') {
          Alert.alert('Charged!');
        } else {
          Alert.alert(`Payment Error: ${response}`);
        }
        this.setState({ isLoading: false });
      })
      .catch(error => {
        Alert.alert(`Payment Error: ${error.message}`);
        this.setState({ isLoading: false });
      });


  }

  renderCardInput(label, inputKey, changeEvent, flexValue, maxLen = undefined, keyType = 'default', placeHolder = '') {
    return (
      <View style={{ flexDirection: 'column', flex: flexValue, marginHorizontal: 5 }}>
        <Text>{label}</Text>
        <TextInput
          underlineColorAndroid='transparent'
          style={styles.inputEmail}
          fontSize={17}
          onChangeText={changeEvent}
          value={this.state[inputKey]}
          placeholderTextColor="#999"
          placeholder={placeHolder}
          keyboardType={keyType}
          maxLength={maxLen}
        />
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{ flexDirection: 'row', height: 53 }}>
          {this.renderCardInput('Card Number', 'cardNumber', this.onChangeCardNum, 0.7, 19, 'numeric')}
          {this.renderCardInput('Expiry', 'expiry', this.onChangeExpiry, 0.3, 7, 'numeric', 'mm/yyyy')}
        </View>

        <View style={{ flexDirection: 'row', height: 53, marginTop: 5 }}>
          {this.renderCardInput('Name On Card', 'cardName', this.onChangeCardName, 0.7)}
          {this.renderCardInput('CVC', 'cvc', this.onChangeCVC, 0.3, undefined, 'numeric')}
        </View>

        <View style={{ flexDirection: 'row', marginTop: 20 }}>
          <  TouchableOpacity
            onPress={this.OnSubmit}
            underlayColor="#bde7ff"
            style={{ marginRight: 20 }}
          >
            <Text allowFontScaling={false} style={styles.btnLogin}>Submit Payment</Text>
          </  TouchableOpacity>
          <  TouchableOpacity
            onPress={this.props.popBack}
            underlayColor="#bde7ff"
          >
            <Text allowFontScaling={false} style={styles.btnLogin}>Back</Text>
          </  TouchableOpacity>
        </View>
        {this.state.isLoading ?
          <View style={styles.loadingScene}>
            <ActivityIndicator
              animating={true}
              size="small"
              color="white"
            />
          </View> : null}
      </View>
    );
  }
}

export default PaymentView;
