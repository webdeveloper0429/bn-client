import { StyleSheet } from 'react-native';

import Dimensions from 'Dimensions';

const window = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 70,
    backgroundColor: '#7dcefb',
  },
  inputEmail: {
    alignSelf: 'stretch',
    flex: 1,
    backgroundColor: '#faffbd',
    borderWidth: 1,
    borderColor: '#008000',
    borderRadius: 5,
    paddingVertical: 0,
    paddingHorizontal: 10,
  },
  btnLogin: {
    color: '#fff',
    fontSize: 16,
    borderWidth: 2,
    borderColor: '#fff',
    borderRadius: 5,
    paddingLeft: 10,
    paddingRight: 7,
    paddingTop: 10,
    paddingBottom: 7,
  },
  loadingScene: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: window.width,
    height: window.height,
    alignSelf: 'stretch',
    backgroundColor: 'rgba(0,0,0,0.5)',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
