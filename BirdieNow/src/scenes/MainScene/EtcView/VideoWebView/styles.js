import { StyleSheet } from 'react-native';

import Dimensions from 'Dimensions';

const window = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingBottom: 70,
    backgroundColor: '#7dcefb',
  },
  btnLogin: {
    color: '#fff',
    fontSize: 16,
    borderWidth: 2,
    borderColor: '#fff',
    borderRadius: 5,
    paddingLeft: 10,
    paddingRight: 7,
    paddingTop: 10,
    paddingBottom: 7,
  },
});
