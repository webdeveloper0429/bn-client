import React, { Component, PropTypes } from 'react';
import { Alert, View, Text,   TouchableOpacity, WebView, BackAndroid } from 'react-native';
import styles from './styles';

class VideoWebView extends Component {
  static propTypes = {
    popBack: PropTypes.func.isRequired,
  };

  constructor(props, context) {
    super(props, context);

  }

  componentDidMount() {
		//BackAndroid.addEventListener('hardwareBackPress', this.onBackPress.bind(this));
  }

	onBackPress() {
		this.props.popBack();
	}

  render() {
    return (
      <View style={styles.container}>
        <WebView
          source={{ uri: 'https://www.youtube.com/embed/ZiRsdLHK2CY' }}
          style={{ height: 211, marginTop: 30 }}
        />

        <WebView
          source={{ uri: 'https://player.vimeo.com/video/188693550' }}
          style={{ height: 211, marginTop: 30 }}
        />

        <  TouchableOpacity
            onPress={this.props.popBack}
            underlayColor="#bde7ff"
            style={{ alignSelf: 'center', marginTop: 30 }}
        >
            <Text allowFontScaling={false} style={styles.btnLogin}>Back</Text>
        </  TouchableOpacity>
      </View>
    );
  }
}

export default VideoWebView;
