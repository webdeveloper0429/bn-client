import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#aac1ce',
  },
  btnLogin: {
    color: '#868788',
    fontFamily: 'Gotham-Bold',
    fontWeight: 'bold',
    fontSize: 15,
    width: 305,
    textAlign: 'center', 
    borderWidth: 0,
    backgroundColor: '#ffffff',
    borderColor: '#fff',
    borderRadius: 5,    
    paddingTop: 8,
    paddingBottom: 7,
  },
});
