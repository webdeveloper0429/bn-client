import React, { Component, PropTypes } from 'react';
import { Alert, View, Image, TouchableOpacity, Text, ScrollView, Platform, ListView, RefreshControl, TouchableHighlight, ActivityIndicator, Navigator, Dimensions, BackAndroid } from 'react-native';
import styles from './styles';
import AppConfig from 'AppConfig';
import AppStyles from 'AppStyles';
import { SearchBar } from 'AppComponents';
import { InstructorScene, FilterScene, WriteReviewSceneNew,  ContactorDetailScene, CancelAppointment } from 'AppScenes';
import { RequestApi, MakeCancelable, GlobalStorage, Appointments } from 'AppUtilities';
import _, { isEqual } from 'lodash';
import StarRating from 'react-native-star-rating';
import moment from 'moment';
import { GoogleAnalyticsTracker } from 'react-native-google-analytics-bridge';

let { width, height } = Dimensions.get('window');
let r_width =  width / 1125;
let r_height = height / 1659;

class AppointmentsView extends Component {
  static propTypes = {
    pushNavScene: PropTypes.func.isRequired,
    pushScene: PropTypes.func.isRequired,
    popNavBack: PropTypes.func.isRequired,
    popNavNBack: PropTypes.func,
    popNavToTop: PropTypes.func.isRequired,
    resetNavToScene: PropTypes.func.isRequired,
  };

  constructor(props, context) {
    super(props, context);

    this.state = {
      isLoading: false,
      isRefreshing: false,
      isDataLoaded: false,
      upCount: 0,
      pastCount: 0,
      ListDataSource: new ListView.DataSource({
        rowHasChanged: (r1, r2) => !isEqual(r1, r2),
        sectionHeaderHasChanged: (s1, s2) => !isEqual(s1, s2),
      }),
    };

    this.ListSource = {};
    this.state.ListDataSource = this.state.ListDataSource.cloneWithRowsAndSections(this.ListSource);

    this.renderRow = this.renderRow.bind(this);
    this.renderSectionHeader = this.renderSectionHeader.bind(this);
    this.renderSeparator = this.renderSeparator.bind(this);
    this.showRatingPage = this.showRatingPage.bind(this);
    this.showContactorDetailPage = this.showContactorDetailPage.bind(this);
    this.pushCallback = this.pushCallback.bind(this);
    this.OnRetrieveAppointments = this.OnRetrieveAppointments.bind(this);
    this.checkListDataCount = this.checkListDataCount.bind(this);
    this.onProfilePage = this.onProfilePage.bind(this);
    this.onCancelAppointment = this.onCancelAppointment.bind(this);
    this.CallBackInstructorScene = ::this.CallBackInstructorScene;

    this.didAppear = ::this.didAppear;
    this.CallBackWriteReviewScene = ::this.CallBackWriteReviewScene;
    this.didAppear = ::this.didAppear;
  }

  didAppear() {
    this.OnRetrieveAppointments();
    if (this.refInstructorScene != null) {
      this.refInstructorScene.handlePopCheck(2);
    } else if (this.refWriteReviewSceneNew != null) {
      this.props.popNavBack();
    } else if (this.refCancelAppointment != null) {
      this.props.popNavBack();
    } else if (this.refContactorDetaileScene != null) {
      this.props.popNavBack();
    }
  }

  checkListDataCount() {
    return this.state.listData.length > 0;
  }

  CallBackInstructorScene() {
    this.OnRetrieveAppointments();
  }

  OnRetrieveAppointments(){

    let tracker = new GoogleAnalyticsTracker(AppConfig.googleAnalyticsTrackerId);
    tracker.trackScreenView('AppointmentsView');
   
    if (AppConfig.global_isLoggedIn == false) {
      return;
    }
    this.setState({ isLoading: true });
  
   
    Appointments(AppConfig.global_userToken)
      .then((response) => {
        let test = AppConfig.global_userToken;
          
        if (!response.error_description) { //success 200
          this.ListSource = { up: [], past: [] };
          response.forEach((item) => {
            if (moment(item.localStartDate).isAfter(moment())) {
              this.ListSource.up.push(item);
            } else {
              this.ListSource.past.push(item);
            }
          });
          this.setState({upCount: this.ListSource.up.length, pastCount: this.ListSource.past.length, 
            isLoading: false, isDataLoaded: true, ListDataSource: this.state.ListDataSource.cloneWithRowsAndSections(this.ListSource)});
        } else { // failed 400
          this.setState({ isLoading: false });
        }
      })
      .catch(error => {
        Alert.alert(error.message);
      });
  }

  CallBackWriteReviewScene() {
    this.didAppear();
  }

  pushCallback() {
  }

  componentDidMount() {
    //this.OnRetrieveAppointments();
		//BackAndroid.addEventListener('hardwareBackPress', this.onBackPress.bind(this));
  }

	onBackPress() {
		this.props.popNavBack();
	}

  showRatingPage(rowData, sectionID, rowID) {
    this.props.pushNavScene(WriteReviewSceneNew, {ref:(c) => { this.refWriteReviewSceneNew = c; }, passProps: {appointment: rowData}, callback: this.CallBackWriteReviewScene});
  }

  showContactorDetailPage(rowData, sectionID, rowID) {
    this.props.pushNavScene(ContactorDetailScene, {ref:(c) => { this.refContactorDetaileScene = c; } });
  }

  onCancelAppointment(instructor) {
    this.props.pushNavScene(CancelAppointment, {ref:(c) => { this.refCancelAppointment = c; }, passProps: {instructor: instructor}, callback: this.CallBackWriteReviewScene});
  }

  onProfilePage(instructor){
    this.props.pushNavScene(InstructorScene, {ref:(c) => { this.refInstructorScene = c; }, passProps: {instructor: instructor, id: instructor.instructorId, lessonTypeId:instructor.lessonTypeId}, callback: this.CallBackWriteReviewScene});
  }

  renderRow(rowData, sectionID, rowID) {
    let reviewWrapper;
    if (sectionID === 'up'){
      reviewWrapper =
        (<TouchableOpacity
          style={{ alignItems: 'center', justifyContent: 'center', height: 40 }}
          onPress={() => this.onCancelAppointment(rowData)}
          underlayColor="#d9d9d9"
        >
          <Text allowFontScaling={false} style={{ fontFamily: 'Gotham-Medium', color: '#32b3fa' }}>Cancel appointment</Text>
        </TouchableOpacity>);
    } else if (rowData.overallRating === 0) {
      reviewWrapper =
        (<TouchableOpacity
          style={{ alignItems: 'center', justifyContent: 'center', height: 40 }}
          onPress={() => this.showRatingPage(rowData, sectionID, rowID)}
          underlayColor="#d9d9d9"
        >
          <Text allowFontScaling={false} style={{ fontFamily: 'Gotham-Medium', color: '#32b3fa' }}>Write a review</Text>
        </TouchableOpacity>);
    } else {
      reviewWrapper =
        (<View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', height: 40 }}>
          <Text allowFontScaling={false} style={{ fontFamily: 'Gotham-Medium', color: '#4b5b64' }}>Your rating </Text>
          <StarRating
            starColor = {'#4b5b64'}
            emptyStar={'ios-star-outline'}
            fullStar={'ios-star'}
            halfStar={'ios-star-half'}
            iconSet="Ionicons"
            disabled={true}
            maxStars={5}
            rating={rowData.overallRating}
            starSize = {15}
            emptyStarColor={'#4b5b64'}
          />
        </View>);
    }

    const startDate = moment(rowData.localStartDate), endDate = moment(rowData.localEndDate);
    let strDate;

    if (startDate.isSame(endDate, 'day')) {
      strDate = `${startDate.format('LLLL')}\n- ${endDate.format('h:mm A')}`;
    } else {
      strDate = `${startDate.format('LLLL')}\n- ${endDate.format('LLLL')}`;
    }

    return (
      <View>
        <TouchableOpacity
          style={{ backgroundColor: '#fff' }}
          onPress={() => this.onProfilePage(rowData)}
          underlayColor="#d9d9d9"
        >
          <View>
            <View style={{flexDirection: 'row',alignItems: 'center',justifyContent: 'center',paddingRight: 15, paddingVertical: 8}}>
              <Image
                source={{uri: 'https://www.birdienow.com/img/instructor/profile/'+ rowData.instructorId +'.jpg'}} 
                style={{width: 246 * r_width, height: 246 * r_width, borderRadius:123 * r_width, marginLeft:36 * r_width}}/>
              <View style={{ flex: 1, flexDirection: 'column', marginLeft: 15 }}>
                <Text allowFontScaling={false} style={{ fontSize: 16, fontFamily: 'Gotham-Bold' }} numberOfLines={1} ellipsizeMode="tail">{`${rowData.firstName} ${rowData.lastName}`}</Text>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                  <Image style={{ width: 16, height: 16, marginRight: 4 }} source={require('img/searchimage/nav-profile.png')} />
                  <Text allowFontScaling={false} style={{ flex: 1, fontSize: 11, fontFamily: 'Gotham-Book' }} numberOfLines={1} ellipsizeMode="tail">{rowData.name}</Text>
                </View>

                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 4 }}>
                  <Image style={{ width: 16, height: 16, marginRight: 4 }} source={require('img/searchimage/nav-home.png')} />
                  <Text allowFontScaling={false} style={{ flex: 1, fontSize: 11, fontFamily: 'Gotham-Book' }} numberOfLines={3} ellipsizeMode="tail">{rowData.address1 + " " + rowData.city + " " + rowData.state + " " + rowData.zip}</Text>
                </View>

                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 6 }}>
                  <Image style={{ width: 16, height: 16, marginRight: 4 }} source={require('img/searchimage/nav-appointment.png')} />
                  <Text allowFontScaling={false} style={{ fontSize: 11, fontFamily: 'Gotham-Book' }}>{strDate}</Text>
                </View>

              </View>
            </View>
            <View style={{ backgroundColor: '#ececec', height: 1, marginRight: 15 }} />
            {reviewWrapper}
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  renderSectionHeader(sectionData, sectionID) {
    return (
      <View>
      <Text allowFontScaling={false} style={{ fontSize: 12, color: '#878787', paddingLeft: 20, paddingVertical: 7, fontFamily: 'Gotham-Bold' }}>
        {sectionID === 'up' ? 'UPCOMING APPOINTMENTS (' + this.state.upCount + ')' : 'PAST APPOINTMENTS (' + this.state.pastCount + ')' }
      </Text>

      {sectionID === 'up' && this.state.isDataLoaded && this.state.upCount == 0 ? 
        <View style={{ paddingTop:10, paddingBottom:30, backgroundColor: '#fff', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
            <Image
              style={{ width: 60, height: 60 }}
              source={require('img/icon/NoAppt.png')}
            />
            <Text style={{ fontSize: 13, color: '#878787', fontFamily: 'Gotham-Bold' }}>Get out of the bunker.  Book a lesson.</Text>
            {/*<Text style={{ fontSize: 17, color: '#5b5b5b', paddingLeft:30, paddingRight:30, textAlign:'center' }}>Book a lesson now and improve your game.</Text>*/}
          </View>
          : null}

      </View>
    );
  }

  renderSeparator(sectionID, rowID) {
    if (parseInt(rowID, 10) !== this.ListSource[sectionID].length - 1) {
      return (
        <View key={`${sectionID}-${rowID}`} style={{ backgroundColor: '#f7f7f7', height: 10 }} />
      );
    }
    return null;
  }

  render() {
    if (!AppConfig.global_isLoggedIn) {
      return (
        <View style={{ flex: 1, backgroundColor: '#F9F9F9'}}>
          <View style={{ flex: 1 }}>
            <View style={{
              alignItems: 'center', backgroundColor: '#aac1ce', borderBottomWidth: 0, borderBottomColor: '#85D3DD',
              height: (Platform.OS === 'ios') ? 65 : 40,
              justifyContent: "center"
            }}>
              <Text allowFontScaling={false} style={{
                textAlign: "center", color: '#ffffff', fontSize: 15,
                marginTop: (Platform.OS === 'ios') ? 20 : 0,
                fontFamily: 'Gotham-Bold'
              }}>
                Appointments
          </Text>
            </View>
          </View>
          <View style={{ flex: 9, paddingBottom:100, backgroundColor: '#F9F9F9', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
            <Image
              style={{ width: 200, height: 200 }}
              source={require('img/image/NoAppts_Icon.png')}
            />
            <Text style={{ fontSize: 17, color: '#5b5b5b', }}>You have no appointments.</Text>
            <Text style={{ fontSize: 17, color: '#5b5b5b', paddingLeft:30, paddingRight:30, textAlign:'center' }}>Book a lesson now.</Text>
          </View>
        </View>
      );
    } else {

      return (
      <View style={{flex: 1, backgroundColor: "white"}}>
        
            <View style={{ 
              alignItems: 'center', backgroundColor: '#aac1ce', borderBottomWidth: 0, borderBottomColor: '#85D3DD',
              height: (Platform.OS === 'ios') ? 65 : 40,
              justifyContent: "center"
            }}>
              <Text allowFontScaling={false} style={{
                textAlign: "center", color: '#ffffff', fontSize: 15,
                marginTop: (Platform.OS === 'ios') ? 20 : 0,
                fontFamily: 'Gotham-Bold'
              }}>
                Appointments
          </Text>
            
          </View>

        <ListView
          style={{ backgroundColor: '#f7f7f7' }}
          dataSource={this.state.ListDataSource}
          renderRow={this.renderRow}
          renderSectionHeader={this.renderSectionHeader}
          renderSeparator={this.renderSeparator}
          enableEmptySections={true}
          automaticallyAdjustContentInsets={false}
        />

        {/*{this.state.isDataLoaded && this.state.upCount == 0 && this.state.pastCount ==0 ? 
        <View style={{ flex: 9, paddingBottom:200, backgroundColor: '#F9F9F9', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
            <Image
              style={{ width: 200, height: 200 }}
              source={require('img/image/NoAppts_Icon.png')}
            />
            <Text style={{ fontSize: 17, color: '#5b5b5b', }}>You have no appointments.</Text>
            <Text style={{ fontSize: 17, color: '#5b5b5b', paddingLeft:30, paddingRight:30, textAlign:'center' }}>Book a lesson now.</Text>
          </View>
          : null}*/}

        {this.state.isLoading ?
          <View style={styles.loadingScene}>
            <ActivityIndicator
              animating={true}
              size="small"
              color="white"
            />
          </View> : null}
      </View>
    );



    }
    
  }
}

export default AppointmentsView;
