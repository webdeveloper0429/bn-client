import React, { Component, PropTypes } from 'react';
import { Dimensions, Alert, View, Image, TouchableOpacity, Text, TextInput, ActivityIndicator, KeyboardAvoidingView, Platform, BackAndroid } from 'react-native';
import styles from './styles';
import AppConfig from 'AppConfig';
import { ContactRequest, GetAccountInfo } from 'AppUtilities';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

class CancelAppointment extends Component {
  static propTypes = {
    popNavBack: PropTypes.func.isRequired,
    pushNavScene: PropTypes.func.isRequired,
    callback: PropTypes.func,
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      data: [],
      showloading: false,
      image: {}
    };

    const { width, height } = Dimensions.get('window');
    this.width = width;
    this.height = height;
    this.r_width =  width / 1125;
    this.r_height = height / 1659;
    this.comment = "";

    this.OnRetriveUserInfo = this.OnRetriveUserInfo.bind(this);
    this.OnSubmit = this.OnSubmit.bind(this);
    this.ContactSuccess = this.ContactSuccess.bind(this);
    this.handlePop = ::this.handlePop;
  }

  handlePop() {
    this.props.popNavBack();
  }

  componentDidMount() {
		//BackAndroid.addEventListener('hardwareBackPress', this.onBackPress.bind(this));
    this.OnRetriveUserInfo();

    if (this.props.passProps != null) {
      this.setState({ data: this.props.passProps.instructor });
      this.setState({ image: { uri: 'https://www.birdienow.com/img/instructor/profile/' + this.props.passProps.instructor.instructorId + '.jpg' } });        
    }
  }

	onBackPress() {
		this.props.popNavBack();
	}

  OnRetriveUserInfo() {
    this.setState({showloading: true});
    GetAccountInfo(AppConfig.global_userToken)
      .then((response) => {
        if (response) { //success 200
          AppConfig.userInfo = response;
          this.setState({ showloading: false });
        } else { // failed 400
          this.setState({showloading: false});
          Alert.alert(response.error_description);
        }
      })
      .catch(error => {
        Alert.alert(error.message);
        this.setState({showloading: false});
      });
  }

  OnSubmit() {
    this.setState({showloading: true});
    fetch(AppConfig.apiUrl + '/api/bookAppointments/?id=' + this.state.data.id + '&comment=' + this.comment, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${AppConfig.global_userToken}`
      }
    })
      .then((responseData) => {
        this.setState({showloading: false});
        Alert.alert(
          'CONFIRMATION',
          'Your appointment has been successfully cancelled.',
          [
            {
              text: 'OK', onPress: () => {
              this.ContactSuccess();
            }
            },
          ],
          {cancelable: false}
        );
      })
      .catch(error => {
        Alert.alert(error.message);
        this.setState({showloading: false});
      });
  }

  ContactSuccess() {
    this.props.callback();
    this.props.popNavBack();
  }

  onError(error){
   this.setState({ image: require('img/profile/profile.png')})
  }

  render() {
    const render_TopBar = (
      <View style={{
        flexDirection: 'row',
        height: (Platform.OS === 'ios') ? 65 : 40,
        alignItems: "center",
        backgroundColor: '#fafafa',
        borderBottomWidth: 1,
        borderBottomColor: '#D3D3D3',
        justifyContent: "center"
      }}>
      <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={() => { this.props.popNavBack(); }}>
          <Text allowFontScaling={false} style={{ 
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, marginTop: (Platform.OS === 'ios') ? 20 : 0, color: '#000', marginRight: 10 }}
            >Back</Text>
        </TouchableOpacity>
        <View style={{ flex: 2, justifyContent: "center"}}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, marginTop: (Platform.OS === 'ios') ? 20 : 0}} 
            >Cancel Appointment</Text>
        </View>
        <TouchableOpacity style={{flex: 1, justifyContent: "center"}} onPress={this.OnSubmit}>
          <Text allowFontScaling={false} style={{ 
            fontFamily: 'Gotham-Bold', fontSize: 15, textAlign: "center", marginTop: (Platform.OS === 'ios') ? 20 : 0, color: '#32b3fa', marginLeft: 10 }}
            >Submit</Text>
        </TouchableOpacity>
      </View>
    );

    const render_ImageBar = (
      <Image source={require('img/image/ui_back_blue.png')} style={styles.imgBack}>
        <Text allowFontScaling={false} style={{ fontSize: 19, color: '#4b5b64', fontFamily: 'Gotham-Medium', backgroundColor: 'transparent' }}>
          {this.state.data.firstName} {this.state.data.lastName}
        </Text>
        <View>
          <Image key="render_image"
            style={{ width: 246 * this.r_width, height: 246 * this.r_width, borderRadius: 123 * this.r_width, marginLeft: 36 * this.r_width }}
            source={ this.state.image }
            onError={ this.onError.bind(this) }
            />         
        {this.state.data.isPga ?
          <Image
            style={{width: 30, height: 30, position: "absolute", marginLeft: 25 * this.r_width, marginTop: 150 * this.r_width}}
            source={{uri: 'https://www.birdienow.com/img/badge/pga_badge_85.png'}}
          />:null}
        </View>
      </Image>
    );

    return (
      <View style={styles.container}>
        {render_TopBar}
        <KeyboardAwareScrollView
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={false}
        >
          {render_ImageBar}
          <View style={[styles.txtContactWrapper, { alignItems: 'flex-start', borderBottomWidth: 1 }]}>
            <Image source={require('img/form/form-comments.png')} style={styles.iconContact} />
            <TextInput
              underlineColorAndroid="transparent"
              style={[styles.txtContact, { height: 200 }]}
              placeholder="Enter a comment for the instructor"
              placeholderTextColor="#9b9b9b"
              multiline={true}
              numberOfLines={100}
              onChangeText={(text) => { this.comment = text; }}
            />
          </View>
        </KeyboardAwareScrollView>
        {this.state.showloading ?
          <View
            style={{position:'absolute', top: 0, left:0, bottom:0, right:0, alignItems:'center', justifyContent:'center', alignSelf: 'stretch', backgroundColor: 'transparent'}}
          >
            <ActivityIndicator
              animating={true}
              size="small"
              color="gray"
            />
          </View>:null}
      </View>
    );
  }
}

export default CancelAppointment;