import React, { PropTypes, Component } from 'react';
import { Text, View, Image, StatusBar, Navigator, Alert, BackAndroid } from 'react-native';
import ScrollableTabView, { ScrollableTabBar, } from 'react-native-scrollable-tab-view';
import { default as styles} from './styles';
import { AppointmentsView, PurchaseLessonsView, FavoritesView, HomeView, ProfileView, SearchView } from 'AppScenes';
import { MainTabBar, HeaderBar } from 'AppComponents';
import AppConfig from 'AppConfig';
import { GlobalStorage } from 'AppUtilities';

class MainScene extends Component {
  static propTypes = {
    pushScene: PropTypes.func.isRequired,
    popBack: PropTypes.func.isRequired,
  };

  constructor(props, context) {
    super(props, context);
    StatusBar.setHidden(false);

    this.onChangeTab = this.onChangeTab.bind(this);
    this.renderScene = this.renderScene.bind(this);

    this.children = [];
    this.moreChildren = [];
    this.state = {
      currentTab: 0,
    };
    this.gotoPage = ::this.gotoPage;
  }

  _REMOVEconfigureScene(route, routeStack) {
    debugger;
    const pushFromRight = Navigator.SceneConfigs.PushFromRight;
    const currentRoute = typeof route === 'string' ? { name: route } : route;
    if (currentRoute.name == 'SearchResultsView' || currentRoute.name =='SearchView') {
      return {
        ...pushFromRight,
        gestures: {}
      };
    } else {
      return pushFromRight;
    }
    // if (route.component.name == 'SearchResultsView' || route.component.name == 'SearchView') {
    //   const config = Navigator.SceneConfigs.PushFromRight;
    //   return { ...config, gestures: {}, };
    //   // return {...Navigator.SceneConfigs.PushFromRight, gestures: {}};
    // } else {
    //   const config = Navigator.SceneConfigs.PushFromRight;
    //   return { ...config};
    // }
  }

  componentDidMount() {
		BackAndroid.addEventListener('hardwareBackPress', () => {
			if (this.children[0] && this.children[0].getCurrentRoutes().length > 1) {
        this.children[0].pop();
        return true;
      }
      if (this.children[1] && this.children[1].getCurrentRoutes().length > 1) {
        this.children[1].pop();
        return true;
      }
      if (this.children[2] && this.children[2].getCurrentRoutes().length > 1) {
        this.children[2].pop();
        return true;
      }
      if (this.children[3] && this.children[3].getCurrentRoutes().length > 1) {
        this.children[3].pop();
        return true;
      }
      
      return true;
    });
    
    setTimeout(async () => {
      try {
        this.curStateIndex = await GlobalStorage.getItem(AppConfig.stor_state);
        this.selTicketArray = await GlobalStorage.getJSON(AppConfig.stor_ticket);

        this.props.updateActiveState(this.curStateIndex);

        if (this.children[0]) {
          const navRoutArray = this.children[0].getCurrentRoutes(0);
          this.moreChildren[navRoutArray.length - 1].getWrappedInstance().setHeaderOptions();
        }
        this.props.updateActiveTicket(this.selTicketArray);
      } catch (error) {
      }
    }, 10)
  }

  gotoPage(i, goToScene) {
    debugger;
    this.tabView.goToPage(i);
    
   debugger;
    if (i == 0 && this.refHomeView != null) {
      this.refHomeView.props.popNavToTop();
    }
    if (i == 1 && this.refSearchView != null) {
      //focus to search input
      this.refSearchView.focusStart();
    }
    if (i == 2 && this.refAppointmentsView != null) {
      this.refAppointmentsView.props.popNavToTop();
      this.refAppointmentsView.OnRetrieveAppointments();
    }
    if (i == 3 && this.refProfileView != null) {
      this.refProfileView.props.popNavToTop();
      if (goToScene == 'PurchaseLessonsView')
      {
        this.refProfileView.GoToPurchaseLessons();
      }
    }
  }

  onChangeTab({ i, ref, from }) {

    
   
    //HOME
    if (this.refHomeView != null) {
      // let routes = this.children[0].getCurrentRoutes();
      // let route = routes[this.children.length];
      // route.transition = "FadeAndroid";
      // route.sceneConfig = Navigator.SceneConfigs.FadeAndroid;
      this.refHomeView.props.popNavToTop();
      //this.refHomeView.didAppear();
    }
    //SEARCH
    if (this.refSearchView != null) {
      //this.refSearchView.props.resetNavToScene(SearchView);
      this.refSearchView.didAppear();
    }
    //APPOINTMENTS
    if (this.refAppointmentsView != null) {
      this.refAppointmentsView.props.popNavToTop();
      this.refAppointmentsView.OnRetrieveAppointments();
      //this.refAppointmentsView.didAppear();
    }
    //PROFILE
    if (this.refProfileView != null) {
      this.refProfileView.props.popNavToTop();
      //this.refProfileView.didAppear();
    }


  }

  renderScene(route, navigator) {
    const methods = {
      popNavBack: () => navigator.pop(),
      popNavToTop: () => navigator.popToTop(),
      popNavNBack: (i) => navigator.popN(i),
      pushNavScene: (component, passProps, transition) =>
        navigator.push({
          component,
          index: route.index + 1,
          transition: "PushFromRight",
          //sceneConfig: Navigator.SceneConfigs.PushFromRight,
          passProps,
        }),
      getCurrentRoutes: () => navigator.getCurrentRoutes(0),
      resetNavToScene: (component, passProps, transition = "FadeAndroid") =>
        navigator.resetTo({
          component,
          index: 0,
          transition: "FadeAndroid",  //need to add logic below to handle other refs
          passProps: { ...this.props, index: 0, wrapStyle: {}, gotoPage:this.gotoPage, ref: (c) => { this.refHomeView = c; } },
        }),
      goToNavTab: (i, goToScene) => this.gotoPage(i, goToScene),
      
    };

    return (
      <View style={styles.container}>
        <route.component
          ref={(c) => { this.moreChildren[route.index] = c; }}
          navigator={navigator}
          route={route}
          {...route.passProps}
          {...methods}
        />
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>

        <ScrollableTabView
          onChangeTab={ this.onChangeTab }
          ref={(tabView) => { this.tabView = tabView; }}
          style={styles.scrollTabView}
          renderTabBar={() => <MainTabBar />}
          tabBarPosition="bottom"
          tabBarActiveTextColor={AppConfig.secondaryColor}
          tabBarInactiveTextColor="#909090"
          locked={true}
          scrollWithoutAnimation={true}
          tabBarUnderlineStyle={{ backgroundColor: '#fff' }}
          contentProps={{ keyboardShouldPersistTaps: 'always' }}
        >
          <Navigator
            ref={(c) => { this.children[0] = c; }}
            tabLabel="Home"
            renderScene={this.renderScene}
            configureScene={(route) => {
              let config = Navigator.SceneConfigs.PushFromRight;
              if (route.transition === "FloatFromBottom") {
                config = Navigator.SceneConfigs.FloatFromBottom;
              } else if (route.transition === "FadeAndroid") {
                config = Navigator.SceneConfigs.FadeAndroid;
              } else if (route.transition === "PushFromRight") {
                config = Navigator.SceneConfigs.PushFromRight;
              }
              return { ...config};
            }}
            initialRoute={{
              component: HomeView,
              index: 0,
              navigator: this.children[0],
              passProps: { ...this.props, index: 0, wrapStyle: {}, gotoPage:this.gotoPage, ref: (c) => { this.refHomeView = c; } },
            }}
          />

          {/*configureScene={ (route) => {
              const config = Navigator.SceneConfigs.PushFromRight;
              if (route.component.name == 'a' || route.component.name == 't' || route.component.name == 'InstructorScene' || route.component.name == 'InstructorContact' || route.component.name == 'BookScene' || route.component.name == 'FullCalendarScene' || route.component.name == 'ReviewScene' || route.component.name == 'CancelAppointment') {
                return { ...config};
              } else {
                return { ...config, gestures: {}};
              }
            }}*/}
          <Navigator
            ref={(c) => { this.children[1] = c; }}
            tabLabel="Search"
            renderScene={this.renderScene}
            configureScene={(route) => {
              let config = Navigator.SceneConfigs.PushFromRight;
              if (route.transition === "FloatFromBottom") {
                config = Navigator.SceneConfigs.FloatFromBottom;
              } else if (route.transition === "FadeAndroid") {
                config = Navigator.SceneConfigs.FadeAndroid;
              } else if (route.transition === "PushFromRight") {
                config = Navigator.SceneConfigs.PushFromRight;
              }
              return { ...config};
            }}
            initialRoute={{
              component: SearchView,
              index: 0,
              navigator: this.children[1],
              passProps: { ...this.props, index: 1, wrapStyle: {}, gotoPage:this.gotoPage, ref: (c) => { this.refSearchView = c; } },
            }}
          />
          <Navigator
            ref={(c) => { this.children[2] = c; }}
            tabLabel="Appointments"
            renderScene={this.renderScene}
            configureScene={(route) => {
              let config = Navigator.SceneConfigs.PushFromRight;
              if (route.transition === "FloatFromBottom") {
                config = Navigator.SceneConfigs.FloatFromBottom;
              } else if (route.transition === "FadeAndroid") {
                config = Navigator.SceneConfigs.FadeAndroid;
              } else if (route.transition === "PushFromRight") {
                config = Navigator.SceneConfigs.PushFromRight;
              }
              return { ...config};
            }}
            initialRoute={{
              component: AppointmentsView,
              index: 0,
              navigator: this.children[2],
              passProps: { ...this.props, index: 2, wrapStyle: {}, gotoPage:this.gotoPage, ref: (c) => { this.refAppointmentsView = c; } },
            }}
          />
          {/*<Navigator
            ref={(c) => { this.children[3] = c; }}
            tabLabel="Favorites"
            renderScene={this.renderScene}
            configureScene={() => {
              const config = Navigator.SceneConfigs.PushFromRight;
              return { ...config};
            }}
            initialRoute={{
              component: FavoritesView,
              index: 0,
              navigator: this.children[3],
              passProps: { ...this.props, index: 3, wrapStyle: {} },
            }}
          />*/}
          <Navigator
            ref={(c) => { this.children[3] = c; }}
            tabLabel="Profile"
            renderScene={this.renderScene}
            configureScene={(route) => {
              let config = Navigator.SceneConfigs.PushFromRight;
              if (route.transition === "FloatFromBottom") {
                config = Navigator.SceneConfigs.FloatFromBottom;
              } else if (route.transition === "FadeAndroid") {
                config = Navigator.SceneConfigs.FadeAndroid;
              } else if (route.transition === "PushFromRight") {
                config = Navigator.SceneConfigs.PushFromRight;
              }
              return { ...config};
            }}
            initialRoute={{
              component: ProfileView,
              index: 0,
              navigator: this.children[3],
              passProps: { ...this.props, index: 3, wrapStyle: {}, gotoPage:this.gotoPage, ref: (c) => { this.refProfileView = c; } },
            }}
          />
        </ScrollableTabView>
      </View>
    );
  }
}

export default MainScene;
