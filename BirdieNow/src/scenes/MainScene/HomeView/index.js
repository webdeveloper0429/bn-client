import React, { Component, PropTypes } from 'react';
import { Alert, View, Image, TouchableOpacity, Text, ListView, TextInput, RefreshControl, Dimensions, StyleSheet, Animated, ActivityIndicator, Platform, ScrollView } from 'react-native';
import styles from './styles';
import AppConfig from 'AppConfig';
import AppStyles from 'AppStyles';
import { SearchBar, InfiniteCarousel } from 'AppComponents';
import { FilterScene, ListDetailScene, InstructorScene, SearchResultsView } from 'AppScenes';
import { RequestApi, MakeCancelable, GlobalStorage } from 'AppUtilities';
import _, { isEqual } from 'lodash';
import StarRating from 'react-native-star-rating';
import { GoogleAnalyticsTracker } from 'react-native-google-analytics-bridge';



let { width, height } = Dimensions.get('window');
this.width = width;
this.height = height;
this.r_width =  width / 400;
this.r_height = height / 647;



const HEIGHT = 200;

const Animal = ({ name, color, animatedScale, index, style, animatedOpacity, imageURL, averageRating, reviewCount, address1, address2, city, state, textcolor1, textcolor2, isPga, dynamicStyle, handleSubmit }) => (
  <View style={[{ backgroundColor: 'transparent' }, styles.animal]}>
    <TouchableOpacity onPress={handleSubmit}>
      <Animated.View
        style={[{ transform: [{ scale: animatedScale }], opacity: animatedOpacity }, style, styles.animalBox]}>
        <View style={{}}>
          {
            imageURL=='' ?
              <Image
                key="render_location"
                style={{width: 140 * this.r_width, height: 140 * this.r_width , borderRadius:70 * this.r_width}}
                source={require('img/profile/profile.png')}
              />
              :
              <Image
                key="render_location"
                style={{width: 140 * this.r_width, height: 140 * this.r_width , borderRadius:70 * this.r_width}}
                source={{uri: imageURL}}
              />
          }

          <Text allowFontScaling={false} style={{ color: textcolor1, fontWeight: 'bold', textAlign: 'center', fontSize: 9, fontFamily:'Gotham-Book' }}>{name}</Text>
          <View style={{flexDirection: 'row', alignItems:'center', justifyContent: 'center'}}>
            <StarRating
              disabled={true}
              emptyStar={'ios-star-outline'}
              fullStar={'ios-star'}
              halfStar={'ios-star-half'}
              iconSet="Ionicons"
              starColor = {'blue'}
              maxStars={5}
              rating={averageRating}
              selectedStar={() => {}}
              starSize = {7}
              emptyStarColor={'#fff'}
            />
            <Text allowFontScaling={false} style={{ color: textcolor2, fontWeight: 'bold', textAlign: 'center', fontSize: 11, fontFamily:'Gotham-Book' }}> {reviewCount} </Text>
          </View>
          <Text allowFontScaling={false} style={{ color: textcolor2, fontWeight: 'bold', textAlign: 'center', fontSize: 10, fontFamily:'Gotham-Book' }}>{city} {state}</Text>
          {isPga ? <Image
            style={{width: 70 * this.r_width, height: 70 * this.r_width, position: 'absolute', marginLeft: -10 * this.r_width, marginTop: 70 * this.r_width}}
            source={require('img/image/pgaImage.png')}
          /> : null}
        </View>
      </Animated.View>
    </TouchableOpacity>
  </View>
);


class HomeView extends Component {
  static propTypes = {
    pushNavScene: PropTypes.func.isRequired,
    //pushScene: PropTypes.func.isRequired,
    popNavBack: PropTypes.func.isRequired,
    //gotoPage: PropTypes.func.isRequired,
    popNavNBack: PropTypes.func.isRequired,
    popNavToTop: PropTypes.func.isRequired,
    resetNavToScene: PropTypes.func.isRequired,
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      dimensions: {},
      isLoading: false,
      data: [],
      ListDataSource: new ListView.DataSource({ rowHasChanged: (r1, r2) => !isEqual(r1, r2) }),
      scrollState: true,
    };

    this.ListSource = [
      {city: "New York, NY ", zip: "10010"},
      {city: "Trenton, NJ", zip: "08601"},
      {city: "Los Angeles, CA ", zip: "90001"},
      {city: "Palm Springs, CA ", zip: "92262"},
      {city: "San Diego, CA", zip: "92101"},
      {city: "Orlando, FL", zip: "32801"},
      {city: "Atlanta, GA", zip: "30301"},
    ];
    this.state.ListDataSource = this.state.ListDataSource.cloneWithRows(this.ListSource);

    this.CallBackSearchResultsView = ::this.CallBackSearchResultsView;
    this.CallBackInstructorScene = ::this.CallBackInstructorScene;
    this.onFocus = ::this.onFocus;
    this.OnCurrentMyLoc = ::this.OnCurrentMyLoc;
    this.onClick = ::this.onClick;
    
    this.renderCityListRow = ::this.renderCityListRow;
    this.OnCity = ::this.OnCity;

    this.didAppear = ::this.didAppear;
    this.onInstructor = ::this.onInstructor;
    this.enableScrollView = ::this.enableScrollView;
  }

  CallBackInstructorScene() {

  }

  
  CallBackSearchResultsView() {
   
  }

  onInstructor(i) {
    this.props.pushNavScene(InstructorScene, {ref:(c) => { this.refInstructorScene = c; } , gotoPage:this.props.gotoPage, source: 'home', passProps: { source: 'home', instructor: this.state.data[i].instructorId, id: this.state.data[i].id, lessonTypeId: this.state.data[i].lessonTypeId }, callback: this.CallBackInstructorScene });
  }

  didAppear() {
   
    
    if (this.refSearchResultsView != null) {
      
      //this.props.navigator.resetToScene(HomeView);
      this.refSearchResultsView.handlePopCheck(0);
    }

    if (this.refInstructorScene != null) {
      this.refInstructorScene.handlePopCheck();
    }
  }

  enableScrollView(flag) {
    this.setState({scrollState: flag});
  }

  renderCityListRow(rowData, rowID) {
    let render_separate = [];
    render_separate.push(
      <View key="render_separate" style={{backgroundColor: 'white', height: 1, width: this.width}}/>
    );
    if (rowData.zip != "30301") {
    }
    return(
      <View>
        {rowData.zip == "10010" ? <View style={{backgroundColor: 'white', height: 0, width: this.width}}/> : <View style={{backgroundColor: 'white', height: 1, width: this.width}}/>}
        <TouchableOpacity
          style={{ borderTopWidth: 1,backgroundColor: 'white', borderColor: '#f4f4f4' , height: 40, width: this.width, justifyContent: 'center', zIndex: 1 }}
          onPress={() => {this.OnCity(rowData);}}
        >
          <Text style={{fontFamily: 'Gotham-Book', fontSize: 11, color: 'black', marginLeft: 10}}>{rowData.city}</Text>
          <Image
            style={{position: 'absolute', width: 20, height: 20, right: 15, top:10}}
            source={require('img/icon/icon_arrow.png')}
          />
        </TouchableOpacity>
        {rowData.zip == "30301" ? <View style={{backgroundColor: 'white', height: 2, width: this.width}}/> : <View style={{backgroundColor: 'white', height: 1, width: this.width}}/>}
      </View>
    );
  }

  OnCity(rowData) {
    AppConfig.checkFocus = false;
    AppConfig.checkGPS = false;
    AppConfig.checkCity = true;
    AppConfig.checkCityName = rowData.city;
    AppConfig.checkCityZip = rowData.zip;

    this.props.pushNavScene(SearchResultsView, {ref:(c) => { this.refSearchResultsView = c; }, gotoPage:this.props.gotoPage, source: 'home', passProps: {source: 'home', searchType: 'city', zip: rowData.zip, city: rowData.city} ,callback: this.CallBackSearchResultsView});
    //this.props.gotoPage();
  }

  _isSameMeasure = (measurement1, measurement2) =>
  measurement1.width === measurement2.width &&
  measurement1.height === measurement2.height;

  _onLayout = ({ nativeEvent }) => {
    const dimensions = nativeEvent.layout;
    if (!this._isSameMeasure(this.state.dimensions, dimensions)) {
      this.setState({ dimensions });
    }
  };

  onFocus() {
    AppConfig.checkFocus = true;
    AppConfig.checkGPS = false;
    AppConfig.checkCity = false;
    debugger;
    this.props.gotoPage(1);
  }

  onClick(i) {
    // AppConfig.insPGA = this.state.data[i - 2].isPga;
    // AppConfig.insID = this.state.data[i - 2].id;
    // this.props.pushNavScene(InstructorScene, { callback: this.CallBackInstructorScene });
    this.props.pushNavScene(InstructorScene, {ref:(c) => { this.refInstructorScene = c; }, gotoPage:this.props.gotoPage, source: 'home', passProps: { source: 'home', instructor: this.state.data[i - 2], id: instructor.id, lessonTypeId: instructor.lessonTypeId }, callback: this.CallBackInstructorScene });
  }

  componentDidMount() {
   
    this.setState({ isLoading: false });

    let tracker = new GoogleAnalyticsTracker(AppConfig.googleAnalyticsTrackerId);
    tracker.trackScreenView('HomeView');



    // fetch(AppConfig.apiUrl + '/api/FeaturedInstructor')
    //   .then((response) => response.json())
    //   .then((responseData) => {
    //     this.setState({isLoading: false, data: responseData});
    //   })
    //   .catch(error => {
    //     Alert.alert(error.message);
    //     this.setState({isLoading: false});
    //   });


  }

  // onNearButton() {
  //   AppConfig.checkFocus = false;
  //   AppConfig.checkGPS = true;
  //   AppConfig.checkCity = false;
  //   this.props.gotoPage();
  // }

  OnCurrentMyLoc() {
    this.props.pushNavScene(SearchResultsView, {ref:(c) => { this.refSearchResultsView = c; }, gotoPage:this.props.gotoPage, source: 'home', passProps: {source: 'home', searchType: 'gps', hasDates: false} ,callback: this.CallBackSearchResultsView});
  }

  OnCurrentMyLocDates() {
    this.props.pushNavScene(SearchResultsView, {ref:(c) => { this.refSearchResultsView = c; },gotoPage:this.props.gotoPage, source: 'home', passProps: {source: 'home', searchType: 'gps', hasDates: true} ,callback: this.CallBackSearchResultsView});
  }

  OnCurrentMyLocPga() {
    this.props.pushNavScene(SearchResultsView, {ref:(c) => { this.refSearchResultsView = c; },gotoPage:this.props.gotoPage, source: 'home', passProps: {source: 'home', searchType: 'gps', isPga: true, hasDates: false} ,callback: this.CallBackSearchResultsView});
  }

  OnCurrentMyLocJunior() {
    this.props.pushNavScene(SearchResultsView, {ref:(c) => { this.refSearchResultsView = c; },gotoPage:this.props.gotoPage, source: 'home', passProps: {source: 'home', searchType: 'gps', isJunior: true, hasDates: false} ,callback: this.CallBackSearchResultsView});
  }

  render() {
    const dynamicContainerStyle = {
      height: HEIGHT,
      width: this.state.dimensions.width,
    };

    const RECTANGLE_RATIO = 0.8;
    const MIN_SCALE = 0.6;
    const MAX_SCALE = 0.9;
    // we will pass an array of functions as children


/*
    let animals = [
      {
        name: '',
        color: '#00b27e',
        imageURL: '',
        textcolor1: '#00b27e',
        textcolor2: '#00b27e',
        averageRating: 0,
        reviewCount: 0,
        address1: "",
        address2: "",
        city: "",
        state: "",
        isPga: false
      },
      {
        name: '',
        color: '#00b27e',
        textcolor1: '#00b27e',
        textcolor2: '#00b27e',
        imageURL: '',
        averageRating: 0,
        reviewCount: 0,
        address1: "",
        address2: "",
        city: "",
        state: "",
        isPga: false
      },
      {
        name: '',
        color: '#00b27e',
        textcolor1: '#00b27e',
        textcolor2: '#00b27e',
        imageURL: '',
        averageRating: 0,
        reviewCount: 0,
        address1: "",
        address2: "",
        city: "",
        state: "",
        isPga: false
      },
    ];

    if (this.state.data.length > 0) {
      animals = [];
      for (let i = 0; i < this.state.data.length; i++) {
        animals.push(
          {
            name: this.state.data[i].firstName + " " + this.state.data[i].lastName,
            color: '#00b27e',
            imageURL: `https://www.birdienow.com/img/instructor/profile/${this.state.data[i].id}.jpg`,
            averageRating: this.state.data[i].averageRating,
            textcolor1: '#fff',
            textcolor2: '#fff',
            reviewCount: this.state.data[i].reviewCount + ' Reviews',
            address1: this.state.data[i].address1,
            address2: this.state.data[i].address2,
            city: this.state.data[i].city,
            state: this.state.data[i].state,
            isPga: this.state.data[i].isPga
          },
        );
      }
    }


    const pages = animals.map((animal, index) =>
      (animatedPosition, pageWidth, pageOffset) => {
        const height = pageWidth * 0.8;
        const width = height * RECTANGLE_RATIO;
        return (
          <Animal
            {...animal}
            index={index}
            style={{ width: 200 * this.r_width, height: 200 * this.r_width, zIndex: 1 }}
            animatedScale={animatedPosition.interpolate({
              inputRange: [pageOffset - pageWidth / 2, pageOffset + pageWidth / 2, pageOffset + pageWidth + pageWidth / 2],
              outputRange: [MIN_SCALE, MAX_SCALE, MIN_SCALE],
              })}
            animatedOpacity={animatedPosition.interpolate({
              inputRange: [pageOffset - pageWidth / 2, pageOffset + pageWidth / 2, pageOffset + pageWidth + pageWidth / 2],
              outputRange: [0.3, 1, 0.3],
              })}
            handleSubmit={() => this.onInstructor(index)}
          />
        );
      });*/

    return (
      <View style={ styles.container } onLayout={this._onLayout}>
        {/*<Image
          style={styles.back}
          source={require('img/image/ui_back.png')}
          />*/}

          <View style={{}}>
            <View style={{
              alignItems: 'center', backgroundColor: '#aac1ce', borderBottomWidth: 0, borderBottomColor: '#85D3DD',
              height: (Platform.OS === 'ios') ? 100 : 100,
              justifyContent: "flex-end", paddingBottom:45
            }}>
              
                <Image
                  style={styles.logo}
                  source={require('img/image/logo_whiteface.png')}
                />
             
            </View>
          </View>

        
            <TextInput
          style={[styles.inputSearch, {marginTop: (Platform.OS === 'ios') ? 65 : 65 }, {zIndex: (Platform.OS === 'ios') ? 1 : null }]}
          underlineColorAndroid='transparent'
          onFocus={() => {this.onFocus();}}
          placeholder="Search">
        </TextInput>
        <Image
          style={[styles.searchImage, {marginTop: (Platform.OS === 'ios') ? 70 : 70}, {zIndex: (Platform.OS === 'ios') ? 1 : null }]}
          source={require('img/searchimage/nav-search.png')}
          />
        <ScrollView
         
          ref={scrollView => {this.scrollView = scrollView;}}
          scrollEnabled={this.state.scrollState}
        >
         

          {/* 


          {/*<View style={dynamicContainerStyle}>
            <InfiniteCarousel onCallBack={(flag) => {this.enableScrollView(flag);}}>
              {pages}
            </InfiniteCarousel>
          </View>*/}

          {/*<Image style={styles.bgContainer} resizeMode='cover' source={require('img/image/Pages_03.jpg')} />
          <Image style={styles.bgContainer} resizeMode='cover' source={require('img/image/Pages_05.jpg')} />
          <Image style={styles.bgContainer} resizeMode='cover' source={require('img/image/Pages_07.jpg')} />
          <Image style={styles.bgContainer} resizeMode='cover' source={require('img/image/Pages_09.jpg')} />*/}

           {/*<View style={styles.imageWrapper}>
        <Image style={styles.image} source={require('img/image/Pages_03.png')}/>
      </View>*/}

      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>

            
              {/*<Image
                style ={{ flex:1,  width: this.state.dimensions.width, height: this.state.dimensions.width*.7  }}
                source={require('img/image/Pages_01.png')}
              />
              <Text style={{
                backgroundColor: 'transparent', color: 'white', fontFamily: 'Gotham-Bold', fontSize: this.state.dimensions.width*.055,
                position: 'absolute', marginLeft: this.state.dimensions.width*.23, marginTop: this.state.dimensions.width*.44
              }}>IT ALL STARTS HERE</Text>*/}
           

          </View>

          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop:4 }}>

            <TouchableOpacity
              onPress={() => { this.OnCurrentMyLocDates() }}>
              <Image
                style ={{ flex:1,  width: this.state.dimensions.width, height: this.state.dimensions.width*.3 }}
                source={require('img/image/Pages_03.png')}
              />
              <Text style={{
                backgroundColor: 'transparent', color: 'white', fontFamily: 'Gotham-Bold', fontSize: 16,
                position: 'absolute', marginLeft: this.state.dimensions.width*.45, marginTop: this.state.dimensions.width/8
              }}>Available Instructors</Text>
            </TouchableOpacity>

          </View>

          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop:4 }}>

            <TouchableOpacity
              onPress={() => { this.OnCurrentMyLoc() }}>
              <Image
                style ={{  width: this.state.dimensions.width, height: this.state.dimensions.width*.3 }}
                source={require('img/image/Pages_05.png')}
              />
              <Text style={{
                backgroundColor: 'transparent', color: 'white', fontFamily: 'Gotham-Bold', fontSize: 16,
                position: 'absolute', marginLeft: this.state.dimensions.width/10, marginTop: this.state.dimensions.width/8
              }}>Instructors Near Me</Text>
            </TouchableOpacity>

          </View>

          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop:4 }}>

            <TouchableOpacity
              onPress={() => { this.OnCurrentMyLocPga() }}>
              <Image
                style ={{  width: this.state.dimensions.width, height: this.state.dimensions.width*.3 }}
                source={require('img/image/Pages_07.png')}
              />
              <Text style={{
                backgroundColor: 'transparent', color: 'white', fontFamily: 'Gotham-Bold', fontSize: 16,
                position: 'absolute', marginLeft: this.state.dimensions.width*.55, marginTop: this.state.dimensions.width/8
              }}>PGA Instructors</Text>
            </TouchableOpacity>

          </View>

          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop:4 }}>

            <TouchableOpacity
              onPress={() => { this.OnCurrentMyLocJunior() }}>
              <Image
                style ={{  width: this.state.dimensions.width, height: this.state.dimensions.width*.3 }}
                source={require('img/image/Pages_09.png')}
              />
              <Text style={{
                backgroundColor: 'transparent', color: 'white', fontFamily: 'Gotham-Bold', fontSize: 16,
                position: 'absolute', marginLeft: this.state.dimensions.width*.1, marginTop: this.state.dimensions.width/8
              }}>U.S. Kids Golf</Text>
            </TouchableOpacity>

          </View>












{/*
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>

            <TouchableOpacity
              onPress={() => { this.OnCurrentMyLocDates() }}>
              <Image
                style={{ height: this.state.dimensions.width/2-2, width: this.state.dimensions.width/2-2, }}
                source={require('img/image/home01.png')}
              />
              <Text style={{
                backgroundColor: 'transparent', color: 'white', fontFamily: 'Gotham-Bold', fontSize: 13,
                position: 'absolute', marginLeft: this.state.dimensions.width/2/4, marginTop: this.state.dimensions.width/2/2.4
              }}>Available Dates</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => { this.OnCurrentMyLoc() }}>
              <Image
                style={{ height: this.state.dimensions.width/2-2, width: this.state.dimensions.width/2-2, marginLeft: 2, marginTop: 1 }}
                source={require('img/image/home02.png')}
              />
              <Text style={{
                backgroundColor: 'transparent', left: 0, color: 'white', fontFamily: 'Gotham-Bold', fontSize: 13,
                position: 'absolute', marginLeft: this.state.dimensions.width/2/7, marginTop: this.state.dimensions.width/2/2.4
              }}>Instructors Near Me</Text>
            </TouchableOpacity>


          </View>
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
             <TouchableOpacity
              onPress={() => { this.OnCurrentMyLocPga() }}>
              <Image
                style={{ height: this.state.dimensions.width/2-2, width: this.state.dimensions.width/2-2 }}
                source={require('img/image/home03.png')}
              />
              <Text style={{
                backgroundColor: 'transparent', color: 'white', fontFamily: 'Gotham-Bold', fontSize: 13,
                position: 'absolute', marginLeft: this.state.dimensions.width/2/5, marginTop: this.state.dimensions.width/2/2.4
              }}>PGA Instructors</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => { this.OnCurrentMyLocJunior() }}>
              <Image
                style={{ height: this.state.dimensions.width/2-2, width: this.state.dimensions.width/2-2, marginLeft: 2, marginTop: 1 }}
                source={require('img/image/home04.png')}
              />
              <Text style={{
                backgroundColor: 'transparent', left: 0, color: 'white', fontFamily: 'Gotham-Bold', fontSize: 13,
                position: 'absolute', marginLeft: this.state.dimensions.width/2/4, marginTop: this.state.dimensions.width/2/2.4
              }}>U.S. Kids Golf</Text>
            </TouchableOpacity>

          </View>

*/}





           <Text style={{ backgroundColor: 'transparent', left: 10, color: 'black', fontFamily: 'Gotham-Bold', marginTop:20,  marginBottom: 10 }}>Cities</Text>
        <ListView
          dataSource={this.state.ListDataSource}
          renderRow={this.renderCityListRow}
          enableEmptySections={true}
          removeClippedSubviews={false}
        />

          
          {/*<TouchableOpacity
            onPress={() => {this.OnCurrentMyLoc()}}>
            <Text allowFontScaling={false} style={ styles.nearbutton} >Instructors Near Me</Text>
          </TouchableOpacity>*/}

          
        </ScrollView>




       

        {this.state.isLoading ?
          <View style={styles.loadingScene}>
            <ActivityIndicator
              animating={true}
              size="small"
              color="white"
            />
          </View> : null}
      </View>
    );
  }
}

export default HomeView;
