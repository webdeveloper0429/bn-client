import React, { Component, PropTypes } from 'react';
import { Alert, View, Image, TouchableOpacity, Text, ScrollView, Platform, ListView, RefreshControl, TouchableHighlight, ActivityIndicator, Navigator, Dimensions, BackAndroid } from 'react-native';
import styles from './styles';
import AppConfig from 'AppConfig';
import AppStyles from 'AppStyles';
import { SearchBar } from 'AppComponents';
import { InstructorScene, FilterScene, WriteReviewSceneNew, ContactorDetailScene, FullCalendarScene } from 'AppScenes';
import { RequestApi, MakeCancelable, GlobalStorage, PurchaseLessons } from 'AppUtilities';
import _, { isEqual } from 'lodash';
import moment from 'moment';
import { GoogleAnalyticsTracker } from 'react-native-google-analytics-bridge';

let { width, height } = Dimensions.get('window');
let r_width =  width / 1125;
let r_height = height / 1659;

class PurchaseLessonsView extends Component {
  static propTypes = {
    pushNavScene: PropTypes.func.isRequired,
    popNavBack: PropTypes.func.isRequired,
    popNavNBack: PropTypes.func,
  };

  constructor(props, context) {
    super(props, context);

    this.state = {
      isLoading: false,
      isRefreshing: false,
      isDataLoaded: false,
      availableCount: 0,
      redeemedCount: 0,
      ListDataSource: new ListView.DataSource({
        rowHasChanged: (r1, r2) => !isEqual(r1, r2),
        sectionHeaderHasChanged: (s1, s2) => !isEqual(s1, s2),
      }),
    };

    this.ListSource = {};
    this.state.ListDataSource = this.state.ListDataSource.cloneWithRowsAndSections(this.ListSource);

    this.renderRow = this.renderRow.bind(this);
    this.renderSectionHeader = this.renderSectionHeader.bind(this);
    this.renderSeparator = this.renderSeparator.bind(this);
    this.showRatingPage = this.showRatingPage.bind(this);
    this.showContactorDetailPage = this.showContactorDetailPage.bind(this);
    this.pushCallback = this.pushCallback.bind(this);
    this.OnRetrieveAppointments = this.OnRetrieveAppointments.bind(this);
    this.checkListDataCount = this.checkListDataCount.bind(this);
    this.onProfilePage = this.onProfilePage.bind(this);
    this.onBookLesson = this.onBookLesson.bind(this);
    this.CallBackInstructorScene = ::this.CallBackInstructorScene;

    this.didAppear = ::this.didAppear;
    this.CallBackWriteReviewScene = ::this.CallBackWriteReviewScene;
    this.didAppear = ::this.didAppear;
  }

  didAppear() {
 
 
    this.OnRetrieveAppointments();
    if (this.refInstructorScene != null) {
      this.refInstructorScene.handlePopCheck(2);
    } else if (this.refWriteReviewSceneNew != null) {
      this.props.popNavBack();
    } else if (this.refCancelAppointment != null) {
      this.props.popNavBack();
    } else if (this.refContactorDetaileScene != null) {
      this.props.popNavBack();
    }
  }

  checkListDataCount() {
    return this.state.listData.length > 0;
  }

  CallBackInstructorScene() {
    this.OnRetrieveAppointments();
  }

  OnRetrieveAppointments(){

    let tracker = new GoogleAnalyticsTracker(AppConfig.googleAnalyticsTrackerId);
    tracker.trackScreenView('PurchaseLessonsView');
    
    
    if (AppConfig.global_isLoggedIn == false) {
      return;
    }


    this.setState({ isLoading: true });
    PurchaseLessons(AppConfig.global_userToken)
      .then((response) => {
         
        
        if (!response.error_description) { //success 200
          this.ListSource = { up: [], past: [] };
          response.forEach((item) => {

            if (item.appointmentId == null) {
              this.ListSource.up.push(item);
            }
            else {
              this.ListSource.past.push(item);
            }
            
          });
          this.setState({ availableCount: this.ListSource.up.length, redeemedCount: this.ListSource.past.length, 
            isLoading: false, isDataLoaded: true, ListDataSource: this.state.ListDataSource.cloneWithRowsAndSections(this.ListSource)});
        } else { // failed 400
          
          this.setState({ isLoading: false });
        }
      })
      .catch(error => {
        Alert.alert(error.message);
        // this.setState({ isLoading: false });
      });
  }

  CallBackWriteReviewScene() {
    this.didAppear();
  }

  pushCallback() {

  }

  componentDidMount() {
		//BackAndroid.addEventListener('hardwareBackPress', this.onBackPress.bind(this));
    this.OnRetrieveAppointments();
  }

	onBackPress() {
		this.props.popNavBack();
	}

  showRatingPage(rowData, sectionID, rowID) {
    AppConfig.insID = rowData.instructorId;
    AppConfig.writeReviewData = rowData;
    this.props.pushNavScene(WriteReviewSceneNew, {ref:(c) => { this.refWriteReviewSceneNew = c; }, callback: this.CallBackWriteReviewScene});
  }

  showContactorDetailPage(rowData, sectionID, rowID) {
    this.props.pushNavScene(ContactorDetailScene, {ref:(c) => { this.refContactorDetaileScene = c; } });
  }

  onBookLesson(rowData) {
    this.props.pushNavScene(FullCalendarScene, {
      ref:(c) => { this.refFullCalendarScene = c; },
      InstructorID: rowData.instructorId,
      LocationID: rowData.locations[0].id,
      LessonTypeID: rowData.lessonTypeId,
    });
  }

  onProfilePage(instructor){
    this.props.pushNavScene(InstructorScene, {ref:(c) => { this.refInstructorScene = c; }, passProps: {instructor: instructor, id: instructor.instructorId, lessonTypeId:instructor.lessonTypeId}, callback: this.CallBackWriteReviewScene});
  }

  renderRow(rowData, sectionID, rowID) {
    const renderLocations = [];

    for (let i = 0; i < rowData.locations.length; i++) {
      renderLocations.push(<View key={rowData.locations[i].name}>
          <View   style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
            <Image style={{ width: 16, height: 16, marginRight: 4 }} source={require('img/searchimage/nav-home.png')} />
            <Text allowFontScaling={false} style={{ flex: 1, fontSize: 11, fontFamily: 'Gotham-Book' }} numberOfLines={1} ellipsizeMode="tail">{rowData.locations[i].name}</Text>
          </View>
          {/*<View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 4 }}>
            <Image style={{ width: 16, height: 16, marginRight: 4 }} source={require('img/searchimage/nav-home.png')} />
            <Text allowFontScaling={false} style={{ flex: 1, fontSize: 11, fontFamily: 'Gotham-Book' }} numberOfLines={3} ellipsizeMode="tail">{rowData.locations[i].address1 + " " + rowData.locations[i].city + " " + rowData.locations[i].state + " " + rowData.locations[i].zip}</Text>
          </View>*/}
        </View>
        );
    }





    let reviewWrapper;
    if (sectionID === 'up'){
      reviewWrapper =
        (<TouchableOpacity
          style={{ alignItems: 'center', justifyContent: 'center', height: 40 }}
          onPress={() => this.onBookLesson(rowData)}
          underlayColor="#d9d9d9"
        >
          <Text allowFontScaling={false} style={{ fontFamily: 'Gotham-Medium', color: '#32b3fa' }}>Book Lesson</Text>
        </TouchableOpacity>);
    } else if (rowData.overallRating === 0) {
      reviewWrapper =
        (<TouchableOpacity
          style={{ alignItems: 'center', justifyContent: 'center', height: 40 }}
          onPress={() => this.showRatingPage(rowData, sectionID, rowID)}
          underlayColor="#d9d9d9"
        >
          <Text allowFontScaling={false} style={{ fontFamily: 'Gotham-Medium', color: '#32b3fa' }}>Write a review</Text>
        </TouchableOpacity>);
    } 

    const startDate = moment(rowData.localStartDate), endDate = moment(rowData.localEndDate);
    let strDate;

    if (startDate.isSame(endDate, 'day')) {
      strDate = `${startDate.format('LLLL')}\n- ${endDate.format('h:mm A')}`;
    } else {
      strDate = `${startDate.format('LLLL')}\n- ${endDate.format('LLLL')}`;
    }

    return (
      <View>
        <TouchableOpacity
          style={{ backgroundColor: '#fff' }}
          onPress={() => this.onProfilePage(rowData)}
          underlayColor="#d9d9d9"
        >
          <View>
            <View style={{flexDirection: 'row',alignItems: 'center',justifyContent: 'center',paddingRight: 15, paddingVertical: 8}}>
              <Image
                source={{uri: 'https://www.birdienow.com/img/instructor/profile/'+ rowData.instructorId +'.jpg'}}
                style={{width: 246 * r_width, height: 246 * r_width, borderRadius:123 * r_width, marginLeft:36 * r_width}}/>
              <View style={{ flex: 1, flexDirection: 'column', marginLeft: 15 }}>
                <Text allowFontScaling={false} style={{ fontSize: 16, fontFamily: 'Gotham-Bold' }} numberOfLines={1} ellipsizeMode="tail">{`${rowData.instructorFirstName} ${rowData.instructorLastName}`}</Text>
                
                
                {renderLocations}

                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 6 }}>
                  <Image style={{ width: 16, height: 16, marginRight: 4 }} source={require('img/searchimage/nav-appointment.png')} />
                  <Text allowFontScaling={false} style={{ fontSize: 11, fontFamily: 'Gotham-Book' }}>{rowData.lessonTypeName}</Text>
                </View>

                {/*<View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 6 }}>
                  <Image style={{ width: 16, height: 16, marginRight: 4 }} source={require('img/searchimage/nav-appointment.png')} />
                  <Text allowFontScaling={false} style={{ fontSize: 11, fontFamily: 'Gotham-Book' }}>{strDate}</Text>
                </View>*/}
              </View>
            </View>
            <View style={{ backgroundColor: '#ececec', height: 1, marginRight: 15 }} />
            {reviewWrapper}
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  renderSectionHeader(sectionData, sectionID) {
    return (
      <Text allowFontScaling={false} style={{ fontSize: 12, color: '#878787', paddingLeft: 20, paddingVertical: 7, fontFamily: 'Gotham-Bold' }}>
        {sectionID === 'up' ? 'AVAILABLE LESSONS (' + this.state.availableCount + ')' : 'REDEEMED LESSONS (' + this.state.redeemedCount + ')' }
      </Text>
    );
  }

  renderSeparator(sectionID, rowID) {
    if (parseInt(rowID, 10) !== this.ListSource[sectionID].length - 1) {
      return (
        <View key={`${sectionID}-${rowID}`} style={{ backgroundColor: '#f7f7f7', height: 10 }} />
      );
    }
    return null;
  }

  render() {
    if (!AppConfig.global_isLoggedIn) {
        return (
      <View style={{ flex: 1, backgroundColor: '#F9F9F9'}}>
          <View style={{ flex: 1 }}>
            <View style={{
              alignItems: 'center', backgroundColor: '#aac1ce', borderBottomWidth: 0, borderBottomColor: '#85D3DD',
              height: (Platform.OS === 'ios') ? 65 : 40,
              justifyContent: "center"
            }}>
              <Text allowFontScaling={false} style={{
                textAlign: "center", color: '#ffffff', fontSize: 15,
                marginTop: (Platform.OS === 'ios') ? 20 : 0,

                fontFamily: 'Gotham-Bold'
              }}>
                Purchase Lessons
          </Text>
            </View>
          </View>
          <View style={{ flex: 9, backgroundColor: '#F9F9F9', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
            <Image
              style={{ width: 200, height: 200 }}
              source={require('img/image/NoPurchases_Icon.png')}
            />
            <Text style={{ fontSize: 17, color: '#5b5b5b', }}>You have no</Text>
            <Text style={{ fontSize: 17, color: '#5b5b5b', paddingLeft:30,
             paddingRight:30, textAlign:'center' }}>purchased lessons</Text>
          </View>
        </View>

        );
      
    } else {

      return (
      <View style={{ flex: 1, backgroundColor: "white"}}>
        <View style={{
        flexDirection: 'row',
        alignItems: 'center', backgroundColor: '#aac1ce', borderBottomWidth: 0, borderBottomColor: '#85D3DD',
        height: (Platform.OS === 'ios') ? 65 : 40,
        justifyContent: "center"
      }}>
        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={() => { this.props.popNavBack(); }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#ffffff',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Back</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 2, justifyContent: "center" }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#ffffff',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Purchased Lessons</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#ffffff',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}></Text>
        </TouchableOpacity>
      </View>

        <ListView
          style={{ backgroundColor: '#f7f7f7' }}
          dataSource={this.state.ListDataSource}
          renderRow={this.renderRow}
          renderSectionHeader={this.renderSectionHeader}
          renderSeparator={this.renderSeparator}
          enableEmptySections={true}
          automaticallyAdjustContentInsets={false}
        />

        {this.state.isDataLoaded && this.state.availableCount == 0 && this.state.redeemedCount ==0 ? 
        <View style={{ flex: 9, paddingBottom:150, backgroundColor: '#F9F9F9', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
            <Image
              style={{ width: 200, height: 200 }}
              source={require('img/image/NoPurchases_Icon.png')}
            />
            <Text style={{ fontSize: 17, color: '#5b5b5b', }}>You have no</Text>
            <Text style={{ fontSize: 17, color: '#5b5b5b', paddingLeft:30,
             paddingRight:30, textAlign:'center' }}>purchased lessons</Text>
          </View>
          : null}

        {this.state.isLoading ?
          <View style={styles.loadingScene}>
            <ActivityIndicator
              animating={true}
              size="small"
              color="white"
            />
          </View> : null}
      </View>
    );



    }
    
  }
}

export default PurchaseLessonsView;
