import React, { Component, PropTypes } from 'react';
import { ActivityIndicator, Alert, View, Platform, Text, TouchableHighlight, TouchableOpacity, StatusBar, TextInput, Image, Keyboard } from 'react-native';
import styles from './styles';
import { PaymentView, VideoWebView, VideoView, EtcView, MainScene, StartLoginView, LoginNavScene, RegisterNavScene, PurchaseLessonsView, ForgotPasswordScene, ChangePasswordScene  } from 'AppScenes';
import { SignIn, GlobalStorage, GetAccountInfo } from 'AppUtilities';
import AppConfig from 'AppConfig';
import ImagePicker from 'react-native-image-crop-picker';
import { GoogleAnalyticsTracker } from 'react-native-google-analytics-bridge';

class ProfileView extends Component {
  static propTypes = {
    pushScene: PropTypes.func,
    popNavBack: PropTypes.func.isRequired,
    popNavNBack: PropTypes.func,
    pushNavScene: PropTypes.func.isRequired,
    callback: PropTypes.func,
    popNavToTop: PropTypes.func.isRequired,
    resetNavToScene: PropTypes.func.isRequired,
  };

  constructor(props, context) {
    super(props, context);

    this.state = {
      image: null,
      loginStatus: !AppConfig.global_isLoggedIn,   
    };

    StatusBar.setHidden(false);
    this.OnSignIn = this.OnSignIn.bind(this);
    this.grant_type = 'password';
    this.username = "";
    this.password = "";

    this.OnAvatar = this.OnAvatar.bind(this);
   
   
    this.RegisterCallback = this.RegisterCallback.bind(this);
    this.PurchaseLessonsCallback = this.PurchaseLessonsCallback.bind(this);
    this.LoginCallback = this.LoginCallback.bind(this);
    this.GoToLogin = this.GoToLogin.bind(this);
    this.GoToRegister = this.GoToRegister.bind(this);
    this.OnLogout = this.OnLogout.bind(this);
    this.didAppear = ::this.didAppear;
    this.GoToPurchaseLessons = ::this.GoToPurchaseLessons;

    
  }

  didAppear() {
    
    // if (this.refInstructorScene != null) {
    //   this.refInstructorScene.handlePopCheck(2);
    // } else if (this.refWriteReviewScene != null) {
    //   this.props.popNavBack();
    // } else if (this.refCancelAppointment != null) {
    //   this.props.popNavBack();
    // } else if (this.refContactorDetaileScene != null) {
    //   this.props.popNavBack();
    // }


    // if (this.refInstructorScene != null) 
    //   this.props.popNavBack();

    // if (this.refPurchaseLessonsView != null)
    //   this.props.popNavBack();
    //this.props.popNavNBack(5);
    
    this.props.popNavBack();
    this.props.popNavBack();
    this.props.popNavBack();
    this.props.popNavBack();
    this.props.popNavBack();
  }

  OnSignIn(){
    Keyboard.dismiss();
    this.setState({ isLoading: true });
    SignIn(this.grant_type, this.username, this.password)
      .then((response) => {
        if (!response.error_description) { //success 200
          GlobalStorage.setItem("storage_user", this.username);
          GlobalStorage.setItem("storage_password", this.password);
          GlobalStorage.setItem("storage_loggedIn", "true");
          GlobalStorage.setItem("storage_token", response.access_token);
          AppConfig.global_userToken = response.access_token; // set the user token
          this.setState({ isLoading: false });
          this.props.resetToScene(MainScene);
        } else { // failed 400
          Alert.alert(response.error_description);
          this.setState({ isLoading: false });
          this.OnLogout();
        }
      })
      .catch(error => {
        Alert.alert(error.message);
        this.setState({ isLoading: false });
        this.OnLogout();
      });
  }

  componentDidMount() {
    this.setState({loginStatus: !AppConfig.global_isLoggedIn});
    let tracker = new GoogleAnalyticsTracker(AppConfig.googleAnalyticsTrackerId);
    tracker.trackScreenView('ProfileView');
  }

  //user functions
  RegisterCallback(username) {
  
    this.username = username;
    this.setState({loginStatus: !AppConfig.global_isLoggedIn});
  }

  PurchaseLessonsCallback() {
    this.setState({loginStatus: !AppConfig.global_isLoggedIn});
  }

  LoginCallback() {
    this.setState({loginStatus: !AppConfig.global_isLoggedIn});
  }

  GoToLogin() {
    this.props.pushNavScene(LoginNavScene, { callBack: this.LoginCallback});
  }

  OnLogout() {
    AppConfig.global_isLoggedIn = false;
    GlobalStorage.setItem("storage_user", "");
    GlobalStorage.setItem("storage_password", "");
    GlobalStorage.setItem("storage_loggedIn", "false");
    GlobalStorage.setItem("storage_token", "");
    AppConfig.global_userToken = "";
    this.setState({loginStatus: !AppConfig.global_isLoggedIn});
  }

  GoToRegister() {
    this.props.pushNavScene(RegisterNavScene, {ref:(c) => { this.refRegisterNavScene = c; }, passProps: { source: 'profile' }, callBack: this.RegisterCallback});
  }

  GoToPurchaseLessons() {
    this.props.pushNavScene(PurchaseLessonsView, {ref:(c) => { this.refPurchaseLessonsView = c; }, callBack: this.RegisterCallback});
  }

  OnChangePassword() {
    this.props.pushNavScene(ChangePasswordScene, {ref:(c) => { this.refChangePasswordScene = c; } });
  }

  OnAvatar() {
    ImagePicker.openPicker({
      width: 500,
      height: 500,
      cropping: true
    }).then(image => {
      this.setState({ image: { uri: image.path, width: image.width, height: image.height, mime: image.mime } });
    }).catch(error => {
      Alert.alert(error.message);
    });
  }

  OnEtc() {
    this.props.pushScene(EtcView);
  }

  render() {

    let RenderUser = [];
    if (!this.state.loginStatus) { //logged in
      RenderUser.push(

        
        <View key="user_authorized" >

          <View style={{}}>
            <View style={{
              alignItems: 'center', backgroundColor: '#aac1ce', borderBottomWidth: 0, borderBottomColor: '#85D3DD',
              height: (Platform.OS === 'ios') ? 65 : 40,
              justifyContent: "flex-end", paddingBottom:10
            }}>
              
                <Image
                  style={styles.logo}
                  source={require('img/image/logo_whiteface.png')}
                />
             
            </View>
          </View>


          <TouchableOpacity onPress={() => {this.GoToPurchaseLessons();}} 
            style={{ marginTop:10, height: 40, backgroundColor: "#ffffff", borderTopWidth: 1, borderTopColor:'#e7e7e9', borderBottomWidth:1, borderBottomColor:'#e7e7e9', flexDirection:'row', alignItems:'center'}}>
      
            <View style={{flex: 1, justifyContent: "center"}}>
              <Image style={{marginLeft: 10, width: 20, height: 20}} source={require('img/icon/profile-rates-50.png')}/>
            </View>
            <View style={{flex: 8, justifyContent: "center"}}>
              <Text allowFontScaling={false} style={[styles.content,{textAlign: "left"}]}>My Purchased Lessons</Text>
            </View>
            <View style={{flex: 1, justifyContent: "center"}}>
              <Image style={{width: 20, height: 20}} source={require('img/icon/icon_arrow.png')}/>
            </View>
          
          </TouchableOpacity>

          <TouchableOpacity onPress={() => {this.OnChangePassword();}}
            style={{ marginTop:10, height: 40, backgroundColor: "#ffffff", borderTopWidth: 1, borderTopColor:'#e7e7e9', borderBottomWidth:1, borderBottomColor:'#e7e7e9', flexDirection:'row', alignItems:'center'}}>
           
            <View style={{flex: 9, justifyContent: "center"}}>
              <Text allowFontScaling={false} style={[styles.content,{paddingLeft: 10, textAlign: "left"}]}>Change Password</Text>
            </View>
            <View style={{flex: 1, justifyContent: "center"}}>
              <Image style={{width: 20, height: 20}} source={require('img/icon/icon_arrow.png')}/>
            </View>
          
          </TouchableOpacity>

          <TouchableOpacity onPress={() => {this.OnLogout();}}
            style={{ marginTop:10, height: 40, backgroundColor: "#ffffff", borderTopWidth: 1, borderTopColor:'#e7e7e9', borderBottomWidth:1, borderBottomColor:'#e7e7e9', flexDirection:'row', alignItems:'center'}}>
           
            <View style={{flex: 1, justifyContent: "center"}}>
              <Text allowFontScaling={false} style={[styles.content,{paddingLeft: 10, textAlign: "left"}]}>Sign Out</Text>
            </View>
          
          </TouchableOpacity>

          {/*<View style={{ paddingTop: 10, paddingLeft: window.width * 0.05 }}>
            <TouchableOpacity
              onPress={() => {this.OnLogout();}}
            underlayColor="#bde7ff"
            style={{ marginTop: 10 }}
              style={{ width: window.width * 0.9, height: 40, borderRadius: 5, backgroundColor: '#989898', justifyContent: 'center' }}>
              <Text style={{ alignSelf: 'center', fontSize: 16, fontWeight: 'bold', color: '#ffffff', fontFamily: 'Gotham-Book' }}>Logout</Text>
            </TouchableOpacity>
          </View>*/}


{/*
          <TouchableHighlight
            onPress={() => {this.OnLogout();}}
            underlayColor="#bde7ff"
          >
            <Text allowFontScaling={false} style={styles.btnLogin}>Logout</Text>
          </TouchableHighlight>*/}


         

           
         {/*<TouchableHighlight
          onPress={() => {this.OnAvatar();}}
          underlayColor="#bde7ff"
          style={{ marginTop: 10 }}
        >
          <Text allowFontScaling={false} style={styles.btnLogin}>Select avatar photo</Text>
        </TouchableHighlight>

        <Image style={{ width: 100, height: 100, resizeMode: 'contain', marginTop: 15 }} source={this.state.image} />*/}


        </View>

        
      );
    } else {  //logged out, free account
      RenderUser.push(
        <View key="user_free">

          <View style={{}}>
            <View style={{
              alignItems: 'center', backgroundColor: '#aac1ce', borderBottomWidth: 0, borderBottomColor: '#85D3DD',
              height: (Platform.OS === 'ios') ? 65 : 40,
              justifyContent: "flex-end", paddingBottom:10
            }}>

              <Image
                style={styles.logo}
                source={require('img/image/logo_whiteface.png')}
              />

            </View>
          </View>



          <View style={[styles.inputWrapper, { marginTop: 10, borderTopWidth: 1 }]}>
            <TextInput
              underlineColorAndroid='transparent'
              style={styles.inputEmail}
              placeholder="Email Address"
              placeholderTextColor="#999"
              keyboardType='email-address'
              autoCapitalize="none"
              autoCorrect={false}
              onChangeText={(text) => { this.username = text }} />
          </View>


          <View style={[styles.inputWrapper, {}]}>
            <TextInput
              underlineColorAndroid='transparent'
              style={styles.inputEmail}
              placeholder="Password"
              placeholderTextColor="#999"
              secureTextEntry={true}
              onChangeText={(text) => { this.password = text }}
            />
          </View>

          <View style={{ paddingTop: 10, paddingLeft: window.width * 0.05, paddingRight: window.width * 0.05 }}>
            <TouchableOpacity
              onPress={() => { this.props.pushNavScene(ForgotPasswordScene, {ref:(c) => { this.refForgotPasswordScene = c; }, passProps: { source: 'profile' }}) }}
            underlayColor="#bde7ff"
            style={{ marginTop: 10 }}
              style={{ justifyContent: "flex-end" }}>
              <Text style={{ alignSelf: 'flex-end', fontSize: 13, fontWeight: 'bold', color: '#32b3fa', fontFamily: 'Gotham-Book' }}>Forgot password?</Text>
            </TouchableOpacity>
          </View>

          <View style={{ paddingTop: 10, paddingLeft: window.width * 0.05 }}>
            <TouchableOpacity
              onPress={() => {
                AppConfig.global_isLoggedIn = true;
                this.OnSignIn();
              }}
              style={{ width: window.width * 0.9, height: 40, borderRadius: 5, backgroundColor: '#32b3fa', justifyContent: 'center' }}>
              <Text style={{ alignSelf: 'center', fontSize: 16, fontWeight: 'bold', color: '#ffffff', fontFamily: 'Gotham-Book' }}>Login</Text>
            </TouchableOpacity>
          </View>

          

          <View style={{ paddingTop: 20, paddingLeft: window.width * 0.05 }}>
            <TouchableOpacity
              onPress={() => {this.GoToRegister();}}
            underlayColor="#bde7ff"
            style={{ marginTop: 10 }}
              style={{ width: window.width * 0.9, height: 40, borderRadius: 5, backgroundColor: '#32b3fa', justifyContent: 'center' }}>
              <Text style={{ alignSelf: 'center', fontSize: 16, fontWeight: 'bold', color: '#ffffff', fontFamily: 'Gotham-Book' }}>Sign Up</Text>
            </TouchableOpacity>
          </View>



{/*
          <Image
            style={styles.splash}
            source={require('img/image/logo.png')}
          />
          <Text allowFontScaling={false} style={styles.topLabel}>ALREADY A MEMBER?</Text>
          <TextInput
            style={styles.inputEmail}
            fontSize={17}
            placeholder="Email"
            defaultValue=""
            placeholderTextColor="#999"
            onChangeText={(text) => { this.username = text }}
            underlineColorAndroid='transparent'
            keyboardType='email-address'
            autoCapitalize="none"
            autoCorrect={false}
          />
          <TextInput
            style={styles.inputEmail}
            fontSize={17}
            placeholder="Password"
            placeholderTextColor="#999"
            defaultValue=""
            secureTextEntry={true}
            onChangeText={(text) => { this.password = text }}
            underlineColorAndroid='transparent'
          />
          <Text allowFontScaling={false} style={styles.forgotLabel}>Forgot your password?</Text>
          


          <View>
            <TouchableHighlight
              onPress={() => {
                AppConfig.global_isLoggedIn = true;
                this.OnSignIn();
              }}
              style={{}}
              underlayColor="#3c8671" >
              <Text allowFontScaling={false} style={styles.btnLogin}>LOGIN</Text>
            </TouchableHighlight>


          </View>


          <View style={{ marginTop: 50 }}>
            

          <TouchableHighlight
            onPress={() => {this.GoToRegister();}}
            underlayColor="#bde7ff"
            style={{ marginTop: 10 }}
          >
            <Text allowFontScaling={false} style={styles.btnRegister}>REGISTER</Text>
          </TouchableHighlight>
            
            </View>
*/}


        </View>
      );
    }

    return (


<View style={styles.container}>
        
       
        {RenderUser}
      


         {/*<TouchableHighlight
          onPress={() => {this.OnEtc();}}
          underlayColor="#bde7ff"
          style={{ marginTop: 10 }}
        >
          <Text allowFontScaling={false} style={styles.btnLogin}>OTHER</Text>
        </TouchableHighlight>*/}


      </View>
    );
  }
}

export default ProfileView;
