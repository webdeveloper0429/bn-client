import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    //flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    backgroundColor: '#F9F9F9',


    // paddingTop: 20,
    // backgroundColor: "#aac1ce",
    // paddingHorizontal: 30,
    // paddingBottom: 0
  },
  topLabel: {
    alignSelf: 'stretch',
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  formButtonWrapper: {
    backgroundColor: '#ffffff',
    borderColor: '#f4f4f4',
    borderWidth: 0,
    borderBottomWidth: 1,
    justifyContent: 'center',
  },
  formButton: {
    height: 40,
    fontSize: 13,
    backgroundColor: '#ffffff',
    borderWidth: 0,
    paddingVertical: 0,
    paddingHorizontal: 0,
    paddingLeft: 10
  },


  inputWrapper: {
    backgroundColor: '#ffffff',
    borderColor: '#f4f4f4',
    borderWidth: 0,
    borderBottomWidth: 1,
    justifyContent: 'center',
  },
  inputEmail: {
    //width: window.width * 0.9,
    height: 40,
    fontSize: 13,
    //alignSelf: 'stretch',
    //flex: 1,
    backgroundColor: '#ffffff',
    borderWidth: 0,
    //borderColor: '#f4f4f4',
    //borderRadius: 5,
    paddingVertical: 0,
    paddingHorizontal: 0,
    paddingLeft: 10
  },
  forgotLabel: {
    marginTop: 5,
    color: '#fff',
    fontSize: 15,
    alignSelf: 'stretch'
  },
  btnLogin: {
    color: '#868788',
    fontFamily: 'Gotham-Bold',
    fontWeight: 'bold',
    fontSize: 15,
    width: 305,
    textAlign: 'center', 
    borderWidth: 0,
    backgroundColor: '#ffffff',
    borderColor: '#fff',
    borderRadius: 5,    
    paddingTop: 8,
    paddingBottom: 7,
  },
  btnRegister: {
    color: '#ffffff',
    fontFamily: 'Gotham-Bold',
    fontWeight: 'bold',
    fontSize: 15,
    width: 305,
    textAlign: 'center', 
    borderWidth: 0,
    backgroundColor: '#868788',
    borderColor: '#fff',
    borderRadius: 5,   
    paddingTop: 8,
    paddingBottom: 7,
  },
  loadingScene: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: window.width,
    height: window.height,
    alignSelf: 'stretch',
    backgroundColor: 'rgba(0,0,0,0.5)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  splash: {
    width: window.width - 120,
    height: (window.width - 120) * 51 / 160,
    marginBottom: 20
  },

  logo: {
    width: window.width * .45,
    height: (window.width * .07),
  },
  // container: {
  //   flex: 1,
  //   alignItems: 'center',
  //   justifyContent: 'center',
  //   backgroundColor: '#aac1ce',
  // },
  // btnLogin: {
  //   color: '#fff',
  //   textAlign: 'center',
  //   fontSize: 16,
  //   borderWidth: 2,
  //   borderColor: '#fff',
  //   borderRadius: 5,
  //   paddingLeft: 10,
  //   paddingRight: 7,
  //   paddingTop: 10,
  //   paddingBottom: 7,
  // },




});
