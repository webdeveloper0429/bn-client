import React, { Component, PropTypes } from 'react';
import {
  Alert,
  StyleSheet,
  Dimensions,
  Keyboard,
  ActivityIndicator,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Platform,
  View,
  Image,
  TouchableOpacity,
  Text,
  TextInput,
  ListView,
  RefreshControl,
  ScrollView,
  InteractionManager,
  Animated,
	PanResponder
} from 'react-native';
import styles from './styles';
import AppConfig from 'AppConfig';
import AppStyles from 'AppStyles';
import { SearchBar, HeaderBar, RefreshListView, ToggleButton } from 'AppComponents';
import { FilterScene, ListDetailScene, InstructorScene, BookScene, FullCalendarScene  } from 'AppScenes';
import { MakeCancelable } from 'AppUtilities';
import _, { isEqual } from 'lodash';
import StarRating from 'react-native-star-rating';
import moment from 'moment';
import { GoogleAnalyticsTracker } from 'react-native-google-analytics-bridge';

const { width, height } = Dimensions.get('window');

class FadeInView extends Component {

  
	constructor(props) {
		super(props);
		this.state = {
			top: new Animated.Value(height),          // Initial value for opacity: 0
      distanceID: 100,
      gender: 'Any',
      priceLevel: 'Any',
      priceLevel1: false,
      priceLevel2: false,
      priceLevel3: false,
      priceLevel4: false,
      isPga: false,
      isJunior: false,
      isDeal: false,
      isFlight: false,
      isVideo: false,
      hasDates: false, 
      price: []
		};
		for (let i = 0; i < 5; i++) {
		  this.state.price[i] = false;
    }
    
    
    
	}
	componentDidMount() {
    
    
    //this.setState({ pga: true});
    //this.state.pga = true;
    // this.setState({ isJunior: this.isJunior });
    
    
		// Animated.timing(                            // Animate over time
		// 	this.state.top,                      // The animated value to drive
		// 	{
		// 		toValue: height,                             // Animate to opacity: 1, or fully opaque
		// 		duration: 0
		// 	}
		// ).start();                                  // Starts the animation
	}
	fadeOut = () => {
    
		Animated.timing(                            // Animate over time
			this.state.top,                      // The animated value to drive
			{
				toValue: height,                             // Animate to opacity: 1, or fully opaque
				duration: 800
			}
		).start();                                  // Starts the animation
	};
	fadeIn = () => {
    
    
		Animated.timing(                            // Animate over time
			this.state.top,                      // The animated value to drive
			{
				toValue: 150,                             // Animate to opacity: 1, or fully opaque
				duration: 800
			}
		).start();                                  // Starts the animation
    
	};

	onGender = (i) => {
    if (this.state.gender == i)
      this.setState({ gender: "Any" });
    else 
		  this.setState({ gender: i });
	};

	onDistance = (i) => {
		this.setState({ distanceID: i });
	};

	onPrice1 = () => {
    this.setState({ priceLevel1: !this.state.priceLevel1 });
	};
  onPrice2 = () => {
    this.setState({ priceLevel2: !this.state.priceLevel2 });
	};
  onPrice3 = () => {
    this.setState({ priceLevel3: !this.state.priceLevel3 });
	};
  onPrice4 = () => {
    this.setState({ priceLevel4: !this.state.priceLevel4 });
	};

	onChangePga = () => {
    this.setState({ isPga: !this.state.isPga });
  };

  onChangeJunior = () => {
    this.setState({ isJunior: !this.state.isJunior });
  };

  onChangeVideo = () => {
    this.setState({ isVideo: !this.state.isVideo });
  };

  onChangeFlight = () => {
    this.setState({ isFlight: !this.state.isFlight });
  };

  onChangeDeal = () => {
    this.setState({ isDeal: !this.state.isDeal });
  };

  onChangeHasDates = () => {
    this.setState({ hasDates: !this.state.hasDates });
  };

	render() {
	  const {gender, distanceID} = this.state;
    
		return (
      <Animated.View                            // Special animatable View
        style={{
					...this.props.style,
          backgroundColor: '#f9f9f9',
					top: this.state.top,          // Bind opacity to animated value
				}}
      >
        <ScrollView>
					{this.props.children}
          
          <View style={{marginTop: 0, alignItems: 'center',justifyContent: 'center', flexDirection: 'row', height: 40, backgroundColor: "#ffffff", borderTopWidth: 0, borderTopColor:'#e7e7e9', borderBottomWidth: 0, borderBottomColor:'#e7e7e9'}}>
            {/*<TouchableOpacity onPress={() => {this.onGender('Any');}} style={{ flex: 1, justifyContent: 'center', alignItems: 'center', height: 40, borderColor: '#e7e7e9', borderTopLeftRadius: 0, borderBottomLeftRadius: 0, borderWidth: 1, backgroundColor: gender=='Any'?'#32b3fa':'transparent'}}>
              <Text>All</Text>
            </TouchableOpacity>*/}
            <TouchableOpacity onPress={() => {this.onGender('Male');}} style={{ flex: 1, justifyContent: 'center', alignItems: 'center', height: 40, borderColor: '#e7e7e9', borderTopLeftRadius: 0, borderBottomLeftRadius: 0, borderWidth: 1, backgroundColor: gender=='Male'?'#32b3fa':'transparent'}}>
              <Text style={{color: gender=='Male'?'white':'black'}}>Male</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {this.onGender('Female');}} style={{ flex: 1, justifyContent: 'center', alignItems: 'center', height: 40, borderColor: '#e7e7e9', borderTopRightRadius: 0, borderBottomRightRadius: 0, borderWidth: 1, backgroundColor: gender=='Female'?'#32b3fa':'transparent'}}>
              <Text style={{color: gender=='Female'?'white':'black'}}>Female</Text>
            </TouchableOpacity>
          </View>
          {/*<Text style={{marginLeft: 15, marginTop: 20}}>Distance</Text>
          <View style={{height: 50, paddingHorizontal: 30, justifyContent: 'center', alignItems: 'center', flexDirection: 'row'}}>
            <TouchableOpacity onPress={() => {this.onDistance(5);}} style={{justifyContent: 'center', alignItems: 'center', width: 60, height: 30, borderColor: '#4b5b64', borderTopLeftRadius: 5, borderBottomLeftRadius: 5, borderWidth: 1, backgroundColor: distanceID===5?'gray':'transparent'}}>
              <Text>5</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {this.onDistance(10);}} style={{justifyContent: 'center', alignItems: 'center', width: 60, height: 30, borderColor: '#4b5b64', borderWidth: 1, backgroundColor: distanceID===10?'#32b3fa':'transparent'}}>
              <Text>10</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {this.onDistance(25);}} style={{justifyContent: 'center', alignItems: 'center', width: 60, height: 30, borderColor: '#4b5b64', borderWidth: 1, backgroundColor: distanceID===25?'#32b3fa':'transparent'}}>
              <Text>25</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {this.onDistance(50);}} style={{justifyContent: 'center', alignItems: 'center', width: 60, height: 30, borderColor: '#4b5b64', borderWidth: 1, backgroundColor: distanceID===50?'#32b3fa':'transparent'}}>
              <Text>50</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {this.onDistance(100);}} style={{justifyContent: 'center', alignItems: 'center', width: 60, height: 30, borderColor: '#4b5b64', borderTopRightRadius: 5, borderBottomRightRadius: 5, borderWidth: 1, backgroundColor: distanceID===100?'#32b3fa':'transparent'}}>
              <Text>100</Text>
            </TouchableOpacity>
          </View>*/}
          
          
          <View style={{marginTop: 15, alignItems: 'center',justifyContent: 'center', flexDirection: 'row', height: 40, backgroundColor: "#ffffff", borderTopWidth: 0, borderTopColor:'#e7e7e9', borderBottomWidth: 0, borderBottomColor:'#e7e7e9'}}>
            <TouchableOpacity onPress={() => {this.onPrice1();}} style={{ flex: 1, justifyContent: 'center', alignItems: 'center', height: 40, borderColor: '#e7e7e9', borderWidth: 1, backgroundColor: this.state.priceLevel1===true?'#32b3fa':'transparent'}}>
              <Text style={{color: this.state.priceLevel1?'white':'black'}}>$</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {this.onPrice2();}} style={{ flex: 1, justifyContent: 'center', alignItems: 'center', height: 40, borderColor: '#e7e7e9', borderWidth: 1, backgroundColor: this.state.priceLevel2===true?'#32b3fa':'transparent'}}>
              <Text style={{color: this.state.priceLevel2?'white':'black'}}>$$</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {this.onPrice3();}} style={{ flex: 1, justifyContent: 'center', alignItems: 'center', height: 40, borderColor: '#e7e7e9', borderWidth: 1, backgroundColor: this.state.priceLevel3===true?'#32b3fa':'transparent'}}>
              <Text style={{color: this.state.priceLevel3?'white':'black'}}>$$$</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {this.onPrice4();}} style={{ flex: 1, justifyContent: 'center', alignItems: 'center', height: 40, borderColor: '#e7e7e9', borderWidth: 1, backgroundColor: this.state.priceLevel4===true?'#32b3fa':'transparent'}}>
              <Text style={{color: this.state.priceLevel4?'white':'black'}}>$$$$</Text>
            </TouchableOpacity>
          </View>


          {/*<View style={{marginTop: 0, paddingHorizontal: 15, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', height: 40, backgroundColor: "#ffffff", borderTopWidth: 0, borderTopColor:'#e7e7e9', borderBottomWidth:1, borderBottomColor:'#e7e7e9'}}>
            <Text>Offering a Deal</Text>
            <ToggleButton isEnabled={this.state.isDeal} onChange={this.onChangeDeal}/>
          </View>*/}

          {
            
          /*<View style={{marginTop: 15, paddingHorizontal: 15, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', height: 40, backgroundColor: "#ffffff", borderTopWidth: 1, borderTopColor:'#e7e7e9', borderBottomWidth:1, borderBottomColor:'#e7e7e9'}}>
            <Text>PGA Class A Professional 2</Text>
            <ToggleButton isEnabled={this.state.pga} onChange={this.onChangePga}/> 
          </View>
          
          <View style={{marginTop: 15, paddingHorizontal: 15, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', height: 40, backgroundColor: "#ffffff", borderTopWidth: 1, borderTopColor:'#e7e7e9', borderBottomWidth:1, borderBottomColor:'#e7e7e9'}}>
            <Text>PGA Class A Professional 3</Text>
            {this.state.pga ? 
            <ToggleButton isEnabled={true} onChange={this.onChangePga}/> :
            <ToggleButton isEnabled={false} onChange={this.onChangePga}/>  }
            
          </View>

          <View style={{marginTop: 0, paddingHorizontal: 15, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', height: 40, backgroundColor: "#ffffff", borderTopWidth: 0, borderTopColor:'#e7e7e9', borderBottomWidth:1, borderBottomColor:'#e7e7e9'}}>
            <Text>U.S. Kids Golf Certified</Text>
            <ToggleButton isEnabled={this.state.isJunior} onChange={this.onChangeJunior}/>
          </View>
          <View style={{marginTop: 0, paddingHorizontal: 15, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', height: 40, backgroundColor: "#ffffff", borderTopWidth: 0, borderTopColor:'#e7e7e9', borderBottomWidth:1, borderBottomColor:'#e7e7e9'}}>
            <Text>Flight Monitor (Trackman)</Text>
            <ToggleButton isEnabled={this.state.isFlight} onChange={this.onChangeFlight}/>
          </View>
          <View style={{marginTop: 0, paddingHorizontal: 15, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', height: 40, backgroundColor: "#ffffff", borderTopWidth: 0, borderTopColor:'#e7e7e9', borderBottomWidth:1, borderBottomColor:'#e7e7e9'}}>
            <Text>Video Analysis</Text>
            <ToggleButton isEnabled={this.state.isVideo} onChange={this.onChangeVideo}/>
          </View>
          
          */}

          

          { this.state.isPga ? 
            <View style={{marginTop: 15, paddingHorizontal: 15, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', height: 40, backgroundColor: "#ffffff", borderTopWidth: 1, borderTopColor:'#e7e7e9', borderBottomWidth:1, borderBottomColor:'#e7e7e9'}}>
            <Text>PGA Class A Professional</Text>
            <ToggleButton isEnabled={true} onChange={this.onChangePga}/> 
            </View> : null
          }
          { this.state.isPga===false ? 
            <View style={{marginTop: 15, paddingHorizontal: 15, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', height: 40, backgroundColor: "#ffffff", borderTopWidth: 1, borderTopColor:'#e7e7e9', borderBottomWidth:1, borderBottomColor:'#e7e7e9'}}>
            <Text>PGA Class A Professional</Text>
            <ToggleButton isEnabled={false} onChange={this.onChangePga}/> 
            </View> : null
          }

          { this.state.isJunior ? 
            <View style={{marginTop: 0, paddingHorizontal: 15, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', height: 40, backgroundColor: "#ffffff", borderTopWidth: 0, borderTopColor:'#e7e7e9', borderBottomWidth:1, borderBottomColor:'#e7e7e9'}}>
            <Text>U.S. Kids Golf Certified</Text>
            <ToggleButton isEnabled={true} onChange={this.onChangeJunior}/> 
            </View> : null
          }
          { this.state.isJunior===false ? 
            <View style={{marginTop: 0, paddingHorizontal: 15, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', height: 40, backgroundColor: "#ffffff", borderTopWidth: 0, borderTopColor:'#e7e7e9', borderBottomWidth:1, borderBottomColor:'#e7e7e9'}}>
            <Text>U.S. Kids Golf Certified</Text>
            <ToggleButton isEnabled={false} onChange={this.onChangeJunior}/> 
            </View> : null
          }

          { this.state.isFlight ? 
            <View style={{marginTop: 0, paddingHorizontal: 15, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', height: 40, backgroundColor: "#ffffff", borderTopWidth: 0, borderTopColor:'#e7e7e9', borderBottomWidth:1, borderBottomColor:'#e7e7e9'}}>
            <Text>Trackman or other Flight Monitor</Text>
            <ToggleButton isEnabled={true} onChange={this.onChangeFlight}/> 
            </View> : null
          }
          { this.state.isFlight===false ? 
            <View style={{marginTop: 0, paddingHorizontal: 15, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', height: 40, backgroundColor: "#ffffff", borderTopWidth: 0, borderTopColor:'#e7e7e9', borderBottomWidth:1, borderBottomColor:'#e7e7e9'}}>
            <Text>Trackman or other Flight Monitor</Text>
            <ToggleButton isEnabled={false} onChange={this.onChangeFlight}/> 
            </View> : null
          }

          { this.state.isVideo ? 
            <View style={{marginTop: 0, paddingHorizontal: 15, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', height: 40, backgroundColor: "#ffffff", borderTopWidth: 0, borderTopColor:'#e7e7e9', borderBottomWidth:1, borderBottomColor:'#e7e7e9'}}>
            <Text>Video Analysis</Text>
            <ToggleButton isEnabled={true} onChange={this.onChangeVideo}/> 
            </View> : null
          }
          { this.state.isVideo===false ? 
            <View style={{marginTop: 0, paddingHorizontal: 15, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', height: 40, backgroundColor: "#ffffff", borderTopWidth: 0, borderTopColor:'#e7e7e9', borderBottomWidth:1, borderBottomColor:'#e7e7e9'}}>
            <Text>Video Analysis</Text>
            <ToggleButton isEnabled={false} onChange={this.onChangeVideo}/> 
            </View> : null
          }

          { this.state.hasDates ? 
            <View style={{marginTop: 0, paddingHorizontal: 15, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', height: 40, backgroundColor: "#ffffff", borderTopWidth: 0, borderTopColor:'#e7e7e9', borderBottomWidth:1, borderBottomColor:'#e7e7e9'}}>
            <Text>Lesson Dates Available</Text>
            <ToggleButton isEnabled={true} onChange={this.onChangeHasDates}/> 
            </View> : null
          }
          { this.state.hasDates===false ? 
            <View style={{marginTop: 0, paddingHorizontal: 15, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', height: 40, backgroundColor: "#ffffff", borderTopWidth: 0, borderTopColor:'#e7e7e9', borderBottomWidth:1, borderBottomColor:'#e7e7e9'}}>
            <Text>Lesson Dates Available</Text>
            <ToggleButton isEnabled={false} onChange={this.onChangeHasDates}/> 
            </View> : null
          }


          


          
          
          
        </ScrollView>
      </Animated.View>
		);
	}
}

class SearchResultsView extends Component {
  static propTypes = {
    popNavBack: PropTypes.func.isRequired,
    popNavNBack: PropTypes.func,
    pushNavScene: PropTypes.func.isRequired,
    callback: PropTypes.func,
    gotoPage: PropTypes.func,
  };

  constructor(props, context) {
    super(props, context);
    this.state = {      
      data: [],
      focusFlag: false,
      refreshData: [],
      
      //debugFlag: false,
     
      isRefreshing: false,
      canLoadMore: true,
      isLoading: true,
      selectedRowID: -1,
      filterState: false,
      isFilterOn: false,

      citysearchstatus: false,
      instructorsearchstatus: false,

			pan: new Animated.ValueXY(), // inits to zero,
			distanceID: 0
    };
		this.state.panResponder = PanResponder.create({
			onStartShouldSetPanResponder: () => true,
			onPanResponderMove: Animated.event([null, {
				dx: this.state.pan.x, // x,y are Animated.Value
				dy: this.state.pan.y,
			}]),
			onPanResponderRelease: () => {
				Animated.spring(
					this.state.pan,         // Auto-multiplexed
					{toValue: {x: 0, y: 0}} // Back to zero
				).start();
			},
		});
    this.width = width;
    this.height = height;
    this.r_width =  width / 356;
    this.r_height = height / 647;
    this.r2_width =  width / 1125;
    this.r2_height = height / 1659;
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => !isEqual(r1, r2)});
    this.ds_result = new ListView.DataSource({rowHasChanged: (r1, r2) => !isEqual(r1, r2)});

    this.isFullyLoaded = false;
    this.pageNum = 0;
    this.totalRows = 0;
    this.resultsPerPage = 20;
    this.longitude = "";
    this.latitude = "";

    //Filter Settings
    this.gender = "Any";
    this.priceLevel = "Any";
    this.isPga = false;
    this.isJunior = false;
    this.isVideo = false;
    this.isFlight = false;
    this.hasDates = false;
    
   
    
    this.apiPromise = null;
    this.started = false;
    this.pushSceneFlag = false;

    this.onRefreshResults = this.onRefreshResults.bind(this);
    this.onKeyboardUpdated = this.onKeyboardUpdated.bind(this);


    this.onRenderResult = this.onRenderResult.bind(this);
    this.onPriceLevel = this.onPriceLevel.bind(this);
    this._loadMoreContentAsync = this._loadMoreContentAsync.bind(this);
    this._loadMoreContentAsync_Refresh = this._loadMoreContentAsync_Refresh.bind(this);
    this.onStarRatingPress = this.onStarRatingPress.bind(this);
    this.onDistance = this.onDistance.bind(this);
    
    this.onPressInstructor = this.onPressInstructor.bind(this);
    this.onBook = this.onBook.bind(this);
    this.onFocusLocationSearchBar = this.onFocusLocationSearchBar.bind(this);
    this.endedSearch = this.endedSearch.bind(this);
    

    this.getDataByZip = this.getDataByZip.bind(this);
    this.getDataByCurrentLocation = this.getDataByCurrentLocation.bind(this);
    this.getDataByInstructor = this.getDataByInstructor.bind(this);

    this.clearFilters = ::this.clearFilters;
    this.checkFocus = ::this.checkFocus;
    this.CallBackInstructorScene = ::this.CallBackInstructorScene;
    this.CallBackBookScene = ::this.CallBackBookScene;

    this.handlePopCheck = ::this.handlePopCheck;
  }


  loadDataToArray(responseData) {

  }

  handlePopCheck(id) {
    if (id === 0) {
      if (!this.refInstructorScene) {
        this.refInstructorScene.handlePopCheck(0);
      } else {
        this.props.popNavBack();
      }



      // if (this.refFullCalendarScene != null) {
      //   this.refFullCalendarScene.handlePop();
      // } else if (this.refBookScene != null) {
      //   this.props.popNavNBack();
      // } else if (this.refInstructorContact != null) {
      //   this.props.popNavNBack();
      // } else if (this.refReviewScene != null) {
      //   this.props.popNavNBack();
      // } else {
      //   this.props.popNavBack();
      // }

    } else {
      if (this.refInstructorScene != null) {
        this.refInstructorScene.handlePopCheck();
      }
    }
  }


  

  getDataByZip() {
    this.pageNum = this.pageNum + 1;
    const array = this.state.data;
    this.setState({ isLoading: true });

    let tracker = new GoogleAnalyticsTracker(AppConfig.googleAnalyticsTrackerId);
    tracker.trackScreenView('SearchResultsView: ' + this.props.passProps.zip);
   
       

    this.apiPromise = MakeCancelable(
      fetch(AppConfig.apiUrl + '/api/InstructorSearchViewModels4?zip=' + this.props.passProps.zip + '&' +
        'miles=100&' +
        'gender=' + this.gender + '&' +
        'priceLevel=' + this.priceLevel + '&' +
        'isPga=' + this.isPga + '&' +
        'isJunior='+ this.isJunior + '&' +
        'isFlight='+ this.isFlight + '&' +
        'isVideo=' + this.isVideo + '&' +
        'pageNum=' + this.pageNum + '&' +
        'numResults=' + this.resultsPerPage + '&' + 
        'attachopenDates=false' + '&' + 
        'hasDates=' + this.hasDates));

    this.apiPromise.promise
      .then((response) => {

        this.setState({ data: array });
        //console.log(response.headers.get('x-totalrows'));
        this.totalRows = response.headers.get('x-totalrows');
        return response.json();
      })
      // .then((response) => response.json() )
      .then((responseData) => {

        //alert('inside responseData');

        if (this.pageNum == 1) array.splice(0, array.length);
        this.setState({ data: array });

        if (this.pageNum == 1 && (responseData == null || responseData.length <= 0)) {
          this.setState({ isLoading: false });
          if (this.props.passProps.city == '' || this.props.passProps.city == null) {
            if (this.state.isFilterOn) {
              Alert.alert('No Instructors matching that filter criteria.');
              this.onFilter();
            }
            else
            {
              Alert.alert('No Instructors Nearby ' + this.props.passProps.zip, 'Currently, the majority of our instructor network is in the north east.  We are making every effort to expand to your local area.');
              this.props.popNavBack();
            }
            
          }
          else {
            if (this.state.isFilterOn) {
              Alert.alert('No Instructors matching that filter criteria.');
              this.onFilter();
            }
            else {
              Alert.alert('No Instructors Nearby ' + this.props.passProps.city, 'Currently, the majority of our instructor network is in the north east.  We are making every effort to expand to your local area.');
              this.props.popNavBack();
            }


          }
        }
        else {
          for (let i = 0; i < responseData.length; i++)
            array.push(responseData[i]);
        }
        this.setState({ isLoading: false, data: array });
      })
      .catch(error => {
        //this.setState({ data: array });
        //this.pageNum = this.pageNum - 1;
        //alert("Debug: api cancelled, page num reset to " + this.pageNum + 'and is loading = ' + this.state.isLoading);
        this.setState({ isLoading: false });
      });


  }

  getDataByCurrentLocation() {

    this.pageNum = this.pageNum + 1;
    const array = this.state.data;   
    this.setState({ isLoading: true });
    
    
    if (this.apiPromise !== null) {
      this.apiPromise.cancel();
    }
    //this.searchListRef.scrollTo({ x: 0, y: 0, animated: false });

    this.setState({ isLoading: true });
    let tracker = new GoogleAnalyticsTracker(AppConfig.googleAnalyticsTrackerId);
    tracker.trackScreenView('SearchResultsView: gps');

    navigator.geolocation.getCurrentPosition(
      (position) => {
        // var initialPosition = JSON.stringify(position);
        // this.setState({initialPosition});
        //console.info(position.coords.longitude);
        //console.info(position.coords.latitude);
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        // override for test
        // this.latitude = "37.800561";
        // this.longitude = "-122.409668";


        // alert('isJunior ' + this.isJunior + ' isPga ' + this.isPga + ' gender ' + ' isVideo ' + this.isVideo + ' gender ' 
        // + ' isPga ' + this.isPga + ' gender ' + this.gender + ' hasDates ' + this.hasDates);
       
        fetch(AppConfig.apiUrl + '/api/InstructorSearchViewModels4?lat=' + position.coords.latitude + '&' +
          'lng=' + position.coords.longitude + '&' +
          'miles=100&' +
          'gender=' + this.gender + '&' +
          'priceLevel=' + this.priceLevel + '&' +
          'isPga=' + this.isPga + '&' +
          'isJunior='+ this.isJunior + '&' +
          'isFlight='+ this.isFlight + '&' +
          'isVideo=' + this.isVideo + '&' +
          'pageNum=' + this.pageNum + '&' +
          'numResults=' + this.resultsPerPage + '&' +
          'attachOpenDates=false' + '&' +
          'hasDates=' + this.hasDates)
          .then((response) => {
            //if (this.state.debugFlag == null || this.state.debugFlag) return;
            console.log(response.headers.get('x-totalrows'));
            if (this.pageNum == 1)
            {
              this.totalRows = response.headers.get('x-totalrows');
            }
            return response.json();
          })
          // .then((response) => response.json() )
          .then((responseData) => {
            
            if (this.pageNum == 1) array.splice(0, array.length);
            //if (this.state.debugFlag == null || this.state.debugFlag) return;
            if (this.pageNum == 1 && (responseData == null || responseData.length <= 0)) {
              this.setState({ isLoading: false });

              if (this.state.isFilterOn) {
                Alert.alert('No Instructors matching that filter criteria.');
                this.onFilter();
              }
              else {
                Alert.alert('No Instructors Nearby', 'Currently, the majority of our instructor network is in the north east.  We are making every effort to expand to your local area.');
                this.props.popNavBack();
              }
              
            }
            else {
              for (let i = 0; i < responseData.length; i++)
                array.push(responseData[i]);
            }
            this.setState({ isLoading: false });
          })
          //Error with api call
          .catch(error => {
            this.setState({ isLoading: false });
          });
      },
      //GPS error
      (error) => {
        this.setState({ isLoading: false });
        Alert.alert("Please check your GPS location services");
      },
      { enableHighAccuracy: (Platform.OS == 'android') ? false: true , timeout: 10000 /*, maximumAge: 1000*/ }
    );

    //using watchPosition and navigator.geolocation.clearWatch could be a solution
    // this.watchID = navigator.geolocation.watchPosition((position) => {
    //   // var lastPosition = JSON.stringify(position);
    //   // this.setState({lastPosition});
    // });



  }

  getDataByInstructor(query) {
    this.pageNum = this.pageNum + 1;
    const array = this.state.data;
    this.setState({ isLoading: true });
    let tracker = new GoogleAnalyticsTracker(AppConfig.googleAnalyticsTrackerId);
    tracker.trackScreenView('SearchResultsView: ' + query);

    fetch(AppConfig.apiUrl + '/api/InstructorSearchViewModels?find_ins=' + query + '&' +
      'gender=' + this.gender + '&' +
      'priceLevel=All&' +
      'pageNum=' + this.pageNum + '&' +
      'numResults=' + this.resultsPerPage + '&attachOpenDates=false')
      .then((response) => {
        //console.log(response.headers.get('x-totalrows'));
        this.totalRows = response.headers.get('x-totalrows');
        return response.json();
      })
      // .then((response) => response.json() )
      .then((responseData) => {
        if (this.pageNum == 1) array.splice(0, array.length);
        if (this.pageNum == 1 && (responseData == null || responseData.length <= 0)) {
          this.setState({ isLoading: false });

          if (this.state.isFilterOn) {
            Alert.alert('No Instructors matching that filter criteria.');
            this.onFilter();
          }
          else {
            Alert.alert('No Instructors Named ' + query, 'Currently, the majority of our instructor network is in the north east.  We are making every effort to expand to your local area.');
            this.props.popNavBack();
          }
          
        }
        else {
          for (let i = 0; i < responseData.length; i++)
            array.push(responseData[i]);
        }
        this.setState({ isLoading: false });
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  }


  checkFocus() {

//tempoarirly disable this funtion to see affect
    //return;


    if (AppConfig.checkFocus) {
      if (this._textInput) {
        this._textInput.focus();
        AppConfig.checkFocus = false;
      } else {
        AppConfig.checkFocus = false;
        this.pageNum = 2;
        this.setState({refreshData:[], selectedRowID: -1, canLoadMore: true, text: "", city: "", instructor: "", citysearchstatus: false, instructorsearchstatus: false});
        this.searchListRef.scrollTo({x: 0, y: 0, animated: false});
      }
    } else if (AppConfig.checkGPS) {
      AppConfig.checkGPS = false;
      //go to gps
      this.pageNum = 2;
    } else if (AppConfig.checkCity) {
      this.setState({refreshData:[], selectedRowID: -1, canLoadMore: true, text: "", city: "", instructor: "", citysearchstatus: false, instructorsearchstatus: false});
      this.searchListRef.scrollTo({x: 0, y: 0, animated: false});
      if (this.apiPromise !== null) {
        this.apiPromise.cancel();
      }


    }

  }


  componentDidMount() {

    
   

    if (Platform.OS == 'android') {

      InteractionManager.runAfterInteractions(() => {
        this.componentStart();
        var test = 1;
      });
      
      // setTimeout(() => {
        
      //   debugger;
      // }, 500);
    }
    else {
      this.componentStart();
      
    }


    
    

    
    
  }

  componentStart() {

   
    //this.props.callback(2);

    //OLD CODCE
    //this.checkFocus();
    //console.log(this.props);
    //alert(this.props.passProps.searchType + ' ' + this.props.passProps.zip + ' ' + this.props.passProps.city);

    this.pageNum = 0;

    //Filter Settings
    var tempFilter = this.refFadeInView;
    if (this.props.passProps.isPga != null) {
      this.isPga = this.props.passProps.isPga;
      tempFilter.state.isPga = this.props.passProps.isPga;
      this.props.passProps.isPga = null;
    }
    if (this.props.passProps.isJunior != null) {
      this.isJunior = this.props.passProps.isJunior;
      tempFilter.state.isJunior = this.props.passProps.isJunior;
      this.props.passProps.isJunior = null;
    }
    if (this.props.passProps.hasDates != null) {
      this.hasDates = this.props.passProps.hasDates;
      tempFilter.state.hasDates = this.props.passProps.hasDates;
      this.props.passProps.hasDates = null;
    }
    
    if (this.gender != 'Any' || this.priceLevel != 'Any' || this.isJunior || this.isPga || this.isVideo || this.isFlight || this.hasDates) this.setState({ isFilterOn: true });




    if (this.props.passProps != null) {
      if (this.props.passProps.searchType == 'city') {
        this.setState({ searchBarValue: this.props.passProps.city });
        this.getDataByZip();
      }
      else if (this.props.passProps.searchType == 'zip') {
        this.setState({ searchBarValue: this.props.passProps.zip });
        this.getDataByZip();
      }
      else if (this.props.passProps.searchType == 'ins') {
        this.setState({ searchBarValue: this.props.passProps.query });
        this.getDataByInstructor(this.props.passProps.query);
      }
      else if (this.props.passProps.searchType == 'gps') {
        this.setState({ searchBarValue: 'Current Location' });
        this.getDataByCurrentLocation();
      }

      // if (this.props.passProps.isPga != null) {
      //   this.isPga = this.props.passProps.isPga
      // }

      // if (this.props.passProps.isJunior != null) {
      //   this.isJunior = this.props.passProps.isJunior
      // }

     
      
      //code added to fix strange bug where keyboard is popping up on android 
      if (Platform.OS == 'android') {
        InteractionManager.runAfterInteractions(() => {
          Keyboard.dismiss();
        });
      }


    }

  }

  componentWillMount() {
    // console.info(AppConfig.searchResultDisplay);
    // if (Platform.OS === 'ios') {
    //   this.subscriptions = [
    //     Keyboard.addListener('keyboardWillHide', (event) => this.onKeyboardUpdated(event, false)),
    //     Keyboard.addListener('keyboardWillShow', (event) => this.onKeyboardUpdated(event, true)),
    //   ];
    // } else {
    //   this.subscriptions = [
    //     Keyboard.addListener('keyboardDidHide', (event) => this.onKeyboardUpdated(event, false)),
    //     Keyboard.addListener('keyboardDidShow', (event) => this.onKeyboardUpdated(event, true)),
    //   ];
    // }
  }




  _loadMoreContentAsync(){
    if (this.state.isLoading) {
      return;
    } 
    if (((this.pageNum * this.resultsPerPage) + 1) < this.totalRows) {
      //alert('CALL: you are requesting page ' + (this.pageNum + 1).toString() + ' and total rows ' + this.totalRows);
      if (this.props.passProps.searchType === 'city') {
        this.getDataByZip();
      }
      else if (this.props.passProps.searchType === 'zip') {
        this.getDataByZip();
      }
      else if (this.props.passProps.searchType === 'ins') {
        this.getDataByInstructor(this.props.passProps.query);
      }
      else if (this.props.passProps.searchType === 'gps') {
        this.getDataByCurrentLocation();
      }

      // if (this.props.passProps.isPga != null) {
      //   this.isPga = this.props.passProps.isPga
      // }
    }
    else {
    }

  }


  CallBackBookScene(){
    this.onRefreshResults();
  }

  CallBackInstructorScene() {
    this.props.callback(3);
  }

  onKeyboardUpdated(event, type) {

  }


  componentWillUnmount() {
    // this.keyboardDidHideListener.remove();
    // this.keyboardDidShowListener.remove();
  }

  onFocusLocationSearchBar() {
    //TODO - Reset all search variables and dataset
    //this.state.data = [];    
    // var newDataSource = [];
    // this.setState({ data : newDataSource })

    
      if (this.props.source == 'home') {
        this.props.gotoPage(1);
      }
      else {
        this.props.popNavBack();
        InteractionManager.runAfterInteractions(() => {
          this.props.callback();
        });
      }
  }

  endedSearch() {
  }


  

  onRefreshResults() {
    this.setState({isRefreshing: true});
    this.componentDidMount();
    this.setState({isRefreshing: false});

  }

  _loadMoreContentAsync_Refresh(){
    
  }

  onStarRatingPress(rating){
  }

  onPriceLevel(data){
    var PriceLevel = '';
    for (var i = 0; i < parseInt(data); i++) {
      PriceLevel = PriceLevel + '$'
    }
    return PriceLevel
  }

  onDistance(distance){
    var text = '';
    var anyString = 'Mozilla';
    text = parseFloat(distance).toString().substring(0,4);
    return text
  }
    onRenderResult(rowData, rowID){
    if (rowData.type === 'Loading') {
      return this.state.isLoading ? (<View style={ styles.loading }>
        <ActivityIndicator
          animating={ true }
          style={[ styles.loading ]}A
          size="small"
          color="#000"
        />
      </View>) : null;
    }

    const render_BookBar_Locations = [];
    let key1 = "render_bookbar_locations_text_1" + rowID;
    let key2 = "render_bookbar_locations_text_2" + rowID;
    let key3 = "render_bookbar_locations_text_3" + rowID;
    let key4 = "render_bookbar_locations_text_4" + rowID;


    let OpenDaysCount = rowData.openDays.length;

    //If No Open Dates
    if (OpenDaysCount == 0) {
      
      render_BookBar_Locations.push(<Text key={key3} style={{ fontSize: 10, marginLeft: 60, paddingTop: 5, 
      color: '#6f6f6f', fontFamily: 'Gotham-Medium' }}>Lesson dates not available</Text>); 
    }
    // Display Open Dates in horizontal Scrollarea
    else {
      let tArray = [];

      /*tArray.push(
          <View
            key={key1 + '1'} style={{ marginLeft: 20 * this.r2_width, width: 160 * this.r2_width, height: 80 * this.r2_width, borderRadius: 15 * this.r2_width, backgroundColor: 'transparent', alignItems: 'center', justifyContent: 'center' }}>
            <Text key={key1 + '2'} allowFontScaling={false} style={{ fontSize: 10, color: '#6f6f6f', fontFamily: 'Gotham-Bold' }}>Next</Text>
            <Text key={key1 + '3'} allowFontScaling={false} style={{ fontSize: 10, color: '#6f6f6f', fontFamily: 'Gotham-Bold' }}>Available</Text>
          </View>
        );*/
      

      //Loop through each date
      for (let j = 0; j < OpenDaysCount; j++) {

        //limit to only 30
        if (j >= 30) break;
        let displayDay = moment(rowData.openDays[j]).format('ddd').toUpperCase();
        let displayDate = moment(rowData.openDays[j]).format('M/DD');
        
        //Generate unique key
        let key4_1 = key1 + "key4_1" + j;
        let key4_1_date = key1 + "key4_1_date" + j;
        

        //Display time buttons
        tArray.push(
          <TouchableOpacity
            onPress={() => { this.onBook(rowData.instructorId, rowData.locationId, rowData.lessonTypeId, rowData.openDays[j]); }}
            key={key4_1} style={{ marginLeft: 20 * this.r2_width, width: 160 * this.r2_width, borderRadius: 15 * this.r2_width, backgroundColor: 'transparent', alignItems: 'center', justifyContent: 'center' }}>
            <Text key={key4_1 + 't'} allowFontScaling={false} style={{ fontSize: 10, color: '#32b3fa', fontFamily: 'Gotham-Medium' }}>{displayDay}</Text>
            <Text key={key4_1 + 'F'} allowFontScaling={false} style={{ fontSize: 10, color: '#32b3fa', fontFamily: 'Gotham-Medium' }}>{displayDate}</Text>
          </TouchableOpacity>
        );
      }

      

      render_BookBar_Locations.push(
        <View key={key4 + 'v1'}>
          <View key={key4} style={{ flexDirection: 'row', marginBottom: 0 }}>
            <ScrollView key={key4 + 's'} horizontal={true} showsHorizontalScrollIndicator={false}>
              {tArray}
            </ScrollView>
          </View>
          
        </View>
      );


    }



   // return this.state.isLoading ? null:null;

    return (
      <View>
        <View>
          <TouchableOpacity onPress={() => { this.onPressInstructor(rowData); }}  >
            <View>
              <View style={{ height: 93 * this.r_height, alignItems: 'center', flexDirection: 'row' }}>
                <View style={{ marginLeft: 5, width: 75 * this.r_width, height: 75 * this.r_width }}>
                  {rowData.isProfilePhoto ?
                    <Image
                      key="render_location"
                      style={{ width: 70 * this.r_width, height: 70 * this.r_width, borderRadius: 35 * this.r_width, marginLeft: 5 }}
                      source={{ uri: 'https://www.birdienow.com/img/instructor/profile/' + rowData.instructorId + '.jpg' }}
                    />
                    :
                    <Image
                      key="render_location"
                      style={{ width: 70 * this.r_width, height: 70 * this.r_width, borderRadius: 35 * this.r_width, marginLeft: 5 }}
                      source={require('img/profile/profile.png')}
                    />
                  }

                  {rowData.isPga ?
                    <Image
                      style={{ width: 30, height: 30, position: 'absolute', left: 0, bottom: 0 }}
                      source={{ uri: 'https://www.birdienow.com/img/badge/pga_badge_85.png' }}
                    /> : null}
                </View>
                <View style={{ marginLeft: 5, flex: 1 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text allowFontScaling={false} style={{ flex: 1, fontWeight: 'bold', fontFamily: 'Gotham-Book' }}>
                      {rowData.firstName} {rowData.lastName}
                    </Text>
                    <Text allowFontScaling={false} style={{ marginRight: 8 * this.r_width, fontSize: 11, fontFamily: 'Gotham-Book' }}>
                      {parseFloat(rowData.distance).toFixed(1)} miles <Text allowFontScaling={false} style={{ fontSize: 11, fontFamily: 'Gotham-Book', marginLeft: 3 * this.r_width }}>{this.onPriceLevel(rowData.priceLevel)}</Text>
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 8 }}> 
                      {
                        rowData.lessonCount > 0 ?
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 3 * this.r_width }}>
                        <Text allowFontScaling={false} style={{ color: '#6f6f6f', fontSize: 11, fontFamily: 'Gotham-Medium' }}>
                          {rowData.lessonCount} Verified Lessons
                        </Text>
                      </View>
                      : null
                      }
                      
                      <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 3 * this.r_width, marginBottom: 3 * this.r_width }}>
                        <StarRating
                          disabled={true}
                          emptyStar={'ios-star-outline'}
                          fullStar={'ios-star'}
                          halfStar={'ios-star-half'}
                          iconSet="Ionicons"
                          starColor={'#32b3fa'}
                          maxStars={5}
                          rating={rowData.averageRating}
                          selectedStar={(rating) => this.onStarRatingPress(rating)}
                          starSize={12}
                          emptyStarColor={'#32b3fa'}
                        />
                        <Text allowFontScaling={false} style={{ color: '#32b3fa', marginLeft: 5, fontSize: 11, fontFamily: 'Gotham-Book' }}>
                          {rowData.reviewCount} Reviews
                        </Text>
                      </View>
                      <Text allowFontScaling={false} style={{ color: '#000000', fontSize: 11, fontFamily: 'Gotham-Book' }}>
                        {rowData.name}
                      </Text>
                      <Text allowFontScaling={false} style={{ color: '#000000', fontSize: 11, fontFamily: 'Gotham-Book' }}>
                        {rowData.address1} {rowData.address2} {rowData.city} {rowData.state} {rowData.zip}
                      </Text>
                    </View>
                    {rowData.isPromo ? <View style={{ flex: 3, flexDirection: 'row' }}>
                      <View>
                        <Image  style={{ width: 20, height: 25 }} source={require('img/icon/exclusive_60x75.png')}  /> 
                      </View>
                      <View style={{ paddingTop: 2, paddingLeft: 3}}>
                        <Text style={{ fontSize: 10, color: '#ff4b00', fontFamily: 'Gotham-Medium' }}>Exclusive</Text>
                        <Text style={{ fontSize: 10, color: '#ff4b00', fontFamily: 'Gotham-Medium' }}>Deal</Text>
                      </View>
                    </View> : null}
                  </View>
                  
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </View>
        {/*<View style={{backgroundColor: "#f7f7f7"}}  >
          { (Platform.OS === 'ios') ? render_BookBar_Locations : null }
        </View>*/}
        <View style={{ flexDirection: 'row', backgroundColor: "#fff"}}  >
          <View style={{flex: 2.5, height: 40, }}>
              <View style={{ alignItems: 'flex-start', justifyContent: 'flex-start', paddingLeft: 20 }}>
                <Text style={{ fontSize: 10, color: '#6f6f6f',  fontFamily: 'Gotham-Bold' }}>Next</Text>
                <Text style={{ fontSize: 10, color: '#6f6f6f', fontFamily: 'Gotham-Bold' }}>Available</Text>
              </View>
          </View>
          <View style={{flex: 7, paddingLeft: 10}}>
            {render_BookBar_Locations}
            {/*{ (Platform.OS === 'ios') ? render_BookBar_Locations : null }*/}
          </View>
          <View style={{flex: 1.5, justifyContent: 'flex-start', alignItems: 'center'}}>
            { rowData.openDays.length > 4 ? 
            <Image style={{width: 20, height: 20}} source={require('img/icon/icon_arrow.png')}/>
            : null
            }
          </View>
        </View>
        <View style={{backgroundColor: "#d9d9d9", height:2}} />
      </View>
      
    )
  }




  onPressInstructor(instructor){

    //if data is loading (more) then dont allow user to navigate to instructor page
    if (this.state.isLoading)
      return;   

    //this.setState({ debugFlag: true });
    // if (this.apiPromise != null) {
    //   this.apiPromise.cancel();
    // }

    //this.setState({ isLoading: false });
    this.props.pushNavScene(InstructorScene, {ref:(c) => { this.refInstructorScene = c; } ,passProps: {instructor: instructor, id: instructor.instructorId, lessonTypeId:instructor.lessonTypeId} , callback: this.CallBackInstructorScene});
  }

  // onBook(openDateId, lessonTypeId)
  // {
  //   if (!AppConfig.global_isLoggedIn) {
  //     Alert.alert("Please log in to access this feature");
  //   } else {
      
  //     this.props.pushNavScene(BookScene, {
  //       ref: (c) => { this.refBookScene = c; },
  //       passProps: {
  //         openDateId: openDateId,
  //         lessonTypeId: lessonTypeId,
  //       }, callback: this.CallBackBookScene
  //     });
  //   }
  // }

  onBook(instructorId, locationId, lessonTypeId, setDate)
  {
    this.props.pushNavScene(FullCalendarScene, {
      ref:(c) => { this.refFullCalendarScene = c; },
      InstructorID: instructorId,
      LocationID: locationId,
      LessonTypeID: lessonTypeId,
      SetDate: setDate
    });
  }

	onFilter = () => {
    this.setState({ filterState: !this.state.filterState });
    this.refFadeInView.fadeIn();
  };

	onFilterOff = () => {
		this.setState({ filterState: false, isFilterOn: false });

    //var test = this.refFadeInView.props;
    //var test1 = this.refFadeInView.state;
    //var test2 = this.refFadeInView.props.state;

    //Filter Settings
    this.gender = this.refFadeInView.state.gender;
    this.isJunior = this.refFadeInView.state.isJunior;
    this.isPga = this.refFadeInView.state.isPga;
    this.hasDates = this.refFadeInView.state.hasDates;
    this.isVideo = this.refFadeInView.state.isVideo;
    this.isFlight = this.refFadeInView.state.isFlight;

    this.refFadeInView.state.priceLevel = "Any";
    if (this.refFadeInView.state.priceLevel1) this.refFadeInView.state.priceLevel = "1"

    if (this.refFadeInView.state.priceLevel2) {
      if (this.refFadeInView.state.priceLevel == 'Any')
        this.refFadeInView.state.priceLevel = "2"
      else
        this.refFadeInView.state.priceLevel =  this.refFadeInView.state.priceLevel  + "-2"
    }

    if (this.refFadeInView.state.priceLevel3) {
      if (this.refFadeInView.state.priceLevel == 'Any')
        this.refFadeInView.state.priceLevel = "3"
      else
        this.refFadeInView.state.priceLevel =  this.refFadeInView.state.priceLevel  + "-3"
    }

    if (this.refFadeInView.state.priceLevel4) {
      if (this.refFadeInView.state.priceLevel == 'Any')
        this.refFadeInView.state.priceLevel = "4"
      else
        this.refFadeInView.state.priceLevel =  this.refFadeInView.state.priceLevel  + "-4"
    }
    this.priceLevel = this.refFadeInView.state.priceLevel;
    
      
    
    
      



    if (this.gender != 'Any' || this.priceLevel != 'Any' ||this.isJunior || this.isVideo|| this.isFlight || this.isPga || this.hasDates) this.setState({ isFilterOn: true });

		this.refFadeInView.fadeOut();
    this.onRefreshResults();
    
    
  };

  clearFilters() {
    //  this.refFadeInView.state.gender == 'Any';
    //  this.refFadeInView.state.priceLevel == 'Any'; 
    //  this.refFadeInView.state.isJunior = false;
    //  this.refFadeInView.state.isVideo = false;
    //  this.refFadeInView.state.isFlight = false;
    //  this.refFadeInView.state.isPga = false;
    //  this.refFadeInView.state.hasDates = false;
     this.refFadeInView.setState({ gender: 'Any', priceLevel: 'Any', priceLevel1: false, priceLevel2: false, priceLevel3: false, priceLevel4:false,
     isJunior: false, isVideo: false, isFlight: false, isPga: false, hasDates: false});
  }

	onDistance = (i) => {
		this.setState({ distanceID: i });
	};

	renderAnimatedView = () => {
    return (
      <View style={styles.animatedViewContainer}>

      </View>
    );
  };

  render() {
    const { filterState, distanceID } = this.state;
    return (
      <View style={styles.container}>
        <View style={{flex:1}}>
          {/*<View style={{height:30 * this.r_height, alignItems:'center', backgroundColor:'#aac1ce', borderBottomWidth:0, borderBottomColor:'#85D3DD'}}>
            <Text allowFontScaling={false} style={{marginTop:0 * this.r_height, fontSize: 13 * this.r_height, color: '#ffffff', fontFamily: 'Gotham-Bold'}}>
              New Results Page
            </Text>
          </View>*/}
          <View style={{height: (Platform.OS === 'ios') ? 65 * this.r_height : 45 * this.r_height, backgroundColor:'#aac1ce', alignItems:'center', justifyContent: 'center', flexDirection: 'row'}}>
            
            <View style={{ flex:8, marginLeft:15, height:25 * this.r_width, backgroundColor:'#f0f0f0',alignItems:'center', borderRadius:10,flexDirection: 'row', marginTop: (Platform.OS === 'ios') ? 20 : 0, }}>
              <Image style={{marginLeft:5 * this.r_width,width:20 * this.r_height, height:20 * this.r_height}} source={require('img/image/search-location.png')}/>
              <TextInput
                underlineColorAndroid='transparent'
                placeholder="Enter text to see location"
                style={{height: 25 * this.r_width,flex:1, fontSize: 13, paddingVertical: 0}}
                //onChangeText={(text) => this.setState({text})}
                onFocus={() => {this.onFocusLocationSearchBar();}}
                //clearTextOnFocus={true}
                value={this.state.searchBarValue}
                defaultValue={AppConfig.searchResultDisplay}
                onSubmitEditing={() => {this.endedSearch();}}
              />
            </View>
            <View style={{ flex: 2, marginRight:5, height: (Platform.OS === 'ios') ? 65 * this.r_height : 45 * this.r_height, justifyContent: 'center', alignItems: 'center', marginTop: (Platform.OS === 'ios') ? 20 : 0}}>
              <TouchableOpacity onPress={this.onFilter} style={{backgroundColor: this.state.isFilterOn===true?'#32b3fa':'white', paddingHorizontal: 10, paddingVertical: 5, borderRadius: 5}}>
                <Text style={{color: this.state.isFilterOn===true?'white':'black'}}>Filter</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{height:this.height - 100 * this.r_height - 16, backgroundColor:'#ffffff'}}>
            <ListView
              ref={(ref) => { this.searchListRef = ref; }}
              keyboardShouldPersistTaps="always"
              style={{}}
              dataSource={this.ds.cloneWithRows([...this.state.data, { type: 'Loading' }])}
              renderRow={(rowData, sectionID, rowID) =>this.onRenderResult(rowData, rowID)}
              refreshControl={
                <RefreshControl
                  refreshing={ this.state.isRefreshing }
                  onRefresh={ this.onRefreshResults }
                />
              }
              onEndReached={ () => this._loadMoreContentAsync() }
              onEndReachedThreshold={20} 
              enableEmptySections={false}
              automaticallyAdjustContentInsets={ false }
            />

            {/*{
              this.state.isLoading ? <View style={styles.loading}>
                <ActivityIndicator
                  animating={true}
                  style={[styles.loading]}
                  size="small"
                  color="#000"
                />
              </View> : null
            }*/}

          </View>
          <View style={{ height: 100 }} />

          
          {/*{this.state.focusFlag ? arrayView : null}*/}
          {this.state.isLoading && false ?
            <View
              style={{position:'absolute', top: 0, left:0, bottom:0, right:0, alignItems:'center', justifyContent:'center', alignSelf: 'stretch', backgroundColor: 'transparent'}}
            >
              <ActivityIndicator
                animating={true}
                size="small"
                color="gray"
              />
            </View>:null}
        </View>
				{
					filterState ?
            <TouchableOpacity style={{width: width, height: height, position: 'absolute'}} onPress={this.onFilterOff}/>
						:
						null
				}
        <FadeInView style={{width: width, height: height-100, backgroundColor: '#fff'}} ref={(ref) => {this.refFadeInView = ref;}}    >
          <View style={{
        flexDirection: 'row', alignItems: "center", justifyContent: "center",
        backgroundColor: '#f9f9f9', borderBottomWidth: 0, borderBottomColor: '#85D3DD',
        height: (Platform.OS === 'ios') ? 65 : 40 }}>
        
        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={this.clearFilters}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#32b3fa',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Clear</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 2, justifyContent: "center" }}>
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#000',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Filter Options</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={this.onFilterOff} >
          <Text allowFontScaling={false} style={{
            fontFamily: 'Gotham-Bold', textAlign: "center", fontSize: 15, color: '#32b3fa',
            marginTop: (Platform.OS === 'ios') ? 20 : 0
          }}>Done</Text>
        </TouchableOpacity>
      </View>
          
        </FadeInView>
      </View>
    );
  }
}

export default SearchResultsView;
